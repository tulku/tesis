% Esta funcion toma las simulaciones generadas anteriormente y ejecuta kmeans
% para separar las muestras en dos grupos, los que pasan y los que no pasan.

function generate_clusters(simulaciones)
    max_iters = 10;
    plot_progress = false;
    % Proceso los datos de las simulaciones para dejarlos en la forma que le
    % sirven a kmeans.
    data = extract_d_data (simulaciones);
    % Tomo dos muestras random como centroides iniciales. El 2 es fijo, porque
    % siempre voy a querer dos clusters.
    initial_centroids = kMeansInitCentroids(data, 2);
    % Ya estoy listo para correr kmaens.
    [centroids, idx] = runkMeans(data, initial_centroids, max_iters, plot_progress);
    % Muestro el grafico lindo con los clusters
    figure;
    hold on;
    plotProgresskMeans(data, centroids, initial_centroids, idx, 2, max_iters);
    hold off;
    % Appendeo el cluster al que pertenecen en la 3ra columna.
    data = [data idx];
    % Guardo el resultado en un archivo.
    save('clusters.oct', 'data');
end



% Esta funcion lee el arreglo de celdas que se generan despues de una simulacion
% y genera la matriz de datos que necesita kmaens para funcionar.
% Lo que necesita kmeans es una matriz donde cada fila es un dato a clasificar.

function kmeans_data = extract_d_data (simulaciones)

i = 1;

for sim = simulaciones
    if (size(sim{1}) == [1 1] && !isnan(sim{1}.fig_error))
        errores(i) = sim{1}.fig_error;
        Dmulti(i) = sim{1}.r_val.D;
        i += 1;
    end
end

[errores, index] = sort(errores, 'ascend');
Dmulti = Dmulti (index);

kmeans_data = [log(errores') Dmulti'];

end

