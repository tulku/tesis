#!/bin/bash

snap="resultados-`date +%F`"
mkdir -p resultados/${snap}
lista=`find ./ | grep -v attic | grep -v '^./resultados/.*$' | grep ^.*.oct$`
cp -a ${lista} resultados/${snap}
tar -cjf ${snap}.tar.bz2 resultados/${snap}
rm -r resultados
