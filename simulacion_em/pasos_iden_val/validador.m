% Nuevo super mega validador. Usa el test de Kolmorov para ver si los errores se
% ajustan a la normal que corresponde y ademas evalua que sean
% descorrelacionados.
 
% Test de Kolmorov Smirnov multivariable

function resultado = validador(errores, e, covar, detectado, umbral, print)
	% Constantes definidas para tener un 18% de rechazo de ensayos verdaderos.
	umbral_cvm = 0.028;
	ks_const = 0.1430;
	resultado = KS_test_multi(errores,0,sqrt(covar(1,1)),sqrt(covar(2,2)), umbral, umbral_cvm, print);
end

function [ks,pasa] = KS_test(X,m,s,const)
        X=sort(X);
        Xecdf=empirical_cdf(X,X);
        assumed_pop=normcdf(X,m,s);
        ks=max(abs(assumed_pop-Xecdf));
        pasa = ks < const;
end

function resultados = KS_test_multi(errores,m,s1,s2, umbral, umbral_cvm, display)

%	Genero los puntos notables.
	samples = apply_normcdf(errores,m,s1,s2);
	[ip,pro_right,pro_top] = calc_points (samples);
%	Calculo la CDF en los puntos notables.
	cdf_samples = empcdf(samples, samples);
	cdf_ip = empcdf(ip, samples);
	cdf_pr = empcdf(pro_right, samples);
	cdf_pt = empcdf(pro_top, samples);
%	Calculo las distancias entre las EmpCDF y las reales
	dist_m = calc_dist(samples, cdf_samples);
	dist_ip = calc_dist(ip, cdf_ip);
	dist_pr = calc_dist(pro_right, cdf_pr);
	dist_pt = calc_dist(pro_top, cdf_pt);
	dists = [ max(dist_m) max(dist_ip) max(dist_pr) max(dist_pt)];

	resultados.cdf_samples = cdf_samples;
	resultados.cdf_ip = cdf_ip;
	resultados.cdf_pt = cdf_pt;
	resultados.cdf_pr = cdf_pr;
	resultados.dists = dists;
	resultados.D = max(dists);
	resultados.W = 0; %max(cvms);
	resultados.pasa = 0;
	resultados.pasa_cvm = 0;
	if resultados.D <= umbral
		resultados.pasa = 1;
	end

	if display == 1
		ip_pts = zeros(length(ip),1);
		ind = 1
		for point = ip
			ip_pts(ind) = abs((point(1)*point(2)));
			ind+=1;
		end

		%	Resultados del generador de puntos.
		total_points = length(samples)+length(ip)+length(pro_right)+length(pro_top);
		disp(['Total points: ', num2str(total_points)]);
		figure();
		plot(samples(1,:),samples(2,:),'ob',ip(1,:),ip(2,:),'or',pro_right(1,:), ...
			pro_right(2,:),'ok',pro_top(1,:),pro_top(2,:),'ok');
		legend('Muestras','IP','Pro right','Pro top','location','southwest');
		title('Puntos notables');
		figure();
		plot((1:length(cdf_samples)),sort(cdf_samples),(1:length(cdf_ip)),sort(cdf_ip));
		legend('Muestras','IP','location','southeast');
		title('CDF en los puntos notables');
		figure();
		plot((1:length(ip_pts)),sort(ip_pts),(1:length(cdf_ip)),sort(cdf_ip));
		legend('IP realCDF','IP eCDF','location','southeast');
		title('Empirical vs Real CDF - IP');
		figure();
		plot((1:length(cdf_pr)),sort(cdf_pr),(1:length(cdf_pt)),sort(cdf_pt));
		legend('Pro right','Pro top','location','southeast');
		title('CDF en los puntos notables');
	end
end

function dist = calc_dist(points, e_cdf)
	dist = zeros(length(points),1);
	i = 1;
	for point = points
		dist(i) = abs(e_cdf(i)-(point(1)*point(2)));
		i+=1;
	end
end

function dcvm = calc_cvm (points, e_cdf)
        N = length(points);
        dist = zeros(N,1);
        i = 1;
        for point = points
                dist(i)=(e_cdf(i) - (point(1)*point(2)))^2;
                i+=1;
        end
        dcvm=(1/12/N + sum(dist))/N;
end

function result = empcdf(points, samples)
	%points = sortrows(points',1)';
	l = length(samples);
	len = length(points);
	result = zeros(len,1);
	i = 1;
	for point = points 
		for sample = samples
			if (point(1) >= sample(1)) && (point(2) >= sample(2))
				result(i) += 1;
			end
		end
		result(i) /= l;
		i += 1;
	end
end

function samples = apply_normcdf(errores,m,s1,s2)
	% La transformacion es asi por ser independientes.
	samples(1,:) = normcdf(errores(1,:),m,s1);
	samples(2,:) = normcdf(errores(2,:),m,s2);
end

function [ip,pro_right,pro_top]=calc_points (samples)
	l=length(samples);
	x = samples(1,:);
	y = samples(2,:);
	ip = intersection_points (l,x,y);
	pro_right = samples;
	pro_right(1,:) = ones(1,l);
	pro_top = samples;
	pro_top(2,:) = ones(1,l);
end

function [ip] = intersection_points (l,x,y)
	ipn = 1;
	for j = (1:l)
		for i = (1:l)
			if (x(i) < x(j)) && (y(i) > y(j))
				ip(1,ipn)=x(j);
				ip(2,ipn)=y(i);
				ipn = ipn +1;
			end
		end
	end
end
