clc;
clear;
close all;
warning ('error','Octave:divide-by-zero');
warning ('error','Octave:undefined-return-values');

CANT_Ms = [50+50];
% Para 50 muestras, el umbral que uso es 0.20
umbrales = [0.15];
cant_iter = 15;
cant_simulaciones = 500;
SNRv = [20]; %dBs

% Matriz con el alfabeto
x_p =  [[-3,-3,-3,-3,-1,-1,-1,-1,1,1,1,1,3,3,3,3];
	[-3,-1,1,3,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3]];

tic
for i = 1:length(CANT_Ms)
	cant_muestras = CANT_Ms(i);
	umbral_actual = umbrales(i);
	for SNR = SNRv
		para_hacer = cant_simulaciones;
		aa = 1;
		acum = 0;
		simulaciones = {};
		archivo_salida = ['sim_KS-',num2str(cant_muestras-50),'s-',num2str(SNR),'dB-',num2str(umbral_actual),'.oct'];
		while (para_hacer > 0)
			datos = {};
			disp(['Generando simulaciones...' num2str(SNR) 'dB - ' num2str(cant_muestras-50) ' muestras.']);
			fflush(stdout);
			for rep = 1:para_hacer
				%h = zeros(2,2);
				%% Generacion de senales y canal.
				h = abs(randn(2,2)*1.2);
				% Generamos el ruido
				covar = zeros(2,2);
				covar(1,1) = 5*h(1,:)*h(1,:)'/(10^(SNR/10));
				covar(2,2) = 5*h(2,:)*h(2,:)'/(10^(SNR/10));
				N = randn(2,cant_muestras);
				% Generamos los datos a transmitir con 4 niveles
				a = sign(randn(2,cant_muestras));
				b = 2*sign(randn(2,cant_muestras));
				X = a+b;
				% Mezclamos todo
				Y = h*X + sqrt(covar)*N;
				hk = abs(randn(2,2)*1.2);
				umbral = umbral_actual;
				tipo = 'comun';
				aux_names = {'X','Y', 'x_p', 'covar', 'cant_iter', 'hk', 'h', 'tipo', 'umbral'};
				aux = cell2struct({X; Y; x_p; covar; cant_iter; hk; h; tipo; umbral},aux_names,1);
				datos{rep}=aux;
			end
	
			errores = 0;
			for rep = 1:para_hacer
				disp(['Simulacion: ',num2str(aa),' ',num2str(SNR),'dB ',num2str(cant_muestras-50),' M. ' num2str(acum),' descartadas.']);
				fflush(stdout);
				try
					simulaciones{aa} = simular(datos{rep});
				catch
					aa = aa -1;
					errores = errores+1;
					acum = acum +1;
				end	
				aa = aa+1;
				if !mod(aa,500)
					save(archivo_salida,'simulaciones');
				end
			end
			para_hacer = errores;
		end
		disp(['Termine las ',num2str(cant_simulaciones),' sims. SNR=',num2str(SNR),' Muestras: ',num2str(cant_muestras-50),'. Rep.: ', num2str(acum)]);
		save(archivo_salida,'simulaciones');
	end
end

if cant_simulaciones >= 20
	disp('Termine!!');
else 
	for rep = 1:cant_simulaciones
		temp = simulaciones{rep};
%		figure();
%		plot (temp.r_val.di, 'or', temp.r_val.ki, '+k');
		disp([num2str(rep),') ', 'Error: ', num2str(temp.fig_error) ...
			' - Pasa: ', num2str(temp.pasa), ' - D: ', num2str(temp.r_val.D) ]);
		fe(rep)=temp.fig_error;
		mostrar_grafico(temp.hv,temp.h,cant_iter,num2str(rep));
	end
end
toc
