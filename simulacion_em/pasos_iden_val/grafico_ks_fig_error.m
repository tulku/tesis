function grafico_ks_fig_error(simulaciones)

	i = 1;
	
	for sim = simulaciones
		if (size(sim{1}) == [1 1] && !isnan(sim{1}.fig_error))
			errores(i) = sim{1}.fig_error;
			Dmulti(i) = sim{1}.r_val.D;
			i += 1;
		end
	end

	[errores, index] = sort(errores, 'ascend');
	Dmulti = Dmulti (index);


figure();
%	subplot(2,2,1);
	%semilogx(errores,Dmulti,'o',errores,0.19*ones(1,length(errores)));
	plot(log(errores),Dmulti,'o',log(errores),0.19*ones(1,length(errores)));

	legend ('Valores KS multi', 'Umbral de aceptacion');
%	subplot(2,2,2);
%	semilogx(errores,D1uni,'o',errores,0.143*ones(1,length(errores)));
%	legend ('Valores KS uni', 'Umbral de aceptacion');
%	subplot(2,2,3);
%	semilogx(errores,D1uni,'o',errores,0.143*ones(1,length(errores)));
%	legend ('Valores KS uni', 'Umbral de aceptacion');

% 	figure();
% 	subplot(2,2,1);
% 	[n1, x1] = hist(log10(errores(a1)),50);
% 	[n2, x2] = hist(log10(errores(an1)),50);
% 	semilogx(10.^x1,n1, 10.^x2,n2);
% 	legend('hard Multi aceptados', 'hard Multi rechazados');
% 	
% 	subplot(2,2,2);
% 	[n1, x1] = hist(log10(errores(a3)),50);
% 	[n2, x2] = hist(log10(errores(an3)),50);
% 	semilogx(10.^x1,n1, 10.^x2,n2);
% 	legend('soft multi aceptados', 'soft multi rechazados');
% 
% 	subplot(2,2,3);
% 	[n1, x1] = hist(log10(errores(a2)),50);
% 	[n2, x2] = hist(log10(errores(an2)),50);
% 	semilogx(10.^x1,n1, 10.^x2,n2);
% 	legend('hard uni aceptados', 'hard uni rechazados');
% 
% 	subplot(2,2,4);
% 	[n1, x1] = hist(log10(errores(a4)),50);
% 	[n2, x2] = hist(log10(errores(an4)),50);
% 	semilogx(10.^x1,n1, 10.^x2,n2);
% 	legend('soft uni aceptados', 'soft uni rechazados');

	print('grafico_ks_fig_error_2pasos.eps');
end
