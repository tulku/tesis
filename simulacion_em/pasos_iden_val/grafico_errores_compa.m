function grafico_errores_compa(simulaciones)
	i = 1;
	for sim = simulaciones
		if (size(sim{1}) == [1 1] && !isnan(sim{1}.fig_error))
			errores(i) = sim{1}.fig_error;
			pasa_multi_hard(i) = sim{1}.r_val.pasa;
			if sim{1}.r_val.pasa_multi > 4
				pasa_uni_hard(i) = 1;
			else
				pasa_uni_hard(i) = 0;
			end
			W(i) = sim{1}.r_val.D;
			i += 1;
		end
	end
	
	[errores, index] = sort(errores, 'ascend');
	pasa_multi_hard = pasa_multi_hard (index);
	pasa_uni_hard = pasa_uni_hard (index);

	a1=find(pasa_multi_hard);
	a2=find(pasa_uni_hard);
	an1=find(!pasa_multi_hard);
	an2=find(!pasa_uni_hard);

	length(a2)
	length(an2)

	disp('KS');
	l1 = length(find(errores(a1)>0.1));
	l2 = length(find(errores(an1)>0.1));
	disp(['% De incorrectas aceptadas: ', num2str( l1*100/(l1+l2))]);
	disp(['% De incorrectas rechazadas: ', num2str( l2*100/(l1+l2))]);
	l1 = length(find(errores(a1)<0.04));
	l2 = length(find(errores(an1)<0.04));
	disp(['% De correctas aceptadas: ', num2str( l1*100/(l1+l2))]);
	disp(['% De correctas rechazadas: ', num2str( l2*100/(l1+l2))]);

	disp('Multi');
	l1 = length(find(errores(a2)>0.1));
	l2 = length(find(errores(an2)>0.1));
	disp(['% De incorrectas aceptadas: ', num2str( l1*100/(l1+l2))]);
	disp(['% De incorrectas rechazadas: ', num2str( l2*100/(l1+l2))]);
	l1 = length(find(errores(a2)<0.04));
	l2 = length(find(errores(an2)<0.04));
	disp(['% De correctas aceptadas: ', num2str( l1*100/(l1+l2))]);
	disp(['% De correctas rechazadas: ', num2str( l2*100/(l1+l2))]);

	figure();
	[n1, x1] = hist(log10(errores(a1)),50);
	[n2, x2] = hist(log10(errores(an1)),50);
	semilogx(10.^x1,n1, 10.^x2,n2);
	legend('KS Validas', 'KS Invalidas');
	
	figure();
	[n1, x1] = hist(log10(errores(a2)),50);
	[n2, x2] = hist(log10(errores(an2)),50);
	semilogx(10.^x1,n1, 10.^x2,n2);
	legend('CVM Validas', 'CVM Invalidas');

	print('grafico_errores_cero.eps');
	figure()
	plot(sort(W),'b',1:length(W),0.028*ones(1,length(W)),'r');
end
