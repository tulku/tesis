%function resultado_simulacion = simular(x_p, cant_total, cant_iter, SNR);
%datos = {Y, x_p, covar, cant_iter, hk, h};
function resultado_simulacion = simular(datos);
    Y_id = datos.Y(:,1:50);
    Y_val = datos.Y(:,51:end);
    x_p = datos.x_p;
    covar = datos.covar;
    cant_iter = datos.cant_iter;
    hk = datos.hk;
    h = datos.h;
    roto = 0;
    [hv] = dfe(Y_id, x_p, covar, cant_iter, hk);

    % Detectamos los simbolos transmitidos.
    ht=hv{length(hv)};
    [detectado, errores, e] = detector(ht,Y_val,x_p);
    fig_error = calc_error (h,ht);
      % Normalizo la figura de error.
    fig_error /= sqrt((norm(vec(h))^2));
    r = validador(errores,e,covar,detectado,datos.umbral,0);
    % Guardo los errores
    resultado_nombres = {'hv','detectado','errores','e','fig_error','h','covar','pasa','r_val','datos'};
    resultado_celda = {hv;detectado;errores;e;fig_error;h;covar;r.pasa;r;datos};
    resultado_simulacion = cell2struct(resultado_celda,resultado_nombres,1);
end
