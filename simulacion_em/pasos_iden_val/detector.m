% Esta funcion detecta los simbolos en el receptor.
% Devuelve en "detectado" el arreglo de simbolos y
% en "errores" el arreglo de diferencias entre la señal
% transmitida y el H estimado por x(k) detectado.
function [detectado, errores, e] = detector(hk,Y,x_p)

dis = norm(x_p(:,1)-x_p(:,2));

e = {};
indi = ones(1,length(x_p));
for i = length(x_p)
	e{i}=[];
end

for j=1:length(Y)
	aux = hk*x_p-(Y(:,j)*ones(1,length(x_p)));
	aux2 = sqrt(aux(1,:).^2+aux(2,:).^2);
	[i,ix] = min(aux2);
	errores(:,j) = aux(:,ix); 
	if aux(:,ix) < dis
		e{ix}(:,indi(ix)) = aux(:,ix);
		indi(ix) += 1;
	end
	detectado(:,j) = x_p(:,ix);
end

for w=1:length(e)
	if length(e{w}) <= 2
		continue;
	end
	e{w} = sacar_media(e{w});
	e{w} /= length(x_p);
end

function errores_cero = sacar_media(errores)
	cant_muestras = length(errores);
	promedio_errores = sum(errores')/cant_muestras;
	media = repmat(promedio_errores',1,cant_muestras);
	errores_cero = errores-media;
end

