% Esta funcion recorre el arreglo y busca cuando dos valores seguidos tienen
% menos de una cota de diferencia. Es para estimar cuando termino de iterar.
% hk tiene que ser de estimaciones x iteraciones. Se devuelve el numero de
% iteracion mayor de todos los estimadores.

function [tiempos] = calc_conver (hk)
cota = 1e-4;
t = size(hk);
%dif = cota*ones(1,t(1));
dif=cota;
tiempos = zeros(1,t(1));

for i = 1:t(1)
	for j = 2:t(2)
		dif = abs(hk(i,j-1)-hk(i,j));
		if (dif <= cota)
			break;
		end
	end
	tiempos(i) = j;
end
