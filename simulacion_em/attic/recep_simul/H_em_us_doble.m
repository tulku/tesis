function[hv]=H_em_us_doble(Y,x_p,covar,cant_iter,hk)
% covar: matriz de covarianza del ruido
icovar = inv(covar);

% Guardamos el valor inicial
hv = {hk};

cant_muestras = length(Y);
cant_iter=cant_iter+1;

for i=2:cant_iter

    acum1 = zeros(2,2);
    acum2 = zeros(2,2);

    for j=1:cant_muestras

	aux = 0;
	for k=1:length(x_p)
	    A=( (Y(:,j) - hk*x_p(:,k))' * icovar * (Y(:,j) - hk*x_p(:,k)) );
	    conjunta(k) = exp(-A/2);
	    aux = aux + x_p(:,k)*x_p(:,k)' * conjunta(k);
	end

	p_y = sum(conjunta);
	aux = aux/p_y;

	acum1 = acum1+aux;
    end

    for j=1:cant_muestras

	aux = 0;
	for k=1:length(x_p)
	    A=( (Y(:,j) - hk*x_p(:,k))' * icovar * (Y(:,j) - hk*x_p(:,k)) );
	    conjunta(k) = exp(-A/2);
	    aux = aux + Y(:,j)*x_p(:,k)' * conjunta(k);
	end
	p_y = sum(conjunta);
	aux = aux/p_y;
	
	%printf("%s%i%s%i%s%g%s%g%s","Iter: ",i, " Muestra: ",j, " Min: ", min(conjunta), " Max: ",max(conjunta),"\n")

	acum2 = acum2+aux;
    end

    %hk = inv(acum1)*acum2;
    hk = acum2*inv(acum1);
    hv{i} = hk;
end
%disp("Hk final")
%hk

