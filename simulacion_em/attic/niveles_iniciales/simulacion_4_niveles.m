clear;

cant_muestras = 100;
cant_iter = 30;
cant_cond_iniciales = 4;
SNR = 20; %dBs

%% Generacion de senales y canal.
% Matriz con el alfabeto
x_p =  [[-3,-3,-3,-3,-1,-1,-1,-1,1,1,1,1,3,3,3,3];
	[-3,-1,1,3,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3]];

% Generamos los datos a transmitir con 4 niveles
a = sign(randn(2,cant_muestras));
b = 2*sign(randn(2,cant_muestras));
X = a+b;

% Generamos el canal
disp('h original:');
h = abs(randn(2,2))

N = randn(2,cant_muestras);

% Generamos el ruido
covar = zeros(2,2);
% Borramos el 5 porque llegamos a la conclusion que nos esta afectando de mas
%covar(1,1) = 5*h(1,:)*h(1,:)'/(10^(SNR/10));
covar(1,1) = h(1,:)*h(1,:)'/(10^(SNR/10));
covar(2,2) = h(2,:)*h(2,:)'/(10^(SNR/10));

% Mezclamos todo
Y = h*X + sqrt(covar)*N;

for l = 1:cant_cond_iniciales
	% Estimamos el h inicial
	hk = abs(randn(2,2));
	
	%% Corremos los estimadores.
	
	% 2 niveles los 4 juntos
	hv_us_doble = H_em_us_doble(Y,x_p,covar,cant_iter,hk);
	
	% 2 niveles de a 2
	%hv_us_simple1 = H_em_us(Y(1,:),x_p,covar(1,1), cant_iter,hk(1,:)');
	%hv_us_simple2 = H_em_us(Y(2,:),x_p,covar(2,2), cant_iter,hk(2,:)');
	
	%% Calculo de errores
	% Calculamos la suma al cuadrado de la diferencia entre el
	% valor real y estimado para los 4 coeficientes.
	
	error_doble = calc_error(h,hv_us_doble{length(hv_us_doble)});
	%hk = zeros(2);
	%hk(1,:) = hv_us_simple1(:,length(hv_us_simple1))';
	%hk(2,:) = hv_us_simple2(:,length(hv_us_simple2))';
	%error_simple = calc_error(h,hk);
	
	%% Calculamos tiempos de convergencia.
	%t_simple = zeros(1,4);
	%t_simple([1 2]) = calc_conver (hv_us_simple1);
	%t_simple([3 4]) = calc_conver (hv_us_simple2);
	%t_simple = max(t_simple);
	
%	for i=1:cant_iter+1
%	        hva(1,i) = hv_us_doble{i}(1,1);
%	        hva(2,i) = hv_us_doble{i}(1,2);
%	        hva(3,i) = hv_us_doble{i}(2,1);
%	        hva(4,i) = hv_us_doble{i}(2,2);
%	end
%	t_doble = max(calc_conver (hva));

	%% Mostramos los errores
	disp('Error calculando todo junto'), disp(error_doble);

	%% Graficos
	aux1 = 1:cant_iter+1;
	aux2 = ones(1,cant_iter+1);
	l=[];
	
	figure();
	clf;
	title('Los 4 juntos');
	hold on;
	
	for i=1:cant_iter+1
		hv(i)=hv_us_doble{i}(1,1);
	end
	plot(aux1, hv, 'r', aux1, h(1,1)*aux2,'r--');
	%l=[l;'H11 estimado';'H11 real'];
	
	for i=1:cant_iter+1
		hv(i)=hv_us_doble{i}(1,2);
	end
	plot(aux1, hv, 'b', aux1, h(1,2)*aux2,'b--');
	%l=[l;'H12 estimado';'H12 real'];
	
	for i=1:cant_iter+1
		hv(i)=hv_us_doble{i}(2,1);
	end
	plot(aux1, hv, 'k', aux1, h(2,1)*aux2,'k--');
	%l=[l;'H21 estimado';'H21 real'];
	
	for i=1:cant_iter+1
		hv(i)=hv_us_doble{i}(2,2);
	end
	plot(aux1, hv, 'g', aux1, h(2,2)*aux2,'g--');
	%l=[l;'H22 estimado';'H22 real'];

	%legend(l);
	hold off;
end

%figure(2);
%clf;
%title('De a pares de 2')
%hold on;
%
%hv=hv_us_simple1(1,:);
%plot(aux1, hv, 'r', aux1, h(1,1)*aux2,'r--');
%
%hv=hv_us_simple1(2,:);
%plot(aux1, hv, 'b', aux1, h(1,2)*aux2,'b--');
%
%hv=hv_us_simple2(1,:);
%plot(aux1, hv, 'k', aux1, h(2,1)*aux2,'k--');
%
%hv=hv_us_simple2(2,:);
%plot(aux1, hv, 'g', aux1, h(2,2)*aux2,'g--');
%
%hold off;

%disp('Error calculando de a dos'), disp(error_simple);
disp('');
%% Mostrar iteracion de convergencia
%disp('Iteracion de convergencia al estimar todo junto'), disp(t_doble);
%disp('Iteracion de convergencia al estimar de a dos'), disp(t_simple);

