% Este es el estimador EM.
% Y: señal recibida.
% x_p: posibles señales recibidas.
% covar: matriz de covarianza del ruido
% cant_iter: cantidad de iteracion a realizar
% hk: valor inicial del estimador

% h: cell con la evolucion de la matriz h.
% s: cell con una señal "detectada" suave.
% sigma_s: cell con la varianza de los s estimados.

function[h, s, sigma_s] = s_sigma_s_em(Y,x_p,covar,cant_iter,hk)
cant_muestras = length(Y);
cant_iter=cant_iter+1;

icovar = inv(covar);
h = {};
s = zeros(2,cant_muestras);

% Guardamos el valor inicial

for i=1:cant_iter
	h = [h {hk}];
	aux2 = zeros(2,2);
	for j=1:cant_muestras
		aux2 = zeros(2,2);
		aux1 = zeros(2,1);
		for k=1:length(x_p)
		    A=( (Y(:,j) - hk*x_p(:,k))' * icovar * (Y(:,j) - hk*x_p(:,k)) )/2;
		    conjunta(k) = exp(-A);
		    aux1 = aux1 + x_p(:,k)*conjunta(k);
		end
		p_y = sum(conjunta);
		simbolo(:,j) = aux1/p_y;
		aux2 = aux2 + Y(:,j)*simbolo(:,j)';
	end

	hk = aux2 / cant_muestras;
	s = simbolo;
	sigma_s = hk;
end

% mse calcula la esperanza de y-xh. y es un escalar, xm es el X estimado e 1x2, xs es el sigma X estimado e 2x2, h e 2x1
function e = mse(h,xm,xs,y)
	e = norm(y-x*h)^2+h'*(xs-xm'*xm)*h;
end
