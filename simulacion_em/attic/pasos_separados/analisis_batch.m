% La Cell por cada simulacion tiene los siguientes datos:
%resultado_simulacion = {hv_us_doble, detectado, errores, var_tr_sigma_n, esperanza_tr_sigma_n, figura_error,
% promedio_errores, tr_sigma_error, esta_en_el_rango, h};
%
% En resultados:
% 1: cant_dentro_rango
% 2: error_dentro_rango
% 3: cant_fuera_rango
% 4: error_fuera_rango

function resultados = analisis_batch(graficar)

load('resultado_batch_mad.oct')

temp = {};
resultados = {};
cant_dentro_rango = 0;
cant_fuera_rango = 0;
error_dentro_rango = [];
error_fuera_rango = [];

%graficar = 1;

for sim = 1:cant_simulaciones
	temp = simulaciones{sim};
	if ( temp{11} == 1 )
		cant_dentro_rango = cant_dentro_rango +1;
		error_dentro_rango = [ error_dentro_rango, temp{8} ];
	else
		cant_fuera_rango = cant_fuera_rango +1;
		error_fuera_rango = [ error_fuera_rango, temp{8} ];
	end
	if ( graficar == 1 )
		mostrar_grafico(temp{1}, temp{12}, cant_iter, temp{11});
	end
end
resultados = { cant_dentro_rango, error_dentro_rango, cant_fuera_rango, error_fuera_rango };
end


function mostrar_grafico ( hv_us_doble, h, cant_iter, rango)
	%% Graficos
	aux1 = 1:cant_iter+1;
	aux2 = ones(1,cant_iter+1);

	figure();
	clf;
	titulo = 'Evolucion del estimador y valores reales. ';
	if ( rango == 1 )
		titulo = [titulo, 'En rango.'];
	else
		titulo = [titulo, 'Fuera de rango.'];
	end

	title(titulo);
	hold on;

	for i=1:cant_iter+1
	    hv(i)=hv_us_doble{i}(1,1);
	end
	plot(aux1, hv, 'r', aux1, h(1,1)*aux2,'r--');
	%l=[l;'H11 estimado';'H11 real'];

	for i=1:cant_iter+1
	    hv(i)=hv_us_doble{i}(1,2);
	end
	plot(aux1, hv, 'b', aux1, h(1,2)*aux2,'b--');
	%l=[l;'H12 estimado';'H12 real'];

	for i=1:cant_iter+1
	    hv(i)=hv_us_doble{i}(2,1);
	end
	plot(aux1, hv, 'k', aux1, h(2,1)*aux2,'k--');
	%l=[l;'H21 estimado';'H21 real'];

	for i=1:cant_iter+1
	    hv(i)=hv_us_doble{i}(2,2);
	end
	plot(aux1, hv, 'g', aux1, h(2,2)*aux2,'g--');
	%l=[l;'H22 estimado';'H22 real'];

	hold off;
end

