% Este es el estimador EM.
% Y: señal recibida.
% x_p: posibles señales recibidas.
% covar: matriz de covarianza del ruido
% cant_iter: cantidad de iteracion a realizar
% hk: valor inicial del estimador

% h: cell con la evolucion de la matriz h.
% s: cell con una señal "detectada" suave.
% sigma_s: cell con la varianza de los s estimados.

function[h, s, sigma_s] = s_sigma_s_em(Y,x_p,covar,cant_iter,hk)
cant_muestras = length(Y);
cant_iter=cant_iter+1;

icovar = inv(covar);
h = {};
s = zeros(2,cant_muestras);

% Guardamos el valor inicial

for i=1:cant_iter
	h = [h {hk}];
	for j=1:cant_muestras
		aux2 = zeros(2,2);
		aux1 = zeros(2,1);
		for k=1:length(x_p)
		    A=( (Y(:,j) - hk*x_p(:,k))' * icovar * (Y(:,j) - hk*x_p(:,k)) )/2;
		    conjunta(k) = exp(-A);
		    aux1 = aux1 + x_p(:,k)*conjunta(k);
		    aux2 = aux2 + x_p(:,k)*x_p(:,k)'*conjunta(k);
		end
		p_y = sum(conjunta);
		simbolo(:,j) = aux1/p_y;
		sigma_simbolo{j} = aux2/p_y;

	end

	temp1 = 0;
	temp2 = 0;
	temp3 = 0;
	for j=1:cant_muestras
		temp1 = temp1 + simbolo(:,j)*Y(1,j);
		temp2 = temp2 + simbolo(:,j)*Y(2,j);
		temp3 = temp3 + sigma_simbolo{j};
	end

	if det(temp3) != 0
		ha = inv(temp3)*temp1;
		hb = inv(temp3)*temp2;
		hk = [ha';hb'];
	elseif
		disp('Determinante cero');
		fflush(stdout);
	end
	s = simbolo;
	sigma_s = sigma_simbolo;
end

% mse calcula la esperanza de y-xh. y es un escalar, xm es el X estimado e 1x2, xs es el sigma X estimado e 2x2, h e 2x1
function e = mse(h,xm,xs,y)
	e = norm(y-x*h)^2+h'*(xs-xm'*xm)*h;
end
