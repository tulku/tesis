clear;

cant_muestras = 50;
cant_iter = 15;
shake = 0;
SNR = 20; %dBs
cant_simulaciones = 2000;

% Matriz con el alfabeto
x_p =  [[-3,-3,-3,-3,-1,-1,-1,-1,1,1,1,1,3,3,3,3];
	[-3,-1,1,3,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3]];

simulaciones = {};
resultado_simulacion = {};

for rep = 1:cant_simulaciones
	disp(['Simulacion ', num2str(rep), ' de ', num2str(cant_simulaciones)]);
	fflush(stdout);
	%% Generacion de senales y canal.
	% Generamos el canal
	h = abs(randn(2,2)*1.2);

	% Generamos el ruido
	covar = zeros(2,2);
	covar(1,1) = 5*h(1,:)*h(1,:)'/(10^(SNR/10));
	covar(2,2) = 5*h(2,:)*h(2,:)'/(10^(SNR/10));

	N = randn(2,cant_muestras);

	% Generamos los datos a transmitir con 4 niveles
	a = sign(randn(2,cant_muestras));
	b = 2*sign(randn(2,cant_muestras));
	X = a+b;

	% Mezclamos todo
	Y = h*X + sqrt(covar)*N;

	% Calculamos la varianza de la traza de la covarianza del ruido.
	var_tr_sigma_n = 2/4*trace(covar^2);
	esperanza_tr_sigma_n = trace(covar);

	% Creamos el h inicial
	hk = abs(randn(2,2)*1.2);

	% Corremos el estimador.
	[hv_us_doble, s, sigma_s] = s_sigma_s_em(Y,x_p,covar,cant_iter,hk,shake);
	% Detectamos los simbolos transmitidos.
	ht=hv_us_doble{length(hv_us_doble)};
	[detectado, errores] = detector(ht,Y,x_p);
	% Calculamos la figura de error.
	figura_error = calc_error(h,hv_us_doble{length(hv_us_doble)});

	promedio_errores = sum(errores')/cant_muestras;
	P = repmat(promedio_errores',1,cant_muestras);

	tr_sigma_error = sum(mad(errores').^2);

	esta_en_el_rango = 0;
	if ( abs(tr_sigma_error - esperanza_tr_sigma_n) < sqrt(var_tr_sigma_n) )
		esta_en_el_rango = 1;
	else
		esta_en_el_rango = 0;
	end

	resultado_simulacion = {hv_us_doble, s, sigma_s, detectado, errores, var_tr_sigma_n, esperanza_tr_sigma_n, figura_error, promedio_errores, tr_sigma_error, esta_en_el_rango, h};
	simulaciones{rep} = resultado_simulacion;
end
resultado_simulacion = {};
save('resultado_batch_mad.oct');
