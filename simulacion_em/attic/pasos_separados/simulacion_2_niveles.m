clear;

cant_muestras = 50;
cant_iter = 15;
shake = 0;
SNR = 20; %dBs

%% Generacion de senales y canal.
% Generamos el canal
disp('h original:');
h = abs(randn(2,2)*1.2)

% Generamos el ruido
covar = zeros(2,2);
covar(1,1) = h(1,:)*h(1,:)'/(10^(SNR/10));
covar(2,2) = h(2,:)*h(2,:)'/(10^(SNR/10));

N = randn(2,cant_muestras);

% Generamos los datos a transmitir con 4 niveles
X = sign(randn(2,cant_muestras));

% Mezclamos todo
Y = h*X + sqrt(covar)*N;

% Calculamos la varianza de la traza de la covarianza del ruido.
var_tr_sigma_n = 2/4*trace(covar^2);
esperanza_tr_sigma_n = trace(covar)

% Estimamos el h inicial
hk = abs(randn(2,2)*1.2);
%hk = h;

% Matriz con el alfabeto
x_p = [[1 1 -1 -1];[1 -1 1 -1]];

% Corremos el estimador.
[hk, s, sigma_s] = s_sigma_s_em(Y,x_p,covar,cant_iter,hk,shake);
% Detectamos los simbolos transmitidos.
ht=hk{length(hk)};
disp('h estimado:')
disp(ht);
[detectado, errores] = detector(ht,Y,x_p);

figura_error = calc_error(h,ht)

promedio_errores = sum(errores')/cant_muestras;
P = repmat(promedio_errores',1,cant_muestras);

tr_sigma_error_2 = 1/cant_muestras * sum(sum((errores - P).^2));
tr_sigma_error = trace(cov(errores'))

if ( abs(tr_sigma_error - esperanza_tr_sigma_n) < sqrt(var_tr_sigma_n) )
	disp('Está en el rango!');
else
	disp('Dio cualquiera');
end


%% Graficos
aux1 = 1:cant_iter+1;
aux2 = ones(1,cant_iter+1);
l=[];

figure(1);
clf;
title('Valores estimados y reales');
hold on;

for i=1:cant_iter+1
    hv(i)=hk{i}(1,1);
end
plot(aux1, hv, 'r', aux1, h(1,1)*aux2,'r--');
%l=[l;'H11 estimado';'H11 real'];

for i=1:cant_iter+1
    hv(i)=hk{i}(1,2);
end
plot(aux1, hv, 'b', aux1, h(1,2)*aux2,'b--');
%l=[l;'H12 estimado';'H12 real'];

for i=1:cant_iter+1
    hv(i)=hk{i}(2,1);
end
plot(aux1, hv, 'k', aux1, h(2,1)*aux2,'k--');
%l=[l;'H21 estimado';'H21 real'];

for i=1:cant_iter+1
    hv(i)=hk{i}(2,2);
end
plot(aux1, hv, 'g', aux1, h(2,2)*aux2,'g--');
%l=[l;'H22 estimado';'H22 real'];

%legend(l);
hold off;
print('simulacion_2_niveles.pdf')
