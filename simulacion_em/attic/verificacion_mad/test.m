load ('resultado_simulacion_batch_1000.oct');

% Tengo que guardar la matriz covar real en todas las simulaciones y no lo estaba haciendo.
l = length(simulaciones{i});
for i = 1:cant_simulaciones
	aux = simulaciones{i};
	errores = aux{3};
	covar = aux{l};
	sigma_n = aux{4};
	media = aux{5};
	var_tr_var_ruido(i) = sigma_n;
	cota_sup(i) = var_tr_var_ruido(i) + sigma_n;
	cota_inf(i) = var_tr_var_ruido(i) - sigma_n;
	tr_var_ruido(i) = trace(covar);

	tr_var_error_cov(i) = trace(cov(errores'));
	tr_var_error_mad(i) = sum(mad(errores').^2);
	rel_cov(i) = tr_var_error_cov(i) - media;
	rel_mad(i) = tr_var_error_mad(i) - media;
end

aux = [1:cant_simulaciones/10];
%hold on
%plot(aux,tr_var_ruido(aux),'r',aux,tr_var_error_cov(aux),'g',aux,tr_var_error_mad(aux),'b');
%legend('real','cov','mad');
plot(aux,tr_var_ruido(aux),'r',aux,rel_cov(aux),'g',aux,rel_mad(aux),'b');
%plot(aux,cota_sup(aux),'r-',aux,cota_inf(aux),'r-');
legend('real','cov','mad');

