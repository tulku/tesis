%% Funcion para calcular la verosimilitud
function[z]=calc_q_us(Y,x_p,sigma2,x,y,hk)
% Y: muestras del canal
% x_p: posibles mensajes trans
% sigma2: varianza del ruido
% x: rango de h_1
% y: rango de h_2
% hk: el valor 'actual' de h

z=zeros(length(x),length(y));

a=4*sqrt(2*pi*sigma2);

pos1=1;
for h1 = x
    pos2=1;
    for h2 = y
        hxy = [h1,h2];
        aux1 = 0;
        for i=1:length(Y)
            aux2 = 0;
	    aux3 = 0;
            for j=1:length(x_p)
		aux3=aux3+(1/a)*exp(-((Y(i)-(hk*x_p(:,j)))^2)/(2*sigma2));
		aux2=aux2+(-log(a)-((Y(i)-(hxy*x_p(:,j)))^2) )*(1/a)*exp(-((Y(i)-(hk*x_p(:,j)))^2)/(2*sigma2));
            end
            aux1 = aux1+(aux2/aux3);
        end
        z(pos1,pos2)=aux1;
        pos2=pos2+1;
    end
    pos1=pos1+1;
end
