% Nuevo super mega validador. Usa el test de Kolmorov para ver si los errores se
% ajustan a la normal que corresponde y ademas evalua que sean
% descorrelacionados.
 
% Test de Kolmorov
% segun http://www.ciphersbyritter.com/JAVASCRP/NORMCHIK.HTM#KolSmir
% y https://www-old.cae.wisc.edu/pipermail/help-octave/2008-April/008940.html

function pasa = validador(errores, covar, detectado, original, print)
	pasa = pasa2 = pasa1 = 0;
	ks_const = 0.1450;

	pasa = test_known (detectado, original);
	if pasa == 0
		return;
	end
	
	[ks,pasa1] = KS_test(errores(1,:), 0 , sqrt(covar(1,1)),ks_const);
	if pasa1 == 0 
		[ks,pasa2] = KS_test(errores(2,:), 0 , sqrt(covar(2,2)),ks_const);
	end
	ks_const = 0.1580;
	if pasa1 == 0 && pasa2 == 1
		[ks,pasa2] = KS_test(errores(1,:), 0 , sqrt(covar(1,1)),ks_const);
	end
	if pasa1 == 1 && pasa2 == 0
		[ks,pasa2] = KS_test(errores(2,:), 0 , sqrt(covar(2,2)),ks_const);
	end
	if pasa == 1 && print == 1
		disp('Da igual el conocido');
	end
	if pasa1 == 1 && print == 1
		disp('Pasa el primer test KS');
	end
	if pasa2 == 1 && print == 1
		disp('Pasa el segundo test KS');
	end
	pasa = pasa1 & pasa2 & pasa;
end

function [ks,pasa] = KS_test(X,m,s,const)
	X=sort(X);
	Xecdf=empirical_cdf(X,X);
	assumed_pop=normcdf(X,m,s);
	ks=max(abs(assumed_pop-Xecdf));
	pasa = ks < const;
end

function [R,pasa] = autocorr (X)
	l = length(X);
	R = zeros(1,l);
	pasa = 0;
	x_shift = X;
	for k = 1:l
		R(k) = sum (X.*x_shift)/l;
		x_shift = shift(X,k);
		x_shift(1:k)=zeros(1,k);
	end
	R = abs(R);
	media = mean(R);
	[val,pos] = max(R);
	if pos == 1
		figure();
		plot(R,'r',3*media*ones(1,l));
		R(pos)=0;
		if R<3*media
			pasa = 1;
		end
	end
end

function pasa = test_known (detectado, original)
	pasa = 0;
	if original == detectado
		pasa = 1;
	elseif original == shift(detectado,1)
		pasa = 1;
	end
end
