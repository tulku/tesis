clc;
clear;
close all;
tic;

cant_muestras = 500;
cant_conocidos = 1;
cant_iter = 15;
max_estimaciones = 2;
SNR = 20; %dBs

%% Generacion de senales y canal.
cant_total = cant_muestras+cant_conocidos;
% Generamos el canal
disp('h original:');
h = abs(randn(2,2)*1.2)

% Generamos el ruido
covar = zeros(2,2);
covar(1,1) = 5*h(1,:)*h(1,:)'/(10^(SNR/10));
covar(2,2) = 5*h(2,:)*h(2,:)'/(10^(SNR/10));
N = randn(2,cant_total);

% Generamos los datos a transmitir con 4 niveles
a = sign(randn(2,cant_total));
b = 2*sign(randn(2,cant_total));
X = a+b;

% Mezclamos todo
Y = h*X + sqrt(covar)*N;

% Estimamos el h inicial
hk = abs(randn(2,2)*1.2);

% Matriz con el alfabeto
x_p =  [[-3,-3,-3,-3,-1,-1,-1,-1,1,1,1,1,3,3,3,3];
	[-3,-1,1,3,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3]];

estimacion = 0;
pasa = 0;
while (pasa == 0 && estimacion < max_estimaciones)
	% Corremos el estimador DFE
	[hv, expo] = dfe(Y,x_p,covar,cant_iter,hk);
	ht=hv{length(hv)};
	% Detectamos los simbolos transmitidos.
	[detectado, errores] = detector(ht,Y,x_p);
	fig_error = calc_error (h,ht);
	estimacion = estimacion +1;
	disp('');
	disp(['Estimacion: ', num2str(estimacion)]);
	disp(['Figura error: ', num2str(fig_error)]);
	% Validamos la estimacion.
	%aux = cant_muestras+1:cant_total;
	%pasa = validador (errores, covar, detectado(:,aux), X(:,aux),1);
	fflush(stdout);
%	yy=Y(:,aux);
%	xx=X(:,aux);
%	hk = (yy*xx')/((xx*xx')+randn(2,2)*0.0001);
	mostrar_grafico(hv,h,cant_iter,['Estimacion ', num2str(estimacion), ' Largo ', num2str(length(Y)) ]);
	Y=Y(:,[1:50]);
	disp('');
	h_estimado=hv{length(hv)}
	toc;
end
