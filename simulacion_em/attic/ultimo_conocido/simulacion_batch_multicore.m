clc;
clear;
close all;

cant_muestras = 500;
cant_conocidos = 1;
cant_iter = 15;
cant_simulaciones = 200;
max_estimaciones = 30;
SNR = 20; %dBs
archivo_salida = 'resultado_simulacion_comparacion_multi.oct';

multicore=0;

% Matriz con el alfabeto
x_p =  [[-3,-3,-3,-3,-1,-1,-1,-1,1,1,1,1,3,3,3,3];
	[-3,-1,1,3,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3]];
cant_total = cant_muestras+cant_conocidos;

simulaciones = {};
datos = {};
fe_500 = zeros(1,cant_simulaciones);
fe_50 = zeros(1,cant_simulaciones);
cant_total = cant_conocidos + cant_muestras;

%simulaciones = startmulticoremaster(@simular, parameterCell);
for rep = 1:cant_simulaciones
        %h = zeros(2,2);
        %% Generacion de senales y canal.
        h = abs(randn(2,2)*1.2);
        % Generamos el ruido
        covar = zeros(2,2);
        covar(1,1) = 5*h(1,:)*h(1,:)'/(10^(SNR/10));
        covar(2,2) = 5*h(2,:)*h(2,:)'/(10^(SNR/10));
        N = randn(2,cant_total);
        % Generamos los datos a transmitir con 4 niveles
        a = sign(randn(2,cant_total));
        b = 2*sign(randn(2,cant_total));
        X = a+b;
        % Mezclamos todo
        Y = h*X + sqrt(covar)*N;
	hk = abs(randn(2,2)*1.2);
	aux = {Y, x_p, covar, cant_iter, hk, h};
	datos{rep}=aux;
end

if multicore == 1
	simulaciones = startmulticoremaster(@simular, datos);
else
	for rep = 1:cant_simulaciones
		simulaciones{rep} = simular(datos{rep});
	end
end

%resultado_simulacion={hv1, hv2, detectado1, detectado2, errores1, errores2, fig_error_500, fig_error_50, h, covar};
for rep = 1:cant_simulaciones
	temp = simulaciones{rep};
	fe_500(rep) = temp{7};
	fe_50(rep) = temp{8};
	mostrar_grafico(temp{1},temp{9},cant_iter,"test");
end
figure();
plot(fe_500,'r',fe_50,'b');

resultado_simulacion = {};
save(archivo_salida);
