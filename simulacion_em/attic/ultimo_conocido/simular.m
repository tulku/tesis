%function resultado_simulacion = simular(x_p, cant_total, cant_iter, SNR);
%datos = {Y, x_p, covar, cant_iter, hk, h};
function resultado_simulacion = simular(datos);
	Y=datos{1};
	x_p=datos{2};
	covar=datos{3};
	cant_iter=datos{4};
	hk=datos{5};
	h=datos{6};

	% Corremos el estimador con 500 muestras
	[hv1, expo] = dfe(Y,x_p,covar,cant_iter,hk);
	ht=hv1{length(hv1)};
	% Detectamos los simbolos transmitidos.
	[detectado1, errores1] = detector(ht,Y,x_p);
	fig_error_500 = calc_error (h,ht);

	% Corremos el estimador con 50 muestras
	Y=Y(:,[1:50]);
	[hv2, expo] = dfe(Y,x_p,covar,cant_iter,hk);
	ht=hv2{length(hv2)};
	% Detectamos los simbolos transmitidos.
	[detectado2, errores2] = detector(ht,Y,x_p);
	fig_error_50 = calc_error (h,ht);

	%guardo los errores
	resultado_simulacion={hv1, hv2, detectado1, detectado2, errores1, errores2, fig_error_500, fig_error_50, h, covar};
end
