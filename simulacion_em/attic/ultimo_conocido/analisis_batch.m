% La Cell por cada simulacion tiene los siguientes datos:

% resultado_simulacion =
% 1 hv,
% 2 detectado,
% 3 errores, 
% 4 fig_error,
% 5 h,
% 6 estimacion,
% 7 pasa,
% 8 covar


function resultados = analisis_batch(graficar)
close all;
clc;
load('resultado_simulacion_batch.oct')

temp = {};
resultados = {};

cant_estimaciones = [];
bien_posta = 0;
cant_menos_30 = 0;
cant_30_fuera = 0;
cant_30_dentro = 0;
errores_menos_30 = [];
indices_menos_30 = [];
errores_30 = [];
indices_30 = [];

malas = 0;
cota = 20;
cota_bien_posta = 0.008;

for sim = 1:cant_simulaciones
	temp = simulaciones{sim};
	if isnan(temp{3})
		malas = malas +1;
		continue;
	end
	if ( temp{4} < cota_bien_posta && temp{7} == 1 ) 
		bien_posta = bien_posta +1;
	end
	cant_estimaciones = [ cant_estimaciones, temp{6} ];	
	% Simulaciones con menos de 30 estimaciones
	if ( temp{6} < cota )
		cant_menos_30 = cant_menos_30 +1;
		errores_menos_30 = [ errores_menos_30, temp{4} ];
		indices_menos_30 = [ indices_menos_30, sim ];
		if ( temp{4} < 0.01 && temp{4} > 0.008 && graficar == 1 )
			mostrar_grafico(temp{1}, temp{5}, cant_iter, ['Error: ', num2str(temp{4})]);
		end
	% Simulaciones con 30 estimaciones
	elseif ( temp{7} == 1 )
		% Pasan
		cant_30_dentro = cant_30_dentro +1;
		errores_30 = [ errores_30, temp{4} ];
		indices_30 = [ indices_30, sim ];
	elseif ( temp{6} >= cota && temp{7} == 0 )
		% No pasan
		cant_30_fuera = cant_30_fuera +1;
	end
end

cant_30 = cant_30_dentro+cant_30_fuera;

correctas = cant_simulaciones - malas;
disp('');
disp([ 'Cantidad simulaciones correctas: ' num2str(correctas)]);
disp([ '% cantidad simulaciones bien posta: ' num2str(bien_posta * 100 /correctas)]);
disp('');
disp('Menos de "la cota" estimaciones (todas correctas)');
disp([ '% Cantidad: ' num2str(cant_menos_30 * 100 /correctas)]); 
disp([ 'Media de errores: ' num2str(mean(errores_menos_30))]);
disp([ 'Mediana de errores: ' num2str(median(errores_menos_30))]);
disp('');
disp('"Cota" estimaciones');
disp([ '% Cantidad: ' num2str(cant_30 * 100 /correctas)]); 
disp([ '% Correctas: ' num2str(cant_30_dentro * 100 /cant_30)]); 
disp([ 'Media de errores aceptadas: ' num2str(mean(errores_30))]);
disp('');

if graficar == 0
	figure(1);
	plot (errores_menos_30,'ro',errores_30,'bo');
	legend('Errores menos 30', 'Errores 30');
	figure(2);
	hist(cant_estimaciones,0:30);

	[val,pos] = graficar_max (simulaciones, cant_iter, errores_menos_30, indices_menos_30, ' Max error menos de "la cota"');
	disp(['Max error menos de "la cota": ' num2str(val)]);
end

resultados = { correctas, cant_menos_30, errores_menos_30, indices_menos_30, cant_30_dentro, cant_30_fuera,...
errores_30, indices_30, bien_posta};
end

function [val, pos] = graficar_max (simulaciones, cant_iter, errores, indices, texto) 
	[val,pos] = max(errores);
	sim = indices(pos);
	temp = simulaciones{sim};
	mostrar_grafico(temp{1}, temp{5}, cant_iter, texto);
end

function [val, pos] = graficar_min (simulaciones, cant_iter, errores, indices, texto) 
	[val,pos] = min(errores);
	sim = indices(pos);
	temp = simulaciones{sim};
	disp(['h','num2str( temp{6})']);
	disp(['hk','num2str( temp{1}{length(temp{1})})']);
	mostrar_grafico(temp{1}, temp{6}, cant_iter, texto);
end

function [val, pos] = graficar_error_min (simulaciones, cant_iter, errores, indices, texto) 
	[val,pos] = min(errores);
	sim = indices(pos);
	temp = simulaciones{sim};
	mean(temp{3});
	mostrar_grafico_error(temp{3}, texto);
end

function [val, pos] = graficar_error_max (simulaciones, cant_iter, errores, indices, texto) 
	[val,pos] = max(errores);
	sim = indices(pos);
	temp = simulaciones{sim};
	mean(temp{3});
	mostrar_grafico_error(temp{3}, texto);
end

function mostrar_grafico ( hv_us_doble, h, cant_iter, texto)
	%% Graficos
	aux1 = 1:cant_iter+1;
	aux2 = ones(1,cant_iter+1);
	figure();
	clf;
	titulo = 'Evolucion del estimador y valores reales. ';
	titulo = [titulo, texto];
	title(titulo);
	hold on;

	for i=1:cant_iter+1
	    hv(i)=hv_us_doble{i}(1,1);
	end
	plot(aux1, hv, 'r', aux1, h(1,1)*aux2,'r--');
	for i=1:cant_iter+1
	    hv(i)=hv_us_doble{i}(1,2);
	end
	plot(aux1, hv, 'b', aux1, h(1,2)*aux2,'b--');
	for i=1:cant_iter+1
	    hv(i)=hv_us_doble{i}(2,1);
	end
	plot(aux1, hv, 'k', aux1, h(2,1)*aux2,'k--');
	for i=1:cant_iter+1
	    hv(i)=hv_us_doble{i}(2,2);
	end
	plot(aux1, hv, 'g', aux1, h(2,2)*aux2,'g--');

	hold off;
end

function mostrar_grafico_error ( errores, texto)
	%% Graficos
	l=length(errores);
	figure();
	clf;
	titulo = 'Errores. ';
	titulo = [titulo, texto];
	title(titulo);
	hold on;
	plot (1:l,errores(1,:),'r',1:l,errores(2,:),'b');
	hold off;
end

