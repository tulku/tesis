clc;
clear;
close all;
tic;

cant_muestras = 500;
cant_conocidos = 1;
cant_iter = 15;
cant_simulaciones = 1000;
max_estimaciones = 30;
SNR = 20; %dBs
archivo_salida = 'resultado_simulacion_batchi_comparacion.oct';

% Matriz con el alfabeto
x_p =  [[-3,-3,-3,-3,-1,-1,-1,-1,1,1,1,1,3,3,3,3];
	[-3,-1,1,3,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3]];
cant_total = cant_muestras+cant_conocidos;

simulaciones = {};
resultado_simulacion = {};
fe_500 = zeros(1,cant_simulaciones);
fe_50 = zeros(1,cant_simulaciones);

for rep = 1:cant_simulaciones
	%% Generacion de senales y canal.
	h = abs(randn(2,2)*1.2);
	% Generamos el ruido
	covar = zeros(2,2);
	covar(1,1) = 5*h(1,:)*h(1,:)'/(10^(SNR/10));
	covar(2,2) = 5*h(2,:)*h(2,:)'/(10^(SNR/10));
	N = randn(2,cant_total);
	% Generamos los datos a transmitir con 4 niveles
	a = sign(randn(2,cant_total));
	b = 2*sign(randn(2,cant_total));
	X = a+b;
	% Mezclamos todo
	Y = h*X + sqrt(covar)*N;
	% Estimamos el h inicial
	hk = abs(randn(2,2)*1.2);
	
	% Corremos el estimador con 500 muestras
	[hv, expo] = dfe(Y,x_p,covar,cant_iter,hk);
	ht=hv{length(hv)};
	% Detectamos los simbolos transmitidos.
	[detectado, errores] = detector(ht,Y,x_p);
	fig_error_500 = calc_error (h,ht);

	% Corremos el estimador con 50 muestras
	Y=Y(:,[1:50]);
	[hv, expo] = dfe(Y,x_p,covar,cant_iter,hk);
	ht=hv{length(hv)};
	% Detectamos los simbolos transmitidos.
	[detectado, errores] = detector(ht,Y,x_p);
	fig_error_50 = calc_error (h,ht);

	%guardo los errores
	fe_500(rep) = fig_error_500;
	fe_50(rep) = fig_error_50;

	disp(['Simulacion: ',num2str(rep),' de ',num2str(cant_simulaciones)]);
	fflush(stdout);
	
	resultado_simulacion={hv, detectado, errores, fig_error_500, fig_error_50, h, covar};
	simulaciones{rep} = resultado_simulacion;
	if !mod(rep,100)
		save(archivo_salida);
	end
end
resultado_simulacion = {};
save(archivo_salida);

toc;
