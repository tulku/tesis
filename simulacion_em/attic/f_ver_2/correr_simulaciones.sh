#!/bin/bash

for i in 1 2 3 4 5; do
	mkdir 4-20-${i}
	cd 4-20-${i}
	octave ../sim_em_4_niveles.m
	cd ..
done

for i in 1 2 3 4 5; do
    mkdir 2-20-${i}
    cd 2-20-${i} 
    octave ../sim_em_2_niveles.m
	cd ..
done

