#!/bin/bash

for i in 1 2 3 4 5; do
	cd 2-12-${i}
	octave ../verosimilitud.m
	cd ..
	cd 2-20-${i}
	octave ../verosimilitud.m
	cd ..
	cd 4-12-${i}
	octave ../verosimilitud.m
	cd ..
	cd 4-20-${i}
	octave ../verosimilitud.m
	cd ..
done

