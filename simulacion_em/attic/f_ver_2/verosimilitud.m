load datos.oct

cant_puntos = 49;

% Creamos la grilla de valores de h
x=linspace(hk(1)-2,hk(1)+2,cant_puntos);
y=linspace(hk(2)-2,hk(2)+2,cant_puntos);

pos1=1;
for h1 = x
	pos2=1;
	for h2 = y
		hxy = [h1,h2];
		aux1 = 1;
		for i=1:length(Y)
			aux2 = 0;
			for j=1:length(x_p)
				aux2 = aux2 + exp(-(Y(i) - (hxy*x_p(:,j)))**2);
			endfor
			aux1 = aux1*aux2;
		endfor
		z(pos1,pos2)=aux1;
		pos2=pos2+1;
	endfor
	pos1=pos1+1;
endfor

mesh(x,y,z);

% Calculamos la posicion del h verdadero en funcion del rango [-2:2] alrededor de hk
hx = round((h(1)-hk(1)+2)/4*cant_puntos);
hy = round((h(2)-hk(2)+2)/4*cant_puntos);

zmin = min(min(z));
zmax = max(max(z));

hold on;
plot3(x(hx)*ones(1,2),y(hy)*ones(1,2),[zmin,zmax],"-@o;h verdadera;")
plot3(x(round(cant_puntos/2))*ones(1,2),y(round(cant_puntos/2))*ones(1,2),[zmin,zmax],"-r@o;h estimada;")
print("vero.eps")
hold off;

save verosim.oct
