%function resultado_simulacion = simular(x_p, cant_total, cant_iter, SNR);
%datos = {Y, x_p, covar, cant_iter, hk, h};
function resultado_simulacion = simular(datos);
	Y=datos{1};
	x_p=datos{2};
	covar=datos{3};
	cant_iter=datos{4};
	hk=datos{5};
	h=datos{6};
	roto = 0;
	pasa=pasa_r=0;
	% Corremos el estimador normal
	[hv, expo] = dfe(Y,x_p,covar,cant_iter,hk);
	ht=hv{length(hv)};
	% Detectamos los simbolos transmitidos.
%	[detectado, errores] = detector(ht,Y,x_p);
	fig_error = calc_error (h,ht);
%	pasa = validador(errores, covar, detectado, 0);

	% Corremos el estimador robusto
	[hv_r, expo_r, roto] = dfe_r(Y,x_p,covar,cant_iter,hk);
	ht=hv_r{length(hv_r)};
	% Detectamos los simbolos transmitidos.
%	[detectado_r, errores_r] = detector(ht,Y,x_p);
	fig_error_r = calc_error (h,ht);
%	pasa_r = validador(errores_r, covar, detectado_r, 0);
	%guardo los errores
	resultado_simulacion={h, hv, fig_error, hv_r, fig_error_r};
end
