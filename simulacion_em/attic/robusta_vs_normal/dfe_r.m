% Este es el estimador EM.
% Y: señal recibida.
% x_p: posibles señales recibidas.
% covar: matriz de covarianza del ruido
% cant_iter: cantidad de iteracion a realizar
% hk: valor inicial del estimador

% h: cell con la evolucion de la matriz h.
% s: cell con una señal "detectada" suave.
% sigma_s: cell con la varianza de los s estimados.

function[h, s, roto] = dfe_r(Y,x_p,covar,cant_iter,hk)
	cant_muestras = length(Y);
	cant_iter=cant_iter+1;
	
	icovar = inv(covar);
	h = {};
	s = zeros(2,cant_muestras);
	roto = 0;	
	med = 0;
	beta = 1.75;
	var = trace(covar)/2;
	% Guardamos el valor inicial
	
	for i=1:cant_iter
		h = [h {hk}];
		temp1=zeros(2,2);
		temp2=zeros(2,2);
		difs=zeros(2,cant_muestras);
		for j=1:cant_muestras
			aux2 = zeros(2,2);
			aux1 = zeros(2,1);
			for k=1:length(x_p)
			    A=( (Y(:,j) - hk*x_p(:,k))' * (Y(:,j) - hk*x_p(:,k)) )/var*2;
			    conjunta(k) = exp(-A);
			    aux1 = aux1 + x_p(:,k)*conjunta(k);
			    aux2 = aux2 + x_p(:,k)*x_p(:,k)'*conjunta(k);
			end
			p_y = sum(conjunta);
			if p_y <= 1e-500
            	p_y = 42e-300;
            	roto = 1;
        	end
			simbolo(:,j) = aux1/p_y;
			difs(:,j)=Y(:,j)-hk*simbolo(:,j);
			b=sqrt(2)*difs(:,j)/var;
			%b = difs(:,j)-med/var;
			if b <= beta
				temp1=temp1+Y(:,j)*simbolo(:,j)';
				temp2=temp2+simbolo(:,j)*simbolo(:,j)';
			end
		end
		var = mean(mad(difs'));
		med = median(difs')';
		if var <= 1e-20
			var = trace(covar)/2;
			roto = 1;
		end
		if det(temp2) != 0	
			hk = temp1/temp2;
		else
			roto = 1;
		end
		s = simbolo;
	end
end
