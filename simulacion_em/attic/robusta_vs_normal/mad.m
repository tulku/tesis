function v = mad(x)
	tam = size(x);
	alfa = (0.6745)^-1;
	for i = 1:tam(2)
		col = x(:,i);
		med_col = median(col);
		aux = abs(col - med_col);
		v(i) = alfa*median(aux);
	end
end
