clc;
clear;
close all;
warning ('error','Octave:divide-by-zero');

cant_muestras = 50;
cant_iter = 20;
SNR = [1 2 5 8 10 12 15 18]; %dBs
cant_simulaciones = length(SNR);
archivo_salida = 'resultado_simulacion_siso_snr_test.oct';

multicore=0;

% Matriz con el alfabeto
x_p =  [-3,-1,1,3];

simulaciones = cell(cant_simulaciones,1);
datos = cell(cant_simulaciones,1);

%% Generacion de datos a simular.
disp('Generando simulaciones...');
fflush(stdout);


%% Generacion de senales y canal.
h = abs(randn(1));
% Generamos el ruido
N = randn(1,cant_muestras);
% Generamos los datos a transmitir con 4 niveles
a = sign(randn(1,cant_muestras));
b = 2*sign(randn(1,cant_muestras));
X = a+b;
% Mezclamos todo
hk = abs(randn(1));

for rep = 1:cant_simulaciones
        % Generamos el ruido
        covar = h^2/(10^(SNR(rep)/10));
        % Mezclamos todo
        Y = h*X + sqrt(covar)*N;
		aux_names = {'Y', 'x_p', 'covar', 'cant_iter', 'hk', 'h'};
		aux = cell2struct({Y; x_p; covar; cant_iter; hk; h},aux_names,1);
		datos{rep}=aux;
end
tic

%% Corro las simulaciones
if multicore == 1
	simulaciones = startmulticoremaster(@simular, datos);
else
	for rep = 1:cant_simulaciones
		disp(['Simulacion: ',num2str(rep)]);
		fflush(stdout);
		simulaciones{rep} = simular(datos{rep});

		if !mod(rep,100)
			save(archivo_salida);
		end
	end
end

%% Hago algun analisis de los resultados
menor_error = -100*ones(1,cant_simulaciones);
for rep = 1:cant_simulaciones
	temp = simulaciones{rep};
	if temp.roto == 1
		if cant_simulaciones < 20
			disp([num2str(SNR(rep)),') ','Simulacion rota']);
		end
		continue;
	end

	if cant_simulaciones < 20
		% Escribo valores
		disp([num2str(SNR(rep)),') ', 'Error comun: ', num2str(temp.fig_error) ]);
		disp([' - Error robusto 0: ', num2str(temp.fig_error_r0)]);
		disp([' - Error robusto 1.75: ', num2str(temp.fig_error_r175)]);
		disp([' - Error robusto 2.5: ', num2str(temp.fig_error_r25)]);
		% Graficos		
		hs.robusto0 = temp.hv_r0;
		hs.robusto25 = temp.hv_r25;
		hs.robusto175 = temp.hv_r175;
		hs.normal = temp.hv;
		mostrar_grafico(hs,temp.h,cant_iter, num2str(SNR(rep)));
	end
	figs_error = [temp.fig_error, temp.fig_error_r0, ...
				temp.fig_error_r175, temp.fig_error_r25];

	[fes, pos] = sort(figs_error);
	menor_error(rep)=pos(1);
end
% 1 - normal
% 2 - robusto con pendiente 0
% 3 - robusto con pendiente 1.75
% 4 - robusto con pendiente 2.5

plot(sort(menor_error(menor_error>0)));
disp(['Simulaciones correctas: ', num2str(length(menor_error(menor_error>0)))]);
toc

save(archivo_salida);
