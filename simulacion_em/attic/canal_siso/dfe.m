% Este es el estimador EM.
% Y: señal recibida.
% x_p: posibles señales recibidas.
% covar: matriz de covarianza del ruido
% cant_iter: cantidad de iteracion a realizar
% hk: valor inicial del estimador

% h: cell con la evolucion de la matriz h.
% s: cell con una señal "detectada" suave.
% sigma_s: cell con la varianza de los s estimados.

function[h, s, sigma_s] = dfe(Y,x_p,covar,cant_iter,hk)
cant_muestras = size(Y)(2);
cant_entradas = size(Y)(1);
cant_iter=cant_iter+1;

icovar = inv(covar);
h = zeros(1,cant_iter);
s = zeros(cant_entradas,cant_muestras);

% Guardamos el valor inicial

for i=1:cant_iter
	h(i) = hk;
	temp = zeros(1,cant_entradas);
	temp3 = 0;
	for j=1:cant_muestras
		aux2 = zeros(cant_entradas,cant_entradas);
		aux1 = zeros(cant_entradas,1);
		for k=1:length(x_p)
		    A=( (Y(:,j) - hk*x_p(:,k))' * icovar * (Y(:,j) - hk*x_p(:,k)) )/2;
		    conjunta(k) = exp(-A);
		    aux1 = aux1 + x_p(:,k)*conjunta(k);
		    aux2 = aux2 + x_p(:,k)*x_p(:,k)'*conjunta(k);
		end
		p_y = sum(conjunta);
		simbolo(:,j) = aux1/p_y;
		sigma_simbolo{j} = aux2/p_y;
		for x=1:cant_entradas 
			temp(x) = temp(x) + simbolo(:,j)*Y(x,j);
		end
		temp3 = temp3 + sigma_simbolo{j};
	end

	for x=1:cant_entradas
		hc(x) = inv(temp3)*temp(x);
		hk(x,:)=hc'(x);
	end
	s = simbolo;
	sigma_s = sigma_simbolo;
end
