% Este es el estimador EM.
% Y: señal recibida.
% x_p: posibles señales recibidas.
% covar: matriz de covarianza del ruido
% cant_iter: cantidad de iteracion a realizar
% hk: valor inicial del estimador

% h: cell con la evolucion de la matriz h.
% s: cell con una señal "detectada" suave.
% sigma_s: cell con la varianza de los s estimados.

function [hv] = dfe_r(Y,x_p,covar,cant_iter,hk,beta,pendiente)
	cant_muestras = size(Y)(2);
	cant_entradas = size(Y)(1);
	cant_iter=cant_iter+1;
	
	icovar = inv(covar);
	hv = zeros(1,cant_iter);
	var = trace(covar)/cant_entradas;
	
	for i=1:cant_iter
		hv(i) = hk;
		temp1=zeros(cant_entradas,cant_entradas);
		temp2=zeros(cant_entradas,cant_entradas);
		difs =zeros(cant_entradas,cant_muestras);
		delta = 0;

		for j=1:cant_muestras
			aux2 = zeros(cant_entradas,cant_entradas);
			aux1 = zeros(cant_entradas,1);
			for k=1:length(x_p)
			    A=( (Y(:,j) - hk*x_p(:,k))' * (Y(:,j) - hk*x_p(:,k)) )/var*2;
			    conjunta(k) = exp(-A);
			    aux1 = aux1 + x_p(:,k)*conjunta(k);
			    aux2 = aux2 + x_p(:,k)*x_p(:,k)'*conjunta(k);
			end
			p_y = sum(conjunta);
			simbolo(:,j) = aux1/p_y;
			hd = x_p - simbolo(:,j)*ones(1,length(x_p));
			for x = 1:length(hd)
           		     hd2(x) = norm(hd(:,x));
        		end
		        [i,ix] = min(hd2);
        		detectado(:,j) = x_p(:,ix);

			difs(:,j)=Y(:,j)-hk*detectado(:,j);
			b=difs(:,j)/var;
			if abs(b) <= beta
				temp1=temp1+Y(:,j)*simbolo(:,j)';
				temp2=temp2+simbolo(:,j)*simbolo(:,j)';
			elseif b < -beta
				delta = delta - (pendiente*simbolo(:,j)');
			elseif b > beta
				delta = delta + (pendiente*simbolo(:,j)');
			end
		end

		hk = ((temp1/var)+delta)/(temp2/var);
		var = abs(sum(mad(difs')))/(sqrt(2)*0.6745);
	end

end
