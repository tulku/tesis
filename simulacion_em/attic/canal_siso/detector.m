% Esta funcion detecta los simbolos en el receptor.
% Devuelve en "detectado" el arreglo de simbolos y
% en "errores" el arreglo de diferencias entre la señal
% transmitida y el H estimado por x(k) detectado.
function [detectado, errores] = detector(hk,Y,x_p)

for j=1:length(Y)
	aux = hk*x_p-(Y(:,j)*ones(1,length(x_p)));
	for x = 1:length(aux)
		aux2(x) = norm(aux(:,x));
	end
	[i,ix] = min(aux2);
	errores(:,j) = aux(:,ix); 
	detectado(:,j) = x_p(:,ix);
end
