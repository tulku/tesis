clc;
clear;
close all;
warning ('error','Octave:divide-by-zero');

cant_muestras = 50;
cant_iter = 20;
cant_simulaciones = 2000;
SNR = 5; %dBs
archivo_salida = 'resultado_simulacion_siso_multi_test.oct';

multicore=1;

% Matriz con el alfabeto
x_p =  [-3,-1,1,3];

simulaciones = cell(cant_simulaciones,1);
datos = cell(cant_simulaciones,1);

%% Generacion de datos a simular.
disp('Generando simulaciones...');
fflush(stdout);
for rep = 1:cant_simulaciones
        %% Generacion de senales y canal.
        h = abs(randn(1));
        % Generamos el ruido
        covar = h^2/(10^(SNR/10));
        N = randn(1,cant_muestras);
        % Generamos los datos a transmitir con 4 niveles
        a = sign(randn(1,cant_muestras));
        b = 2*sign(randn(1,cant_muestras));
        X = a+b;
        % Mezclamos todo
        Y = h*X + sqrt(covar)*N;
		hk = abs(randn(1));
		aux_names = {'Y', 'x_p', 'covar', 'cant_iter', 'hk', 'h'};
		aux = cell2struct({Y; x_p; covar; cant_iter; hk; h},aux_names,1);
		datos{rep}=aux;
	end
tic

%% Corro las simulaciones
if multicore == 1
	simulaciones = startmulticoremaster(@simular, datos, '/tmp');
else
	for rep = 1:cant_simulaciones
		disp(['Simulacion: ',num2str(rep)]);
		fflush(stdout);
		simulaciones{rep} = simular(datos{rep});

		if !mod(rep,100)
			save(archivo_salida);
		end
	end
end

%% Hago algun analisis de los resultados
menor_error = -100*ones(1,cant_simulaciones);
repi=1;
for rep = 1:cant_simulaciones
	temp = simulaciones{rep};
	if temp.roto == 1
		if cant_simulaciones < 20
			disp([num2str(rep),') ','Simulacion rota']);
		end
		continue;
	end

	if cant_simulaciones < 20
		% Escribo valores
		disp([num2str(rep),') ', 'Error comun: ', num2str(temp.fig_error) ]);
		disp([' - Error robusto 0: ', num2str(temp.fig_error_r0)]);
		disp([' - Error robusto 1.75: ', num2str(temp.fig_error_r175)]);
		disp([' - Error robusto 2.5: ', num2str(temp.fig_error_r25)]);
		% Graficos		
		hs.robusto0 = temp.hv_r0;
		hs.robusto25 = temp.hv_r25;
		hs.robusto175 = temp.hv_r175;
		hs.normal = temp.hv;
		mostrar_grafico(hs,temp.h,cant_iter, num2str(rep));
	end
	figs_error = [temp.fig_error, temp.fig_error_r0, ...
		temp.fig_error_r175, temp.fig_error_r25];
	pasan = [temp.pasa, temp.pasa_r0, temp.pasa_r25, temp.pasa_r175];

	normal.errores(repi) = temp.fig_error;
	r25.errores(repi) = temp.fig_error_r25;
	r175.errores(repi) = temp.fig_error_r175;
	r0.errores(repi) = temp.fig_error_r0;

	normal.pasa(repi) = temp.pasa;
	r25.pasa(repi) = temp.pasa_r25;
	r175.pasa(repi) = temp.pasa_r175;
	r0.pasa(repi) = temp.pasa_r0;

	[fes, pos] = sort(figs_error);
	menor_error(rep)=pos(1);
	repi=repi+1;
end
% 0 - normal
% 1 - robusto con pendiente 0
% 2 - robusto con pendiente 1.75
% 3 - robusto con pendiente 2.5

plot(sort(menor_error(menor_error>0)));
disp(['Simulaciones correctas: ', num2str(length(menor_error(menor_error>0)))]);

[normal.pasa, i] = sort(normal.pasa,'ascend');
normal.errores = normal.errores(i);
temp = normal.errores;
normal.errores(normal.pasa==0) = sort (temp(normal.pasa==0));
normal.errores(normal.pasa==1) = sort (temp(normal.pasa==1));

[r0.pasa, i] = sort(r0.pasa,'ascend');
r0.errores = r0.errores(i);
temp = r0.errores;
r0.errores(r0.pasa==0) = sort (temp(r0.pasa==0));
r0.errores(r0.pasa==1) = sort (temp(r0.pasa==1));

[r25.pasa, i] = sort(r25.pasa,'ascend');
r25.errores = r25.errores(i);
temp = r25.errores;
r25.errores(r25.pasa==0) = sort (temp(r25.pasa==0));
r25.errores(r25.pasa==1) = sort (temp(r25.pasa==1));

[r175.pasa, i] = sort(r175.pasa,'ascend');
r175.errores = r175.errores(i);
temp = r175.errores;
r175.errores(r175.pasa==0) = sort (temp(r175.pasa==0));
r175.errores(r175.pasa==1) = sort (temp(r175.pasa==1));

plot (normal.errores,'b',r0.errores,'r',r25.errores,'g',r175.errores,'k');
toc

save(archivo_salida);
