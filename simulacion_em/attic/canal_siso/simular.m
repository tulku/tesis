%function resultado_simulacion = simular(x_p, cant_total, cant_iter, SNR);
%datos = {Y, x_p, covar, cant_iter, hk, h};
function resultado_simulacion = simular(datos);
	Y = datos.Y;
	x_p = datos.x_p;
	covar = datos.covar;
	cant_iter = datos.cant_iter;
	hk = datos.hk;
	h = datos.h;
	pasa = pasa_r0 = pasa_r25 = pasa_r175 = 0;
	roto = 0;

	% Corremos los estimadores
	try	
		[hv, expo] = dfe(Y,x_p,covar,cant_iter,hk);
		[hv_r25] = dfe_r(Y,x_p,covar,cant_iter,hk,1.75,2.5);
		[hv_r175] = dfe_r(Y,x_p,covar,cant_iter,hk,1.75,1.75);
		[hv_r0] = dfe_r(Y,x_p,covar,cant_iter,hk,1.75,0);
	catch
		resultado_simulacion.roto = 1;
		return
	end

	% Procesamos los datos del estimador normal	
	ht=hv(end);
	% Detectamos los simbolos transmitidos.
	[detectado, errores] = detector(ht,Y,x_p);
	fig_error = calc_error (h,ht);
	pasa = validador(errores, covar, detectado, 0);

	% Procesamos los datos del estimador robusto
	ht25=hv_r25(end);
	ht175=hv_r175(end);
	ht0=hv_r0(end);
	% Detectamos los simbolos transmitidos.
	fig_error_r0 = calc_error (h,ht0);
	fig_error_r25 = calc_error (h,ht25);
	fig_error_r175 = calc_error (h,ht175);
	[detectado_r0, errores_r0] = detector(ht0,Y,x_p);
	[detectado_r25, errores_r25] = detector(ht25,Y,x_p);
	[detectado_r175, errores_r175] = detector(ht175,Y,x_p);
	pasa_r0 = validador(errores_r0, covar, detectado_r0, 0);
	pasa_r25 = validador(errores_r25, covar, detectado_r25, 0);
	pasa_r175 = validador(errores_r175, covar, detectado_r175, 0);

	resultado_nombres = {'h','covar','hv','fig_error', 'detectado', 'errrores', ...
			'hv_r25','hv_r0','hv_r175','fig_error_r0', 'fig_error_r25', ...
			'fig_error_r175','detectado_r0', 'detectado_r25', 'detectado_r175', ...
			'errores_r0', 'errores_r25', 'errores_r175','roto', ...
			'pasa','pasa_r0','pasa_r25','pasa_r175',};

	celda = {h;covar;hv;fig_error;detectado;errores; hv_r25; hv_r0; hv_r175; ...
		fig_error_r0;fig_error_r25;fig_error_r175;detectado_r0;detectado_r25; ...
		detectado_r175 ;errores_r0; errores_r25; errores_r175 ;roto; ...
		pasa;pasa_r0;pasa_r25;pasa_r175};

	resultado_simulacion = cell2struct(celda,resultado_nombres,1);
end
