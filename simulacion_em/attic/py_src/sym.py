#!/usr/bin/env python

from numpy import *
from numpy.random import randn
from numpy.random import rand
from numpy.linalg import inv
from pylab import *

class sym:
	"""Esta es la clase principal del simulador"""
	def __init__(self):
		cant_muestras = 40
		c = canal(20,cant_muestras)
		tx1 = nodo(c.sigma2, cant_muestras)
		tx2 = nodo(c.sigma2, cant_muestras)
		x1 = tx1.transmitir()
		x2 = tx2.transmitir()
		y = c.mezclar (x1,x2)

		hk1 = tx1.generar_h_inicial(y[0:9])
		hk2 = tx2.generar_h_inicial(y[0:9])

		cant_iter = 10
		tx1.estimar_h (y, hk1, cant_iter)
		tx2.estimar_h (y, hk2, cant_iter)

		print "H verdadera:"
		print c.h
		print "H calculada por rx1:"
		print tx1.h
		print "H calculada por rx2:"
		print tx2.h

		self.graficar_h(c.h,tx1.hv,range(tx1.cant_iter),"RX1")
		show()

	def graficar_h(self, h, hv, iters, titulo):
		title(titulo)
		aux = ones(hv.shape[1])
		plot(iters,hv[0,:],'r', iters,hv[1,:],'b', iters,h[0]*aux,'r--', iters,h[1]*aux,'b--')



### clase canal ###
class canal:
	"""Esta clase implementa un canal AWGN"""

	def __init__ (self, SNR, cant_muestras = 20 ):
		self.cant_muestras = cant_muestras
		self.h=abs(randn(2,1))
		# El numero 5 viene dado por la constelacion y es por dot(x_p,x_p.T)
		# .25*(-3)^2 + .25*3^2 + .25*(-1)^2 + .25*1^2
		self.sigma2 = (5*dot(self.h.T,self.h)/pow(10,SNR/10)).reshape(1,)

	def mezclar (self, X1, X2):
		X=hstack( (X1, X2) )
		N=randn(self.cant_muestras,1)*self.sqrt(sigma2)
		Y=dot(X,self.h)+N
		return Y
### clase canal ###


### clase nodo ###
class nodo:
	"""Esta es la clase de los nodos"""
	
	def __init__(self, sigma2, cant_muestras = 20):
		#matriz con las posibles combinaciones de x1 y x2
		self.x_p = array([[-3,-3,-3,-3,-1,-1,-1,-1,1,1,1,1,3,3,3,3],
		[-3,-1,1,3,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3]])
	#	self.x_p = array([[1,1,-1,-1],[1,-1,1,-1]])

		#esto estara bien? o tenemos que estimarlo?
		self.sigma2 = sigma2

		self.cant_muestras = cant_muestras

	def transmitir(self):
		""" Esta funcion crea el vector de transmision con 4 niveles"""
		xt1 = sign(randn(1,self.cant_muestras)).T
		xt2 = 2*sign(randn(1,self.cant_muestras)).T
		self.x = xt1 + xt2
	#	self.x=sign(randn(1,self.cant_muestras)).T
		return self.x

	def generar_h_inicial(self, Y):
		hsum = max(abs(Y))
		# El /6 se debe a la constelacion de 4 niveles
		# sino, es /2, con 2 niveles.
		hk = abs(rand(2,1) - 0.5 + hsum/6)
		return hk

	def estimar_h(self, Y, hk, cant_iter = 20):
		self.cant_iter = cant_iter

		hv = zeros((2,cant_iter),dtype=float64)
		hv[:,0] = hk.reshape(-1,)

		for iter in range(cant_iter-1):
			acum1 = zeros(4,dtype=float64).reshape((2,2))
			acum2 = zeros(2,dtype=float64).reshape((2,1))
			for i in range(self.cant_muestras):
				# Tomamos las columnas de x_p en una list comprehension
				Bk = array([pow((Y[i]-dot(j,hk)),2) for j in self.x_p.T],dtype=float64)
				Bk = Bk.reshape(-1,)
				Bk = Bk/(2*self.sigma2)
				conjunta = exp(-Bk)
				p_y = conjunta.sum()
				aux = 0
				for l in range(self.x_p.shape[1]):
					aux = aux + mat(self.x_p[:,l]).T * mat(self.x_p[:,l]) * conjunta[l]
				aux = aux/p_y
				acum1 = acum1+aux
	
			for i in range(self.cant_muestras):
				Bk = array([pow((Y[i]-dot(j,hk)),2) for j in self.x_p.T],dtype=float64)
				Bk = Bk.reshape(-1,)
				Bk = Bk/(2*self.sigma2)
				conjunta = exp(-Bk)
				p_y = conjunta.sum()
				aux = 0
				for l in range(self.x_p.shape[1]):
					aux = aux + mat(self.x_p[:,l]).T * conjunta[l]
				aux = Y[i].tolist()[0]*aux/p_y
				acum2 = acum2+aux
	
			try:
				hk = dot(inv((acum1)),acum2)
			except linalg.LinAlgError:
				print "##################"
				print " matriz singular! "
				print "##################"
				print acum1
				#dejamos de estimar
				break
			hv[:,iter+1] = hk.reshape(-1,)

		self.h = hk
		self.hv = hv

	def detectar(self, Y):
		t = argmin(abs((self.x_p.T*self.h) - Y[0]),0)
		detectado = array(self.x_p[:,t]).reshape(2,)
		for muestra in range(self.cant_muestras-1):
			t = argmin(abs((self.x_p.T*self.h) - Y[muestra+1]),0)
			nuevo = array(self.x_p[:,t]).reshape(2,)
			detectado = vstack ((detectado, nuevo))
		# Esto lo haciamos en sym3_em.py
		#print "Cantidad de errores:"
		#print sum((sum (abs (X - detectado))))/2
		return detectado
### clase nodo ###

simulacion = sym()
