%% Funcion para calcular la verosimilitud
function[z]=calc_vero_us(Y,x_p,sigma2,x,y)
% Y: muestras del canal
% x_p: posibles mensajes trans
% sigma2: varianza del ruido
% x: rango de h_1
% y: rango de h_2

z=zeros(length(x),length(y));

pos1=1;
for h1 = x
    pos2=1;
    for h2 = y
        hxy = [h1,h2];
        aux1 = 1;
        for i=1:length(Y)
            aux2 = 0;
            for j=1:length(x_p)
                aux2 = aux2 + exp(-((Y(i) - (hxy*x_p(:,j)))^2)/(2*sigma2));
            end
            % a continuacion tendriamos que dividir por 4*sqrt(2*pi*sigma2)
            % pero tenemos problemas de convergencia por este factor
            aux1 = aux1*aux2;
        end
        z(pos1,pos2)=aux1;
        pos2=pos2+1;
    end
    pos1=pos1+1;
end
