function[Hk1]=H_em_diego(Y,Xo,s2,Nem,Hk)
%[Hk1]=H_em(Y,Xo,s2,Nem,Hk)
%

[K,M]=size(Xo); M=M^(1/K);
B=length(Y);

Hk1=zeros(K,Nem+1);
Hk1(:,1)=Hk;

for jem=1:Nem
%   disp(['Progreso funcion canales_em: ' num2str(jem/Nem*100) '%'])
    varu=0;
    vard=0;
    for p=1:B
        var1=0;
        var2=zeros(K,1);
        var3=0;
        for jx=1:M^K       
            vare=exp(-((Y(p)-Hk'*Xo(:,jx))'*(Y(p)-Hk'*Xo(:,jx)))/(2*s2));
            var1=var1+vare;
            var2=var2+vare*Xo(:,jx);
            var3=var3+vare*(Xo(:,jx)*Xo(:,jx)');
        end
        varu=varu+Y(p)*var2/var1;
        vard=vard+var3/var1;
    end
%     vard
%     varu
    Hk1(:,jem+1)=inv(vard)*varu;
    Hk=Hk1(:,jem+1);
end






















% M=length(Omega);
% L=length(Y);
% Ltr=length(Xtr);
% 
% if Hk==0
%     Hk=(Y(1:Ltr)*pinv(Xtr));
% end
% 
% evo=zeros(1,rep+1);
% evo(1)=Hk;
% 
% for j=1:rep
%     varu=0;
%     vard=0;
%     for p=1:L
%         var1=0;
%         var2=0;
%         var3=0;
%         for m=1:M
%             %vare=exp(-(Y(p)-Hk*Omega(m))*(Y(p)-Hk*Omega(m))'/(2*s2));
%             vare=exp(-(Y(p)-Hk*Omega(m))^2/(2*s2));
%             var1=var1+vare;
%             var2=var2+vare*Omega(m);
%             %var3=var3+Omega(m)'*Omega(m)*vare;
%             var3=var3+vare*Omega(m)^2;
%         end
%         varu=varu+Y(p)*(var2/var1);
%         vard=vard+var3/var1;
%     end
%     Hk1=varu/vard;
%     evo(j+1)=Hk1;
%     Hk=Hk1;
% end    
