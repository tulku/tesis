cant_muestras = 1000;
cant_iter = 30;

% Generamos el canal
disp('h original:');
h = abs(randn(2,1))

% Generamos el ruido
SNR = 20; %dBs
sigma2_2 = h'*h/(10^(SNR/10));
sigma2_4 = 5*h'*h/(10^(SNR/10));
N = randn(1,cant_muestras);

% Generamos los datos a transmitir con 2 niveles
X2 = sign(randn(2,cant_muestras));

% Generamos los datos a transmitir con 4 niveles
a = sign(randn(2,cant_muestras));
b = 2*sign(randn(2,cant_muestras));
X4 = a+b;

% Mezclamos todo
Y2 = h'*X2 + N*sqrt(sigma2_2);
Y4 = h'*X4 + N*sqrt(sigma2_4);

% Estimamos el h inicial
hsum = max(abs(Y2(1:10)));
hk2 = abs(rand(2,1) -1 + hsum/2);
hsum = max(abs(Y4(1:10)));
hk4 = abs(rand(2,1) -1 + hsum/6);

% Matriz con el alfabeto
x_p2 = [[1 1 -1 -1];[1 -1 1 -1]];

% Matriz con el alfabeto
x_p4 =  [[-3,-3,-3,-3,-1,-1,-1,-1,1,1,1,1,3,3,3,3];
	[-3,-1,1,3,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3]];

disp('***********Nosotros 2 niveles');
hv2_us = H_em_us(Y2,x_p2,sigma2_2,cant_iter,hk2);

disp('***********Nosotros 4 niveles');
hv4_us = H_em_us(Y4,x_p4,sigma2_4,cant_iter,hk2);

% Calculo de la funcion en 3d
cant_puntos = 50;
x=linspace(h(1)-2,h(1)+2,cant_puntos);
y=linspace(h(2)-2,h(2)+2,cant_puntos);

%Lh_us = calc_vero_us(Y2,x_p2,sigma2_2,x,y);
% Usamos el algoritmo de diego para el calculo de la funcion de verosimilitud
% ya que al emplear los valores logaritmicos no tiene problemas con el rango
Lh = calc_vero_diego(Y2,x_p2,sigma2_2,x,y);


%% Graficos
% Graficos de convergencia 2d
aux1 = 1:cant_iter+1;
aux2 = ones(1,cant_iter+1);
figure(1);
clf;
title('Convergencia con 2 niveles');
hold on;
hv=hv2_us;
plot(aux1,hv(1,:),'r', aux1,hv(2,:),'b', aux1,h(1)*aux2,'r--', aux1,h(2)*aux2,'b--');
legend('H1','H2','H1 orig','H2 orig');
hold off;

figure(2);
clf;
title('Convergencia con 4 niveles');
hold on;
hv=hv4_us;
plot(aux1,hv(1,:),'r', aux1,hv(2,:),'b', aux1,h(1)*aux2,'r--', aux1,h(2)*aux2,'b--');
legend('H1','H2','H1 orig','H2 orig');
hold off;

% Graficos de convergencia 3d
figure(3);
clf;
title('Funcion de verosimilitud segun nuestro algoritmo para 2 niveles');
hold on;
contour(x,y,Lh);
plot(x,zeros(length(x)),'k')
plot(zeros(length(y)),y,'k')
plot(h(1),h(2),"-@o;h verdadera;")
plot(hv2_us(1,:),hv2_us(2,:),"r;Caminito;")
plot(hv2_us(1,1),hv2_us(2,1),"rx;Inicial;")
plot(hv2_us(1,length(hv2_us)),hv2_us(2,length(hv2_us)),"r*;Final;")
hold off;

figure(4);
clf;
hold on;
mesh(x,y,Lh);
plot3([h(1) h(1)], [h(2) h(2)], [max(max(Lh)) min(min(Lh))],"-@o;h verdadera;")
plot3([hv2_us(1,length(hv2_us)) hv2_us(1,length(hv2_us))], [hv2_us(2,length(hv2_us)) hv2_us(2,length(hv2_us))], [max(max(Lh)) min(min(Lh))],"-r*;Final;")
plot3(hv2_us(1,:),hv2_us(2,:),max(max(Lh))*ones(1,length(hv2_us)),"r;Caminito;")
hold off;

