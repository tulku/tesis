%% Funcion para calcular la verosimilitud
function[Lh]=calc_vero_diego(Y,x_p,s2,x,y)
% Y: muestras del canal
% x_p: posibles mensajes trans
% s2: varianza del ruido
% x: rango de h_1
% y: rango de h_2

%................Constantes.....................................
B=length(Y);     %Largo de las muestras de entrada
M=size(x_p)(1);  %tama�o de la costelacion (fijo por el momento)
K=2;             %cantidad de usuarios (1 a 4)
%...............................................................

%Omega=[1 -1  1 -1  1 -1  1 -1  1 -1  1 -1  1 -1  1 -1; ...
%       1  1 -1 -1  1  1 -1 -1  1  1 -1 -1  1  1 -1 -1; ...
%       1  1  1  1 -1 -1 -1 -1  1  1  1  1 -1 -1 -1 -1; ...
%       1  1  1  1  1  1  1  1 -1 -1 -1 -1 -1 -1 -1 -1]; 
%Xo=Omega(1:K,1:M^K);
Xo=x_p;

H1=x;
H2=y;
Lh=zeros(length(H1));
for p=1:B
    p;
    vare=zeros(length(H1));
    for jx=1:M^K
        jh1=0;
        for h1=H1
            jh1=jh1+1;
            jh2=0;
            for h2=H2
                jh2=jh2+1;
                vare(jh1,jh2)=vare(jh1,jh2)+exp(-(Y(p)-[h1;h2]'*Xo(:,jx))'*(Y(p)-[h1;h2]'*Xo(:,jx))/(2*s2));
            end
        end
    end
    Lh=Lh+log(vare);
end

%save('likehood3d','H1','H2','Lh')
