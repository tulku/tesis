cant_muestras = 20;
cant_iter = 30;

% Generamos el canal
disp('h original:\n');
h = abs(randn(2,1))

% Generamos el ruido
SNR = 20; %dBs
sigma2_4 = 5*h'*h/(10^(SNR/10));
N = randn(1,cant_muestras);

% Generamos los datos a transmitir con 4 niveles
a = sign(randn(2,cant_muestras));
b = 2*sign(randn(2,cant_muestras));
X4 = a+b;

% Mezclamos todo
Y4 = h'*X4 + N*sqrt(sigma2_4);

% Estimamos el h inicial
hsum = max(abs(Y4(1:10)));
hk4 = abs(rand(2,1) -1 + hsum/6);

% Matriz con el alfabeto
x_p4 =  [[-3,-3,-3,-3,-1,-1,-1,-1,1,1,1,1,3,3,3,3];
	[-3,-1,1,3,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3]];

disp('***********Nosotros 4 niveles');
hv4_us = H_em_us(Y4,x_p4,sigma2_4,cant_iter,hk4);
disp('***********Diego 4 niveles');
hv4_diego = H_em_diego(Y4,x_p4,sigma2_4,cant_iter,hk4);


%% Graficos
aux1 = 1:cant_iter+1;
aux2 = ones(1,cant_iter+1);
figure(2);
clf;
hold on;
hv=hv4_us;
plot(aux1,hv(1,:),'r', aux1,hv(2,:),'b', aux1,h(1)*aux2,'r--', aux1,h(2)*aux2,'b--');
hv=hv4_diego;
plot(aux1,hv(1,:),'g', aux1,hv(2,:),'m');
legend('H1','H2','H1 orig','H2 orig','H1 Diego','H2 Diego');
hold off;
