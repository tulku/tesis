%Esta funcion devuelve descarta los valores "outliers" del arreglo que se le
%pasa por argumento. El criterio que usa es el descripto en el cap. 2 del libro
%Statistics for Engineers & Scientists, sección 2.7. Se calcula el z-score de
%cada muestra y si ese valor es mayor a la cota, se descarta. 
%z =x-median(x)/mad(x). Se usan estimadores robustos para que tenga más sentido.

function a=remove_outliers(x)
	cota = 2;
	m = median(x);
	var = mad(x');
	a=x((x-m)/var < cota);
end
