function mostrar_grafico ( hv, h, cant_iter, texto)
        %% Graficos
        aux1 = 1:cant_iter+1;
        aux2 = ones(1,cant_iter+1);

        figure();
        clf;
        titulo = 'Evolucion del estimador y valores reales. ';
        titulo = [titulo, texto];
        title(titulo);
        hold on;
		curvas = fieldnames(hv);

		for c = 1:length(curvas)
			plot(aux1, getfield (hv,curvas{c}), num2str(mod(c,6)+1), 'linewidth', 1.5);
		end
		plot (aux1,h*aux2,'k', 'linewidth', 1.5);
		curvas{length (curvas)+1} = 'real';
		x=axis();
		if h > (x(3)+x(4))/2
			legend(curvas,'location','southeast');
		else
			legend(curvas,'location','northeast');
		end
        hold off;
end
