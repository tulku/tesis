clc;
clear;
close all;
warning ('error','Octave:divide-by-zero');

cant_muestras = 50;
cant_iter = 20;
cant_simulaciones = 2;
SNR = 5; %dBs
archivo_salida = 'resultado_simulacion_test.oct';

multicore=0;

% Matriz con el alfabeto
x_p =  [[-3,-3,-3,-3,-1,-1,-1,-1,1,1,1,1,3,3,3,3];
	[-3,-1,1,3,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3]];

simulaciones = cell(cant_simulaciones,1);
datos = cell(cant_simulaciones,1);

%% Generacion de datos a simular.
disp('Generando simulaciones...');
fflush(stdout);
for rep = 1:cant_simulaciones
        h = abs(randn(2,2)*1.2);
        % Generamos el ruido
        covar = zeros(2,2);
        covar(1,1) = 5*h(1,:)*h(1,:)'/(10^(SNR/10));
        covar(2,2) = 5*h(2,:)*h(2,:)'/(10^(SNR/10));
        N = randn(2,cant_muestras);
        % Generamos los datos a transmitir con 4 niveles
        a = sign(randn(2,cant_muestras));
        b = 2*sign(randn(2,cant_muestras));
        X = a+b;
        % Mezclamos todo
        Y = h*X + sqrt(covar)*N;
		hk = abs(randn(2,2)*1.2);
		aux_names = {'Y', 'x_p', 'covar', 'cant_iter', 'hk', 'h'};
		aux = cell2struct({Y; x_p; covar; cant_iter; hk; h},aux_names,1);
		datos{rep}=aux;
	end
tic

%% Corro las simulaciones
if multicore == 1
	simulaciones = startmulticoremaster(@simular, datos, '/tmp');
else
	for rep = 1:cant_simulaciones
		disp(['Simulacion: ',num2str(rep)]);
		fflush(stdout);
		simulaciones{rep} = simular(datos{rep});

		if !mod(rep,100)
			save(archivo_salida);
		end
	end
end

%% Hago algun analisis de los resultados
menor_error = -100*ones(1,cant_simulaciones);
repi=1;
for rep = 1:cant_simulaciones
	temp = simulaciones{rep};
	if temp.roto == 1
		if cant_simulaciones < 20
			disp([num2str(rep),') ','Simulacion rota']);
		end
		continue;
	end

	if cant_simulaciones < 20
		% Escribo valores
		disp([num2str(rep),') ', 'Error comun: ', num2str(temp.fig_error) ]);
		disp([' - Error robusto 0: ', num2str(temp.fig_error_r0)]);
		% Graficos		
		hs.robusto0 = temp.hv_r0;
		hs.normal = temp.hv;
%		mostrar_grafico(hs,temp.h,cant_iter, num2str(rep));
	end
	figs_error = [temp.fig_error, temp.fig_error_r0];
	pasan = [temp.pasa, temp.pasa_r0];

	normal.errores(repi) = temp.fig_error;
	r0.errores(repi) = temp.fig_error_r0;

	normal.pasa(repi) = temp.pasa;
	r0.pasa(repi) = temp.pasa_r0;

	[fes, pos] = sort(figs_error);
	menor_error(rep)=pos(1);
	repi=repi+1;
end
% 0 - normal
% 1 - robusto con pendiente 0

%plot(sort(menor_error(menor_error>0)));
disp(['Simulaciones correctas: ', num2str(length(menor_error(menor_error>0)))]);

if any(menor_error>0)
	[normal.pasa, i] = sort(normal.pasa,'ascend');
	normal.errores = normal.errores(i);
	temp = normal.errores;
	normal.errores(normal.pasa==0) = sort (temp(normal.pasa==0));
	normal.errores(normal.pasa==1) = sort (temp(normal.pasa==1));

	[r0.pasa, i] = sort(r0.pasa,'ascend');
	r0.errores = r0.errores(i);
	temp = r0.errores;
	r0.errores(r0.pasa==0) = sort (temp(r0.pasa==0));
	r0.errores(r0.pasa==1) = sort (temp(r0.pasa==1));

	%plot (normal.errores,'b',r0.errores,'r');
end
toc

save(archivo_salida);
