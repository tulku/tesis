function[hv,expo]=H_em_us(Y,x_p,sigma2,cant_iter,hk,shake)
shake_freq=10;
% Guardamos el valor inicial
hv = hk;
expo = {};

cant_muestras = length(Y);
cant_iter=cant_iter+1;

for i=2:cant_iter

    acum1 = zeros(2,2);
    acum2 = zeros(2,1);

    for j=1:cant_muestras

	aux = 0;
	for k=1:length(x_p)
	    temp(k) = ((Y(j)-x_p(:,k)'*hk)^2)/(2*sigma2);
	    conjunta(k) = exp(-temp(k));
	    aux = aux + x_p(:,k) * x_p(:,k)' * conjunta(k);
	end
	expo{i-1,j} = temp;

	p_y = sum(conjunta);
	aux = aux/p_y;

	acum1 = acum1+aux;
    end

    for j=1:cant_muestras

	aux = 0;
	for k=1:length(x_p)
	    conjunta(k) = exp (- ((Y(j)-x_p(:,k)'*hk)^2)/(2*sigma2));
	    aux = aux + x_p(:,k) * conjunta(k);
	end
	p_y = sum(conjunta);
	aux = Y(j)*aux/p_y;
	
	%printf("%s%i%s%i%s%g%s%g%s","Iter: ",i, " Muestra: ",j, " Min: ", min(conjunta), " Max: ",max(conjunta),"\n")

	acum2 = acum2+aux;
    end
    
    hk = inv(acum1)*acum2;
    if (shake == 1 && mod(i,shake_freq) == 0)
		hk = hk+(randn(2,1)*0.5);
    end
    hv(:,i) = hk;
end

