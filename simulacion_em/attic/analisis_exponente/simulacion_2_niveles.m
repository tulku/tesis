clear;

cant_muestras = 30;
cant_iter = 50;
shake = 1;

%% Generacion de senales y canal.
% Generamos el canal
disp('h original:');
h = abs(randn(2,2))

% Generamos el ruido
SNR = 15; %dBs

covar = zeros(2,2);
covar(1,1) = h(1,:)*h(1,:)'/(10^(SNR/10));
covar(2,2) = h(2,:)*h(2,:)'/(10^(SNR/10));

N = randn(2,cant_muestras);

% Generamos los datos a transmitir con 2 niveles
X = sign(randn(2,cant_muestras));

% Mezclamos todo
Y = h*X + sqrt(covar)*N;

% Estimamos el h inicial
hk = abs(randn(2,2))

% Matriz con el alfabeto
x_p = [[1 1 -1 -1];[1 -1 1 -1]];

%% Corremos los estimadores.

% 2 niveles los 4 juntos
[hv_us_doble, expo_doble] = H_em_us_doble(Y,x_p,covar,cant_iter,hk, shake);

% 2 niveles de a 2
[hv_us_simple1, expo_simple1] = H_em_us(Y(1,:),x_p,covar(1,1),cant_iter,hk(1,:)', shake);
[hv_us_simple2, expo_simple2] = H_em_us(Y(2,:),x_p,covar(2,2),cant_iter,hk(2,:)', shake);

%% Calculo de los coeficientes de peso
for i=1:cant_iter
	tmp2 = 0;
	for j=1:cant_muestras
		tmp1 = exp(-expo_doble{i,j});
		tmp3 = max(tmp1)/sum(tmp1);
		tmp2 = tmp2 + tmp3; 
		lambda_doble(i,j) = tmp3;
	end
	prom_lambda_doble(i) = tmp2/cant_muestras;
end

for i=1:cant_iter
	tmp2 = 0;
	for j=1:cant_muestras
		tmp1 = exp(-expo_simple1{i,j});
		tmp3 = max(tmp1)/sum(tmp1);
		tmp2 = tmp2 + tmp3;
		lambda_simple1(i,j) = tmp3;
	end
	prom_lambda_simple1(i) = tmp2/cant_muestras;
end

for i=1:cant_iter
	tmp2 = 0;
	for j=1:cant_muestras
		tmp1 = exp(-expo_simple2{i,j});
		tmp3 = max(tmp1)/sum(tmp1);
		tmp2 = tmp2 + tmp3;
		lambda_simple2(i,j) = tmp3;
	end
	prom_lambda_simple2(i) = tmp2/cant_muestras;
end

%% Calculo de errores
% Calculamos la raiz de la suma al cuadrado de la diferencia entre el
% valor real y estimado para los 4 coeficientes.

%error_doble = calc_error(h,hv_us_doble{length(hv_us_doble)});
%hk = zeros(2);
%hk(1,:) = hv_us_simple1(:,length(hv_us_simple1))';
%hk(2,:) = hv_us_simple2(:,length(hv_us_simple2))';
%error_simple = calc_error(h,hk);

%% Calculamos tiempos de convergencia.
%t_simple = zeros(1,4);
%t_simple([1 2]) = calc_conver (hv_us_simple1);
%t_simple([3 4]) = calc_conver (hv_us_simple2);
%t_simple = max(t_simple);

%for i=1:cant_iter+1
%        hva(1,i) = hv_us_doble{i}(1,1);
%        hva(2,i) = hv_us_doble{i}(1,2);
%        hva(3,i) = hv_us_doble{i}(2,1);
%        hva(4,i) = hv_us_doble{i}(2,2);
%end
%t_doble = max(calc_conver (hva));

%% Graficos
aux1 = 1:cant_iter+1;
aux2 = ones(1,cant_iter+1);
l=[];

figure(1);
clf;
title('Los 4 juntos');
hold on;

for i=1:cant_iter+1
	hv(i)=hv_us_doble{i}(1,1);
end
plot(aux1, hv, 'r', aux1, h(1,1)*aux2,'r--');
%l=[l;'H11 estimado';'H11 real'];

for i=1:cant_iter+1
	hv(i)=hv_us_doble{i}(1,2);
end
plot(aux1, hv, 'b', aux1, h(1,2)*aux2,'b--');
%l=[l;'H12 estimado';'H12 real'];

for i=1:cant_iter+1
	hv(i)=hv_us_doble{i}(2,1);
end
plot(aux1, hv, 'k', aux1, h(2,1)*aux2,'k--');
%l=[l;'H21 estimado';'H21 real'];

for i=1:cant_iter+1
	hv(i)=hv_us_doble{i}(2,2);
end
plot(aux1, hv, 'g', aux1, h(2,2)*aux2,'g--');
%l=[l;'H22 estimado';'H22 real'];

%legend(l);
hold off;

figure(2);
clf;
title('De a pares de 2')
hold on;

hv=hv_us_simple1(1,:);
plot(aux1, hv, 'r', aux1, h(1,1)*aux2,'r--');

hv=hv_us_simple1(2,:);
plot(aux1, hv, 'b', aux1, h(1,2)*aux2,'b--');

hv=hv_us_simple2(1,:);
plot(aux1, hv, 'k', aux1, h(2,1)*aux2,'k--');

hv=hv_us_simple2(2,:);
plot(aux1, hv, 'g', aux1, h(2,2)*aux2,'g--');

hold off;

%% Mostramos los errores
%disp('Error calculando todo junto'), disp(error_doble);
%disp('Error calculando de a dos'), disp(error_simple);
disp('');

%% Mostrar iteracion de convergencia
%disp('Iteracion de convergencia al estimar todo junto'), disp(t_doble);
%disp('Iteracion de convergencia al estimar de a dos'), disp(t_simple);

aux1 = 1:cant_iter;
aux2 = ones(1,cant_iter);

figure(3)
clf;
title('Evolucion de los pesos');
hold on;
plot(aux1, prom_lambda_doble, 'k');
plot(aux1, prom_lambda_simple1, 'r', aux1, prom_lambda_simple2, 'b');
legend('doble', 'simple1', 'simple2');
hold off;

%figure(4)
%clf;
%hold on;
%title('Evolucion de los pesos para el caso de los 4 juntos');
%for j=1:cant_muestras
%	plot3(aux1, j*aux2, lambda_doble(:,j));
%end
%
%figure(5)
%clf;
%hold on;
%title('Evolucion de los pesos para el caso de a pares');
%for j=1:cant_muestras
%	plot3(aux1, j*aux2, lambda_simple1(:,j),'r');
%	plot3(aux1, j*aux2, lambda_simple2(:,j),'b');
%end
