clear;
close all;

cant_muestras = 50;
cant_iter = 25;
shake = 0;
%% Generacion de senales y canal.
% Generamos el canal
disp('h original:');
h = (randn(4,2)*1.2)

% Generamos el ruido
SNR = 20; %dBs

covar = zeros(4,4);
covar(1,1) = 5*h(1,:)*h(1,:)'/(10^(SNR/10));
covar(2,2) = 5*h(2,:)*h(2,:)'/(10^(SNR/10));
covar(3,3) = 5*h(3,:)*h(3,:)'/(10^(SNR/10));
covar(4,4) = 5*h(4,:)*h(4,:)'/(10^(SNR/10));

N = randn(4,cant_muestras);

% Generamos los datos a transmitir con 4 niveles
a = sign(randn(2,cant_muestras));
b = 2*sign(randn(2,cant_muestras));
X = a+b;

% Mezclamos todo
Y = h*X + sqrt(covar)*N;

% Estimamos el h inicial
hk = abs(randn(4,2))

% Matriz con el alfabeto
x_p =  [[-3,-3,-3,-3,-1,-1,-1,-1,1,1,1,1,3,3,3,3];
	[-3,-1,1,3,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3]];

%% Corremos los estimadores.

hv_us_doble = H_em_us_doble(Y,x_p,covar,cant_iter,hk, shake);

ht=hv_us_doble{length(hv_us_doble)};
[detectado_doble, errores_doble] = detector(ht,Y,x_p);

validador(errores_doble, covar, detectado_doble, 1);

%% Calculo de errores
% Calculamos la suma al cuadrado de la diferencia entre el
% valor real y estimado para los 4 coeficientes.

error_doble = calc_error(h,hv_us_doble{length(hv_us_doble)});

for i=1:cant_iter+1
        hva(1,i) = hv_us_doble{i}(1,1);
        hva(2,i) = hv_us_doble{i}(1,2);
        hva(3,i) = hv_us_doble{i}(2,1);
        hva(4,i) = hv_us_doble{i}(2,2);
        hva(5,i) = hv_us_doble{i}(3,2);
        hva(6,i) = hv_us_doble{i}(3,2);
        hva(7,i) = hv_us_doble{i}(4,2);
        hva(8,i) = hv_us_doble{i}(4,2);
end
t_doble = max(calc_conver (hva));

%% Graficos
aux1 = 1:cant_iter+1;
aux2 = ones(1,cant_iter+1);

figure(1);
clf;
title('Los primeros 4');
hold on;

for i=1:cant_iter+1
	hv(i)=hv_us_doble{i}(1,1);
end
plot(aux1, hv, 'r', aux1, h(1,1)*aux2,'r--');

for i=1:cant_iter+1
	hv(i)=hv_us_doble{i}(1,2);
end
plot(aux1, hv, 'b', aux1, h(1,2)*aux2,'b--');

for i=1:cant_iter+1
	hv(i)=hv_us_doble{i}(2,1);
end
plot(aux1, hv, 'k', aux1, h(2,1)*aux2,'k--');

for i=1:cant_iter+1
	hv(i)=hv_us_doble{i}(2,2);
end
plot(aux1, hv, 'g', aux1, h(2,2)*aux2,'g--');

hold off;

figure(2);
clf;
title('Los segundos 4');
hold on;

for i=1:cant_iter+1
	hv(i)=hv_us_doble{i}(3,1);
end
plot(aux1, hv, 'r', aux1, h(3,1)*aux2,'r--');

for i=1:cant_iter+1
	hv(i)=hv_us_doble{i}(3,2);
end
plot(aux1, hv, 'b', aux1, h(3,2)*aux2,'b--');

for i=1:cant_iter+1
	hv(i)=hv_us_doble{i}(4,1);
end
plot(aux1, hv, 'k', aux1, h(4,1)*aux2,'k--');

for i=1:cant_iter+1
	hv(i)=hv_us_doble{i}(4,2);
end
plot(aux1, hv, 'g', aux1, h(4,2)*aux2,'g--');

hold off;
%% Mostramos los errores
disp('Error calculando todo junto'), disp(error_doble);
disp('');
%% Mostrar iteracion de convergencia
disp('Iteracion de convergencia al estimar todo junto'), disp(t_doble);


%% Estimacion del canal con el estimador MISO.
%hv_us_simple1 = H_em_us(Y(1,:),x_p,covar(1,1), cant_iter,hk(1,:)',shake);
%hv_us_simple2 = H_em_us(Y(2,:),x_p,covar(2,2), cant_iter,hk(2,:)',shake);
%
%ht = zeros(2);
%ht(1,:) = hv_us_simple1(:,length(hv_us_simple1))';
%ht(2,:) = hv_us_simple2(:,length(hv_us_simple2))';
%[detectado_simple, errores_simple] = detector(ht,Y,x_p);
%% Calculo error del estimador simple
%hk = zeros(2);
%hk(1,:) = hv_us_simple1(:,length(hv_us_simple1))';
%hk(2,:) = hv_us_simple2(:,length(hv_us_simple2))';
%error_simple = calc_error(h,hk);

%% Calculamos tiempos de convergencia para el caso MISO.
%t_simple = zeros(1,4);
%t_simple([1 2]) = calc_conver (hv_us_simple1);
%t_simple([3 4]) = calc_conver (hv_us_simple2);
%t_simple = max(t_simple);


%disp('Error calculando de a dos'), disp(error_simple);
%disp('Iteracion de convergencia al estimar de a dos'), disp(t_simple);
