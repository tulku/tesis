clear;

cant_muestras = 500;
cant_iter = 15;
shake = 0;
SNR = 20; %dBs
cant_simulaciones = 2000;

% Matriz con el alfabeto
x_p =  [[-3,-3,-3,-3,-1,-1,-1,-1,1,1,1,1,3,3,3,3];
	[-3,-1,1,3,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3]];

simulaciones = {};
resultado_simulacion = {};

for rep = 1:cant_simulaciones
	disp(['Simulacion ', num2str(rep), ' de ', num2str(cant_simulaciones)]);
	fflush(stdout);
	%% Generacion de senales y canal.
	% Generamos el canal
	h = abs(randn(2,2)*1.2);

	% Generamos el ruido
	covar = zeros(2,2);
	covar(1,1) = 5*h(1,:)*h(1,:)'/(10^(SNR/10));
	covar(2,2) = 5*h(2,:)*h(2,:)'/(10^(SNR/10));

	N = randn(2,cant_muestras);

	% Generamos los datos a transmitir con 4 niveles
	a = sign(randn(2,cant_muestras));
	b = 2*sign(randn(2,cant_muestras));
	X = a+b;

	% Mezclamos todo
	Y = h*X + sqrt(covar)*N;

	% Creamos el h inicial
	hk = abs(randn(2,2)*1.2);

	% Corremos el estimador.
	[hv_us_doble, expo] = H_em_us_doble(Y,x_p,covar,cant_iter,hk,shake);
	% Detectamos los simbolos transmitidos.
	ht=hv_us_doble{length(hv_us_doble)};
	[detectado, errores] = detector(ht,Y,x_p);
	% Calculamos la figura de error.
	figura_error = calc_error(h,hv_us_doble{length(hv_us_doble)});

	promedio_errores = sum(errores')/cant_muestras;
	P = repmat(promedio_errores',1,cant_muestras);
	errores_cero = errores-P;

	pasa_con_media = validador (errores, covar);
	pasa_sin_media = validador (errores_cero, covar);

	resultado_simulacion = {hv_us_doble, detectado, errores, promedio_errores, figura_error, h, pasa_sin_media, pasa_con_media, covar};
	simulaciones{rep} = resultado_simulacion;
end
resultado_simulacion = {};
save('resultado_simulacion_batch_validador_500.oct');
