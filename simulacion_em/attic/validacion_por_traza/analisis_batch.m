% La Cell por cada simulacion tiene los siguientes datos:
%
% resultado_simulacion = 
% 1 hv_us_doble, 
% 2 detectado,
% 3 errores, 
% 4 promedio_errores,
% 5 figura_error,
% 6 h,
% 7 pasa_sin_media,
% 8 pasa_con_media,
% 9 covar


function resultados = analisis_batch(graficar)
close all
load('resultado_simulacion_batch_validador.oct')
%load('resultado_batch_2_niveles_mad_ks.oct')

temp = {};
resultados = {};

cant_dentro_rango_con_media = 0;
cant_fuera_rango_con_media = 0;
error_dentro_rango_con_media = [];
indices_dentro_rango_con_media = [];
error_fuera_rango_con_media = [];
indices_fuera_rango_con_media = [];

cant_dentro_rango_sin_media = 0;
cant_fuera_rango_sin_media = 0;
error_dentro_rango_sin_media = [];
indices_dentro_rango_sin_media = [];
error_fuera_rango_sin_media = [];
indices_fuera_rango_sin_media = [];

malas = 0;
%graficar = 1;
aux = length(simulaciones{1})-1;

for sim = 1:cant_simulaciones
	temp = simulaciones{sim};
	if isnan(temp{3})
		malas = malas +1;
		continue;
	end
	% Simulaciones correctas con media
	if ( temp{8} == 1 )
		cant_dentro_rango_con_media = cant_dentro_rango_con_media +1;
		error_dentro_rango_con_media = [ error_dentro_rango_con_media, temp{5} ];
		indices_dentro_rango_con_media = [ indices_dentro_rango_con_media, sim ];
	else
	% Simulaciones malas con media
		cant_fuera_rango_con_media = cant_fuera_rango_con_media +1;
		error_fuera_rango_con_media = [ error_fuera_rango_con_media, temp{5} ];
		indices_fuera_rango_con_media = [ indices_fuera_rango_con_media, sim ];
	end

	% Simulaciones correctas sin media
	if ( temp{7} == 1 )
		cant_dentro_rango_sin_media = cant_dentro_rango_sin_media +1;
		error_dentro_rango_sin_media = [ error_dentro_rango_sin_media, temp{5} ];
		indices_dentro_rango_sin_media = [ indices_dentro_rango_sin_media, sim ];
	else
	% Simulaciones malas sin media
		cant_fuera_rango_sin_media = cant_fuera_rango_sin_media +1;
		error_fuera_rango_sin_media = [ error_fuera_rango_sin_media, temp{5} ];
		indices_fuera_rango_sin_media = [ indices_fuera_rango_sin_media, sim ];
	end

	if ( graficar == 1 )
		mostrar_grafico(temp{1}, temp{6}, cant_iter, 'todos');
	end
end


correctas = cant_simulaciones - malas;
disp('');
disp([ 'Cantidad simulaciones correctas: ' num2str(correctas)]);
disp('');
disp('Con media');
disp([ 'Correctas: ' num2str(cant_dentro_rango_con_media * 100 /correctas)]); 
disp([ 'Media de errores aceptados: ' num2str(mean(error_dentro_rango_con_media))]);
disp([ 'MAD de errores aceptadas: ' num2str(mad(error_dentro_rango_con_media'))]);
disp([ 'Media de errores no aceptadas: ' num2str(mean(error_fuera_rango_con_media))]);
disp('');
disp('Sin media');
disp([ 'Correctas: ' num2str(cant_dentro_rango_sin_media * 100 /correctas)]); 
disp([ 'Media de errores aceptadas: ' num2str(mean(error_dentro_rango_sin_media))]);
disp([ 'MAD de errores aceptadas ks: ' num2str(mad(error_dentro_rango_sin_media'))]);
disp([ 'Media de errores no aceptadas: ' num2str(mean(error_fuera_rango_sin_media))]);
disp('');

plot (error_dentro_rango_con_media,'ro',error_dentro_rango_sin_media,'bo');
legend('Errores con media', 'Errores sin media');

[val,pos] = graficar_max (simulaciones, cant_iter, error_dentro_rango_con_media, indices_dentro_rango_con_media, ' Max error aceptado con media');
disp(['Max error aceptado con media: ' num2str(val)]);
sim = indices_dentro_rango_con_media(pos);
temp = simulaciones{sim};
temp{6};
temp{1}{length(temp{1})};
d=temp{2};
e=temp{3};
cov(d',e');

[val,pos] = graficar_min (simulaciones, cant_iter, error_dentro_rango_con_media, indices_dentro_rango_con_media, ' MIN error aceptado con media');
disp(['Min error aceptado con media: ' num2str(val)]);
sim = indices_dentro_rango_con_media(pos);
temp = simulaciones{sim};
temp{6}
temp{1}{length(temp{1})}
d=temp{2};
e=temp{3};
cov(d',e')
error_dentro_rango_con_media(pos)=200;

[val,pos] = graficar_min (simulaciones, cant_iter, error_dentro_rango_con_media, indices_dentro_rango_con_media, ' MIN error aceptado con media');
disp(['Min error aceptado con media: ' num2str(val)]);
sim = indices_dentro_rango_con_media(pos);
temp = simulaciones{sim};
temp{6}
temp{1}{length(temp{1})}
d=temp{2};
e=temp{3};
cov(d',e')
%[val,pos] = graficar_min (simulaciones, cant_iter, error_fuera_rango_con_media, indices_fuera_rango_con_media, ' MIN error NO aceptado con media');
%disp(['Min error NO aceptado con media: ' num2str(val)]);
%disp('');
%pause;
%[val,pos] = graficar_max (simulaciones, cant_iter, error_dentro_rango_sin_media, indices_dentro_rango_sin_media, ' Max error aceptado sin media');
%disp(['Max error aceptado sin media: ' num2str(val)]);
%[val,pos] = graficar_min (simulaciones, cant_iter, error_dentro_rango_sin_media, indices_dentro_rango_sin_media, ' MIN error aceptado sin media');
%disp(['Min error aceptado sin media: ' num2str(val)]);
%[val,pos] = graficar_min (simulaciones, cant_iter, error_fuera_rango_sin_media, indices_fuera_rango_sin_media, ' MIN error NO aceptado sin media');
%disp(['Min error NO aceptado sin media: ' num2str(val)]);

%graficar_error_min (simulaciones, cant_iter, error_fuera_rango_con_media, indices_fuera_rango_con_media, ' MIN error NO aceptado con media');
%graficar_error_max (simulaciones, cant_iter, error_dentro_rango_con_media, indices_dentro_rango_con_media, ' MAX error SI aceptado con media');
resultados = {
cant_dentro_rango_con_media,cant_fuera_rango_con_media,error_dentro_rango_con_media,indices_dentro_rango_con_media,error_fuera_rango_con_media,indices_fuera_rango_con_media,cant_dentro_rango_sin_media,cant_fuera_rango_sin_media,error_dentro_rango_sin_media,indices_dentro_rango_sin_media,error_fuera_rango_sin_media,indices_fuera_rango_sin_media};
end


function [val, pos] = graficar_max (simulaciones, cant_iter, errores, indices, texto) 
	[val,pos] = max(errores);
	sim = indices(pos);
	temp = simulaciones{sim};
	mostrar_grafico(temp{1}, temp{6}, cant_iter, texto);
end

function [val, pos] = graficar_min (simulaciones, cant_iter, errores, indices, texto) 
	[val,pos] = min(errores);
	sim = indices(pos);
	temp = simulaciones{sim};
	disp(['h','num2str( temp{6})']);
	disp(['hk','num2str( temp{1}{length(temp{1})})']);
	mostrar_grafico(temp{1}, temp{6}, cant_iter, texto);
end

function [val, pos] = graficar_error_min (simulaciones, cant_iter, errores, indices, texto) 
	[val,pos] = min(errores);
	sim = indices(pos);
	temp = simulaciones{sim};
	mean(temp{3});
	mostrar_grafico_error(temp{3}, texto);
end

function [val, pos] = graficar_error_max (simulaciones, cant_iter, errores, indices, texto) 
	[val,pos] = max(errores);
	sim = indices(pos);
	temp = simulaciones{sim};
	mean(temp{3});
	mostrar_grafico_error(temp{3}, texto);
end
function mostrar_grafico ( hv_us_doble, h, cant_iter, texto)
	%% Graficos
	aux1 = 1:cant_iter+1;
	aux2 = ones(1,cant_iter+1);

	figure();
	clf;
	titulo = 'Evolucion del estimador y valores reales. ';
	titulo = [titulo, texto];

	title(titulo);
	hold on;

	for i=1:cant_iter+1
	    hv(i)=hv_us_doble{i}(1,1);
	end
	plot(aux1, hv, 'r', aux1, h(1,1)*aux2,'r--');
	%l=[l;'H11 estimado';'H11 real'];

	for i=1:cant_iter+1
	    hv(i)=hv_us_doble{i}(1,2);
	end
	plot(aux1, hv, 'b', aux1, h(1,2)*aux2,'b--');
	%l=[l;'H12 estimado';'H12 real'];

	for i=1:cant_iter+1
	    hv(i)=hv_us_doble{i}(2,1);
	end
	plot(aux1, hv, 'k', aux1, h(2,1)*aux2,'k--');
	%l=[l;'H21 estimado';'H21 real'];

	for i=1:cant_iter+1
	    hv(i)=hv_us_doble{i}(2,2);
	end
	plot(aux1, hv, 'g', aux1, h(2,2)*aux2,'g--');
	%l=[l;'H22 estimado';'H22 real'];

	hold off;
end

function mostrar_grafico_error ( errores, texto)
	%% Graficos
	l=length(errores);

	figure();
	clf;
	titulo = 'Errores. ';
	titulo = [titulo, texto];

	title(titulo);
	hold on;

	plot (1:l,errores(1,:),'r',1:l,errores(2,:),'b');

	hold off;
end

