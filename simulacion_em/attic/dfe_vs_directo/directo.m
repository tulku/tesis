% Este es el estimador EM.
% Y: señal recibida.
% x_p: posibles señales recibidas.
% covar: matriz de covarianza del ruido
% cant_iter: cantidad de iteracion a realizar
% hk: valor inicial del estimador
% shake: 1 para activar el "shake control" :) 

% hv: cell con la evolucion de la matriz h.
% expo: cell con todos los valores de los exponentes durante la
%		estimacion.

function[hv, expo]=directo(Y,x_p,covar,cant_iter,hk,shake)
icovar = inv(covar);
shake_freq=10;

% Guardamos el valor inicial
hv = {hk};
expo = {};

cant_muestras = length(Y);
cant_iter=cant_iter+1;

for i=2:cant_iter

    acum1 = zeros(2,2);
    acum2 = zeros(2,2);

    
	for j=1:cant_muestras
		aux = 0;
		for k=1:length(x_p)
		    A=( (Y(:,j) - hk*x_p(:,k))' * icovar * (Y(:,j) - hk*x_p(:,k)) )/2;
		    temp(k) = A;
		    conjunta(k) = exp(-A);
		    aux = aux + x_p(:,k)*x_p(:,k)' * conjunta(k);
		end
		expo{i-1,j} = temp;
	
		p_y = sum(conjunta);
		aux = aux/p_y;
	
		acum1 = acum1+aux;
    end

    for j=1:cant_muestras
		aux = 0;
		for k=1:length(x_p)
		    A=( (Y(:,j) - hk*x_p(:,k))' * icovar * (Y(:,j) - hk*x_p(:,k)) );
		    conjunta(k) = exp(-A/2);
		    aux = aux + Y(:,j)*x_p(:,k)' * conjunta(k);
		end
		p_y = sum(conjunta);
		aux = aux/p_y;
		
		acum2 = acum2+aux;
    end

    hk = acum2*inv(acum1);
    if (shake == 1 && mod(i,shake_freq) == 0)
    	hk = hk + (randn(2,2)*0.5);
    end
    hv{i} = hk;
end

