clear;

function mostrar_grafico ( hv_us_doble, h, cant_iter, texto)
        %% Graficos
        aux1 = 1:cant_iter+1;
        aux2 = ones(1,cant_iter+1);

        figure();
        clf;
        titulo = 'Evolucion del estimador y valores reales. ';
        titulo = [titulo, texto];

        title(titulo);
        hold on;

        for i=1:cant_iter+1
            hv(i)=hv_us_doble{i}(1,1);
        end
        plot(aux1, hv, 'r', aux1, h(1,1)*aux2,'r--');
        %l=[l;'H11 estimado';'H11 real'];

        for i=1:cant_iter+1
            hv(i)=hv_us_doble{i}(1,2);
        end
        plot(aux1, hv, 'b', aux1, h(1,2)*aux2,'b--');
        %l=[l;'H12 estimado';'H12 real'];

        for i=1:cant_iter+1
            hv(i)=hv_us_doble{i}(2,1);
        end
        plot(aux1, hv, 'k', aux1, h(2,1)*aux2,'k--');
        %l=[l;'H21 estimado';'H21 real'];

        for i=1:cant_iter+1
            hv(i)=hv_us_doble{i}(2,2);
        end
        plot(aux1, hv, 'g', aux1, h(2,2)*aux2,'g--');
        %l=[l;'H22 estimado';'H22 real'];

        hold off;
end

cant_muestras = 50;
cant_iter = 15;
shake = 0;

%% Generacion de senales y canal.
% Generamos el canal
disp('h original:');
h = abs(randn(2,2)*1.2)

% Generamos el ruido
SNR = 20; %dBs

covar = zeros(2,2);
covar(1,1) = 5*h(1,:)*h(1,:)'/(10^(SNR/10));
covar(2,2) = 5*h(2,:)*h(2,:)'/(10^(SNR/10));

N = randn(2,cant_muestras);

% Generamos los datos a transmitir con 4 niveles
a = sign(randn(2,cant_muestras));
b = 2*sign(randn(2,cant_muestras));
X = a+b;

% Mezclamos todo
Y = h*X + sqrt(covar)*N;

% Estimamos el h inicial
hk = abs(randn(2,2)*1.2);

% Matriz con el alfabeto
x_p =  [[-3,-3,-3,-3,-1,-1,-1,-1,1,1,1,1,3,3,3,3];
	[-3,-1,1,3,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3]];

% Corremos el estimador directo.
[hv_directo, expo] = directo(Y,x_p,covar,cant_iter,hk,shake);
% Corremos el estimador DFE
[hv_dfe, expo] = dfe(Y,x_p,covar,cant_iter,hk,shake);

% Detectamos los simbolos transmitidos.
disp('h estimado:')
ht_directo=hv_directo{length(hv_directo)}
ht_dfe=hv_dfe{length(hv_dfe)}

tic
[detectado_directo, errores_directo] = detector(ht_directo,Y,x_p);
toc
tic
[detectado_dfe, errores_dfe] = detector(ht_dfe,Y,x_p);
toc

disp('Estimador directo:');
pasa1 = validador (errores_directo, covar);
disp('');
fflush(stdout);
disp('Estimador DFE:');
pasa2 = validador (errores_dfe, covar);
disp('');
fflush(stdout);
if (pasa1 == 1 && pasa2 == 1)
	disp ('Pasan los dos');
elseif (pasa1 == 1 && pasa2 == 0)
	disp ('Pasa directo');
elseif (pasa1 == 0 && pasa2 == 1)
	disp ('Pasa DFE');
elseif (pasa1 == 0 && pasa2 == 0)
	disp ('Dio cualquiera');
end

mostrar_grafico(hv_directo,h,cant_iter,'Estimador directo');
mostrar_grafico(hv_dfe,h,cant_iter,'Estimador DFE');

