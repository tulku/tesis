% Nuevo super mega validador. Usa el test de Kolmorov para ver si los errores se
% ajustan a la normal que corresponde y ademas evalua que sean
% descorrelacionados.
 
% Test de Kolmorov
% segun http://www.ciphersbyritter.com/JAVASCRP/NORMCHIK.HTM#KolSmir
% y https://www-old.cae.wisc.edu/pipermail/help-octave/2008-April/008940.html

function pasa = validador(errores, covar)
	[ks,pasa1] = KS_test(errores(1,:), 0 , sqrt(covar(1,1)));
	[ks,pasa2] = KS_test(errores(2,:), 0 , sqrt(covar(2,2)));
%	[R,pasa3] = autocorr(errores(1,:));
%	[R,pasa4] = autocorr(errores(2,:));
	pasa3=pasa4=1;
	disp(['pasa  ks 1:', num2str(pasa1)]);
	disp(['pasa  ks 2:', num2str(pasa2)]);
%	disp(['pasa  corr 3:', num2str(pasa3)]);
%	disp(['pasa  corr 4:', num2str(pasa4)]);
	pasa = pasa1 & pasa2 & pasa3 & pasa4;
end

function [ks,pasa] = KS_test(X,m,s)
	X=sort(X);
	Xecdf=empirical_cdf(X,X);
	assumed_pop=normcdf(X,m,s);
	ks=max(abs(assumed_pop-Xecdf));
	if ks < 0.1250
		pasa = 1;
	else
		pasa = 0;
	end
end

function [R,pasa] = autocorr (X)
	l = length(X);
	R = zeros(1,l);
	pasa = 0;
	x_shift = X;
	for k = 1:l
		R(k) = sum (X.*x_shift)/l;
		x_shift = shift(X,k);
		x_shift(1:k)=zeros(1,k);
	end
	R = abs(R);
	media = mean(R);
	[val,pos] = max(R);
	if pos == 1
		figure();
		plot(R,'r',3*media*ones(1,l));
		R(pos)=0;
		if R<3*media
			pasa = 1;
		end
	end
end
