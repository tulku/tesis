function [sims_cero sims_norm] = test_val_norm (simulaciones)
	tic
	sims_norm = {};
	sims_cero = {};
	i = 1;
	for sim = simulaciones
		if (size(sim{1}) == [1 1] && !isnan(sim{1}.fig_error))
			sim_media_cero = sim{1};
			sim_norm = sim{1};
			errores_cero = sacar_media(sim{1}.errores);
			errores_norm = sacar_covar(errores_cero);

			sim_norm.errores = errores_norm;
			sim_media_cero.errores = errores_cero;

			sim_media_cero.r_val = validador(errores_cero, sim{1}.covar, 0);
			sim_norm.r_val = validador(errores_norm, eye(2), 0);

			sims_cero{i} = sim_media_cero;
			sims_norm{i} = sim_norm;

			disp(['Sim: ', num2str(i)]);
			fflush(stdout);
			i += 1;
		end
	end
	save ('sim_KS_cero_y_norm.oct');
	toc
end

function errores_cero = sacar_media(errores)
	cant_muestras = length(errores);
	promedio_errores = sum(errores')/cant_muestras;
	media = repmat(promedio_errores',1,cant_muestras);
	errores_cero = errores-media;
end

function errores_norm = sacar_covar(errores)
	errores_norm = zeros(size(errores));
	acum = zeros(2,2);
	for e = errores
		acum += (e*e');
	end
	acum /= length(errores);
	icovar = inv(chol(acum))';

	i = 1;
	for e = errores
		errores_norm(:,i) = icovar*e;
		i += 1;
	end
end
