load('sim_KS_500.oct');

for i=1:cant_simulaciones
	if (size(simulaciones{i}) == [1 1])
		simulaciones{i}.fig_error /= (norm(vec(simulaciones{i}.datos.h))^2);
		simulaciones{i}.fig_error = sqrt(simulaciones{i}.fig_error);
	end
end
save('sim_KS_500_efix.oct');
	
