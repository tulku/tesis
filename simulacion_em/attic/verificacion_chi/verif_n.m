clear;
j = 0;
NN = [5 20 50 100 200];	% cantidad de muestras

M = 4;	% cantidad de canales
L = 10000;	% cantidad de realizaciones

% desvio del ruido
%des = diag(abs(2*randn(1,M)));
des = diag([1 1.4 .8 1.2]);

for N=NN

	for i=1:L
		n1 = randn(M, N);
		n = des * n1;	% realizaciones del ruido
		alfa(i) = trace(cov(n'));
	end

	j = j+1;
	estimado(j) = cov(alfa');
	teorico(j) = 2/N*trace(des^4);

end

mayor = max(estimado)*1.2;
menor = min(teorico)*0.8;

loglog(NN,estimado,'b', NN,teorico,'r', NN,teorico./estimado,'k');
grid;
grid("minor");
axis([5 200 menor mayor],"equal");
legend("Estimado", "Teorico", "Razon");
legend("boxon");
xlabel ("Cantidad de muestras")

%print("estimacion.ps")

