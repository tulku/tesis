clear;

N = 10;	% cantidad de muestras
M = 4;	% cantidad de canales
L = 10000;	% cantidad de realizaciones

% desvio del ruido
%des = diag(abs(2*randn(1,M)));
des = diag([1 1.4 .8 1.2]);
n_sum=0;

for i=1:L
	n1 = randn(M, N);
	n_sum = n_sum + n1;
	n = des * n1;	% realizaciones del ruido
	alfa(i) = trace(cov(n'));
	alfa2(i) = 1/N * sum(sum(n.^2));
end
disp('Media de ruido');
disp(n_sum/L);
disp('Varianza sobre las diferentes realizaciones:');
disp(var(alfa'));
disp(var(alfa2',1));
aux= (1/L) * sum((alfa2-mean(alfa2)).^2);
disp (aux)


disp('Varianza calculada:');
disp(2/N*trace(des^4));
