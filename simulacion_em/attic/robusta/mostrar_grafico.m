function mostrar_grafico ( hv_us_doble, h, cant_iter, texto)
        %% Graficos
        aux1 = 1:cant_iter+1;
        aux2 = ones(1,cant_iter+1);

        figure();
        clf;
        titulo = 'Evolucion del estimador y valores reales. ';
        titulo = [titulo, texto];

        title(titulo);
        hold on;

        for i=1:cant_iter+1
            hv(i)=hv_us_doble{i}(1,1);
        end
        plot(aux1, hv, 'r', aux1, h(1,1)*aux2,'r--');
        %l=[l;'H11 estimado';'H11 real'];

        for i=1:cant_iter+1
            hv(i)=hv_us_doble{i}(1,2);
        end
        plot(aux1, hv, 'b', aux1, h(1,2)*aux2,'b--');
        %l=[l;'H12 estimado';'H12 real'];

        for i=1:cant_iter+1
            hv(i)=hv_us_doble{i}(2,1);
        end
        plot(aux1, hv, 'k', aux1, h(2,1)*aux2,'k--');
        %l=[l;'H21 estimado';'H21 real'];

        for i=1:cant_iter+1
            hv(i)=hv_us_doble{i}(2,2);
        end
        plot(aux1, hv, 'g', aux1, h(2,2)*aux2,'g--');
        %l=[l;'H22 estimado';'H22 real'];

        hold off;
end
