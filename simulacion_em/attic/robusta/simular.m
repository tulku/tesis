%function resultado_simulacion = simular(x_p, cant_total, cant_iter, SNR);
%datos = {Y, x_p, covar, cant_iter, hk, h};
function resultado_simulacion = simular(datos);
	Y=datos{1};
	x_p=datos{2};
	covar=datos{3};
	cant_iter=datos{4};
	hk=datos{5};
	h=datos{6};
	roto = 0;
	% Corremos el estimador con 500 muestras
	[hv, expo, roto] = dfe(Y,x_p,covar,cant_iter,hk);
	ht=hv{length(hv)};
	% Detectamos los simbolos transmitidos.
	[detectado, errores] = detector(ht,Y,x_p);
	fig_error = calc_error (h,ht);
	pasa = validador(errores, covar, detectado, 0);
	%guardo los errores
	resultado_simulacion={hv,detectado, errores, fig_error, h, covar, pasa, roto};
end
