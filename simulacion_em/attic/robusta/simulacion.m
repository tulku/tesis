clc;
clear;
close all;

cant_muestras = 50;
cant_conocidos = 0;
cant_iter = 20;
cant_simulaciones = 400;
max_estimaciones = 30;
SNR = 20; %dBs
archivo_salida = 'resultado_simulacion_robusta_test.oct';

multicore=1;

% Matriz con el alfabeto
x_p =  [[-3,-3,-3,-3,-1,-1,-1,-1,1,1,1,1,3,3,3,3];
		[-3,-1,1,3,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3]];
cant_total = cant_muestras+cant_conocidos;

simulaciones = {};
datos = {};
cant_total = cant_conocidos + cant_muestras;

%simulaciones = startmulticoremaster(@simular, parameterCell);
disp('Generando simulaciones...');
fflush(stdout);
for rep = 1:cant_simulaciones
        %h = zeros(2,2);
        %% Generacion de senales y canal.
        h = abs(randn(2,2)*1.2);
        % Generamos el ruido
        covar = zeros(2,2);
        covar(1,1) = 5*h(1,:)*h(1,:)'/(10^(SNR/10));
        covar(2,2) = 5*h(2,:)*h(2,:)'/(10^(SNR/10));
        N = randn(2,cant_total);
        % Generamos los datos a transmitir con 4 niveles
        a = sign(randn(2,cant_total));
        b = 2*sign(randn(2,cant_total));
        X = a+b;
        % Mezclamos todo
        Y = h*X + sqrt(covar)*N;
		hk = abs(randn(2,2)*1.2);
		aux = {Y, x_p, covar, cant_iter, hk, h};
		datos{rep}=aux;
end

tic
if multicore == 1
	simulaciones = startmulticoremaster(@simular, datos);
else
	for rep = 1:cant_simulaciones
		disp(['Simulacion: ',num2str(rep)]);
		fflush(stdout);
		simulaciones{rep} = simular(datos{rep});
		if !mod(rep,100)
			save(archivo_salida);
		end
	end
end

%resultado_simulacion={hv, detectado, errores, fig_error, h, covar, pasa, roto};
for rep = 1:cant_simulaciones
	temp = simulaciones{rep};
	disp([num2str(rep),') ', 'Error: ', num2str(temp{4}) ...
		' - Pasa: ', num2str(temp{7}), ' - Roto: ', num2str(temp{8})]);
	if temp{8} == 0
		fe(rep)=temp{4};
	end
	mostrar_grafico(temp{1},temp{5},cant_iter,num2str(rep));
end
toc

% figure();
% hold on;
% plot(fe,'ro');
% fe=remove_outliers(fe);
% plot(fe,'bo');
% legend('Figura de error','Figura de error sin outliers');
% hold off

save(archivo_salida);
