function grafico_errores_multi(simulaciones)
	close all;
	porcentaje = 10;

	i = 1;
	errores = zeros(1,length(simulaciones));
	pasan = zeros(1,length(simulaciones));
	
	for sim = simulaciones
		if (size(sim{1}) == [1 1])
			errores(i) = sim{1}.fig_error;
			pasan(i) = sim{1}.pasa;
			i += 1;
		end
	end

	[pasan, index] = sort(pasan, 'ascend');
	errores = errores (index);
	errores_no_pasan = sort (errores(pasan==0));
	errores_pasan = sort (errores(pasan==1));
	errores_no_pasan = abs(errores_no_pasan);
	errores_pasan = abs(errores_pasan);

	cota_y = errores_no_pasan(floor(porcentaje*length(errores_no_pasan)/100))
	a = ones(1,length(errores_no_pasan))*cota_y;
	b = errores_pasan(errores_pasan < cota_y);
	c = ones(1,length(errores_pasan))*b(end);
	p = length(b)*100/length(errores_pasan);

	figure()
	subplot(1,2,1);
	semilogy(errores_no_pasan,'b', 'linewidth', 2.0, a, 'k', 'linewidth', 2.0);
	legend('Error',[num2str(ceil(porcentaje)),'% de las simulaciones']);
	title('Identificaciones invalidadas');
	xlabel('Numero de simulacion');
	ylabel('Error');
	ax = axis;
	subplot(1,2,2);
	semilogy(errores_pasan,'b', 'linewidth', 2.0, c, 'k', 'linewidth', 2.5);
	legend('Error',[num2str(ceil(p)),'% de las simulaciones']);
	title('Identificaciones aceptadas');
	xlabel('Numero de simulacion');
	ylabel('Error');
	axis([1 length(errores_pasan) ax(3) ax(4)]);

	print('grafico_errores_multi.eps');
end
