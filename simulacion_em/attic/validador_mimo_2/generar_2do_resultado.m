load ('datos_2do_resultado.oct');

sim_rota=simulaciones{2};
sim_buena=simulaciones{4};


aux1 = 1:11;
aux2 = ones(1,11);

h_v = sim_rota.hv;

clf;
subplot(1,2,1)

%titulo = 'Identificacion invalidada.';
%title(titulo);
%ylabel('Ganancia del canal');
%xlabel('Iteracion')
hold on;

for i=1:11
    hv(i)=h_v{i}(1,1);
end
plot(aux1, hv, 'r','linewidth', 2.0, aux1, h(1,1)*aux2,'r--','linewidth', 2.0);

for i=1:11
    hv(i)=h_v{i}(1,2);
end
plot(aux1, hv, 'b', 'linewidth', 2.0, aux1, h(1,2)*aux2,'b--','linewidth', 2.0);

for i=1:11
    hv(i)=h_v{i}(2,1);
end
plot(aux1, hv,'k', 'linewidth', 2.0, aux1, h(2,1)*aux2,'k--','linewidth', 2.0);

for i=1:11
    hv(i)=h_v{i}(2,2);
end
plot(aux1, hv, 'g', 'linewidth', 2.0, aux1, h(2,2)*aux2,'g--','linewidth', 2.0);

hold off;

h_v = sim_buena.hv;
subplot(1,2,2)
%titulo = 'Identificacion valida.';
%title(titulo);
%ylabel('Ganancia del canal');
%xlabel('Iteracion')
hold on;

for i=1:11
    hv(i)=h_v{i}(1,1);
end
plot(aux1, hv, 'r','linewidth', 2.0, aux1, h(1,1)*aux2,'r--','linewidth', 2.0);

for i=1:11
    hv(i)=h_v{i}(1,2);
end
plot(aux1, hv, 'b', 'linewidth', 2.0, aux1, h(1,2)*aux2,'b--','linewidth', 2.0);

for i=1:11
    hv(i)=h_v{i}(2,1);
end
plot(aux1, hv,'k', 'linewidth', 2.0, aux1, h(2,1)*aux2,'k--','linewidth', 2.0);

for i=1:11
    hv(i)=h_v{i}(2,2);
end
plot(aux1, hv, 'g', 'linewidth', 2.0, aux1, h(2,2)*aux2,'g--','linewidth', 2.0);

hold off;

print('2do_resultado.eps');
