function [w,w2,d,d2] = ks_vs_cvm (n)
%n = cantidad de simulaciones que se quieren hacer.
	d = zeros(1,n);
	d2 = zeros(1,n);
	w = zeros(1,n);
	w2 = zeros(1,n);
	for i = 1:n
		sample = randn(2,50);
		r = KS_test_multi(sample,0,1);
%		r2 = KS_test_multi(sample*5,0,1);
		d(i) = r.D;
%		d2(i) = r2.D;
		w(i) = r.W;
%		w2(i) = r2.W;
	end

%	figure()
%	plot(d, 'r', w, 'g');
%	legend('KS','CVM');
%	title('Comparacion de dos poblaciones iguales');
%	print('ks-cvm-iguales.eps');
%	figure()
%	plot(d2, 'r', w2, 'g');
%	legend('KS','CVM');
%	title('Comparacion de dos poblaciones distintas');
%	print('ks-cvm-distintas.eps');
%	figure()
%	plot(w, 'r', w2, 'g');
%	legend('CVM iguales','CVM distintas');
%	title('Comparacion de dos poblaciones distintas');
%	print('cvm-iguales-cvm-distintas.eps');
%	figure()
%	plot(d, 'r', d2, 'g');
%	legend('KS iguales','KS distintas');
%	title('Comparacion de dos poblaciones distintas');
%	print('ks-iguales-ks-distintas.eps');
%
%	disp(['Var d: ', num2str(var(d)),' mean d: ', num2str(mean(d))]);
%	disp(['Var w: ', num2str(var(w)),' mean w: ', num2str(mean(w))]);
%	disp(['Var d2: ', num2str(var(d2)),' mean d2: ', num2str(mean(d2))]);
%	disp(['Var w2: ', num2str(var(w2)),' mean w2: ', num2str(mean(w2))]);

	figure()
	d = sort(d);
	w = sort(w);
	plot (1:n,d,1:n,w);
	legend('KS','CVM');
	title('Comportamiento de los tests ante poblaciones iguales');
end

