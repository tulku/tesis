clc;
clear;
close all;
warning ('error','Octave:divide-by-zero');

cant_muestras = 50;
cant_iter = 15;
cant_simulaciones = 2000;
SNR = 20; %dBs
archivo_salida = 'sim_KS_vs_CVM_1.oct';

multicore=0;

% Matriz con el alfabeto
x_p =  [[-3,-3,-3,-3,-1,-1,-1,-1,1,1,1,1,3,3,3,3];
	[-3,-1,1,3,-3,-1,1,3,-3,-1,1,3,-3,-1,1,3]];

simulaciones = {};
datos = {};

%simulaciones = startmulticoremaster(@simular, parameterCell);
disp('Generando simulaciones...');
fflush(stdout);
for rep = 1:cant_simulaciones
	%h = zeros(2,2);
	%% Generacion de senales y canal.
	h = abs(randn(2,2)*1.2);
	% Generamos el ruido
	covar = zeros(2,2);
	covar(1,1) = 5*h(1,:)*h(1,:)'/(10^(SNR/10));
	covar(2,2) = 5*h(2,:)*h(2,:)'/(10^(SNR/10));
	N = randn(2,cant_muestras);
	% Generamos los datos a transmitir con 4 niveles
	a = sign(randn(2,cant_muestras));
	b = 2*sign(randn(2,cant_muestras));
	X = a+b;
	% Mezclamos todo
	Y = h*X + sqrt(covar)*N;
	hk = abs(randn(2,2)*1.2);
	tipo = 'comun';
	aux_names = {'X','Y', 'x_p', 'covar', 'cant_iter', 'hk', 'h', 'tipo'};
	aux = cell2struct({X; Y; x_p; covar; cant_iter; hk; h; tipo},aux_names,1);
	datos{rep}=aux;
end
tic
if multicore == 1
	simulaciones = startmulticoremaster(@simular, datos);
else
	for rep = 1:cant_simulaciones
		disp(['Simulacion: ',num2str(rep)]);
		fflush(stdout);
		simulaciones{rep} = simular(datos{rep});
%		simulaciones{rep} = test_implicacion(datos{rep});
		if !mod(rep,100)
			save(archivo_salida);
		end
	end
end

if cant_simulaciones >= 20
	disp('Terminado');	
else 
	for rep = 1:cant_simulaciones
%		resultado_nombres = {'hv','detectado','errores','fig_error','h','covar',
%			... 'pasa','roto','r_val','datos'};
		temp = simulaciones{rep};
		disp([num2str(rep),') ', 'Error: ', num2str(temp.fig_error) ...
			' - Pasa: ', num2str(temp.pasa), ' - PasaCVM: ', num2str(temp.pasa_cvm),
			' - Roto: ', num2str(temp.roto), ' - D: ', num2str(temp.r_val.D), ...
			' - Dcvm: ', num2str(temp.r_val.W)]);
		if temp.roto == 0
			fe(rep)=temp.fig_error;
		end
%		mostrar_grafico(temp.hv,temp.h,cant_iter,num2str(rep));
	end
end

save(archivo_salida);
toc
