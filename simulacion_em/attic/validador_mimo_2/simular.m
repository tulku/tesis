%function resultado_simulacion = simular(x_p, cant_total, cant_iter, SNR);
%datos = {Y, x_p, covar, cant_iter, hk, h};
function resultado_simulacion = simular(datos);
	Y = datos.Y;
	x_p = datos.x_p;
	covar = datos.covar;
	cant_iter = datos.cant_iter;
	hk = datos.hk;
	h = datos.h;
	roto = 0;
	try
		if datos.tipo == 'robus'
			hv = dfe_r(Y, x_p, covar, cant_iter, hk);
		elseif datos.tipo == 'comun'
			hv = dfe(Y, x_p, covar, cant_iter, hk);
		end
	catch
		roto = 1;
		return
	end

	% Detectamos los simbolos transmitidos.
	ht=hv{length(hv)};
	[detectado, errores] = detector(ht,Y,x_p);
	fig_error = calc_error (h,ht);
	r = validador(errores, covar, detectado, 0);
	% Guardo los errores
	resultado_nombres = {'hv','detectado','errores','fig_error','h','covar','pasa','pasa_cvm','roto','r_val','datos'};
	resultado_celda = {hv;detectado;errores;fig_error;h;covar;r.pasa;r.pasa_cvm;roto;r;datos};
	resultado_simulacion = cell2struct(resultado_celda,resultado_nombres,1);
end
