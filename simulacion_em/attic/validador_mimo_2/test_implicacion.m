%function resultado_simulacion = simular(x_p, cant_total, cant_iter, SNR);
%datos = {Y, x_p, covar, cant_iter, hk, h};
function resultado_simulacion = test_implicacion(datos);
	Y = datos.Y;
	x_p = datos.x_p;
	covar = datos.covar;
	cant_iter = datos.cant_iter;
	hk = datos.hk;
	h = datos.h;
	roto = 0;

	% Detectamos los simbolos transmitidos.
	% hv la creamos solo para poder devolverla, pero no significa nada.
	hv = randn(2,2);
	% en ht ponemos el resultado que queremos.
	%ht = randn(2,2)*1.2; % para resultados falsos.
	ht = datos.h;		  % para resultados siempre correctos.

	[detectado, errores] = detector(ht,Y,x_p);
	fig_error = calc_error (h,ht);
	r = validador(errores, covar, detectado, 0);
	% Guardo los errores
	resultado_nombres = {'hv','detectado','errores','fig_error','h','covar','pasa','roto','r_val'};
	resultado_celda = {hv;detectado;errores;fig_error;h;covar;r.pasa;roto;r};
	resultado_simulacion = cell2struct(resultado_celda,resultado_nombres,1);
end
