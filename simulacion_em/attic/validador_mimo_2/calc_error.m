% Esta funcion calcula un error entre las estimaciones y el valor real
% Como un resultado cruzado sería "correcto" el algoritmo busca el valor
% real mas cercano al estimado y calcula el error con ese.

function e = calc_error (h, hk)

h = sort(vec(h));
hk = sort(vec(hk));
l = length(hk);

e = zeros(l,1);

for i = 1:l
	e(i) = (h(i)-hk(i))^2;
end

e=(sum(e));
