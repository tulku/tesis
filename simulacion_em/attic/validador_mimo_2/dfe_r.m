% Este es el estimador EM.
% Y: señal recibida.
% x_p: posibles señales recibidas.
% covar: matriz de covarianza del ruido
% cant_iter: cantidad de iteracion a realizar
% hk: valor inicial del estimador

% h: cell con la evolucion de la matriz h.
% s: cell con una señal "detectada" suave.
% sigma_s: cell con la varianza de los s estimados.

function h = dfe(Y,x_p,covar,cant_iter,hk)
	cant_muestras = length(Y);
	cant_iter=cant_iter+1;
	icovar = inv(covar);
	h = {};
	beta = 1.75;
	var = trace(covar)/2;
	% Guardamos el valor inicial
	
	for i=1:cant_iter
		h = [h {hk}];
		temp1=zeros(2,2);
		temp2=zeros(2,2);
		difs=zeros(2,cant_muestras);
		delta = 0;

		for j=1:cant_muestras
			aux2 = zeros(2,2);
			aux1 = zeros(2,1);
			for k=1:length(x_p)
			    A=( (Y(:,j) - hk*x_p(:,k))' * (Y(:,j) - hk*x_p(:,k)) )/var*2;
			    conjunta(k) = exp(-A);
			    aux1 = aux1 + x_p(:,k)*conjunta(k);
			    aux2 = aux2 + x_p(:,k)*x_p(:,k)'*conjunta(k);
			end
			p_y = sum(conjunta);
			simbolo(:,j) = aux1/p_y;
			difs(:,j)=Y(:,j)-hk*simbolo(:,j);
			b=sqrt(2)*difs(:,j)/var;
			%b = difs(:,j)-med/var;

			if abs(b) <= beta
				temp1=temp1+Y(:,j)*simbolo(:,j)';
				temp2=temp2+simbolo(:,j)*simbolo(:,j)';
			end
		end
		hk = (temp1/var)/(temp2/var);
		var = mean(mad(difs'));
	%	hk = temp1/temp2;
	end
end
