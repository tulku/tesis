from numpy import *
from numpy.random import randn
from numpy.linalg import inv
from pylab import *

cant_muestras=20
cant_iter=20

h=abs(randn(2,1))
print "h:"
print h

sigma2 = array ([0.1, 0.25, 0.49, 0.81])

x1=sign(randn(1,cant_muestras))
x2=sign(randn(1,cant_muestras))

xa=array([[1], [1]])
xb=array([[1], [-1]])
xc=array([[-1], [1]])
xd=array([[-1], [-1]])

x_p = hstack((xa,xb,xc,xd))

X=hstack( (x1.reshape((-1,1)), x2.reshape((-1,1))) )

hkorig=abs(randn(2,1))

#Variabes auxiliares
hv11=empty(cant_iter)
hv12=empty(cant_iter)
aux1=arange(cant_iter)
aux2=ones(cant_iter)
k=221

Ns=randn(cant_muestras,1)


for s in sigma2:
	N=Ns*s
	Y=dot(X,h)+N
	hk=hkorig
	hv11[0]=hk[0]
	hv12[0]=hk[1]

	for iter in range(cant_iter-1):
		acum1=zeros(4,dtype=float64).reshape((2,2))
		acum2=zeros(2,dtype=float64).reshape((2,1))
		for i in aux1:
			Bk=array([pow((Y[i]-dot(j.T,hk)),2) for j in [xa,xb,xc,xd]],dtype=float64)
			Bk=Bk.flat
			Bk=Bk/(2*s)
			conjunta=exp(-Bk)
			p_y=conjunta.sum()
			aux=0
			for l in range(4):
				aux = aux + mat(x_p[:,l]).T * mat(x_p[:,l]) * conjunta[l]
			aux = aux/p_y
			acum1=acum1+aux

		for i in aux1:
			Bk=array([pow((Y[i]-dot(j.T,hk)),2) for j in [xa,xb,xc,xd]],dtype=float64)
			Bk=Bk.flat
			Bk=Bk/(2*s)
			conjunta=exp(-Bk)
			p_y=conjunta.sum()
			aux=0
			for l in range(4):
				aux = aux + mat(x_p[:,l]).T * conjunta[l]
			aux = Y[i].tolist()[0]*aux/p_y
			acum2=acum2+aux

		hk=dot(inv((acum1)),acum2)
		hv11[iter+1]=hk[0]
		hv12[iter+1]=hk[1]

	print "hk:"
	print hk
	subplot(k)
	plot (aux1,hv11,'r--',aux1,hv12,'b--',aux1,h[0]*aux2,'ro',aux1,h[1]*aux2,'bo')
	k=k+1
show()
