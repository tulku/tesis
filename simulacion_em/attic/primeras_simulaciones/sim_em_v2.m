clear
%clearplot

cant_muestras=20;
cant_iter=10;
% Mostramos los valores reales de H.
h=abs(randn(2,1))
%h=[10; 1];

sigma2=[0.1 0.25 0.49 0.81];

xa=[1 1]';
xb=[1 -1]';
xc=[-1 1]';
xd=[-1 -1]';

x_p=[xa xb xc xd];

x1=sign(randn(1,cant_muestras));
x2=sign(randn(1,cant_muestras));

X=[x1' x2']';

% Mostramos el valor inicial de hk.
hkorig=abs(randn(2,1))
%hkorig=h;

% aux1 y aux2 las vamos a usar para graficar
aux1=[1:cant_iter];
aux2=ones(1,cant_iter);
%multiplot(2,length(sigma2)/2);

%sigma2 son los valores de desvio del ruido que se van a probar
Ns=randn(cant_muestras,1);

for k=1:length(sigma2)
	N=Ns*(sigma2(k));
	Y=X'*h+N;
	hk=hkorig;
	hv11=hk(1);
	hv12=hk(2);

for j=2:cant_iter
	acum1=acum2=0;
	i=1;
	for i=1:cant_muestras
		Bk=[ (Y(i)-xa'*hk)**2 (Y(i)-xb'*hk)**2 (Y(i)-xc'*hk)**2 (Y(i)-xd'*hk)**2 ];
		Bk=Bk/(2*sigma2(k));
		conjunta=exp(-Bk);
		p_y=sum(conjunta);
		aux=0;
		for l=1:4
			aux = aux + ((x_p(:,l)*x_p(:,l)')*conjunta(l));
		endfor
		aux=aux/p_y;
		acum1 = acum1 + aux;
	endfor

	for i=1:cant_muestras
		Bk=[ (Y(i)-xa'*hk)**2 (Y(i)-xb'*hk)**2 (Y(i)-xc'*hk)**2 (Y(i)-xd'*hk)**2 ];
		Bk=Bk/(2*sigma2(k));
                conjunta=exp(-Bk);
                p_y=sum(conjunta);
                aux=0;
                for l=1:4
                        aux = aux + x_p(:,l)*conjunta(l);
                endfor
                aux=Y(i)*aux/p_y;
		acum2 = acum2 + aux;
	endfor
	hk = acum1**(-1) * acum2;
	hv11(j)=hk(1);
	hv12(j)=hk(2);
endfor

% Mostramos el valor final de hk
hk

%sw1=mod(k-1,length(sigma2)/2)+1;
%sw2=k/(length(sigma2)/2);
%subwindow(sw1,sw2);
%ti=num2str(sigma2(k));
%title(ti);
%plot(aux1,hv11,'r;H1;',aux1,hv12,'b;H2;',aux1,h(1)*aux2,'r',aux1,h(2)*aux2,'b')
endfor
