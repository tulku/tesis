from numpy import *
from numpy.random import randn
from numpy.random import rand
from numpy.linalg import inv
from pylab import *

cant_muestras=50
cant_iter=20

h=abs(randn(2,1))
print "h:"
print h

SNR = 12 #dBs
#sigma2 = array([0.1])
sigma2 = (dot(h.T,h)/pow(10,SNR/10)).reshape(1,)

x1=sign(randn(1,cant_muestras))
x2=sign(randn(1,cant_muestras))

xa=array([[1], [1]])
xb=array([[1], [-1]])
xc=array([[-1], [1]])
xd=array([[-1], [-1]])

x_p = hstack((xa,xb,xc,xd))

X=hstack( (x1.reshape((-1,1)), x2.reshape((-1,1))) )

N=randn(cant_muestras,1)*sigma2
Y=dot(X,h)+N

hkorig=abs(rand(2,4))

#Variabes auxiliares
#hv11=empty(cant_iter)
#hv12=empty(cant_iter)
hv11=zeros((4,cant_iter),dtype=float64)
hv12=zeros((4,cant_iter),dtype=float64)
aux1=arange(cant_iter)
aux2=ones(cant_iter)

for k in range(4):
	hk=hkorig[:,k].reshape(-1,1)
	hv11[k,0]=hk[0]
	hv12[k,0]=hk[1]

	for iter in range(cant_iter-1):
		acum1=zeros(4,dtype=float64).reshape((2,2))
		acum2=zeros(2,dtype=float64).reshape((2,1))
		for i in aux1:
			Bk=array([pow((Y[i]-dot(j.T,hk)),2) for j in [xa,xb,xc,xd]],dtype=float64)
			Bk=Bk.flat
			Bk=Bk/(2*sigma2)
			conjunta=exp(-Bk)
			p_y=conjunta.sum()
			aux=0
			for l in range(4):
				aux = aux + mat(x_p[:,l]).T * mat(x_p[:,l]) * conjunta[l]
			aux = aux/p_y
			acum1=acum1+aux

		for i in aux1:
			Bk=array([pow((Y[i]-dot(j.T,hk)),2) for j in [xa,xb,xc,xd]],dtype=float64)
			Bk=Bk.flat
			Bk=Bk/(2*sigma2)
			conjunta=exp(-Bk)
			p_y=conjunta.sum()
			aux=0
			for l in range(4):
				aux = aux + mat(x_p[:,l]).T * conjunta[l]
			aux = Y[i].tolist()[0]*aux/p_y
			acum2=acum2+aux

		try:
			hk=dot(inv((acum1)),acum2)
		except LinAlgError:
			print "##################"
			print " matriz singular! "
			print "##################"
			print acum1
			break
		hv11[k,iter+1]=hk[0]
		hv12[k,iter+1]=hk[1]

	print "hk:"
	print hk
	#figure(k)
	subplot(2,2,k+1)
	title(k)
	plot(aux1,hv11[k,:],'r',aux1,hv12[k,:],'b',aux1,h[0]*aux2,'r--',aux1,h[1]*aux2,'b--')
	
	print "hv12:"
	print hv12[k,:]
show()
