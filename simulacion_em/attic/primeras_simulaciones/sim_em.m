x1=1;
x2=1;
h11=20;
h12=5;

N=[randn(1) randn(1)]';

y=x1*h11+x2*h12+N(1);

%h11v(1)=randn(1);
%h12v(1)=randn(1);
h11v(1)=5;
h12v(1)=20;

for i=2:100

	% Calculo el numerador de h11k
	% x1=1 y x2=1 | x1=1 y x2=-1
	a1= (2*y-2*h12v(i-1))*exp(-(y-h11v(i-1)-h12v(i-1))**2) + (2*y+2*h12v(i-1))*exp(-(y-h11v(i-1)+h12v(i-1))**2);
	% x1=-1 y x2=1 | x1=-1 y x2=-1
	a2= (-2*y+2*h12v(i-1))*exp(-(y+h11v(i-1)-h12v(i-1))**2) + (-2*y-2*h12v(i-1))*exp(-(y+h11v(i-1)+h12v(i-1))**2);
	h11kn=a1+a2;
	
	% Calculo el denominador de h11k
	a1= 2*exp(-(y-h11v(i-1)-h12v(i-1))**2)+2*exp(-(y-h11v(i-1)+h12v(i-1))**2);
	a2= 2*exp(-(y+h11v(i-1)-h12v(i-1))**2)+2*exp(-(y+h11v(i-1)+h12v(i-1))**2);
	h11kd=a1+a2;
	% Calculo h11k;
	h11v(i)=h11kn/h11kd;
	%h11k=h11kn/h11kd;
	
	% Calculo el numerador de h12k
	% x1=1 y x2=1 | x1=1 y x2=-1
	a1= (2*y-2*h11v(i-1))*exp(-(y-h11v(i-1)-h12v(i-1))**2) + (-2*y+2*h11v(i-1))*exp(-(y-h11v(i-1)+h12v(i-1))**2);
	% x1=-1 y x2=1 | x1=-1 y x2=-1
	a2= (2*y+2*h11v(i-1))*exp(-(y+h11v(i-1)-h12v(i-1))**2) + (-2*y-2*h11v(i-1))*exp(-(y+h11v(i-1)+h12v(i-1))**2);
	h12kn=a1+a2;
	
	% Calculo el denominador de h12k
	a1= 2*exp(-(y-h11v(i-1)-h12v(i-1))**2)+2*exp(-(y-h11v(i-1)+h12v(i-1))**2);
	a2= 2*exp(-(y+h11v(i-1)-h12v(i-1))**2)+2*exp(-(y+h11v(i-1)+h12v(i-1))**2);
	h12kd=a1+a2;
	% Calculo h12k;
	h12v(i)=h12kn/h12kd;
	%h12k=h12kn/h12kd;
endfor

