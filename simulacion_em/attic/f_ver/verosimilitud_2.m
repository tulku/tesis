load datos.oct

cant_puntos = 50;

# Creamos la grilla de valores de h
x=linspace(hk(1)-2,hk(1)+2,cant_puntos);
y=linspace(hk(2)-2,hk(2)+2,cant_puntos);

# Copiamos x_p al pedo
x_p =  [[-1,-1,1,1];[-1,1,-1,1]];

pos1=1;
for h1 = x
	pos2=1;
	for h2 = y
		hxy = [h1,h2];
		aux1 = 1;
		for i=1:length(Y)
			aux2 = 0;
			for j=1:length(x_p)
				aux2 = aux2 + exp(-(Y(i) - (hxy*x_p(:,j)))**2);
			endfor
			aux1 = aux1*aux2;
		endfor
		z(pos1,pos2)=aux1;
		pos2=pos2+1;
	endfor
	pos1=pos1+1;
endfor

mesh(x,y,z);

