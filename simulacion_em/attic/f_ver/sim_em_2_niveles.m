cant_muestras = 20;
cant_iter = 30

% Generamos el canal
printf("h original:\n");
h = abs(randn(2,1))

% Generamos el ruido
SNR = 20; #dBs
sigma2 = h'*h/(10**(SNR/10));
N = sqrt(sigma2)*randn(1,cant_muestras);

% Generamos los datos a transmitir
X = sign(randn(2,cant_muestras));

% Mezclamos todo
Y = h'*X + N;

% Estimamos el h inicial
hsum = max(abs(Y(1:10)));
hk = abs(rand(2,1) - 0.5 + hsum/2);

% Guardamos el valor inicial
hv = hk;

% Matriz con el alfabeto
x_p = [[1 1 -1 -1];[1 -1 1 -1]];

for i=2:cant_iter

    acum1 = zeros(2,2);
    acum2 = zeros(2,1);

    for j=1:cant_muestras
    	for k=1:length(x_p)
	    Bk(k) = (Y(j)-x_p(:,k)'*hk)^2;
	endfor

	Bk = Bk/(2*sigma2);
	conjunta = exp(-Bk);
	p_y = sum(conjunta);

	aux = 0;
	for k=1:length(x_p)
	    aux = aux + x_p(:,k) * x_p(:,k)' * conjunta(k);
	endfor
	aux = aux/p_y;

	acum1 = acum1+aux;
    endfor

    for j=1:cant_muestras
    	for k=1:length(x_p)
	    Bk(k) = (Y(j)-x_p(:,k)'*hk)^2;
	endfor

	Bk = Bk/(2*sigma2);
	conjunta = exp(-Bk);
	p_y = sum(conjunta);

	aux = 0;
	for k=1:length(x_p)
	    aux = aux + x_p(:,k) * conjunta(k);
	endfor
	aux = Y(j)*aux/p_y;

	acum2 = acum2+aux;
    endfor

    hk = inv(acum1)*acum2;
    hv(:,i) = hk;
endfor

printf("hk:\n");
hk

%save datos.oct h hk hv X Y

aux1 = 1:cant_iter;
aux2 = ones(1,cant_iter);
plot(aux1,hv(1,:),'r;H1;', aux1,hv(2,:),'b;H2;', aux1,h(1)*aux2,'ro', aux1,h(2)*aux2,'bo');

