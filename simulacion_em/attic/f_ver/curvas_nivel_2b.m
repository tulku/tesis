cant_muestras = 200;

cant_iter = 3;

cant_puntos = 21;
% Generamos el canal
printf("h original:\n");
h = abs(randn(2,1))

% Generamos el ruido
SNR = 20; #dBs
sigma2 = h'*h/(10**(SNR/10));
N = sqrt(sigma2)*randn(1,cant_muestras);

% Generamos los datos a transmitir
X = sign(randn(2,cant_muestras));

% Mezclamos todo
Y = h'*X + N;

% Estimamos el h inicial
hsum = max(abs(Y(1:10)));
hk = abs(rand(2,1) -1 + hsum/2);

% Guardamos el valor inicial
hv = hk;

% Matriz con el alfabeto
x_p = [[1 1 -1 -1];[1 -1 1 -1]];

for i=2:cant_iter

    acum1 = zeros(2,2);
    acum2 = zeros(2,1);

    for j=1:cant_muestras

	aux = 0;
	for k=1:length(x_p)
	    conjunta(k) = exp(-((Y(j)-x_p(:,k)'*hk)^2)/(2*sigma2));
	    aux = aux + x_p(:,k) * x_p(:,k)' * conjunta(k);
	endfor

	p_y = sum(conjunta);
	aux = aux/p_y;

	acum1 = acum1+aux;
    endfor

    for j=1:cant_muestras

	aux = 0;
	for k=1:length(x_p)
	    conjunta(k) = exp (- ((Y(j)-x_p(:,k)'*hk)^2)/(2*sigma2));
	    aux = aux + x_p(:,k) * conjunta(k);
	endfor
	p_y = sum(conjunta);
	aux = Y(j)*aux/p_y;
	
	printf("%s%i%s%i%s%g%s%g%s","Iter: ",i, " Muestra: ",j, " Min: ", min(conjunta), " Max: ",max(conjunta),"\n")

	acum2 = acum2+aux;
    endfor

    hk = inv(acum1)*acum2;
    hv(:,i) = hk;
endfor

printf("hk:\n");
hk

%save datos.oct h hk hv X Y

aux1 = 1:cant_iter;
aux2 = ones(1,cant_iter);
figure(1);
plot(aux1,hv(1,:),'r;H1;', aux1,hv(2,:),'b;H2;', aux1,h(1)*aux2,'ro', aux1,h(2)*aux2,'bo');


# Creamos la grilla de valores de h
x=linspace(hk(1)-2,hk(1)+2,cant_puntos);
y=linspace(hk(2)-2,hk(2)+2,cant_puntos);

pos1=1;
for h1 = x
	pos2=1;
	for h2 = y
		hxy = [h1,h2];
		aux1 = 1;
		for i=1:length(Y)
			aux2 = 0;
			for j=1:length(x_p)
				aux2 = aux2 + exp(-(Y(i) - (hxy*x_p(:,j)))**2);
			endfor
			aux1 = aux1*aux2;
		endfor
		z(pos1,pos2)=aux1;
		pos2=pos2+1;
	endfor
	pos1=pos1+1;
endfor

figure(2);
contour(x,y,z);

hold on;
plot3(h(1),h(2),[0],"-@o;h verdadera;")
plot3(hv(1,:),hv(2,:),zeros(1,length(hv)),"r;Caminito;")
plot3(hv(1,1),hv(2,1),zeros(1,1),"rx;Inicial;") 
plot3(hv(1,length(hv)),hv(2,length(hv)),zeros(1,1),"r*;Final;") 
hold off;

figure(3);
mesh(x,y,z);

hold on;
plot3(h(1),h(2),[0],"-@o;h verdadera;")
plot3(hv(1,:),hv(2,:),1000*ones(1,length(hv)),"r;Caminito;")
plot3(hv(1,1),hv(2,1),1000*ones(1,1),"rx;Inicial;") 
plot3(hv(1,length(hv)),hv(2,length(hv)),1000*ones(1,1),"r*;Final;") 
hold off;

