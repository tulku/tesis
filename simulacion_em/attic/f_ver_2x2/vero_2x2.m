tic
load ('datos.oct')
% necesito Y, x_p, h, icovar

cant_puntos = 200;

% Creamos la grilla de valores de h
grilla = zeros(4,cant_puntos);
err = zeros(1,cant_puntos);
vero = zeros(1,cant_puntos);

cota = 2.25*(norm(vec(h))^2)/4;
% Creamos la grilla centrada en el valor real.
grilla(1,:)=linspace(h(1,1),h(1,1)+cota,cant_puntos);
grilla(2,:)=linspace(h(1,2),h(1,2)+cota,cant_puntos);
grilla(3,:)=linspace(h(2,1),h(2,1)+cota,cant_puntos);
grilla(4,:)=linspace(h(2,2),h(2,2)+cota,cant_puntos);

z = 1;
for a = grilla
	aux1 = 1;
	hk = [a(1,1) a(2,1); a(3,1) a(4,1)];
	for i=1:length(Y)
		conjunta = 0;
		for j=1:length(x_p)
			A=( (Y(:,i) - hk*x_p(:,j))' * icovar * (Y(:,i) - hk*x_p(:,j)) )/2;
			conjunta += exp(-A);
		end
		aux1 *= conjunta;
	end
	err1(z) = calc_error(h, hk);
	err1(z) /= (norm(vec(h))^2);
	vero1(z) = aux1;
	z += 1;
end

% Creamos la grilla centrada en el valor real.
grilla(1,:)=linspace(h(1,1)-cota,h(1,1),cant_puntos);
grilla(2,:)=linspace(h(1,2)-cota,h(1,2),cant_puntos);
grilla(3,:)=linspace(h(2,1)-cota,h(2,1),cant_puntos);
grilla(4,:)=linspace(h(2,2)-cota,h(2,2),cant_puntos);
z = 1;
for a = grilla
	aux2 = 1;
	hk = [a(1,1) a(2,1); a(3,1) a(4,1)];
	for i=1:length(Y)
		conjunta = 0;
		for j=1:length(x_p)
			A=( (Y(:,i) - hk*x_p(:,j))' * icovar * (Y(:,i) - hk*x_p(:,j)) )/2;
			conjunta += exp(-A);
		end
		aux2 *= conjunta;
	end
	err2(z) = calc_error(h, hk);
	err2(z) /= (norm(vec(h))^2);
	vero2(z) = aux2;
	z += 1;
end

err1 = sqrt(err1);
err2 = sqrt(err2);
toc
