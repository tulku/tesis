% Para los 4 casos mas extremos dentro de las siguientes caterias:
% * Identificacion mala aceptada
% * Identificacion buena aceptada
% * Identificacion mala no aceptada
% * Identificacion buena no aceptada
% Se quiere: * el DeltaH (H orig, H final, fig_error)
% * delta X (señal transmitida, señal recivida)
% * covar

function datos = analizar_casos_interesantes (sim_5, sim_10, sim_15, sim_20, sim_25, sim_30, sim_35, graficar)
	
	snrs = [5, 10, 15, 20, 25, 30, 35];

	a5 = analizar_un_snr(sim_5);
	a10 = analizar_un_snr(sim_10);
	a15 = analizar_un_snr(sim_15);
	a20 = analizar_un_snr(sim_20);
	a25 = analizar_un_snr(sim_25);
	a30 = analizar_un_snr(sim_30);
	a35 = analizar_un_snr(sim_35);

	%%% Concateno todos los errores de deteccion segun SNR.	
	datos.e_p_bien = [a5.e_p_bien, a10.e_p_bien, a15.e_p_bien, a20.e_p_bien, a25.e_p_bien, a30.e_p_bien, a35.e_p_bien];
	datos.e_p_mal = [a5.e_p_mal, a10.e_p_mal, a15.e_p_mal, a20.e_p_mal, a25.e_p_mal, a30.e_p_mal, a35.e_p_mal];
	datos.e_no_p_bien = [a5.e_no_p_bien, a10.e_no_p_bien, a15.e_no_p_bien, a20.e_no_p_bien, a25.e_no_p_bien, a30.e_no_p_bien, a35.e_no_p_bien];
	
	datos.e_no_p_mal = [a5.e_no_p_mal, a10.e_no_p_mal, a15.e_no_p_mal, a20.e_no_p_mal, a25.e_no_p_mal, a30.e_no_p_mal, a35.e_no_p_mal];

	%%% Concateno todas las varianzas de ruido segun SNR.	
	datos.n_p_bien = [a5.n_p_bien, a10.n_p_bien, a15.n_p_bien, a20.n_p_bien, a25.n_p_bien, a30.n_p_bien, a35.n_p_bien];
	datos.n_p_mal = [a5.n_p_mal, a10.n_p_mal, a15.n_p_mal, a20.n_p_mal, a25.n_p_mal, a30.n_p_mal, a35.n_p_mal];
	datos.n_no_p_bien = [a5.n_no_p_bien, a10.n_no_p_bien, a15.n_no_p_bien, a20.n_no_p_bien, a25.n_no_p_bien, a30.n_no_p_bien, a35.n_no_p_bien];
	
	datos.n_no_p_mal = [a5.n_no_p_mal, a10.n_no_p_mal, a15.n_no_p_mal, a20.n_no_p_mal, a25.n_no_p_mal, a30.n_no_p_mal, a35.n_no_p_mal];

	%%% Concateno todas las figura de error segun SNR.	
	datos.f_p_bien = [a5.f_p_bien, a10.f_p_bien, a15.f_p_bien, a20.f_p_bien, a25.f_p_bien, a30.f_p_bien, a35.f_p_bien];
	datos.f_p_mal = [a5.f_p_mal, a10.f_p_mal, a15.f_p_mal, a20.f_p_mal, a25.f_p_mal, a30.f_p_mal, a35.f_p_mal];
	datos.f_no_p_bien = [a5.f_no_p_bien, a10.f_no_p_bien, a15.f_no_p_bien, a20.f_no_p_bien, a25.f_no_p_bien, a30.f_no_p_bien, a35.f_no_p_bien];
	datos.f_no_p_mal = [a5.f_no_p_mal, a10.f_no_p_mal, a15.f_no_p_mal, a20.f_no_p_mal, a25.f_no_p_mal, a30.f_no_p_mal, a35.f_no_p_mal];

	datos.s_p_bien = [a5.s_p_bien, a10.s_p_bien, a15.s_p_bien, a20.s_p_bien, a25.s_p_bien, a30.s_p_bien, a35.s_p_bien];
	datos.s_p_mal = [a5.s_p_mal, a10.s_p_mal, a15.s_p_mal, a20.s_p_mal, a25.s_p_mal, a30.s_p_mal, a35.s_p_mal];
	datos.s_no_p_bien = [a5.s_no_p_bien, a10.s_no_p_bien, a15.s_no_p_bien, a20.s_no_p_bien, a25.s_no_p_bien, a30.s_no_p_bien, a35.s_no_p_bien];

	datos.s_no_p_mal = [a5.s_no_p_mal, a10.s_no_p_mal, a15.s_no_p_mal, a20.s_no_p_mal, a25.s_no_p_mal, a30.s_no_p_mal, a35.s_no_p_mal];

	if (graficar == 1)
		graficar_datos_interesantes(datos, snrs);
	end

	[a1, a2] = lo_que_pidio_ceci (datos, snrs, graficar);

	datos.pesos_p_mal = a1;
	datos.pesos_p_bien = a2;

end

function [a1, a2, a3, a4] = lo_que_pidio_ceci (datos, snrs, graficar)
	% Estas fueron sus palabras:
	% Se ve claramente que el test KS falla cuando DeltaH y DeltaX son
	% grandes. Lo que quiero ver es en esos casos precisos, exactamente
	% cuánto valen y cuánto vale la combinación de (Hhat-H).X + Hhat.
	% (Xhat-X). Por ejemplo, agarrá el caso 20DB de SNR para los mal
	% aceptados

	snr = 4; %20 dB

	% 20dB mal aceptados
	disp('20 dB Mal identificado, aceptado por el validador');
	s = datos.s_p_mal(snr);
	f = datos.f_p_mal(snr);
	e = datos.e_p_mal(snr);
	n11 = sqrt(s.datos.covar(1,1));
	n12 = sqrt(s.datos.covar(2,2));

	disp(['Delta H: ', num2str(f), ' Delta X: ', num2str(e), ' Var ruido: ', num2str(n11)]);
	A = (s.h - s.hv{end})*s.datos.X(:,51:end);
	B = s.hv{end}*(s.detectado - s.datos.X(:,51:end));
	%disp('(Hhat- H).X + Hhat.(Xhat-X): ');
	a1 = A-B;
	disp(['Xcor: ', num2str(cor(a1(1,:)',a1(2,:)'))]);
	% Repetí el análisis para el caso de 20dB de SNR de la gráfica de bien aceptados.

	disp('20 dB Bien identificado, aceptado por el validador');
	s = datos.s_p_bien(snr);
	f = datos.f_p_bien(snr);
	e = datos.e_p_bien(snr);
	n21 = sqrt(s.datos.covar(1,1));
	n22 = sqrt(s.datos.covar(2,2));

	disp(['Delta H: ', num2str(f), ' Delta X: ', num2str(e), ' Var ruido: ', num2str(n21)]);
	A = (s.h - s.hv{end})*s.datos.X(:,51:end);
	A = (s.h - s.hv{end})*s.datos.X(:,51:end);
	B = s.hv{end}*(s.detectado - s.datos.X(:,51:end));
	%disp('(Hhat-H).X + Hhat.(Xhat-X): ');
	a2 = A-B;
	disp(['Xcor: ', num2str(cor(a2(1,:)',a2(2,:)'))]);

	disp('20 dB Bien identificado, rechazado por el validador');
	s = datos.s_no_p_bien(snr);
	f = datos.f_no_p_bien(snr);
	e = datos.e_no_p_bien(snr);
	n31 = sqrt(s.datos.covar(1,1));
	n32 = sqrt(s.datos.covar(2,2));

	disp(['Delta H: ', num2str(f), ' Delta X: ', num2str(e),' Var ruido: ', num2str(n31) ]);
	A = (s.h - s.hv{end})*s.datos.X(:,51:end);
	A = (s.h - s.hv{end})*s.datos.X(:,51:end);
	B = s.hv{end}*(s.detectado - s.datos.X(:,51:end));
	%disp('(Hhat-H).X + Hhat.(Xhat-X): ');
	a3 = A-B;
	disp(['Xcor: ', num2str(cor(a3(1,:)',a3(2,:)'))]);

	disp('20 dB Mal identificado, rechazado por el validador');
	s = datos.s_no_p_mal(snr);
	f = datos.f_no_p_mal(snr);
	e = datos.e_no_p_mal(snr);
	n41 = sqrt(s.datos.covar(1,1));
	n42 = sqrt(s.datos.covar(2,2));

	disp(['Delta H: ', num2str(f), ' Delta X: ', num2str(e),' Var ruido: ', num2str(n41) ]);
	A = (s.h - s.hv{end})*s.datos.X(:,51:end);
	A = (s.h - s.hv{end})*s.datos.X(:,51:end);
	B = s.hv{end}*(s.detectado - s.datos.X(:,51:end));
	%disp('(Hhat-H).X + Hhat.(Xhat-X): ');
	a4 = A-B;
	disp(['Xcor: ', num2str(cor(a4(1,:)',a4(2,:)'))]);

	Unos = ones(1,length(a1));
	if (graficar == 1)
		figure();
		plot(a1(1,:),'r',a1(2,:),'b',Unos*n11,'k',Unos*-n11,'k',Unos*n12,'k',Unos*-n12,'k');
		grid on;
		title(['Suma del error - Mal aceptado - ' num2str(cor(a1(1,:)',a1(2,:)'))]);
		print('suma-mal-aceptado.eps');

		figure();
		plot(a2(1,:),'r',a2(2,:),'b',Unos*n21,'k',Unos*-n21,'k',Unos*n22,'k',Unos*-n22,'k');
		grid on;
		title(['Suma del error - Bien aceptado - ' num2str(cor(a2(1,:)',a2(2,:)'))] );
		print('suma-bien-aceptado.eps');

		figure();
		plot(a3(1,:),'r',a3(2,:),'b',Unos*n31,'k',Unos*-n31,'k',Unos*n32,'k',Unos*-n32,'k');
		grid on;
		title(['Suma del error - Bien rechazado - ' num2str(cor(a3(1,:)',a3(2,:)'))]);
		print('suma-bien-rechazado.eps');

		figure();
		plot(a4(1,:),'r',a4(2,:),'b',Unos*n41,'k',Unos*-n41,'k',Unos*n42,'k',Unos*-n42,'k');
		grid on;
		title(['Suma del error - Mal rechazado - ' num2str(cor(a4(1,:)',a4(2,:)')) ]);
		print('suma-mal-rechazado.eps');
	end
end

function graficar_datos_interesantes (datos, snrs)

	figure()
	plot(snrs, datos.n_no_p_bien,'r', snrs, datos.e_no_p_bien,'b', snrs, log(datos.f_no_p_bien),'k' )
	grid on;
	legend('Varianza de ruido','Error de deteccion','logaritmo de la figura de error')
	title('Bien identificado, rechazado');
	xlabel('Cantidad de muestras');
	print('bien-rechazado.eps');
	
	figure()
	plot(snrs, datos.n_p_bien,'r', snrs, datos.e_p_bien,'b', snrs, log(datos.f_p_bien),'k' )
	grid on;
	legend('Varianza de ruido','Error de deteccion','logaritmo de la figura de error')
	title('Bien identificado, aceptado');
	xlabel('Cantidad de muestras');
	print('bien-aceptado.eps');
	
	figure()
	plot(snrs, datos.n_no_p_mal,'r', snrs, datos.e_no_p_mal,'b', snrs, log(datos.f_no_p_mal),'k' )
	grid on;
	legend('Varianza de ruido','Error de deteccion','logaritmo de la figura de error')
	title('Mal identificado, rechazado');
	xlabel('Cantidad de muestras');
	print('mal-rechazado.eps');
	
	figure()
	plot(snrs, datos.n_p_mal,'r', snrs, datos.e_p_mal,'b', snrs, log(datos.f_p_mal),'k' )
	grid on;
	legend('Varianza de ruido','Error de deteccion','logaritmo de la figura de error')
	title('Mal identificado, aceptado');
	xlabel('Cantidad de muestras');
	print('mal-aceptado.eps');
	
end

function a = analizar_un_snr(simulaciones)
	[s_p_bien, s_p_mal, s_no_p_bien, s_no_p_mal] = extrear_casos_interesantes (simulaciones);
	% Calculo la cantidad de errores en la decodificacion de los datos enviados
	a.e_p_bien = calc_error_rate (s_p_bien.datos.X(:,51:end), s_p_bien.detectado);
	a.e_p_mal = calc_error_rate (s_p_mal.datos.X(:,51:end), s_p_mal.detectado);
	a.e_no_p_mal = calc_error_rate (s_no_p_mal.datos.X(:,51:end), s_no_p_mal.detectado);
	a.e_no_p_bien = calc_error_rate (s_no_p_bien.datos.X(:,51:end), s_no_p_bien.detectado);
	% Busco la maxima varianza de ruido.
	a.n_p_bien = max(max(s_p_bien.datos.covar));
	a.n_p_mal = max(max(s_p_mal.datos.covar));
	a.n_no_p_bien = max(max(s_no_p_bien.datos.covar));
	a.n_no_p_mal = max(max(s_no_p_mal.datos.covar));
	% Guardo las figuras de ruido.
	a.f_p_bien = s_p_bien.fig_error;
	a.f_p_mal = s_p_mal.fig_error;
	a.f_no_p_bien = s_no_p_bien.fig_error;
	a.f_no_p_mal = s_no_p_mal.fig_error;
	% Guardo tambien toda la simulacion por si me piden mas cosas despues :)
	a.s_p_bien = s_p_bien;
	a.s_p_mal = s_p_mal;
	a.s_no_p_bien = s_no_p_bien;
	a.s_no_p_mal = s_no_p_mal;
end

function errores = calc_error_rate (X_trans, X_detectado)
	l = length(X_trans);
	ll = length(X_detectado);
	if (l != ll)
		errores = NaN;
		return;
	end
	errores = 0;
	% Creo un vector invertido por si se me dieron vuelta los canales.
	% t = zeros(2,1);
	for i = 1:l
		%t(1) = X_trans(:,l)(2);
		%t(2) = X_trans(:,l)(1);
		if ( X_trans(:,l) != X_detectado(:,l) ) % && t != X_detectado(:,l) )
			errores = errores + 1;
		end
	end
	errores = errores/l;
end

function [s_p_bien, s_p_mal, s_no_p_bien, s_no_p_mal] = extrear_casos_interesantes (simulaciones)
	ip = 1;
	inp = 1;
	pasan = [];
	no_pasan = [];
	errores_pasan = [];
	errores_no_pasan = [];

	for i = 1:length(simulaciones)
		sim = simulaciones{i};
		if (sim.r_val.pasa == 1)
			pasan(ip) = i;
			errores_pasan(ip) = sim.fig_error;
			ip = ip + 1; 
		elseif (sim.r_val.pasa == 0)
			no_pasan(inp) = i;
			errores_no_pasan(inp) = sim.fig_error;
			inp = inp + 1;
		end
	end
	index1 = zeros(1, ip-1);
	index2 = zeros(1, inp-1);

	% Ordeno las simulaciones. Con el mayor error primero
	[errores_pasan, index1] = sort(errores_pasan, 'descend');
	pasan = pasan(index1);
	[errores_no_pasan, index2] = sort(errores_no_pasan, 'descend');
	no_pasan = no_pasan(index2);

	% Sim que pasa el validador y esta muy mal estimado
	s_p_mal = simulaciones{pasan(1)};
	% Sim que pasa el validador y esta muy bien estimado
	s_p_bien = simulaciones{pasan(length(pasan))};
	% Sim que no pasa el validador y esta muy bien estimado
	s_no_p_mal = simulaciones{no_pasan(1)};
	% Sim que no pasa el validador y esta muy mal estimado
	s_no_p_bien = simulaciones{no_pasan(length(no_pasan))};
end
