function [i_a, i_r, c_a, c_r, SNRs] = grafico_snrs(sim_5, sim_10, sim_15, sim_20, sim_25, sim_30, sim_35)

	SNRs = [5, 10, 15, 20, 25, 30, 35];
	i_a = zeros(1,length(SNRs));
	i_r = zeros(1,length(SNRs));
	c_a = zeros(1,length(SNRs));
	c_r = zeros(1,length(SNRs));
	[i_a(1), i_r(1), c_a(1), c_r(1)] = calcular_porcentajes(sim_5);
	[i_a(2), i_r(2), c_a(2), c_r(2)] = calcular_porcentajes(sim_10);
	[i_a(3), i_r(3), c_a(3), c_r(3)] = calcular_porcentajes(sim_15);
	[i_a(4), i_r(4), c_a(4), c_r(4)] = calcular_porcentajes(sim_20);
	[i_a(5), i_r(5), c_a(5), c_r(5)] = calcular_porcentajes(sim_25);
	[i_a(6), i_r(6), c_a(6), c_r(6)] = calcular_porcentajes(sim_30);
	[i_a(7), i_r(7), c_a(7), c_r(7)] = calcular_porcentajes(sim_35);

%	[i_a(1), i_r(1), c_a(1), c_r(1)] = calcular_porcentajes(sim_10);
%	[i_a(2), i_r(2), c_a(2), c_r(2)] = calcular_porcentajes(sim_15);
%	[i_a(3), i_r(3), c_a(3), c_r(3)] = calcular_porcentajes(sim_20);
%	[i_a(4), i_r(4), c_a(4), c_r(4)] = calcular_porcentajes(sim_25);
%	[i_a(5), i_r(5), c_a(5), c_r(5)] = calcular_porcentajes(sim_30);
	plot (SNRs, i_a, 'r', SNRs, c_r, 'b');
	legend ('Incorrectos aceptados','Correctos rechazados');
	xlabel('SNR [dB]');
	ylabel('% de modelos');
	grid on;

end


function [i_a, i_r, c_a, c_r] = calcular_porcentajes (simulaciones)
	i = 1;
	for sim = simulaciones
		if (size(sim{1}) == [1 1] && !isnan(sim{1}.fig_error))
			errores(i) = sim{1}.fig_error;
			pasa(i) = sim{1}.r_val.pasa;
			W(i) = sim{1}.r_val.D;
			i += 1;
		end
	end

	[errores, index] = sort(errores, 'ascend');
	pasa = pasa (index);

	a1=find(pasa);
	an1=find(!pasa);

	l1 = length(find(errores(a1)>0.1));
	l2 = length(find(errores(an1)>0.1));
	i_a = l1*100/(l1+l2);
	i_r = l2*100/(l1+l2);

	l1 = length(find(errores(a1)<0.04));
	l2 = length(find(errores(an1)<0.04));
	c_a = l1*100/(l1+l2);
	c_r = l2*100/(l1+l2);
end
