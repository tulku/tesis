% Armo el grafico de los histogramas y calculo los % a partir de los datos
% procesados con kmeans.

function grafico_histograma_clusters(data)
	
	for i = [1:size(data,1)]
			errores(i) = data(i,1);
			pasa_multi_hard(i) = data(i,3)-1;
			W(i) = data(i,2);
		end
	end

	[errores, index] = sort(errores, 'ascend');
	pasa_multi_hard = pasa_multi_hard (index);

	a1=find(pasa_multi_hard);
	an1=find(!pasa_multi_hard);

	disp('KS');
	l1 = length(find(exp(errores(a1))>0.1));
	l2 = length(find(exp(errores(an1))>0.1));
	disp(['% De incorrectas aceptadas: ', num2str( l1*100/(l1+l2))]);
	disp(['% De incorrectas rechazadas: ', num2str( l2*100/(l1+l2))]);
	l1 = length(find(exp(errores(a1))<0.04));
	l2 = length(find(exp(errores(an1))<0.04));
	disp(['% De correctas aceptadas: ', num2str( l1*100/(l1+l2))]);
	disp(['% De correctas rechazadas: ', num2str( l2*100/(l1+l2))]);

	figure();
	[n1, x1] = hist(errores(a1),50);
	[n2, x2] = hist(errores(an1),50);
	semilogx(10.^x1,n1, 10.^x2,n2);
	legend('KS Validas', 'KS Invalidas');

%	print('histograma_correctas_incorrectas.eps');
%	figure()
%	plot(sort(W),'b',1:length(W),0.028*ones(1,length(W)),'r');
	%figure();
	%semilogx(errores,W(index),'o' ,errores,0.2*ones(1,length(errores)),'g');
	%legend('Simulacion','K')
	%print('ValorKS_error.eps')
end
