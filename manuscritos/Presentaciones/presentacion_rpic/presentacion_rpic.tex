\documentclass[compress]{beamer}

\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{subfigure}

\mode<presentation>
{
	\usetheme{Warsaw}
	\setbeamertemplate{navigation symbols}{}
	\setbeamercovered{transparent}
	\useinnertheme{rectangles}
}
\setcounter{tocdepth}{1}

\title[Validación de identificaciones MIMO]{Identificación ciega de canales
MIMO}
\subtitle{Validación del modelo}
\author[A.L. Chiesa, C.G. Galarza]
{
Alberto L. Chiesa \and Cecilia G. Galarza
}
\institute[LPSC]{LPSC, Departamento de Electrónica \\
Facultad de Ingeniería\\
Universidad de Buenos Aires}
\date[RPIC 2009]{XIII Reunión de Trabajo en Procesamiento de la Información y Control, 2009}
%\usepackage{default}

\begin{document}
\frame{\titlepage}

\section{Introducción}
\subsection{MIMO-OFDM}
\frame{
\frametitle{Marco de trabajo}
\begin{columns}
\column{0.3\textwidth}
\begin{center}
 \includegraphics[width=\columnwidth]{./img/pen.JPG}
\end{center}
\column{0.7\textwidth}
\begin{itemize}
\item Invalidación de modelos de canal.
\item Identificación ciega de canal MIMO-OFDM.
\item Empleo de algoritmos de estimación iterativos.
\item Analizar el comportamiento frente a distintas condiciones iniciales.
\item Poder descartar modelos inválidos.
\end{itemize}
\end{columns}
}

\frame{
\frametitle{Modelo del canal}
\begin{center}
 \includegraphics[width=.7\textwidth]{../presentacion_leo/img/estimador_mimo2.pdf}
\end{center}
Para el instante $j$,
\begin{equation*}
 Y_j=H_jX_j+N_j,\ \ 1<j<P \quad X \in 2 \times 1,\ Y \in 2 \times 1,\ H \in 2 \times 2
\end{equation*}
\vspace{-0.7cm}
\begin{itemize}
\item $N$ es el ruido blanco aditivo gaussiano de media nula y matriz de
covarianza diagonal $\varSigma$.
\item Las ganancias $H$ son valores reales.
\item Los símbolos $X$ pertenecen a una constelación de $B$ elementos equiprobables.
\end{itemize}
}

\subsection{Identificación}

\frame{
\frametitle{Identificación de máxima verosimilitud}
La función de verosimilitud depende de la señalización de X.
\begin{equation*}
 \mathcal{L}(H;X,Y) = \chi \prod_{j=1}^P \exp \left(-\dfrac{1}{2} (Y_j-HX_j)^T
\varSigma^{-1} (Y_j-HX_j)\right)
\end{equation*}

Ejemplos de funciones de verosimilitud, para el caso de un canal MISO:

\begin{figure}[h]
 \centering
 \subfigure[Constelación binaria] {
 \includegraphics[width=.45\textwidth]{img/vero_uni.pdf}
 }
 \subfigure[Constelación de 4 elementos] {
 \includegraphics[width=.45\textwidth]{img/verosimilitud.pdf}
 }
\end{figure}
}

\frame{
\frametitle{Identificación ciega}
Se propone utilizar \textbf{E}xpectation \textbf{M}aximization. Dado $H_0$:
\begin{equation*}
H_{k+1} = \arg\max_{H} \sum_{j=0}^P E_{X/Y_j}\left[\lVert Y_j-HX \rVert_{\varSigma^{-1}}^{2};
H_k \right] \qquad k =1,2,\dots
\end{equation*}
\vspace{-.8cm}
\begin{columns}
 \column{.4\textwidth}
 \begin{itemize}
  \item Resultado de identificación con dos $H_0$ distintos
\item ¿Podríamos calificar la bondad del modelo obtenido?
 \end{itemize}
 \column{.6\textwidth}
 \begin{center}
  \includegraphics[width=\columnwidth]{img/2do_resultado.pdf}
 \end{center}
\end{columns}
}

\subsection{Solución propuesta}

\frame{
\frametitle{Análisis propuesto}

Utilizando un detector duro y $\hat{H}$ se obtiene la secuencia de datos transmitida: $\hat{X}_j$.

\begin{block}{Error de estimación en $j$ dado $\hat H$ y $\hat {X}_j$ }
\begin{equation*}
\hat N_j = Y_j-\hat X_j \hat H \qquad 1 < j < P.
\label{eq:despeje}
\end{equation*}
\end{block}

\begin{itemize}
\item $X_j$ símbolos independientes,
\item $H$ constante en el tiempo de bloque,
\end{itemize}

Se propone un ensayo de bondad de ajuste de la distribución multivariable de $\hat N$ a la distribución de $N$.
}

\frame{
\frametitle{Prueba elegida}
Se buscó un ensayo estadístico independiente de la distribución del
error obtenido y que tenga un buen desempeño con poca cantidad de muestras. Se
elige utilizar el ensayo \textbf{Kolmogorov–Smirnov}.
\begin{columns}
\column{.5\textwidth}
\begin{itemize}
\item Es no paramétrico.
\item Cuantifica cuan disímiles son dos funciones de distribución.
\begin{equation*}
\label{eq:ksuni}
 D_P = \max_{\hat n \in \mathbb{R}} | F_P(\hat n)-F_0(\hat n)|
\end{equation*}
\begin{equation*}
 D_P = \max_{0 \leq \nu \leq 1} | G_P(\nu) -\nu |\ \nu = F_0(\hat{n})
\label{eq:indep}
\end{equation*}
\end{itemize}
\column{.5\textwidth}
\begin{center}
 \includegraphics[width=\columnwidth]{img/ks.pdf}
\end{center}
\end{columns}

Se elige un error $\alpha = 5\%$, fijando la cota $D_{max}$ en 0.19.
}

\subsection{KS}

\frame{
\frametitle{KS-test multivariable (I)}
$D_P$ no es independiente de la distribución $F_0$ en el caso multivariable. 
\begin{block}{Transformación de Rosenblatt:}
Transforma un vector aleatorio $\hat N$ con distribución $F$ en otro vector
$\Upsilon = T(\hat N)$ de iguales dimensiones distribuido uniformemente, cuyas
componentes están entre 0 y 1.
\end{block}
\begin{itemize}
\item $\hat{N} = (\hat{N}_1, \dots, \hat{N}_k)$, vector aleatorio con  $F=(\hat{n}_1, \dots, \hat{n}_k)$.
\item $\Upsilon = (\upsilon_1, \dots, \upsilon_k) = T(\hat{N}) = T(\hat{n}_1, \dots, \hat{n}_k)$.
\item $\upsilon_1 = P\{\hat{N}_1 \leqslant \hat{n}_1 \} = F_1(\hat{n}_1)$,
\item $\upsilon_2 = P\{\hat{N}_2 \leqslant \hat{n}_2 | \hat{N}_1 = \hat{n}_1 \} = F_2(\hat{n}_2|\hat{n}_1)$,
\item $\upsilon_k = P\{\hat{N}_k \leqslant \hat{n}_k | \hat{N}_{k-1} = \hat{n}_{k-1}, \dots, \hat{N}_1 = \hat{n}_1 \} = F_k(\hat{n}_k|\hat{n}_{k-1}, \dots, \hat{n}_1)$.
\end{itemize}
}

\frame{
\frametitle{KS-test multivariable (II)}
Se obtiene $Z=TX$ es un vector aleatorio uniforme de k dimensiones:
{\small
\begin{eqnarray*}
 P(\Upsilon_i &\leqslant & \upsilon_i; i = 1,\dots ,k) \\
 &=& \int_{\Upsilon|\upsilon_i \leqslant \upsilon_i} \cdots \int d_{x_k} F_k(\hat{n}_k|\hat{n}_{k-1}, \dots, \hat{n}_1) \cdots d_{\hat{n}_1} F_1(\hat{n}_1)\\
 &=& \int_0^{\upsilon_k} \cdots \int_0^{\upsilon_1} d_{\upsilon_1} \cdots d_{\upsilon_k} = \prod_{i=1}^k \upsilon_i 
\end{eqnarray*}
}
\begin{block}{Utilizando esta transformación, el test se reescribe como:}
\begin{equation*}
D_P = \max_{\Upsilon} \left|G_{P}(\Upsilon)- \prod_{j=1}^{k}
{\Upsilon_{j}} \right|
\end{equation*}
\end{block}
donde $G_{P}$ es la función de distribución empírica de las muestras
transformadas $\Upsilon_j$.
}

\section{Resultados}
\subsection{Método de cuantificación}
\frame{
\frametitle{Resultados (I)}
Para cuantificar el rendimiento del algoritmo se definió:
\begin{equation*}
\varepsilon =
\mathtt{sqrt}\left[||\mathtt{vec}(\hat H - H)||^2/||\mathtt{vec}(\hat H)||^2\right]
\end{equation*}

\begin{enumerate}
 \item Se generaron 2500 simulaciones usando el canal descripto.
 \item Cada canal se identificó usando el algoritmo EM, con $P=50$.
 \item Se detectaron los símbolos transmitidos con un bloque.
 \item Se ejecutó el algoritmo validador con otro bloque de muestras.
 \item Se separaron los resultados según fueron o no aceptados.
 \item Se calcularon histogramas que representan la cantidad de simulaciones
con un determinado error $\varepsilon$.
\end{enumerate}
}

\subsection{Resultados obtenidos}
\frame{
\frametitle{Resultados (II)}
\begin{center}
 \includegraphics[width=\textwidth]{img/histograma.pdf}
\end{center}
}

\frame{
\frametitle{Resultados (III)}

\begin{center}
\includegraphics[height=0.6\textheight]{img/scatter.pdf}
\end{center}
\begin{description}
 \item[Válidas $\varepsilon > 0.1$:] $12.37\%$.
 \item[Inválidas $\varepsilon < 0.04$:] $13.69\%$.
\end{description}
}

\section{Conclusiones}
\frame{
\frametitle{Conclusiones}
\begin{itemize}
\item Se propuso un método de validación de modelos a realizarse en forma ciega.
\item Se propuso utilizar la prueba KS en dicho método.
\item Se probó mediante experimentación numérica, obteniendo resultados satisfactorios.
\end{itemize}
\vspace{1.5cm}
\begin{columns}
\column{.7\textwidth}
\centering
¡Gracias por su atención!
\column{.3\textwidth}
\centering
\includegraphics[height=.2\textheight]{./img/pregunta.png}
 
 ¿Preguntas?
\end{columns}
}

\frame{
\frametitle{Algoritmo EM}
\begin{equation*}
 H_{k+1} = \arg\max_H \left( E_{X/Y} \left[ \log
\dfrac{P(X,Y;H)}{P(X,Y;H_k)};H_k \right] \right)
\end{equation*}

Resolviendo, se obtiene la siguiente expresión:
{\tiny
\begin{equation*}
H_{k+1} = \sum_{j=1}^{P}Y_j \sum_{X \in \mathcal{A}} \left[ \dfrac{X^T \exp
\left( \frac{-\mathcal{E}_{jk}(X)}{2} \right) }{\sum_{X \in \mathcal{A}} \exp
 \left( \frac{-\mathcal{E}_{jk}(X)}{2} \right)} \right]
\times \left\lbrace \sum_{j=1}^{P} \sum_{X \in \mathcal{A}} \left[\dfrac{XX^T
\exp \left( \frac{-\mathcal{E}_{jk}(X)}{2} \right) }{\sum_{X \in \mathcal{A}}
\exp \left( \frac{-\mathcal{E}_{jk}(X)}{2} \right)} \right] \right\rbrace^{-1}
\label{eq:em}
\end{equation*}
}
donde $\mathcal{E}_{jk}(X) = \left((Y_j-H_{k}X)^T \varSigma^{-1}
(Y_j-H_{k}X)\right)$
}

\frame{
\frametitle{Implementación (I)}
Este ensayo requiere calcular la función de distribución empírica de las
muestras, lo cual implica un alto costo computacional. Para reducir la
complejidad se utiliza una simplificación válida para variables
bidimensionales. La CDF se calcula en los ``puntos notables'':
\begin{itemize}
\item Las muestras,
 \item los puntos de intersección:
 $\{(x_j,y_i) | x_i < x_j$ y $y_i > y_j\}$,
\item las proyecciones al borde superior del cuadrado unitario,
\item y las proyecciones al borde derecho del cuadrado unitario.
\end{itemize}
}

\frame{
\frametitle{Implementación (II)}
\begin{enumerate}
\item Se calcula el conjunto de puntos notables y las CDF.
\begin{columns}
 \column{.5\textwidth}
 \begin{center}
  Muestras y ``puntos notables''
 \includegraphics[width=\columnwidth,bb=0 0 336 242]{../presentacion_leo/img/notables.pdf}
 % notables.pdf: 336x242 pixel, 72dpi, 11.85x8.54 cm, bb=0 0 336 242
 \end{center}
\column{.5\textwidth}
  \begin{center}
  CDF empírica vs. teórica
 \includegraphics[width=\columnwidth,bb=0 0 335 243]{../presentacion_leo/img/empvsreal.pdf}
 % empvsreal.pdf: 335x243 pixel, 72dpi, 11.82x8.57 cm, bb=0 0 335 243
\end{center}
\end{columns}

\item Se buscan máxima diferencia entre empírica y real ($D_{max}$).
\item Se decide que la identificación es válida si $D_{max} < K$.
\end{enumerate}
}

\end{document}
