\documentclass{beamer}

\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{graphicx}

\mode<presentation>
{
	\usetheme{Warsaw}
	\setbeamertemplate{navigation symbols}{}
	\setbeamercovered{transparent}
}
\setcounter{tocdepth}{1}

\title[Validación de identificaciones MIMO]{Identificación ciega de canales
MIMO: validación del modelo}
\author[Chiesa, Galarza]
{
Alberto Lucas Chiesa Vaccaro\newline
Cecilia G. Galarza
}

\begin{document}
\frame{\titlepage}

\section{Anteriormente en el LPSC}
\subsection{Primeros pasos}

\frame{
\frametitle{Donde estábamos}
Al momento de la presentación anterior el trabajo de llamaba \textbf{Estimador
ciego para canales MIMO}. El objetivo era:
\begin{columns}
\column{.6\textwidth}
\begin{quote}
... diseñar un estimador ciego para canales MIMO con
una posible aplicación en redes de sensores. Idealmente, debería ser de baja
complejidad computacional y distribuible entre varios nodos de una red.
\end{quote}
\column{.4\textwidth}
\begin{center}
 \includegraphics[width=\textwidth]{img/sensor_network.jpg}
 % sensor_network.gif: 792x612 pixel, 72dpi, 27.94x21.59 cm, bb=0 0 792 612
\end{center}
\end{columns}
}

\frame{
\frametitle{Modelo del canal (I)}
Se tiene un sistema como el de la figura:
\begin{center}
 \includegraphics{img/estimador_mimo.pdf}
\end{center}

Matricialmente, el sistema se representa usando:
\begin{equation*}
X= \left[\begin{matrix}
 x_1 \\
 x_2
\end{matrix}\right]
\ N= \left[\begin{matrix}
 n_1 \\
 n_2
\end{matrix}\right]
\ Y= \left[\begin{matrix}
 y_1 \\
 y_2
\end{matrix}\right]
\ \ H= \left[\begin{matrix}
 h_{11} & h_{12} \\
 h_{21} & h_{22}
\end{matrix}\right]
\end{equation*}

Donde $Y$ resulta de:

\begin{equation*}
 Y=HX+N
\end{equation*}
}

\frame{
\frametitle{Modelo del canal (II)}
\begin{itemize}
\item $N$ es el ruido blanco aditivo guassiano de media nula y varianza
$\sigma^{2}$.

\item Las ganancias $h_{ij}$ son valores reales.

\item La distribución de $X$, que contiene las dos señales transmitidas es:
\begin{equation*}
 P(X)=\dfrac{1}{B^2} \delta(x-x_1) + \ldots +\dfrac{1}{B^2} \delta(x-x_{B^2})
\end{equation*}
Si la constelación posee $B$ símbolos.

\item A cada $y_{i}$ se suma un $n_i$ diferente, por lo tanto $P(N)$ es una
normal multivariable.
\begin{equation*}
 P(N) = \dfrac{1}{2\pi |\varSigma|^{1/2}} \exp \left(-\dfrac{1}{2} N^T
\varSigma^{-1} N\right)
\end{equation*}
\end{itemize}
}

\subsection{Estimadores desarrollados}
\frame{
\frametitle{Estimadores desarrollados}

Buscando usar algoritmos simples, se tomó como base el método
\textbf{E}xpectation \textbf{M}aximization y se probaron tres variantes:
\begin{itemize}
 \item Estimador directo.
 \item Estimador ``DFE''.
 \item Estimador robusto.
\end{itemize}

\begin{columns}
\column{.7\textwidth}
Para después darse cuenta que los dos primeros son el mismo estimador, expresado
de forma diferente y que la estadística robusta no es la solución a nuestros
problemas.
\column{.3\textwidth}
\begin{center}
 \includegraphics[width=\columnwidth]{img/sad_face.jpg}
\end{center}
\end{columns}
}

\frame{
\frametitle{Estimador directo}
El primer estimador que se diseñó se obtiene a partir de aplicar el algoritmo EM
directamente al problema a resolver.

\begin{equation*}
 H_{k+1} = \arg\max_H \left( E_{X/Y} \left[ \log
\dfrac{P(X,Y;H)}{P(X,Y;H_k)};H_k \right] \right)
\end{equation*}

Resolviendo, se obtiene la siguiente expresión:
{\tiny 
\begin{equation*}
H_{k+1} = \sum_{i=1}^{M} \sum_{x \in \mathcal{X}} \left[ \dfrac{Y(i)X^T \exp
\left( \frac{-\mathcal{E}(i)_k}{2} \right) }{\sum_{x \in \mathcal{X}} \exp
\left( \frac{-\mathcal{E}(i)_k}{2} \right)} \right] \cdot \left\lbrace
\sum_{i=1}^{M} \sum_{x \in \mathcal{X}} \left[\dfrac{XX^T \exp \left(
\frac{-\mathcal{E}(i)_k}{2} \right) }{\sum_{x \in \mathcal{X}} \exp \left(
\frac{-\mathcal{E}(i)_k}{2} \right)} \right] \right\rbrace^{-1}
\end{equation*}
}
Donde
{\tiny
$\mathcal{E} = \left((Y-HX)^T \varSigma^{-1} (Y-HX)\right)$ y $\mathcal{E}_{k} =
\left((Y-H_{k}X)^T \varSigma^{-1} (Y-H_{k}X)\right)$.
}
}

\frame{
\frametitle{Estimador DFE (I)}
\begin{columns}
\column{0.5\textwidth}
Siguiendo el consejo de algunos papers, se implementó un estimador ``partido'':
\column{0.5\textwidth}
\begin{center}
 \includegraphics[width=\textwidth]{img/loop.pdf}
\end{center}
\end{columns}

\begin{eqnarray}
H_{k+1} &=& \arg\max_{H} E_{X/Y}\left[\sum_{i=0}^N \lVert Y(i)-HX(i) \rVert^{2}
\right] \\
\hat{X}_{k}(i) &=& E_{X/Y}[X(i);H_{k-1}] \\
\tilde{X}_{k}(i) &=& E_{X/Y}[X(i)X(i)^{T};H_{k-1}]
\end{eqnarray}
}

\frame{
\frametitle{Estimador DFE (II)}
Resolviendo las esperanzas para $\tilde{X}_{k}(i)$ y $\hat{X}_{k}(i)$ se
obtiene:
\begin{eqnarray}
\hat{X}(i) &=& E_{X/Y(i)} [X;H] = \sum_{x \in \mathcal{X}} x P(X/Y(i); H) \\
\tilde{X}(i) &=& E_{X/Y(i)} [X^{T}X;H] = \sum_{x \in \mathcal{X}} x^{T}x
P(X/Y(i); H)
\end{eqnarray}
y el $H_{k+1}$ que maximiza la esperanza de la filmina anterior,
\begin{equation*}
h_{k+1}^{(a)} = \left[ \sum_{i=0}^N \tilde{X}(i) \right]^{-1} \sum_{i=0}^N
\hat{X}(i)y_{1}(i)
\end{equation*}
\begin{equation*}
h_{k+1}^{(b)} = \left[ \sum_{i=0}^N \tilde{X}(i) \right]^{-1} \sum_{i=0}^N
\hat{X}(i)y_{2}(i)
\end{equation*}
}

\frame{
\frametitle{Estimador robusto}
Basados en el razonamiento que el problema del estimador es que hay muchos
datos discordantes, se lo modifica adoptando nociones de estadística robusta.

\begin{columns}
\column{0.4\textwidth}
Se reemplaza el detector $\hat{X}(i)$ por uno basado en un
\textit{M-estimator}, partiendo de la idea de evitar que los símbolos mal
detectados tengan mucha influencia en el estimador.
\column{0.6\textwidth}
\begin{center}
 \includegraphics[width=\columnwidth]{img/ejemplos-rho.pdf}
\end{center}
\end{columns}
}

\frame{
\frametitle{Triste realidad...}
No se logró un estimador con buen rendimiento. El desempeño es fuertemente
dependiente del valor inicial usado, en todos los casos probados.
\begin{center}
 \includegraphics[width=0.7\textwidth]{img/directo_shake.pdf}
\end{center}
... pero de este mismo problema surge una idea interesante.
}
\section{Trabajo actual}
\subsection{Nuevo objetivo}
\frame{
\frametitle{¿Podemos saber si la estimación fue buena?}
De poder detectar una estimación incorrecta se podría volver a estimar con otras
condiciones iniciales.
\begin{block}{Es evidente que:}
\begin{columns}
\column{.4\textwidth}
Si la ecuación del canal es:
\begin{equation*}
 Y=HX+N
\end{equation*}
\column{.5\textwidth}
Con una estimación $\hat{H}$ correcta:
\begin{equation*}
 Y-\hat{H}\hat{X} = N
\end{equation*}
\end{columns}
\end{block}

\begin{itemize}
\item No se conoce la realización del ruido!
\item Se debe hace un ensayo estadístico para ver si $Y-\hat{H}\hat{X} = Y-HX$
\end{itemize}
}

\frame{
\frametitle{Primer intento: Igual varianza}
El primer método de validación se basó en el análisis de la varianza del resto
$Y-\hat{H}\hat{X}$, comparándolo contra la del ruido (asumida conocida).

\begin{enumerate}
 \item Se toma la varianza de la traza de $\varSigma$ como variable aleatoria: $
\textrm{Var}\left[tr(\varSigma)\right] =
\dfrac{2}{4}tr\left(\varSigma^{2}\right)$
 \item Se evalua: $\lvert tr(\hat{\varSigma}) - tr(\varSigma) \rvert < \alpha
\textrm{Var}\left[tr(\varSigma)\right]$
\end{enumerate}

Este método no funciona, principalmente debido a la poca cantidad de muestras
para calcular $\hat{\varSigma}$.
}

\frame{
\frametitle{Segundo intento: Igual distribución}
Se busca un ensayo estadístico más poderoso, que no asuma la distribución del
resto obtenido y que tenga un buen desempeño con poca cantidad de muestras. Se
elige utilizar el ensayo \textbf{Kolmogorov–Smirnov}.

\begin{columns}
 \column{.5\textwidth}
 \begin{itemize}
 \item Es no paramétrico (no asume una distribución de las muestras).
\item Cuantifica cuan disímiles son dos funciones de distribución.
\item Originalmente descripto para variables unidimensionales.
\end{itemize}
\column{.5\textwidth}
\begin{center}
 \includegraphics[width=\columnwidth]{./img/ks.jpg}
\end{center}
\end{columns}
}
\subsection{Kolmogorov–Smirnov}
\frame{
\frametitle{KS-test multivariable (I)}
Para aprovechar la información disponible en un sistema MIMO se usa una
variante de la prueba KS para variables multidimensionales.

Formulación tradicional:
\begin{equation}
\label{eq:ksuni}
 D_P = \sup_{\hat n \in \mathbb{R}} | F_P(\hat n)-F_0(\hat n)|,
\end{equation} 

Si transformamos $\hat{n}$ mediante $\nu=F(\hat{n})$, obtenemos una variable
uniforme, lo que nos permite reescribir \ref{eq:ksuni}:
\begin{equation}
 D_P = \sup_{0 \leq \nu \leq 1} | G_P(\nu) -\nu |.
\label{eq:indep}
\end{equation}
Esta última ecuación permite evaluar cuan uniforme es el resultado de aplicar
la CDF supuesta a las muestras bajo estudio.
}

\frame{
\frametitle{KS-test multivariable (II)}
¿Y la parte de multivariable?
\begin{block}{Transformación de Rosenblatt:}
Transforma un vector aleatorio $\hat N$ con distribución $F$ en otro vector
$\Upsilon = T(\hat N)$ de iguales dimensiones distribuido uniformemente, cuyas
componentes están entre 0 y 1.
\end{block}
Utilizando esta transformación, (\ref{eq:indep}) se reescribe como:
\begin{equation}
D_P = \sup_{\Upsilon} \left|G_{P}(\Upsilon)- \prod_{j=1}^{P}
{\Upsilon_{j}} \right|
\end{equation}
donde $G_{P}$ es la función de distribución empírica de las muestras
transformadas $\Upsilon_j$.
}
\subsection{Implementación del método}
\frame{
\frametitle{Implementación (I)}
Este ensayo requiere calcular la función de distribución empírica de las
muestras, lo cual implica un alto costo computacional. Para reducir la
complejidad se utiliza una simplificación válida para variables
bidimensionales. La CDF se calcula en los ``puntos notables'':
\begin{itemize}
\item Las muestras,
 \item los puntos de intersección:
 $\{(x_j,y_i) | x_i < x_j$ y $y_i > y_j\}$,
\item las proyecciones al borde superior del cuadrado unitario,
\item y las proyecciones al borde derecho del cuadrado unitario.
\end{itemize}

}

\frame{
\frametitle{Implementación (II)}
\begin{enumerate}
\item Se calcula el conjunto de puntos notables y las CDF.
\begin{columns}
 \column{.5\textwidth}
 \begin{center}
  Muestras y ``puntos notables''
 \includegraphics[width=\columnwidth,bb=0 0 336 242]{./img/notables.pdf}
 % notables.pdf: 336x242 pixel, 72dpi, 11.85x8.54 cm, bb=0 0 336 242
 \end{center}
\column{.5\textwidth}
  \begin{center}
  CDF empírica vs. teórica
 \includegraphics[width=\columnwidth,bb=0 0 335 243]{./img/empvsreal.pdf}
 % empvsreal.pdf: 335x243 pixel, 72dpi, 11.82x8.57 cm, bb=0 0 335 243
\end{center}
\end{columns}

\item Se buscan máxima diferencia entre empírica y real ($D_{max}$).
\item Se decide que la identificación es válida si $D_{max} < K$.
\end{enumerate}
 Fijamos $K = 0.20$. Rechaza 18\% de casos correctos.
}

\section{Resultados}
\subsection{Método de cuantificación}
\frame{
\frametitle{Resultados (I)}
Para cuantificar el rendimiento del algoritmo se definió:
\begin{equation*}
\varepsilon =
\mathtt{sqrt}\left[(||\mathtt{vec}(\hat H) -
\mathtt{vec}(H)||^2)/||\mathtt{vec}(\hat H)||^2\right]
\end{equation*}
y se
\begin{enumerate}
 \item generaron 2000 simulaciones usando el canal descripto,
 \item cada canal se identificó usando el algoritmo EM.
 \item Se detectaron los símbolos transmitidos y
 \item se ejecutó el algoritmo validador.
 \item Se separaron los resultados según fueron o no aceptados,
 \item y se calcularon histogramas que representan la cantidad de simulaciones
con un determinado error $\varepsilon$.
\end{enumerate}
}

\subsection{Resultados obtenidos}
\frame{
\frametitle{Resultados (II)}
\begin{center}
 \includegraphics[width=.9\textwidth]{./img/grafico_errores.pdf}
\end{center}
}

\frame{
\frametitle{Resultados (III)}

\begin{columns}
 \column{.7\textwidth}
\begin{center}
 \includegraphics[width=\columnwidth]{./img/grafico_ks_fig_error_cero.pdf}
\end{center}
 \column{.4\textwidth}
Y en números...

Del total de las simulaciones con
\begin{itemize}
 \item $\varepsilon > 0.1$, sólo $15.4\%$ es aceptado, y
 \item $\varepsilon < 0.04$, sólo el $16.9\%$ es rechazado.
\end{itemize}
\end{columns}
}

\section{Conclusiones}

\frame{
\frametitle{Análisis de los resultados}

\begin{columns}
\column{.7\textwidth}
\begin{itemize}
 \item El algoritmo es capaz de rechazar el $84.6\%$ de las identificaciones
incorrectas.
\item En trabajos futuros se tratará de elevar aún más el rendimiento del
algoritmo desarrollado.
\begin{itemize}
 \item Por ejemplo: \textit{two-stage $\delta$-corrected Kolmogorov-Smirnov
test}
\end{itemize}
\end{itemize}
\column{0.3\textwidth}
\begin{center}
 \includegraphics[width=\columnwidth]{./img/happy-face.jpg}
\end{center}
\end{columns}

}

\frame{
\frametitle{Estado del trabajo}
\begin{columns}
\column{0.3\textwidth}
\begin{center}
 \includegraphics[width=\columnwidth]{./img/pen.jpg}
\end{center}
\column{.7\textwidth}
\begin{itemize}
 \item El desarrollo del algoritmo está terminado.
\item Se han realizado numerosas simulaciones, en diversas condiciones para
probar el funcionamiento del mismo.
\item Se quiere enmarcar el trabajo dentro de la teoría de
\textbf{Identificación de Sistemas}.
\item Falta escribir el texto de la tesis.
\end{itemize}
\end{columns}
}

\end{document}
