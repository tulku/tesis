\documentclass[compress]{beamer}

\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{subfigure}

\mode<presentation>
{
	\usetheme{Warsaw}
	\setbeamertemplate{navigation symbols}{}
	\setbeamercovered{transparent}
	\useinnertheme{rectangles}
}
\setcounter{tocdepth}{1}

\title[Validación de identificaciones MIMO]{Identificación ciega de canales
MIMO}
\subtitle{Validación del modelo}
\author[A.L. Chiesa, C.G. Galarza]
{
Alberto L. Chiesa \and Cecilia G. Galarza
}
\institute[LPSC]{LPSC, Departamento de Electrónica \\
Facultad de Ingeniería\\
Universidad de Buenos Aires}
%\date[STACS 2003]{STACS Conference, 2003}
%\usepackage{default}

\begin{document}
\frame{\titlepage}

\section{Introducción}
\subsection{MIMO-OFDM}
\frame{
\frametitle{Marco de trabajo}
\begin{columns}
\column{0.3\textwidth}
\begin{center}
 \includegraphics[width=\columnwidth]{./img/pen.JPG}
\end{center}
\column{0.7\textwidth}
\begin{itemize}
\item Identificación de canal MIMO-OFDM.
\item Realizar la identificación en forma ciega.
\item Empleo de algoritmos de estimación iterativos.
\item Analizar el comportamiento frente a distintas condiciones iniciales.
\item Determinar si el modelo estimado ``explica'' los datos experimentales.
\item Poder descartar modelos inválidos.
\end{itemize}
\end{columns}
}

\frame{
\begin{itemize}
\item MIMO es una técnica que usa múltiples antenas tanto en transmisor como en receptor.
\item OFDM divide el ancho de banda disponible (BW) en N subcanales angostos de ancho BW/N.
\item Cada subcanal se modela como un factor de ganancia constante, que copia
la ganancia real del canal en ese intervalo.
\item En un sistema de 2x2 antenas, un subcanal presenta 4 ganancias.
\end{itemize}
\begin{center}
 \includegraphics[width=\textwidth]{../presentacion_leo/img/ofdm.pdf}
 \end{center}
}
\frame{
\frametitle{Modelo del canal (I)}
\begin{center}
 \includegraphics[width=.7\textwidth]{../presentacion_leo/img/estimador_mimo2.pdf}
\end{center}

Matricialmente, el sistema se representa usando:
\begin{equation*}
X= \left[\begin{matrix}
 x_1 \\
 x_2
\end{matrix}\right]
\ N= \left[\begin{matrix}
 n_1 \\
 n_2
\end{matrix}\right]
\ Y= \left[\begin{matrix}
 y_1 \\
 y_2
\end{matrix}\right]
\ \ H= \left[\begin{matrix}
 h_{11} & h_{12} \\
 h_{21} & h_{22}
\end{matrix}\right]
\end{equation*}
Para el instante $j$,
\begin{equation*}
 Y_j=H_jX_j+N_j \qquad 1<j<P
\end{equation*}
}

\frame{
\frametitle{Modelo del canal (II)}
\begin{itemize}
\item $N$ es el ruido blanco aditivo gaussiano de media nula y matriz de
covarianza diagonal $\varSigma$. Su distribución es:
\begin{equation*}
 P(N) = \dfrac{1}{2\pi |\varSigma|^{1/2}} \exp \left(-\dfrac{1}{2} N^T
\varSigma^{-1} N\right)
\end{equation*}
\item Las ganancias $h_{ij}$ son valores reales.
\item La distribución de $X$, que contiene las dos señales transmitidas es:
\begin{equation*}
 P(X)=\dfrac{1}{B^2} \delta(x-x_1) + \ldots +\dfrac{1}{B^2} \delta(x-x_{B^2})
\end{equation*}
si la constelación posee $B$ símbolos que forman el conjunto $\mathcal{A}$.
\end{itemize}
}

\subsection{Identificación}

\frame{
\frametitle{Identificación de máxima verosimilitud}
La función de verosimilitud depende de la señalización de X.
\begin{equation*}
 \mathcal{L}(H;X,Y) = \chi \prod_{j=1}^P \exp \left(-\dfrac{1}{2} (Y_j-HX_j)^T
\varSigma^{-1} (Y_j-HX_j)\right)
\end{equation*}

Ejemplos de funciones de verosimilitud, para el caso de un canal MISO:

\begin{figure}[h]
 \centering
 \subfigure[Constelación binaria] {
 \includegraphics[width=.45\textwidth]{../rpic09/vero_uni.pdf}
 }
 \subfigure[Constelación de 4 elementos] {
 \includegraphics[width=.45\textwidth]{../rpic09/verosimilitud.pdf}
 }
\end{figure}
}

\frame{
\frametitle{Identificación ciega}
Se propone utilizar estimadores basados en el algoritmo \textbf{E}xpectation \textbf{M}aximization: no requiere sobremuestrar $Y$; simple de implementar; relativo bajo costo computacional.

Dado $H_0$:
\begin{equation*}
 H_{k+1} = \arg\max_{H} \sum_{j=0}^P E_{X/Y_j}\left[\lVert Y_j-HX \rVert^{2};
H_k \right] \qquad k =1,2,\dots
\label{eq:h_ls}
\end{equation*}

Trabajando con la anterior, se obtiene que:
\begin{equation*}
H_{k+1} = \left[ \sum_{j=0}^P {\mathbf {R}}_j \right]^{-1} \sum_{j=0}^P
\hat{\mathbf{X}}_jY_j
\label{eq:hcuadrados}
\end{equation*}
donde $j$ es el número de muestra, $k$ la iteración, y
\begin{eqnarray*}
 \hat{\mathbf{X}}_j = E_{X/Y_j}[X;H_{k}], \qquad
{\mathbf{R}}_j = E_{X/Y_j}[XX^{T};H_{k}].
\end{eqnarray*}
}

\section{Validación}
\frame{
\frametitle{Muchos resultados posibles}
\begin{columns}
\column{.4\textwidth}
\begin{itemize}
\item La función de verosimilitud tiene numerosos máximos locales.
\item EM garantiza convergencia, pero a cualquier máximo.
\item Fuertemente dependiente de las condiciones iniciales.
\end{itemize}
\column{.6\textwidth}
\begin{center}
 \includegraphics[width=\columnwidth]{../rpic09/2do_resultado.pdf}
\end{center}
\end{columns}
\vspace*{1cm}
¿Podríamos calificar la bondad del modelo obtenido?
}

\subsection{Solución propuesta}

\frame{
\frametitle{Análisis propuesto}

Utilizando un detector duro y $\hat{H}$ se obtiene la secuencia de datos transmitida: $\hat{X}_j$.

\begin{block}{Error de estimación en $j$ dado $\hat H$ y $\hat {X}_j$ }
\begin{equation*}
\hat N_j = Y_j-\hat X_j \hat H \qquad 1 < j < P.
\label{eq:despeje}
\end{equation*}
\end{block}

\begin{itemize}
\item $X_j$ símbolos independientes,
\item $H$ constante en el tiempo de bloque,
\end{itemize}

Se propone un ensayo de bondad de ajuste de la distribución multivariable de $\hat N$ a la distribución de $N$.
}

\frame{
\frametitle{Prueba elegida}
Se buscó un ensayo estadístico independiente de la distribución del
error obtenido y que tenga un buen desempeño con poca cantidad de muestras. Se
elige utilizar el ensayo \textbf{Kolmogorov–Smirnov}.

\begin{columns}
\column{.5\textwidth}
\begin{itemize}
\item Es no paramétrico (no asume una distribución de las muestras).
\item Cuantifica cuan disímiles son dos funciones de distribución.
\item Originalmente descripto para variables unidimensionales.
\end{itemize}
\column{.5\textwidth}
\begin{center}
 \includegraphics[width=\columnwidth]{img/ks.pdf}
\end{center}
\end{columns}

Se elige un error $\alpha = 5\%$, fijando la cota $D_{max}$ en 0.19.
}

\subsection{Kolmogorov-Smirnov}
\frame{
\frametitle{KS-test multivariable (I)}
Para aprovechar la información disponible en un sistema MIMO se usa una
variante de la prueba KS para variables multidimensionales.
%\vspace{1cm}

\begin{block}{Formulación tradicional}
\begin{equation*}
\label{eq:ksuni}
 D_P = \max_{\hat n \in \mathbb{R}} | F_P(\hat n)-F_0(\hat n)|
\end{equation*} 
\end{block}

Si transformamos $\hat{n}$ mediante $\nu=F(\hat{n})$, obtenemos una variable
uniforme, lo que nos permite escribir:
\begin{equation*}
 D_P = \max_{0 \leq \nu \leq 1} | G_P(\nu) -\nu |.
\label{eq:indep}
\end{equation*}
Esta última ecuación permite evaluar cuán uniforme es el resultado de aplicar
la CDF supuesta a las muestras bajo estudio.
}

\frame{
\frametitle{KS-test multivariable (II)}
¿Y la parte de multivariable? Se necesita lograr que sea independiente de la distribución...
\begin{block}{Transformación de Rosenblatt:}
Transforma un vector aleatorio $\hat N$ con distribución $F$ en otro vector
$\Upsilon = T(\hat N)$ de iguales dimensiones distribuido uniformemente, cuyas
componentes están entre 0 y 1.
\end{block}
Utilizando esta transformación, el test se reescribe como:
\begin{equation*}
D_P = \max_{\Upsilon} \left|G_{P}(\Upsilon)- \prod_{j=1}^{P}
{\Upsilon_{j}} \right|
\end{equation*}
donde $G_{P}$ es la función de distribución empírica de las muestras
transformadas $\Upsilon_j$.
}

\section{Resultados}
\subsection{Método de cuantificación}
\frame{
\frametitle{Resultados (I)}
Para cuantificar el rendimiento del algoritmo se definió:
\begin{equation*}
\varepsilon =
\mathtt{sqrt}\left[||\mathtt{vec}(\hat H - H)||^2/||\mathtt{vec}(\hat H)||^2\right]
\end{equation*}

\begin{enumerate}
 \item Se generaron 2500 simulaciones usando el canal descripto.
 \item Cada canal se identificó usando el algoritmo EM.
 \item Se detectaron los símbolos transmitidos con un bloque.
 \item Se ejecutó el algoritmo validador con otro bloque de muestras.
 \item Se separaron los resultados según fueron o no aceptados.
 \item Se calcularon histogramas que representan la cantidad de simulaciones
con un determinado error $\varepsilon$.
\end{enumerate}
}

\subsection{Resultados obtenidos}
\frame{
\frametitle{Resultados (II)}
\begin{center}
 \includegraphics[width=\textwidth]{img/histograma.pdf}
\end{center}
}

\frame{
\frametitle{Resultados (III)}

\begin{columns}
 \column{.7\textwidth}
\begin{center}
 \includegraphics[width=\columnwidth]{img/scatter.pdf}
\end{center}
 \column{.4\textwidth}
\begin{itemize}
 \item $\varepsilon > 0.1$, el $12.37\%$ es aceptado.
 \item $\varepsilon < 0.04$, el $13.69\%$ es rechazado.
\end{itemize}
\end{columns}
}

\section{Conclusiones}
\frame{
\frametitle{Conclusiones}
\begin{itemize}
\item Se propuso un método de validación de modelos a realizarse en forma ciega.
\item Se propuso utilizar la prueba KS en dicho método.
\item Se probó mediante experimentación numérica, obteniendo resultados satisfactorios.
\end{itemize}
Trabajos futuros incluyen:
\begin{itemize}
\item Análisis de la relación entre el error de modelado y el error de estimación.
\item Utilizar este esquema para reiniciar el algoritmo estimador.
\item Aplicar este método a sistemas de comunicación reales.
\end{itemize}

\begin{center}
 \includegraphics[width=.8\textwidth]{./img/to_be_cont.png}
\end{center}
}

\frame{
\frametitle{Gracias por su atención}
%\vspace{0.2cm}
\begin{center}
 \includegraphics[height=.2\textheight]{./img/pregunta.png}

¿Preguntas?
\end{center}
}

\frame{
\frametitle{Algoritmo EM}
\begin{equation*}
 H_{k+1} = \arg\max_H \left( E_{X/Y} \left[ \log
\dfrac{P(X,Y;H)}{P(X,Y;H_k)};H_k \right] \right)
\end{equation*}

Resolviendo, se obtiene la siguiente expresión:
{\tiny
\begin{equation*}
H_{k+1} = \sum_{j=1}^{P}Y_j \sum_{X \in \mathcal{A}} \left[ \dfrac{X^T \exp
\left( \frac{-\mathcal{E}_{jk}(X)}{2} \right) }{\sum_{X \in \mathcal{A}} \exp
\left( \frac{-\mathcal{E}_{jk}(X)}{2} \right)} \right]
\times \left\lbrace \sum_{j=1}^{P} \sum_{X \in \mathcal{A}} \left[\dfrac{XX^T
\exp \left( \frac{-\mathcal{E}_{jk}(X)}{2} \right) }{\sum_{X \in \mathcal{A}}
\exp \left( \frac{-\mathcal{E}_{jk}(X)}{2} \right)} \right] \right\rbrace^{-1}
\label{eq:em}
\end{equation*}
}
donde $\mathcal{E}_{jk}(X) = \left((Y_j-H_{k}X)^T \varSigma^{-1}
(Y_j-H_{k}X)\right)$
}

\frame{
\frametitle{Transformación de Rosenblatt}
\begin{itemize}
\item $X = (X_1, \dots, X_k)$, vector aleatorio con  $F=(x_1, \dots, x_k)$.
\item $z = (z_1, \dots, z_k) = Tx = T(x_1, \dots, x_k)$, donde $T()$ es la transformación considerada.
\item $z_1 = P\{X_1 \leqslant x_1 \} = F_1(x_1)$,
\item $z_2 = P\{X_2 \leqslant x_2 | X_1 = x_1 \} = F_2(x_2|x_1)$,
\item $z_k = P\{X_k \leqslant x_k | X_{k-1} = x_{k-1}, \dots, X_1 = x_1 \} = F_k(x_k|x_{k-1}, \dots, x_1)$.
\end{itemize}
$Z=TX$ es un vector aleatorio uniforme en un hipercubo de k dimensiones:
{\small
\begin{eqnarray*}
 P(Z_i &\leqslant & z_i; i = 1,\dots ,k) \\
 &=& \int_{Z|Z_i \leqslant z_i} \cdots \int d_{x_k} F_k(x_k|x_{k-1}, \dots, x_1) \cdots d_{x_1} F_1(x_1)\\
 &=& \int_0^{z_k} \cdots \int_0^{z_1} d_{z_1} \cdots d_{z_k} = \prod_{i=1}^k z_i 
\end{eqnarray*}
}
}

\frame{
\frametitle{Implementación (I)}
Este ensayo requiere calcular la función de distribución empírica de las
muestras, lo cual implica un alto costo computacional. Para reducir la
complejidad se utiliza una simplificación válida para variables
bidimensionales. La CDF se calcula en los ``puntos notables'':
\begin{itemize}
\item Las muestras,
 \item los puntos de intersección:
 $\{(x_j,y_i) | x_i < x_j$ y $y_i > y_j\}$,
\item las proyecciones al borde superior del cuadrado unitario,
\item y las proyecciones al borde derecho del cuadrado unitario.
\end{itemize}

}

\frame{
\frametitle{Implementación (II)}
\begin{enumerate}
\item Se calcula el conjunto de puntos notables y las CDF.
\begin{columns}
 \column{.5\textwidth}
 \begin{center}
  Muestras y ``puntos notables''
 \includegraphics[width=\columnwidth,bb=0 0 336 242]{../presentacion_leo/img/notables.pdf}
 % notables.pdf: 336x242 pixel, 72dpi, 11.85x8.54 cm, bb=0 0 336 242
 \end{center}
\column{.5\textwidth}
  \begin{center}
  CDF empírica vs. teórica
 \includegraphics[width=\columnwidth,bb=0 0 335 243]{../presentacion_leo/img/empvsreal.pdf}
 % empvsreal.pdf: 335x243 pixel, 72dpi, 11.82x8.57 cm, bb=0 0 335 243
\end{center}
\end{columns}

\item Se buscan máxima diferencia entre empírica y real ($D_{max}$).
\item Se decide que la identificación es válida si $D_{max} < K$.
\end{enumerate}
}

\end{document}
