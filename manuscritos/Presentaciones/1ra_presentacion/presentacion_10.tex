\documentclass{beamer}

\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{graphicx}

\mode<presentation>
{
	\usetheme{Warsaw}
	\setbeamertemplate{navigation symbols}{}
	\setbeamercovered{transparent}
}
\setcounter{tocdepth}{1}
\title[Estimador ciego para canales MIMO]{Desarrollo de un estimador ciego \\ para canales MIMO}
\author[Chiesa, Galarza]
{
Alberto Lucas Chiesa Vaccaro\newline
Cecilia G. Galarza
}

\begin{document}
\frame{\titlepage}

\section{Introducción}
\subsection{Objetivos}

\frame{
\frametitle{Objetivos}
El objetivo de este trabajo es diseñar un estimador para canales MIMO con una posible aplicación en redes de sensores. Dicho estimador debe cumplir con las siguentes características:
\begin{columns}
\column{.5\textwidth}
\begin{itemize}
\item Ciego
\item Baja complejidad computacional
\item Eventualmente distribuible entre varios nodos de una red.
\end{itemize}
\column{.5\textwidth}
\begin{center}
 \includegraphics[width=\textwidth]{img/sensor_network.jpg}
 % sensor_network.gif: 792x612 pixel, 72dpi, 27.94x21.59 cm, bb=0 0 792 612
\end{center}
\end{columns}
}

\subsection{Modelo del canal}
\frame{
\frametitle{Modelo del canal (I)}
Se tiene un sistema como el de la figura:
\begin{center}
 \includegraphics{img/estimador_mimo.pdf}
\end{center}

Matricialmente, el sistema se representa usando:
\begin{equation*}
X= \left[\begin{matrix}
 x_1 \\
 x_2
\end{matrix}\right]
\ N= \left[\begin{matrix}
 n_1 \\
 n_2
\end{matrix}\right]
\ Y= \left[\begin{matrix}
 y_1 \\
 y_2
\end{matrix}\right]
\ \ H= \left[\begin{matrix}
 h_{11} & h_{12} \\
 h_{21} & h_{22}
\end{matrix}\right]
\end{equation*}

Donde $Y$ resulta de:

\begin{equation*}
 Y=HX+N
\end{equation*}
}

\frame{
\frametitle{Modelo del canal (II)}
\begin{itemize}
\item $N$ es el ruido blanco aditivo guassiano de media nula y varianza $\sigma^{2}$.

\item Las ganancias $h_{ij}$ son valores reales.

\item La distribución de $X$, que contiene las dos señales transmitidas es:
\begin{equation*}
 P(X)=\dfrac{1}{B^2} \delta(x-x_1) + \ldots +\dfrac{1}{B^2} \delta(x-x_{B^2})
\end{equation*}
Si la constelación posee $B$ símbolos.

\item A cada $y_{i}$ se suma un $n_i$ diferente, por lo tanto $P(N)$ es una normal multivariable.
\begin{equation*}
 P(N) = \dfrac{1}{2\pi |\varSigma|^{1/2}} \exp \left(-\dfrac{1}{2} N^T
\varSigma^{-1} N\right)
\end{equation*}
\end{itemize}
}

\section{Desarrollo experimental}
\subsection{Actividades}

\frame{
\frametitle{Actividades específicas}
El objetivo de este trabajo se puede separar en varios actividades específicas,
\begin{columns}
\column{.6\textwidth}
\begin{itemize}
\item Diseño del estimador.
\item Diseño de un método de validación de la estimación.
\item Evaluación de la performance de los estimadores.
\item Evaluar la complejidad del estimador.
\item Proponer una forma de distribuir el algoritmo entre varios nodos.
\end{itemize}
\column{.4\textwidth}
\begin{flushright}
 \includegraphics[width=\textwidth]{img/pencils.jpg}
\end{flushright}
\end{columns}
}

\subsection{Estimadores propuestos}

\frame{
\frametitle{Estimador directo}
El primer estimador que se diseñó se obtiene a partir de aplicar el algoritmo EM directamente al problema a resolver.

\begin{equation*}
 H_{k+1} = \arg\max_H \left( E_{X/Y} \left[ \log
\dfrac{P(X,Y;H)}{P(X,Y;H_k)};H_k \right] \right)
\end{equation*}

Resolviendo, se obtiene la siguiente expresión:
{\tiny 
\begin{equation*}
H_{k+1} = \sum_{i=1}^{M} \sum_{x \in \mathcal{X}} \left[ \dfrac{Y(i)X^T \exp
\left( \frac{-\mathcal{E}(i)_k}{2} \right) }{\sum_{x \in \mathcal{X}} \exp
\left( \frac{-\mathcal{E}(i)_k}{2} \right)} \right] \cdot \left\lbrace
\sum_{i=1}^{M} \sum_{x \in \mathcal{X}} \left[\dfrac{XX^T \exp \left(
\frac{-\mathcal{E}(i)_k}{2} \right) }{\sum_{x \in \mathcal{X}} \exp \left(
\frac{-\mathcal{E}(i)_k}{2} \right)} \right] \right\rbrace^{-1}
\end{equation*}
}
Donde
{\tiny
$\mathcal{E} = \left((Y-HX)^T \varSigma^{-1} (Y-HX)\right)$ y $\mathcal{E}_{k} = \left((Y-H_{k}X)^T \varSigma^{-1} (Y-H_{k}X)\right)$.
}
}

\frame{
\frametitle{Estimador DFE (I)}
\begin{columns}
\column{0.5\textwidth}
Siguiendo el consejo de algunos papers, se implementó un estimador ``partido'':
\column{0.5\textwidth}
\begin{center}
 \includegraphics[width=\textwidth]{img/loop.pdf}
\end{center}
\end{columns}

\begin{eqnarray}
H_{k+1} &=& \arg\max_{H} E_{X/Y}\left[\sum_{i=0}^N \lVert Y(i)-HX(i) \rVert^{2} \right] \\
\hat{X}_{k}(i) &=& E_{X/Y}[X(i);H_{k-1}] \\
\tilde{X}_{k}(i) &=& E_{X/Y}[X(i)X(i)^{T};H_{k-1}]
\end{eqnarray}
}

\frame{
\frametitle{Estimador DFE (II)}
Resolviendo las esperanzas para $\tilde{X}_{k}(i)$ y $\hat{X}_{k}(i)$ se obtiene:
\begin{eqnarray}
\hat{X}(i) &=& E_{X/Y(i)} [X;H] = \sum_{x \in \mathcal{X}} x P(X/Y(i); H) \\
\tilde{X}(i) &=& E_{X/Y(i)} [X^{T}X;H] = \sum_{x \in \mathcal{X}} x^{T}x P(X/Y(i); H)
\end{eqnarray}
y el $H_{k+1}$ que maximiza la esperanza de la filmina anterior,
\begin{equation*}
h_{k+1}^{(a)} = \left[ \sum_{i=0}^N \tilde{X}(i) \right]^{-1} \sum_{i=0}^N \hat{X}(i)y_{1}(i)
\end{equation*}
\begin{equation*}
h_{k+1}^{(b)} = \left[ \sum_{i=0}^N \tilde{X}(i) \right]^{-1} \sum_{i=0}^N \hat{X}(i)y_{2}(i)
\end{equation*}
}

\subsection{Validador de estimación}
\frame{
\frametitle{Validador de estimación}
\begin{block}{Es evidente que:}
\begin{columns}
\column{.4\textwidth}
Si la ecuación del canal es:
\begin{equation*}
 Y=HX+N
\end{equation*}
\column{.5\textwidth}
Con una estimación $\hat{H}$ correcta:
\begin{equation*}
 Y-\hat{H}\hat{X} = N
\end{equation*}
\end{columns}
\end{block}

\begin{itemize}
\item No se conoce la realización del ruido!
\item Se hace un ensayo estadístico para ver si $Y-\hat{H}\hat{X} = Y-HX$
\item Como $\varSigma$ se conoce (covarianza del ruido), se toma la varianza de su traza como variable aleatoria:
\begin{equation*}
 \textrm{Var}\left[tr(\varSigma)\right] = \dfrac{2}{4}tr\left(\varSigma^{2}\right)
\end{equation*}
\item Finalmente se evalua: $\lvert tr(\hat{\varSigma}) - tr(\varSigma) \rvert < \alpha \textrm{Var}\left[tr(\varSigma)\right]$
\end{itemize}
}

\subsection{Evaluación de performance}
\frame{
\frametitle{¿Y anda esto?}
Para probar los estimadores se programaron usando Octave con las siguientes condiciones:
\begin{itemize}
\item Canal real, con 20 dB de SNR y 50 muestras.
\item Sistema de 2x2 (dos transmisores y dos receptores).
\item Se asume una perfecta comunicación entre receptores (dos antenas en el mismo nodo).
\item Se usaron dos constelaciones distintas:
\begin{columns}
\column{0.6\textwidth}
\begin{eqnarray*}
 x_i &\in& \{-1,\ \ 1\} \\
 x_i &\in& \{-3,\ \ -1,\ \ 1,\ \ 3\}
\end{eqnarray*}
\column{0.4\textwidth}
\begin{center}
 \includegraphics[width=\textwidth]{img/sombrero.pdf}
\end{center}
\end{columns}
\end{itemize}
}

\frame{
\frametitle{Resultados estimador directo}
\begin{itemize}
 \item Con la constelación binaria funciona correctamente el 74\% de las veces.
\item Con la de cuatro niveles$\ldots$ el 48\%.
\end{itemize}

\begin{columns}
 \column{0.5\textwidth}
\begin{center}
 \includegraphics[width=0.9\textwidth]{img/4_niveles_directo_bien.pdf}
\end{center}
 \column{0.5\textwidth}
\begin{center}
 \includegraphics[width=0.9\textwidth]{img/4_niveles_directo_mal.pdf}
\end{center}
\end{columns}
}

\frame{
\frametitle{Resultados estimador DFE}
\begin{itemize}
 \item Con la constelación binaria funciona correctamente el 74\% de las veces.
\item Con la de cuatro niveles$\ldots$ el 25\%.
\end{itemize}

\begin{columns}
 \column{0.5\textwidth}
\begin{center}
 \includegraphics[width=0.8\textwidth]{img/4_niveles_dfe_bien.pdf}
\end{center}
 \column{0.5\textwidth}
\begin{center}
 \includegraphics[width=0.8\textwidth]{img/4_niveles_dfe_mal.pdf}
\end{center}
\end{columns}
}

\frame{
\frametitle{Sensibilidad a valores iniciales}
Se observa que algunas veces estima bien y otras mal. Depende de las condiciones iniciales, que son aleatorias. Para probar su efecto se realizó la siguiente experiencia:
\begin{center}
 \includegraphics[width=0.7\textwidth]{img/directo_shake.pdf}
\end{center}
}

\section{Conclusiones}
\subsection{Discusión de resultados}
\frame{
\frametitle{Validez de los resultados}
\begin{itemize}
\item Para calcular los porcentajes correctos, se usó el validador.
\item El validador no anda bien, puede dar tanto falsos positivos como falsos negativos.
\item De observar muchas simulaciones se puede concluir que el DFE tiene menor probabilidad de estimar correctamente.
\begin{columns}
\column{0.4\textwidth}
\item En el caso de la constelación binaria, funcionan mejor que el 74\%, pero por problemas numéricos, en ocasiones el estimador no puede resolver el problema.
\column{0.3\textwidth}
\begin{center}
 \includegraphics[width=\textwidth]{img/sad_face.jpg}
 % sad_face.JPG: 1050x1050 pixel, 96dpi, 27.78x27.78 cm, bb=
\end{center}
\end{columns}
\end{itemize}
}

\subsection{Futuros desarrollos}
\frame{
\frametitle{Futuros desarrollos}
\begin{itemize}
\item Se necesita mejorar el validador de estimación.
\item Se necesita tener mejores estimadores.
\begin{itemize}
 \item Para estas dos cosas se va a probar usar estadística robusta.
\end{itemize}
\item Calcular la complejidad de los algoritmos.
\item Identificar los datos distribuibles para resolver el problema en forma cooperativa.
\begin{columns}
\column{0.4\textwidth}
\item Terminar la carrera
\begin{itemize}
 \item Hacer todo lo anterior y escribir la tesis.
\end{itemize}
\column{0.3\textwidth}
\begin{center}
 \includegraphics[width=\textwidth]{img/happy-face.jpg}
\end{center}
\end{columns}
\end{itemize}
\begin{center}
 FIN!!
\end{center}

}

\end{document}
