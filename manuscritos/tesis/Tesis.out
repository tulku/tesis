\BOOKMARK [0][-]{chapter.1}{Introducci\363n}{}
\BOOKMARK [0][-]{chapter.2}{Estimaci\363n de canal}{}
\BOOKMARK [1][-]{section.2.1}{\277Por qu\351 conocer el canal?}{chapter.2}
\BOOKMARK [2][-]{subsection.2.1.1}{Detecci\363n no coherente}{section.2.1}
\BOOKMARK [2][-]{subsection.2.1.2}{Detecci\363n coherente}{section.2.1}
\BOOKMARK [2][-]{subsection.2.1.3}{Comparaci\363n de ambos sistemas}{section.2.1}
\BOOKMARK [1][-]{section.2.2}{Identificaci\363n de par\341metros}{chapter.2}
\BOOKMARK [2][-]{subsection.2.2.1}{Estimador de m\341xima verosimilitud}{section.2.2}
\BOOKMARK [2][-]{subsection.2.2.2}{Algoritmo Expectation Maximization}{section.2.2}
\BOOKMARK [2][-]{subsection.2.2.3}{Convergencia del algoritmo EM}{section.2.2}
\BOOKMARK [2][-]{subsection.2.2.4}{Canal utilizado en este trabajo}{section.2.2}
\BOOKMARK [2][-]{subsection.2.2.5}{Desarrollo del estimador}{section.2.2}
\BOOKMARK [1][-]{section.2.3}{Estimaci\363n ciega de canal}{chapter.2}
\BOOKMARK [2][-]{subsection.2.3.1}{Identificabilidad del canal}{section.2.3}
\BOOKMARK [2][-]{subsection.2.3.2}{Forma de la funci\363n de verosimilitud}{section.2.3}
\BOOKMARK [2][-]{subsection.2.3.3}{Dependencia de las condiciones iniciales}{section.2.3}
\BOOKMARK [0][-]{chapter.3}{Validaci\363n de modelos}{}
\BOOKMARK [1][-]{section.3.1}{M\351todos de validaci\363n}{chapter.3}
\BOOKMARK [2][-]{subsection.3.1.1}{An\341lisis de residuos}{section.3.1}
\BOOKMARK [1][-]{section.3.2}{Uso de los residuos en la validaci\363n ciega}{chapter.3}
\BOOKMARK [2][-]{subsection.3.2.1}{C\341lculo de los residuos}{section.3.2}
\BOOKMARK [2][-]{subsection.3.2.2}{An\341lisis de la varianza}{section.3.2}
\BOOKMARK [2][-]{subsection.3.2.3}{Ensayo de bondad de ajuste}{section.3.2}
\BOOKMARK [0][-]{chapter.4}{Ensayo de ajuste Kolmogorov-Smirnov}{}
\BOOKMARK [1][-]{section.4.1}{Ensayo Kolmogorov-Smirnov univariable}{chapter.4}
\BOOKMARK [1][-]{section.4.2}{Ensayo Kolmogorov-Smirnov multivariable}{chapter.4}
\BOOKMARK [2][-]{subsection.4.2.1}{Transformaci\363n de Rosenblatt}{section.4.2}
\BOOKMARK [2][-]{subsection.4.2.2}{Uso de la transformada en el ensayo KS}{section.4.2}
\BOOKMARK [1][-]{section.4.3}{Implementaci\363n del ensayo KS multivariable}{chapter.4}
\BOOKMARK [0][-]{chapter.5}{Discusi\363n de resultados}{}
\BOOKMARK [1][-]{section.5.1}{Validaci\363n ciega de modelos}{chapter.5}
\BOOKMARK [2][-]{subsection.5.1.1}{Prueba de la validez del m\351todo}{section.5.1}
\BOOKMARK [1][-]{section.5.2}{Desempe\361o del algoritmo de validaci\363n}{chapter.5}
\BOOKMARK [2][-]{subsection.5.2.1}{Comportamiento general}{section.5.2}
\BOOKMARK [2][-]{subsection.5.2.2}{Comportamiento a distintos SNR}{section.5.2}
\BOOKMARK [2][-]{subsection.5.2.3}{Comportamiento tomando distinto largo de bloques}{section.5.2}
\BOOKMARK [0][-]{chapter.6}{Conclusiones}{}
\BOOKMARK [0][-]{section*.30}{Ap\351ndices}{}
\BOOKMARK [0][-]{appendix.A}{Bondad de ajuste y prueba de hip\363tesis}{}
\BOOKMARK [1][-]{section.A.1}{Inferencia estad\355stica}{appendix.A}
\BOOKMARK [1][-]{section.A.2}{Ensayo de hip\363tesis}{appendix.A}
\BOOKMARK [2][-]{subsection.A.2.1}{Nivel de probabilidad, o valor-p}{section.A.2}
\BOOKMARK [2][-]{subsection.A.2.2}{Potencia de un ensayo}{section.A.2}
\BOOKMARK [0][-]{appendix.B}{Introducci\363n a los estimadores de cuadrados m\355nimos}{}
\BOOKMARK [1][-]{section.B.1}{Estimadores l.s. - Propiedades b\341sicas}{appendix.B}
\BOOKMARK [2][-]{subsection.B.1.1}{Estimadores l.s. como esperanzas condicionales}{section.B.1}
\BOOKMARK [2][-]{subsection.B.1.2}{Estimaci\363n dada varias variables aleatorias}{section.B.1}
\BOOKMARK [0][-]{appendix.C}{Desarrollo del estimador EM}{}
\BOOKMARK [0][-]{appendix.D}{Estad\355stica robusta}{}
\BOOKMARK [1][-]{section.D.1}{Punto de quiebre}{appendix.D}
\BOOKMARK [1][-]{section.D.2}{Estimadores M}{appendix.D}
\BOOKMARK [2][-]{subsection.D.2.1}{Motivaci\363n}{section.D.2}
\BOOKMARK [2][-]{subsection.D.2.2}{Definici\363n}{section.D.2}
\BOOKMARK [2][-]{subsection.D.2.3}{C\341lculo}{section.D.2}
\BOOKMARK [2][-]{subsection.D.2.4}{Ejemplos}{section.D.2}
\BOOKMARK [0][-]{subsection.D.2.4}{Bibliograf\355a}{}
