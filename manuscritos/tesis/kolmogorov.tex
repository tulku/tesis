\chapter{Ensayo de ajuste Kolmogorov-Smirnov}
\label{sec:ks}

Los ensayos de bondad de ajuste se usan para responder la siguiente pregunta: La
distribución de $n$ elementos que forman una muestra ¿Se corresponde con una
distribución teórica dada? Es decidir, los ensayos de bondad de ajuste, miden la
discrepancia entre una distribución observada y otra teórica, indicando en qué
medida las diferencias existentes entre ambas, de haberlas, se deben al azar.

Los ensayos de bondad de ajuste se distinguen de otros tipos de ensayos de
inferencia estadística\footnote{Se presenta un resumen de inferencia estadística
en el apéndice \ref{cap:estadistica}}, principalmente porque cuando se realiza
un ensayo de bondad de ajuste, se pretende o espera demostrar que la hipótesis
nula es verdadera, en otras palabras, se pretende demostrar que una muestra
pertenece a una distribución específica (por ejemplo, una distribución normal).
Para el caso de la mayoría de los otros ensayos de inferencia, se espera
rechazar la hipótesis nula. Es interesante notar que la hipótesis alternativa en
un ensayo de bondad de ajuste no sugiere una distribución alternativa para la
muestra si la hipótesis nula es rechazada. Sin embargo, a pesar de estas
diferencias, son un caso particular de ensayo de hipótesis y como tal van a
presentar los mismos errores tipo I y tipo II ($\alpha$ y $\beta$), y se van a
mantener los conceptos de potencia y significancia del ensayo.

\section{Ensayo Kolmogorov-Smirnov univariable}

Antes de definir las hipótesis nula y alternativa, vamos a notar lo siguiente:
\begin{enumerate}
\item El protocolo para el ensayo Kolmogorov-Smirnov requiere calcular la
función de distribución de la muestra y la población hipotética. Siempre se va a
suponer que la función de distribución calculada es la mejor aproximación
posible a la función real. La estadística del test está definida por el punto
que representa la mayor distancia vertical entre las dos funciones de
distribución.
\item Dentro del marco de la hipótesis nula y la alternativa, $F(X)$ representa
la distribución a partir de la que se obtuvo la muestra, mientras que $F_0(X)$
representa la teórica o hipotética distribución contra la que la muestra se va a
evaluar.
\end{enumerate}

\begin{description}
 \item[Hipótesis nula:] $H_0: F(X) = F_0(X)$ para todo valor de $X$.

Es decir, la distribución de los elementos de la muestra es consistente con la
población teórica. Otra forma de establecer la hipótesis nula es que la máxima
distancia vertical entre la función de distribución acumulada de las muestras y
la teórica nunca supera lo que sería esperable por el azar, si la muestra es
producida con la misma función $F(X)$.
\item[Hipótesis alternativa:] $H_1: F(X) \neq F_0(X)$ para por lo menos 1 valor
de $X$.

Es decir, la distribución de los elementos de la muestra no es consistente con
la población teórica. Alternativamente, existe un punto donde la distancia
vertical entre las dos funciones de distribución es más grande que lo esperable
por azar.
\end{description}

Es conveniente interpretar gráficamente estas hipótesis. En la figura
\ref{fig:ks} se observan dos curvas, representando dos funciones de
distribución, la función empírica calculada a partir de los elementos de la
muestra y la función teórica. Se va a buscar la máxima diferencia de estas
curvas y luego se decide si esa distancia es aceptable o no.

\begin{figure}
 \centering
 \includegraphics[width=0.8\textwidth]{./img/ks.pdf}
 \caption{Representación gráfica del ensayo Kolmogorov-Smirnov}
 \label{fig:ks}
\end{figure}

Matemáticamente, esta prueba se describe de la siguiente forma. Se tiene una
muestra formada por $\hat{n}_1,\ldots,\hat{n}_P$, realizaciones  i.i.d
(independientes e idénticamente distribuidas) de una variable aleatoria con
función de distribución $F$. Se quiere verificar la hipótesis $F=F_0$ contra $F
\neq F_0$, donde $F_0$ es una función de distribución teórica completamente
especificada. Se define la siguiente magnitud $D_P$:
\begin{equation}
\label{eq:ksuni}
 D_P = \max_{\hat n \in \mathbb{R}} | F_P(\hat n)-F_0(\hat n)|,
\end{equation} 
donde $F_P$ es la función de distribución empírica de la muestra.

Ahora, si se tiene una variable aleatoria $\hat{n}$ con una función de
distribución $F$, se la puede transformar en una variable uniforme $\nu$
aplicando la transformación $\nu=F(\hat{n})$ \cite{leon-garcia}. Aplicando este
procedimiento a las muestras $\hat {n}$, se obtiene $\nu_j = F_0(\hat {n}_j)$,
con $j = 1,\ldots,P$. De esta forma, la ecuación (\ref{eq:ksuni}) se reescribe:

\begin{equation}
 D_P = \max_{0 \leq \nu \leq 1} | G_P(\nu) -\nu |.
\label{eq:indep}
\end{equation}

Esta última ecuación permite evaluar cuan uniforme es el resultado de aplicar la
CDF\footnote{Del inglés \textit{Cumulative distribution function}} (función de
distribución acumulada) supuesta a las muestras bajo estudio, en lugar de
comparar las funciones de distribución como en (\ref{eq:ksuni}). Se demuestra
que, a diferencia de (\ref{eq:ksuni}), la estadística de (\ref{eq:indep}) es
independiente de la distribución $F$. El hecho que $D_P$ sea independiente de la
distribución $F$ es de suma importancia, ya que significa que su estadística va
a ser la misma para todos los ensayos. La magnitud $D_P$ conforma la estadística
del ensayo Kolmogorov-Smirnov y su distribución es conocida y está tabulada.

Para realizar el ensayo, se debe seleccionar una cota de aceptación, es decir,
hasta que valor se considera que la discrepancia entre las dos distribuciones se
debe al azar o a que efectivamente la muestra proviene de una distribución
diferente a la ensayada. Antes de poder fijar dicha cota se debe decidir con que
probabilidad queremos rechazar una hipótesis cuando debería ser aceptada, o en
términos estadísticos, cometer un error tipo I, o $\alpha$. Ésta probabilidad es
conocida como nivel de significancia. Una vez que se decide por un nivel de
significancia, y la cantidad de elementos que se van a tomar dentro de la
muestra, se consultan las tablas de percentiles del ensayo de Kolmogorov-Smirnov
y se obtiene el valor $D_{max}$, que se va a usar de cota. En \cite{handbook}
se encuentra una referencia muy completa de este ensayo, incluidas las tablas
de percentiles.

Por ejemplo, para una muestra de $35$ elementos, con un nivel de significancia
de $0.05$, $D_{max} = 0.230$, lo que significa que si después de realizar un
ensayo se obtiene $D_P$ mayor a $0.230$ se rechaza la hipótesis nula.

En este trabajo se pretende utilizar el ensayo de Kolmogorov-Smirnov para
realizar una prueba de bondad de ajuste a una variable bidimensional, sin
embargo, lo explicado anteriormente no se puede aplicar para este tipo de
variables, por lo que se utilizará una variante para variables
multidimensionales descripta a continuación.

\section{Ensayo Kolmogorov-Smirnov multivariable}
\label{sec:ks_multi}

Si se pretende usar esta prueba para una variable multidimensional, o para una
variable compleja, una alternativa es utilizar la modificación propuesta por
Juster \textit{et al.} \cite{ksmulti}. La transformación de Rosenblatt
\cite{rosenblatt}, fundamental para esta variación, permite transformar un
vector aleatorio $\hat N$ con distribución $F$ en otro vector $\Upsilon = T(\hat
N; F)$ de iguales dimensiones distribuido uniformemente, cuyas componentes están
entre 0 y 1. Se realiza una explicación detallada de esta transformación en la
sección \ref{sec:rosenblatt}. Utilizándola, la ecuación (\ref{eq:indep}) se
reescribe como:
\begin{equation}
D_P = \max_{\Upsilon} \left|G_{P}(\Upsilon)- \prod_{j=1}^{P}
{\Upsilon_{j}} \right|
\label{eq:ks_m}
\end{equation}
donde $G_{P}$ es la función de distribución empírica de la muestra transformada
$\Upsilon_j$. Obteniendo así una forma del ensayo válida para variables
multidimensionales.

En su forma más general para este trabajo se necesitaría realizar un ensayo
sobre una variable compleja bidimensional, que se puede modelizar como dos
variables reales bidimensionales. Si se toman las matrices de covarianza de
estas dos variables diagonales, se reduce notablemente la complejidad de la
transformación, quedando (para cada variable) de la siguiente forma, sea $\hat N
= \left[\hat{N}_1 \hat{N}_2 \right]'$:
\begin{equation}
\label{eq:transformacion}
 \Upsilon = \left[F_{1}(\hat {N}_{1})\ F_{2}(\hat{N}_{2})\right]',
\end{equation}
donde $F_1$ y $F_2$ son las distribuciones marginales de $\hat{N}$.

\subsection{Transformación de Rosenblatt}
\label{sec:rosenblatt}

La transformación de Rosenblatt transforma una distribución continua de $k$
variables $F(x_1,\ldots ,x_k)$ en una distribución uniforme en un hipercubo
k-dimensional.

\paragraph*{}

Sea $X = (X_1, \dots, X_k)$ un vector aleatorio con  $F=(x_1, \dots, x_k)$ y $z
= (z_1, \dots, z_k) = Tx = T(x_1, \dots, x_k)$, donde $T()$ es la transformación
considerada y está dada por:
\begin{eqnarray*}
z_1 &=& P\{X_1 \leqslant x_1 \} = F_1(x_1) \\
z_2 &=& P\{X_2 \leqslant x_2 | X_1 = x_1 \} = F_2(x_2|x_1)\\
z_k &=& P\{X_k \leqslant x_k | X_{k-1} = x_{k-1}, \dots, X_1 = x_1 \} =
F_k(x_k|x_{k-1}, \dots, x_1)
\end{eqnarray*}

Se muestra fácilmente que $Z=TX$ es un vector aleatorio uniforme en un hipercubo
de k dimensiones:

\begin{eqnarray}
 P(Z_i &\leqslant & z_i; i = 1,\dots ,k) \\
 &=& \int_{Z|Z_i \leqslant z_i} \cdots \int d_{x_k} F_k(x_k|x_{k-1}, \dots, x_1)
\cdots d_{x_1} F_1(x_1)\\
 &=& \int_0^{z_k} \cdots \int_0^{z_1} d_{z_1} \cdots d_{z_k} = \prod_{i=1}^k
z_i 
\end{eqnarray}

donde $0 \leqq z_i \leqq 1$, $i=1,\ldots , k$. Por lo tanto $Z_1, \ldots ,Z_k$
son uniformes e independientes. Es de notar que esta transformación es válidad
para cualquiera valor de $k$, sean o no independientes las variables, lo que
significa que emplearla no implica una pérdida de generalidad.

\subsection{Uso de la transformada en el ensayo KS}

Sea $X_{(i)} = (X_{1(i)}, \ldots, X_{k(i)})$, $i=1, \ldots, n$ una muestra
aleatoria de $n$ vectores de una población con distribución $F(x_1, \ldots,
x_k)$ y sea $G(x_1, \ldots, x_k)$ la función de distribución de las muestras. Se
encontró que la distribución de la estadística de Kolmogorov-Smirnov,
\begin{equation}
\max_{x_1,\ldots, x_k} | F(x_1, \ldots,  x_k) - G(x_1, \ldots, x_k) |
\end{equation}
no se mantiene igual para todas las $F$ si $k>1$. Sin embargo, uno puede probar
si $X_{(i)}$, $i=1,\ldots, n$ son una muestra de la población de $F(x_1, \ldots,
x_k)$ usando el ensayo de Kolmogorov-Smirnov para medir si $Z_{(i)}=TX_{(i)}$
son una muestra de una población uniformemente distribuida en un hipercubo de
$k$ dimensiones. Existen $k!$ posibles transformaciones con las que se puede
realizar el ensayo descripto, dependiendo del orden en que se tomen las $x_i$
componentes de $F()$.

La estadística de este ensayo no es la misma que para el tradicional,
unidimensional. Sin embargo, la estadística de esta variante multidimensional
fue analizada y tabulada por Justel \textit{et al.} en \cite{ksmulti}. En ese
trabajo se calculan tablas de los valores de $D_{max}$ según un valor de
significancia y cantidad de muestras dadas. Para obtener una cota superior al
nivel de significancia global, ante el problema de las múltiples permutaciones,
se utiliza la cota de Bonferroni. Finalmente el ensayo queda escrito de la
siguiente forma:
\begin{equation}
 D_n = \max_{j=1,2,\ldots} D_n^j
\end{equation}
donde $D_n$ es el resultado global del ensayo y el índice $j$ representa las
distintas permutaciones. Si se desea, se pueden hacer $k$ ensayos. En lugar de
contrastar sólo el máximo contra una cota, se pueden comparar cada uno de los
$D_n^j$ por separado. En ese caso, para mantener un nivel de significancia
$\alpha$, cada $D_n^j$ se compara contra $\alpha k!$.

\section{Implementación del ensayo KS multivariable}
\label{sec:implementacion_ks}
%TODO: Es necesario explicar matemáticamente los conjuntos de puntos?

En este trabajo se va a utilizar una modificación del ensayo de
Kolmogorov-Smirnov para variables multidimensionales. Como en su forma
tradicional, la realización de este ensayo requiere del cómputo de la función de
distribución empírica de las muestras bajo estudio.

Como se dijo anteriormente, para que el ensayo de Kolmogorov-Smirnov sea válido
es necesario obtener una buena aproximación a la función de distribución de la
cual proviene la muestra. Sin embargo, en el caso multidimensional es importante
seleccionar correctamente los puntos del espacio sobre los que se va a calcular
esta función, para tener una buena aproximación y al mismo tiempo mantener baja
la complejidad del cálculo.

Como se explicó en la sección \ref{sec:ks_multi} se puede reducir el problema
complejo a dos problemas reales bidimensionales, y esto permite la adopción del
algoritmo para variables bidimensionales descripto en \cite{ksmulti}. Este
trabajo define un conjunto de puntos notables sobre los cuales va a calcular la
función de distribución empírica y comparar contra la distribución supuesta.

Si la muestra $\Upsilon$ (resultado de la transformación de Rosenblatt) son los
pares $\{(\nu^1_{1},\nu^2_{1}), \ldots, (\nu^1_{j},\nu^2_{j}),
(\nu^1_{l},\nu^2_{l}), \ldots, (\nu^1_{P},\nu^2_{P})\}$, el conjunto de puntos
notables $\mathcal{B} \subset \mathbb{R}^2$ está formado por los elementos
$\beta \in \mathbb{R}^2$ que satisfacen las siguientes condiciones:

\begin{itemize}
\item Las muestras:

$\{(\beta^1, \beta^2) | \beta_1 = \nu^1_j, \beta_2 = \nu^2_j, j = 1,\cdots,P\}$,
\item los puntos de intersección:

 $\{(\beta^1,\beta^2) | \beta^1 = \nu^1_{l}, \beta_2 = \nu^2_{j}$  y $\nu^1_{l}
> \nu^1_{j},\ \nu^2_{l} < \nu^2_{j}\, j,l=1,\cdots,P \}$,
\item las proyecciones al borde superior del cuadrado unitario:

$\{(\beta^1,\beta^2) | \beta^1 = 1, \beta_2 = \nu^2_{j}, j=1,\cdots, P\}$,
\item y las proyecciones al borde derecho del cuadrado unitario:

$\{(\beta^1,\beta^2) | \beta^1 = \nu^1_{j}, \beta_2 = 1, j=1,\cdots, P\}$.
\end{itemize}

\begin{figure}
 \centering
 \includegraphics[width=0.8\textwidth]{./img/notables.pdf}
 % notables.pdf: 337x232 pixel, 72dpi, 11.89x8.18 cm, bb=0 0 337 232
 \caption{Puntos notables calculados a partir de una muestra dada.}
 \label{fig:notables}
\end{figure}

Al calcular la función de probabilidad acumulada en $\mathcal{B}$ se sabe que se
está teniendo una buena aproximación a la función real y se mantiene acotada la
complejidad del cálculo entre $3n$ y $3n + \binom{n}{2}$, donde $n$ es la
cantidad de elementos que formar la muestra. En la figura \ref{fig:notables} se
muestran los cuatro conjuntos de puntos, las muestras, los puntos de
intersección y las dos proyecciones. Se aprecia como a partir de una muestra se
generan una mayor cantidad de puntos, lo que hace que la aproximación a la
función de distribución sea mejor.

De esta forma, y recordando la ecuación (\ref{eq:ks_m}) el ensayo queda
implementado con el siguiente algoritmo:

\begin{enumerate}
 \item Se aplica la transformación de Rosenblatt en dos dimensiones a la muestra
original.
 \item Se calcula $\mathcal{B} = \{ \beta \}$.
 \item Se calcula $G_P(\beta)\ \forall \beta \in \mathcal{B}$.
 \item Se calcula $\beta^1.\beta^2\ \forall \beta \in \mathcal{B}$.
 \item Se busca la máxima diferencia, $D_{max} = \max_{\beta \in \mathcal{B}}
\left[|G_P(\beta)-\beta^1.\beta^2|\right]$.
\item Se decide que la identificación es válida si $D_{max} < K$.
\end{enumerate}
donde K es el umbral de decisión del ensayo y $G_P(\beta)$ es la función
descripta en (\ref{eq:ks_m}). Los valores de $K$ se tomaron de tablas de
percentiles del ensayo Kolmogorov-Smirnov, como por ejemplo las presentes en
\cite{ksmulti} y luego se ajustaron para obtener los errores deseados en
este trabajo. A continuación se tabulan los $3$ diferentes valores de $K$
adoptados para las tres cantidades de muestras utilizadas:

\begin{center}
\begin{tabular}{crrr}
\toprule
 & 50 muestras & 300 muestras & 500 muestras\\
\midrule
$K$ & $0.2$ & $0.09$ & $0.07$ \\
\bottomrule
\end{tabular}
\end{center}
\label{pag:ks_valores}
Es interesante recordar la ecuación (\ref{eq:transformacion}), donde se muestra
la expresión de la transformación de Rosenblatt utilizada en el primer paso del
algoritmo y  la formulación matemática de dicha transformada. Resulta inmediato,
al no haber correlación entre las dimensiones de la variable a transformar, que
esta transformación en particular no es sensible a las permutaciones, por lo que
sólo un ensayo es necesario.