\chapter{Validación de modelos}
\label{sec:validacion}

El proceso de estimación de un parámetro selecciona el ``mejor'' modelo dentro
de una estructura de modelo seleccionada. El interrogante principal ahora es
saber si este ``mejor'' modelo es suficientemente bueno para lo que se necesita.
Éste es el problema de la validación de modelos, y tiene diferentes aspectos:

\begin{enumerate}
\item \label{item:preg1}¿El modelo estimado concuerda suficientemente bien con
los datos observados?
\item \label{item:preg2}¿El modelo estimado es suficientemente bueno para mis
propósitos?
\item \label{item:preg3}¿El modelo estimado describe el modelo real?
\end{enumerate}

Generalmente, el método para responder a estas preguntas es confrontar el modelo
estimado, $M(\hat{\theta})$, con la mayor cantidad de información conocida sobre
el sistema, esto incluye conocimiento a priori, datos experimentales y
experiencia obtenida por el uso del modelo. En un problema de identificación la
entidad más natural con la cual confrontar el modelo es con los datos
experimentales mismos. Por eso, los sistemas de validación de sistemas se
tienden a concentrar en la pregunta \ref{item:preg1}.

\section{Métodos de validación}
A continuación se van a presentar métodos de validación de modelos comunes en la
literatura \cite{ljung}, analizando su aplicabilidad al problema bajo estudio:
una transmisión inalámbrica sin secuencias de entrenamiento.

% En las siguientes 3 secciones uso subsubsection y no subsection que sería lo
% ``correcto'' para que los títulos queden más chicos.
\subsubsection*{Validación con respecto al propósito del modelado}

Mientras que la pregunta \ref{item:preg3} es intrigante al mismo tiempo es
filosóficamente imposible de responder. La pregunta que importa para este
enfoque es la segunda, ya que siempre hay algún propósito en el modelado. En ese
caso, la prueba definitiva a realizar sería evaluar si el problema que motivó el
modelado se puede resolver con el modelo obtenido. En nuestro caso particular,
la pregunta a resolver sería  si la señal transmitida se puede decodificar
correctamente utilizando el modelo de canal estimado. En comunicaciones esto se
podría realizar utilizando secuencias de entrenamiento, donde se sabe cual es la
señal decodificada, sin embargo de no tener estos datos presentes, se va a tener
que recurrir a otros métodos.

\subsubsection*{Viabilidad de parámetros físicos}

Para una estructura de modelo obtenida a partir de parámetros físicos, una forma
de validar el modelo es utilizar el conocimiento a priori del sistema para
analizar si los valores estimados y sus varianzas concuerdan con lo esperable.
Al ser una variable física ésta se puede medir y así recolectar datos que se
pueden usar para caracterizar estadísticamente dicha variable. Esto constituye
el conocimiento a priori mencionado anteriormente.

Cómo se va a propagar una señal en un medio inalámbrico se podría calcular a
partir de parámetros físicos del entorno en donde se está realizando la
comunicación, estos incluyen las dimensiones del recinto, interferencia de otros
campos electromagnéticos, velocidad de la fuente y el receptor. Utilizando las
ecuaciones de Maxwell se podría calcular el campo generado por la fuente y como
sería la señal en el receptor. Sin embargo, realizar esta operación es
impensable. Resolver correctamente el problema para una situación estática sería
complejo, pero cuando se tienen en cuenta las variaciones, como aparición de
nuevos obstáculos, interferencias, movimientos tanto de la fuente como el
receptor, este modelo que se calculó va a perder vigencia muy rápidamente. Es
por este motivo que no se puede utilizar este enfoque y es necesario generar
modelos de canales basados en estadística de una gran cantidad de mediciones.
Esta naturaleza de las comunicaciones inalámbricas hace que no se puedan aplicar
los métodos utilizados para validar modelos basados en parámetros físicos de un
suceso.

\subsection{Análisis de residuos}
\label{sec:residuos}
%\label{sec:bondad_de_ajuste_con_datos}
Otro tipo de método de validación de modelos son los que analizan las
propiedades de los residuos que genera el modelo. Estos residuos contienen la
diferencia entre la señal producida por el sistema real y el estimado, por lo
que, indudablemente, van a contener información útil sobre la calidad de la
estimación. Estos son de particular interés ya que un análisis de este tipo se
va a emplear en el desarrollo del validador propuesto.

Primero es necesario definir formalmente los residuos, que llamaremos
$\varepsilon(t:\hat{\varTheta})$. Si $y(t:\varTheta)$ es la salida producida por
el sistema real e $\hat{y}(t:\hat{\varTheta})$ la estimación usando el parámetro
$\hat{\varTheta}$, entonces:
\begin{displaymath}
 \varepsilon(t:\hat{\varTheta}) = y(t:\varTheta) - \hat{y}(t:\hat{\varTheta}).
\end{displaymath}

Una vez obtenidos estos residuos se pueden utilizar de diversas maneras para
comprobar la calidad del modelo. Lo primero que se propone realizar es un
estudio estadístico de los mismos. Por ejemplo, se puede calcular el máximo:

\begin{equation}
 S_1 = \max_t |\varepsilon(t)|
\label{eq:e_1}
\end{equation}
o un promedio cuadrático:
\begin{equation}
 S^2_2 = \dfrac{1}{N}\sum_{t=1}^{N} |\varepsilon^2(t)|
 \label{eq:e_2}
\end{equation}
donde $N$ es el largo del vector de residuos.

Esto se basa en el conocimiento a priori del modelo. Uno puede saber, a partir
de todas las pruebas realizadas, que el modelo que debe estimar nunca produciría
un residuo mayor a $S_1$ o una media cuadrática mayor a $S^2_2$, por lo tanto,
no cumplir con estas estadísticas, indicaría un error en el modelo estimado.
Estos métodos imponen una condición importante para que tengan sentido. Los
residuos no pueden depender de algo que es probable que cambie. De particular
importancia es que no dependan de los datos de entrada ya que, de hacerlo, la
utilidad de las ecuaciones (\ref{eq:e_1}) y (\ref{eq:e_2}) sería muy limitado.

Desde un punto de vista más formal y general, utilizando el estimador de máxima
verosimilitud, y asumiendo que los datos fueron generados según
\begin{equation}
 y(t:\varTheta) = g(Z^N:\varTheta) + n(t)
 \label{eq:recibidos}
\end{equation}
donde $g(\cdot)$ representa la respuesta del canal ante la secuencia de datos
enviados $Z^N$ con un valor $\varTheta$ particular, se supone que los valores de
$n(t)$ tienen una distribución $f_n$ y son independientes entre ellos y de los
datos pasados. La pregunta que nos vamos a hacer para validar este sistema va a
ser, ``¿Es probable que los datos recibidos hayan sido generados por
(\ref{eq:recibidos})?''. Esta pregunta es equivalente a preguntar: ``¿Es
probable que
\begin{equation}
 \varepsilon(t) = y(t:\varTheta) - g(Z^N:\hat{\varTheta})
\label{eq:res_t}
\end{equation}
sea una secuencia de variables aleatorias independientes con distribución
$f_\varepsilon = f_n$?''. Es decir, haciendo un análisis de residuos,
transformamos el problema de validación en un problema que cuantifica la
diferencia entre dos funciones de distribución de probabilidad.

Como vamos a trabajar con canales AWGN\footnote{\textit{Additive white Gaussian
noise}: Modelo de canal en el cual el ruido aditivo es un ruido blanco
gaussiano} la $f_\varepsilon$ esperada va a ser una normal. Por lo tanto, una
forma de verificar si el modelo es correcto es probando si las muestras de
$\varepsilon(t)$ se corresponden a una distribución normal. Para ello se
utilizan ensayos de bondad de ajuste, que son un tipo de ensayo de inferencia
estadística que se utilizan para probar si una población se corresponde con una
función de distribución particular. En el apéndice \ref{cap:estadistica} se
resumen las bases de la inferencia estadística necesarias para la completa
comprensión de este trabajo.

Un tipo de ensayo de bondad de ajuste es la prueba $\chi^2$. Este ensayo
consiste en calcular la estadística $\chi^2$, para luego decidir según el error
que se quiere cometer si ese valor calculado es aceptable o no.

Si definimos primero la correlación entre los residuos mismos:
\begin{equation}
 \hat{R}^N_{\varepsilon}(\tau) = \dfrac{1}{N}\sum_{t=1}^N \varepsilon(t)
\varepsilon(t - \tau).
 \label{eq:auto_correlacion}
\end{equation}
y adoptamos la condición de canal AWGN,
\begin{equation}
 \dfrac{1}{\sqrt{N}} \sum_{t=1}^N \left[\varepsilon(t-1), \ldots,
\varepsilon(t-M)\right]' \varepsilon(t)
\label{eq:res_normales}
\end{equation}
resulta ser asintóticamente normal, con media cero y varianza $\lambda^2\ I$
\cite{ljung}. La $k$-ésima fila de este vector es
$\sqrt{\hat{R}^N_\varepsilon(k)}$. Utilizando nuevamente la suposición que
$\varepsilon$ son blancos,
\begin{equation}
 \dfrac{N}{\lambda ^2} \sum_{\tau=1}^M \left(\hat{R}^N_\varepsilon(\tau)
\right)^2
 \label{eq:res_chi}
\end{equation}
debe estar distribuido según una $\chi^2(M)$ (donde $M$ es la cantidad de grados
de libertad de la distribución $\chi^2$). Si se reemplaza la varianza $\lambda$
por una varianza de ruido estimada, esto no cambia ya que es un comportamiento
asintótico.

Por lo tanto, la forma de ver si los residuos son blancos gaussianos va a ser
verificar si
\begin{equation}
\zeta_{N,M} = \dfrac{N}{\left(\hat{R}^N_\varepsilon(0) \right)^2}
\sum_{\tau=1}^M \left(\hat{R}^N_\varepsilon(\tau) \right)^2
\label{eq:res_con_datos}
\end{equation}
pasa el test de ser $\chi^2(M)$ verificando si $\zeta_{N,M} < 
\chi^2_\alpha(M)$, el nivel $\alpha$ de la distribución $\chi^2(M)$. 

\subsubsection{Otros ensayos de normalidad}
\begin{figure}
\centering
 \subfigure[Normal]{
 \includegraphics[width=.35\textwidth]{img/normal.pdf}
  \label{fig:qqplot_normal}
 }
 \subfigure[Gamma(9,5)]{
 \includegraphics[width=.35\textwidth]{img/gamma1.pdf}
  \label{fig:qqplot_gamma1}
 }
 \subfigure[Gamma(1,2)]{
 \includegraphics[width=.35\textwidth]{img/gamma2.pdf}
  \label{fig:qqplot_gamma2}
 }
 \subfigure[Uniforme]{
 \includegraphics[width=.35\textwidth]{img/uniforme.pdf}
  \label{fig:qqplot_uniforme}
 }
 \caption{Ejemplos de QQPlot realiza a distintas muestras}
 \label{fig:qqplot}
\end{figure}

También existen métodos gráficos para ensayar normalidad, por ejemplo, se usan
gráficos conocidos como \textit{QQPlot}. Se toman las muestras bajo estudio
($\varepsilon$) y se calcula el valor de la función $Q(\cdot)$ normal para cada
uno de los puntos, este resultado es graficado en función de los valores de la
misma función $Q(\cdot)$ pero calculada en una población normal. Si
$\varepsilon$ tiene una distribución normal, entonces el gráfico tiene que ser
una línea recta. En la figura \ref{fig:qqplot} se ven ejemplos de este método
aplicado a distintas muestras, sólo las muestras de una población normal se
aproximan a una curva identidad. Para automatizar este proceso, se calcula la
regresión lineal a un polinomio de orden 1 y se evalúa la distancia de la curva
resultado al polinomio.

Estos dos métodos presentados sufren del mismo problema. Asumen conocimiento de
los datos transmitidos, o necesitan suponer la distribución de las muestras bajo
estudio. En la ecuación (\ref{eq:res_con_datos}) se pone en manifiesto que se
conocen los datos de entrada en el momento de calcular los residuos, esto
permite utilizar el ensayo de bondad de ajuste propuesto ya que de no conocer
$Z^N$, la distribución de (\ref{eq:res_normales}) no se puede asumir normal y la
de (\ref{eq:res_chi}) no está garantizada de ser $\chi^2$.

Sin embargo, el análisis de los residuos es el método más adecuado para el
problema que nos interesa resolver.

\section{Uso de los residuos en la validación ciega}

\subsection{Cálculo de los residuos}
\label{sec:calc_res}

Una vez obtenida la estimación del parámetro del canal $\hat{\varTheta}$, se lo
utiliza para obtener una secuencia aproximada a los datos transmitidos,
$\hat{Z}^N$. Para este paso es necesario utilizar algún método de detección,
como por ejemplo un detector duro. Una vez que se tienen estas dos estimaciones
se puede calcular la respuesta del canal estimado a la supuesta señal de entrada
y luego, a partir de ese resultado, calcular los nuevos residuos. Formalmente,
siguiendo la notación de la sección anterior, definimos los residuos:
\begin{equation}
\varepsilon(t) = y(t:\varTheta) - g(\hat{Z}^N:\hat{\varTheta})
\label{eq:res_ciegos}
\end{equation}

Se observa que las ecuaciones (\ref{eq:res_t}) y la (\ref{eq:res_ciegos})
difieren solamente en que la segunda utiliza una estimación de la señal de
entrada en lugar de los datos transmitidos realmente. A continuación se elaboran
dos métodos de validación utilizando estos residuos.

\subsection{Análisis de la varianza}

Se propone utilizar la traza de la matriz de covarianza de los residuos para
evaluar si se encuentra dentro de uno conjunto de posibles valores. Este método
se basa en los enunciados por las ecuaciones (\ref{eq:e_1}) y (\ref{eq:e_2}).

\subsubsection{Estimación de la traza de la matriz de covarianza del ruido}

Para este desarrollo, nos vamos a situar en un sistema con $M$ canales
receptores y se van a utilizar $N$ muestras de $\varepsilon(t)$,
$\varepsilon(i)$, $i=1,\ldots, N$. La traza de la matriz de covarianza de los
residuos la estimaremos de la siguiente forma:
\begin{equation}
tr(\Sigma) = \frac1{N} \sum_{i=1}^{N} \sum_{j=1}^{M} \varepsilon_j(i)^2,
\label{traza1}
\end{equation}

Si se multiplica y divide la ecuación (\ref{traza1}) por la varianza de los
residuos en cada canal, y se invierte el orden de las sumatorias, se obtiene
esta expresión:
\begin{equation}
tr(\Sigma) = \frac1{N} \sum_{j=1}^{M} \sigma_{\varepsilon_j}^2 \sum_{i=1}^{N}
\frac{\varepsilon_j(i)^2}{\sigma_{\varepsilon_j}^2} =  \frac1{N} \sum_{j=1}^{M}
\sigma_{\varepsilon_j}^2 \left( \mathcal{X}_N^2 \right)_j \label{traza2}
\end{equation}

en la que se puede ver que la sumatoria más interna es una distribución de
probabilidad $\chi^2$ con $N$ grados de libertad que tiene las siguientes
propiedades: \begin{eqnarray}
\textrm{E} \! \left[ \left( \mathcal{X}_N^2 \right)_j \right] &=& N
\label{e_chi} \\
\textrm{Var} \! \left[ \left( \mathcal{X}_N^2 \right)_j \right] &=& 2N
\label{var_chi}
\end{eqnarray}

Entonces, empleando las ecuaciones (\ref{traza2}), (\ref{e_chi}) y
(\ref{var_chi}) podemos calcular la esperanza y la varianza de la estimación de
la traza de la matriz de covarianza de los residuos. Recordemos que los $\left(
\mathcal{X}_N^2 \right)_j$ son independientes entre sí.

\begin{eqnarray}
\textrm{E} [tr(\Sigma)] &=& \frac1{N} \sum_{j=1}^{M} \sigma_{n_j}^2 \textrm{E}
\! \left[ \left( \mathcal{X}_N^2 \right)_j \right] = \frac1{N} \sum_{j=1}^{M}
\sigma_{n_j}^2 N = \sum_{j=1}^{M} \sigma_{n_j}^2 \label{e_tr} \\
\textrm{Var} [tr(\Sigma)] &=& \frac1{N^2} \sum_{j=1}^{M} \sigma_{n_j}^4
\textrm{E} \! \left[ \left( \mathcal{X}_N^2 \right)_j \right] = \frac1{N^2}
\sum_{j=1}^{M} \sigma_{n_j}^4 2N = \frac{2}{N} \sum_{j=1}^{M} \sigma_{n_j}^4
\label{var_tr}
\end{eqnarray}

\subsubsection{Método de validación}
\label{sec:validacion_por_traza}

\begin{figure}
 \centering
 \includegraphics[width=0.8\textwidth]{./img/traza.pdf}
 % traza.pdf: 354x250 pixel, 72dpi, 12.49x8.82 cm, bb=0 0 354 250
 \caption{Convergencia de la estimación de la traza de la covarianza.}
 \label{fig:traza}
\end{figure}

Una vez calculados los residuos, utilizando la estimación del parámetro del
canal y la detección de los símbolos enviados, se puede estimar la traza de su
matriz de covarianza empleando la ecuación (\ref{traza1}). Suponiendo conocidos
los verdaderos valores de esta matriz (por ejemplo, obtenidos durante el proceso
de estimación del canal), se va a determinar un rango de valores posibles para
la traza estimada. Este rango está centrado en la media teórica, calculada en la
ecuación (\ref{e_tr}), y se extiende por dos desvíos estándares (ecuación
\ref{var_tr}). Finalmente, se decide invalidar o no el modelo dependiendo de si
el valor estimado de la traza cae dentro de este rango.

Si bien éste es un método válido, no resulta útil cuando el número $N$ de
muestras es pequeño debido a la lenta convergencia del estimador de covarianza.
En la figura \ref{fig:traza} se muestra la traza de la matriz de covarianza
estimada a partir de diferentes cantidades de muestras de una variable normal
bidimensional, y se la compara con el valor real de dicha traza. Se puede
concluir que no se obtiene una buena aproximación con menos de 200 muestras.

\subsection{Ensayo de bondad de ajuste}
\label{sec:ensayo_bondad}

El segundo método propuesto es un ensayo de bondad de ajuste de la distribución
de los residuos a la distribución que se sabe deberían tener.

Si se observan las ecuaciones (\ref{eq:recibidos}) y (\ref{eq:res_ciegos}) se
puede ver que si $\hat{Z}^N = Z^N$ y $\hat{\varTheta} = \varTheta$ entonces se
va a poder afirmar que la función de distribución del ruido $n(t)$ va a tener
que ser igual a la de $\varepsilon(t)$. Al igual que en el método anterior, si
la matriz de covarianza de $n(t)$ es conocida, y utilizando la hipótesis de
canal AWGN, se puede saber que la distribución de $n(t)$ es una normal de $M$
dimensiones con la matriz de covarianza $\varSigma$ y media nula.

Sabiendo esto, ahora es necesario buscar un método de inferencia estadística que
nos permita evaluar cuan bien se ajusta la distribución de $\varepsilon(t)$ a la
de $n(t)$. Como es importante que el proceso de estimación y validación sea
rápido dado que el tiempo de coherencia de los canales inalámbricos es corto, se
debe trabajar con poca cantidad de muestras. Otro punto a tener en cuenta es que
a diferencia de los métodos explicados en \S\ref{sec:residuos} no podemos hacer
afirmaciones sobre la distribución de $\varepsilon(t)$ ni tenemos conocimiento
de las señales de entrada al canal. Esta limitación nos fuerza a usar
estadística no paramétrica ya que este tipo de estadística no asume una
distribución de las muestras bajo estudio.

La carencia de presunciones de esta rama de la estadística amplía enormemente su
campo de aplicación y aumenta su robustez. Recordemos que el objetivo de agregar
un proceso de invalidación de modelos es poder efectuar detección coherente en
un sistema de comunicaciones. Por ende, es de primordial importancia seleccionar
un ensayo estadístico que nos permita descartar la mayor parte de los modelos de
los cuales no tenemos certeza de que sean válidos. En el próximo capítulo se va
a analizar en detalle el ensayo que se propone utilizar: en ensayo de bondad de
ajuste de Kolmogorov-Smirnov\cite{sthepens}.