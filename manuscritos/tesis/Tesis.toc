\select@language {spanish}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Estimaci\IeC {\'o}n de canal}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}\IeC {\textquestiondown }Por qu\IeC {\'e} conocer el canal?}{6}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Detecci\IeC {\'o}n no coherente}{7}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Detecci\IeC {\'o}n coherente}{8}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Comparaci\IeC {\'o}n de ambos sistemas}{9}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}Identificaci\IeC {\'o}n de par\IeC {\'a}metros}{10}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Estimador de m\IeC {\'a}xima verosimilitud}{11}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Algoritmo Expectation Maximization}{13}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Convergencia del algoritmo EM}{16}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Canal utilizado en este trabajo}{16}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Desarrollo del estimador}{17}{subsection.2.2.5}
\contentsline {section}{\numberline {2.3}Estimaci\IeC {\'o}n ciega de canal}{20}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Identificabilidad del canal}{20}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Forma de la funci\IeC {\'o}n de verosimilitud}{22}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Dependencia de las condiciones iniciales}{24}{subsection.2.3.3}
\contentsline {chapter}{\numberline {3}Validaci\IeC {\'o}n de modelos}{27}{chapter.3}
\contentsline {section}{\numberline {3.1}M\IeC {\'e}todos de validaci\IeC {\'o}n}{27}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}An\IeC {\'a}lisis de residuos}{29}{subsection.3.1.1}
\contentsline {subsubsection}{Otros ensayos de normalidad}{31}{section*.14}
\contentsline {section}{\numberline {3.2}Uso de los residuos en la validaci\IeC {\'o}n ciega}{32}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}C\IeC {\'a}lculo de los residuos}{32}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}An\IeC {\'a}lisis de la varianza}{33}{subsection.3.2.2}
\contentsline {subsubsection}{Estimaci\IeC {\'o}n de la traza de la matriz de covarianza del ruido}{33}{section*.16}
\contentsline {subsubsection}{M\IeC {\'e}todo de validaci\IeC {\'o}n}{34}{section*.17}
\contentsline {subsection}{\numberline {3.2.3}Ensayo de bondad de ajuste}{35}{subsection.3.2.3}
\contentsline {chapter}{\numberline {4}Ensayo de ajuste Kolmogorov-Smirnov}{37}{chapter.4}
\contentsline {section}{\numberline {4.1}Ensayo Kolmogorov-Smirnov univariable}{38}{section.4.1}
\contentsline {section}{\numberline {4.2}Ensayo Kolmogorov-Smirnov multivariable}{40}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Transformaci\IeC {\'o}n de Rosenblatt}{41}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Uso de la transformada en el ensayo KS}{42}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Implementaci\IeC {\'o}n del ensayo KS multivariable}{43}{section.4.3}
\contentsline {chapter}{\numberline {5}Discusi\IeC {\'o}n de resultados}{47}{chapter.5}
\contentsline {section}{\numberline {5.1}Validaci\IeC {\'o}n ciega de modelos}{47}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Prueba de la validez del m\IeC {\'e}todo}{47}{subsection.5.1.1}
\contentsline {section}{\numberline {5.2}Desempe\IeC {\~n}o del algoritmo de validaci\IeC {\'o}n}{49}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Comportamiento general}{50}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Comportamiento a distintos $SNR$}{52}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Comportamiento tomando distinto largo de bloques}{55}{subsection.5.2.3}
\contentsline {chapter}{\numberline {6}Conclusiones}{59}{chapter.6}
\contentsline {chapter}{Ap\IeC {\'e}ndices}{61}{section*.30}
\contentsline {chapter}{\numberline {A}Bondad de ajuste y prueba de hip\IeC {\'o}tesis}{61}{appendix.A}
\contentsline {section}{\numberline {A.1}Inferencia estad\IeC {\'\i }stica}{61}{section.A.1}
\contentsline {section}{\numberline {A.2}Ensayo de hip\IeC {\'o}tesis}{62}{section.A.2}
\contentsline {subsection}{\numberline {A.2.1}Nivel de probabilidad, o \textit {valor-p}}{65}{subsection.A.2.1}
\contentsline {subsection}{\numberline {A.2.2}Potencia de un ensayo}{67}{subsection.A.2.2}
\contentsline {chapter}{\numberline {B}Introducci\IeC {\'o}n a los estimadores de cuadrados m\IeC {\'\i }nimos}{71}{appendix.B}
\contentsline {section}{\numberline {B.1}Estimadores l.s. - Propiedades b\IeC {\'a}sicas}{72}{section.B.1}
\contentsline {subsection}{\numberline {B.1.1}Estimadores l.s. como esperanzas condicionales}{72}{subsection.B.1.1}
\contentsline {subsection}{\numberline {B.1.2}Estimaci\IeC {\'o}n dada varias variables aleatorias}{73}{subsection.B.1.2}
\contentsline {chapter}{\numberline {C}Desarrollo del estimador EM}{75}{appendix.C}
\contentsline {chapter}{\numberline {D}Estad\IeC {\'\i }stica robusta}{79}{appendix.D}
\contentsline {section}{\numberline {D.1}Punto de quiebre}{79}{section.D.1}
\contentsline {section}{\numberline {D.2}Estimadores M}{80}{section.D.2}
\contentsline {subsection}{\numberline {D.2.1}Motivaci\IeC {\'o}n}{80}{subsection.D.2.1}
\contentsline {subsection}{\numberline {D.2.2}Definici\IeC {\'o}n}{81}{subsection.D.2.2}
\contentsline {subsection}{\numberline {D.2.3}C\IeC {\'a}lculo}{82}{subsection.D.2.3}
\contentsline {subsection}{\numberline {D.2.4}Ejemplos}{83}{subsection.D.2.4}
\contentsline {chapter}{Bibliograf\IeC {\'\i }a}{85}{subsection.D.2.4}
