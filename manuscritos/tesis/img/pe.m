%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Creative Commons
% Attribution-Noncommercial 2.5 India
% You are free:
% to Share â€” to copy, distribute and transmit the work
% to Remix â€” to adapt the work
% Under the following conditions:
% Attribution. You must attribute the work in the manner 
% specified by the author or licensor (but not in any way 
% that suggests that they endorse you or your use of the work). 
% Noncommercial. You may not use this work for commercial purposes. 
% For any reuse or distribution, you must make clear to others the 
% license terms of this work. The best way to do this is with a 
% link to this web page.
% Any of the above conditions can be waived if you get permission 
% from the copyright holder.
% Nothing in this license impairs or restricts the author's moral rights.
% http://creativecommons.org/licenses/by-nc/2.5/in/

% Script for simulating binary phase shift keyed transmission and
% reception and compare the simulated and theoretical bit error
% probability
% Checked for proper operation with Octave Version 3.0.0
% Author	: Krishna
% Email		: krishna@dsplog.com
% Version	: 1.0
% Date		: 5 August 2007
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clear
N = 10^6 % number of bits or symbols
rand('state',100); % initializing the rand() function
randn('state',200); % initializing the randn() function

% Transmitter
ip = rand(1,N)>0.5; % generating 0,1 with equal probability
s = 2*ip-1; % BPSK modulation 0 -> -1; 1 -> 0 
n = 1/sqrt(2)*[randn(1,N) + j*randn(1,N)]; % white gaussian noise, 0dB variance 
Eb_N0_dB = [-20:40]; % multiple Eb/N0 values

snr = 10.^(Eb_N0_dB/10);

%theoryBer = 0.5*erfc(sqrt(snr)); % theoretical ber
fading_coherente = 0.5*(1-sqrt(snr./(1+snr)));
fading_no_coherente = 1./(2*(1+snr));

% plot
close all
figure
%semilogy(Eb_N0_dB, theoryBer,Eb_N0_dB, fading_coherente, Eb_N0_dB, fading_no_coherente);
semilogy(Eb_N0_dB, fading_coherente, Eb_N0_dB, fading_no_coherente);

axis([-20 40 10^-5 1])
grid on
legend('coherente antipodal', 'no coherente ortogonal');
xlabel('SNR [dB]');
ylabel('pe');
title('Probabilidad de error');

