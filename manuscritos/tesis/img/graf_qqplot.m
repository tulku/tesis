muestras = 5000;

normal = randn(1,muestras);
gamma1 = gamrnd (9, 0.5, 1, muestras);
gamma2 = gamrnd (1, 2, 1, muestras);
unif = rand (1, muestras);

[Qn,Sn] = qqplot(normal);
[Qg1,Sg1] = qqplot(gamma1);
[Qg2,Sg2] = qqplot(gamma2);
[Qu,Su] = qqplot(unif);

figure
plot(Qn,Sn);
xlabel ('Q');
ylabel('muestras');
print('normal.eps');

figure
plot(Qg1,Sg1);
xlabel ('Q')
ylabel('muestras')
print('gamma1.eps');

figure
plot(Qg2,Sg2);
xlabel ('Q')
ylabel('muestras')
print('gamma2.eps');

figure
plot(Qu,Su);
xlabel ('Q')
ylabel('muestras')
print('uniforme.eps');

% si se quieren hacer graficos mas bananas, se pueden usar graficos de
% regresion lineal, usando wpolyfit(Qg2,Sg2',1);
