
Var1 = 1.2;
Var2 = 2;
max_samples = 200;
cant_puntos = 100;
cant_repeticiones = 20;

covar = zeros(2,2);
covar(1,1) = Var1;
covar(2,2) = Var2;

a = sqrt(covar)*randn(2,max_samples);
tr = zeros(1,cant_puntos);
e = zeros(2,cant_puntos);
puntos = linspace (2, max_samples, cant_puntos);

for j=1:cant_puntos
	reps = zeros(2,cant_repeticiones);
	for i=1:cant_repeticiones
		a = sqrt(covar)*randn(2,puntos(j));
		reps(1,i) = cov(a')(1,1);
		reps(2,i) = cov(a')(2,2);
	end
	e(1,j) = mean(reps(1,:));
	e(2,j) = mean(reps(2,:));
end
% calculo el error porcentual
plot(puntos, sum(e), 'r', puntos, trace(covar)*ones(1,cant_puntos),'b')
