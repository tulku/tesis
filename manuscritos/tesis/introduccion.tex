% Introduccion: Algo similar a lo que dice el plan de trabajo
% MIMO-OFDM -> Presentacion, modelo propuesto.
% y hablar sobre la motivación para poder validar una estimación.

%TODO: Aclarar que se va a usar KS para hacer la validación.
\chapter{Introducción}

Las comunicaciones, tanto cableadas como inalámbricas son un problema complejo y
constituyen un campo muy activo de investigación y desarrollo. Constantemente se
introducen nuevas tecnologías que posibilitan mejoras en los esquemas. Pudiendo
así brindar más servicios, que a su vez incitan la creación de nuevas
tecnologías. En las comunicaciones inalámbricas existe un problema adicional que
es el desconocimiento de la capacidad teórica máxima del canal. Por ejemplo, el
estándar GSM original sólo podía transmitir $9.6$ kbits/s por canal, en 1997 fue
propuesta una mejora para subirlo hasta $14.4$ utilizando turbo códigos
\cite{Hindelang}. Solamente 7 años después, en 2004, se presentó un trabajo para
elevar la tasa de una red 3G a $28.8$ Mbits/s utilizando canales MIMO
\cite{Garrett}.

Además de mejorar constantemente la capacidad y calidad de las redes actuales,
surgen nuevas posibilidades, como por ejemplo las redes de sensores. Éstas, a su
vez, presentan numerosas complicaciones que no están resueltas, lo que genera
oportunidades para realizar nuevos desarrollos. Por ejemplo en \cite{Pei, Bassi,
Burdin} se utilizan técnicas MIMO (real o simulado) para aumentar la tasa de
transferencia.

En la búsqueda de mejorar las tasas de transferencia se desarrollan nuevos
códigos, técnicas y mecanismos de transmisión, pero también es de vital
importancia maximizar la cantidad de datos útiles que se pueden mandar por el
canal. De nada sirve un duplicar la velocidad del enlace si debo agregar
una cantidad de datos redundantes iguales a los datos útiles que transmito.

Un ejemplo de dato redundante muy común que se transmite son las secuencias de
entrenamiento para los receptores. Los esquemas de comunicaciones más modernos
y complejos imponen la necesidad de conocer el estado del medio de transmisión
para poder recuperar la información transmitida, sin embargo tener este
conocimiento no es gratis y en medios inalámbricos, particularmente costosos.
Las transferencias de los canales inalámbricos suelen ser complejas y altamente
dinámicas por lo que se requieren muchos datos para identificar el canal y
monitorizar sus variaciones. Por ejemplo, en una trama WiFi 802.11n se pueden
transmitir hasta $36\ \mu s$ sólo en entrenamiento. Para disminuir la necesidad
de estas largas secuencias se desarrollan y estudias las técnicas de estimación
ciega. Éstas consisten en obtener el estado del canal sin entrenamiento.

Diseñar un estimador ciego apto para identificar un canal MIMO
rápidamente (lo que se traduce en pocas muestras disponibles para realizar este
proceso) es un problema complejo. Las funciones costo a considerar no son
convexas, por lo que existen muchos máximos locales. Por otro lado, existen
algoritmos de baja complejidad computacional (comparado con la capacidad
disponible en los procesadores modernos) que se pueden aplicar a este problema.
Éstos se suelen utilizar en esquemas semi ciego, donde el aprovechamiento de
las técnicas ciegas reduce el largo de la secuencia de entrenamiento necesaria.

Estos algoritmos no van a dar siempre resultados correctos por las dificultades
mencionadas anteriormente y es por esto que se propone agregar una etapa
novedosa a la cadena de procesamiento del receptor, un validador de
estimaciones. Luego de ejecutar el estimador de canal, los datos obtenidos son
analizados por el algoritmo validador y éste decide si se acepta o no el
resultado. De esta forma se puede realizar otra estimación, con diferentes
condiciones y así poder recuperar los datos transmitidos.

\section*{Objetivo}

El objetivo de este trabajo es diseñar y caracterizar un algoritmo capaz de
decidir si la identificación ciega de un canal MIMO es inválida.

En este trabajo se va a utilizar el estimador EM (\textit{Expectation
Maximization}) por su baja complejidad computacional y ser un método ampliamente
estudiado en la literatura sobre un canal MIMO de dos antenas receptoras y dos
antenas transmisores, por ser una de las configuraciones más populares
actualmente.

Una vez definido este marco de trabajo se va a estudiar la teoría de
validación de modelos y se va a hacer uso de la estadística para diseñar el
algoritmo validador propuesto. Para ello se va a utilizar una variante ensayo de
bondad de ajuste Kolmogorov-Smirnov para poder aplicarlo a variables
multidimensionales.

Finalmente el sistema de identificación y validación se va a simular mediante
experimentación numérica. Para ello se van a crear modelos informáticos de los
algoritmos propuestos utilizando el software GNU Octave \cite{eaton}. Con ellos
se va a poder evaluar el desempeño y caracterizar completamente el
comportamiento de forma tal de servir de base para futuros trabajos en el área
de validación de identificaciones de canal.

\section*{Organización del trabajo}

El cuerpo de esta tesis está distribuído en cuatro capítulos más un conjuntos
de apéndices.

Cada capítulo se centra en uno de los temas implicados en este trabajo:
estimación ciega de canales MIMO, técnicas de validación de modelos y el ensayo
de bondad de ajuste Kolmogorov-Smirnov. Cada uno comienza con una presentación
teórica del tema y luego desarrolla los aportes realizados en cada área. Estos
capítulos son complementados con apéndices de no menor importancia que tratan
temas específicos en mayor profundidad en caso de ser necesario. Estos no fueron
incluidos en el cuerpo principal del trabajo para facilitar la lectura.

En el cuarto capítulo, discusión de resultados, se hace un repaso del algoritmo
propuesto y estudia su desempeño utilizando simulaciones numéricas, incluyendo
una explicación del comportamiento que presenta.

Finalmente, en las conclusiones se repasa lo realizado en esta tesis y se
plantean posibles trabajos futuros que se desprenden del presentado.

\section*{Aportes}

Los aportes fundamentales de este trabajo son los siguientes:

\begin{description}
\item[Diseño de una etapa validadora en un receptor.] Se diseña una nueva etapa
en la cadena de procesamiento de un receptor inalámbrico. Ésta debe poder
invalidar identificaciones erróneas de canal en forma completamente ciega.

\item[Implementación de un validador.] Se diseñó e implementó un algoritmo
invalidador de identificaciones erróneas de canales MIMO. Éste está basado en
métodos estadísticos de ensayo de hipótesis.

\item[Estudio e implementación del algoritmo EM.] No sólo se implementó este
algoritmo para poder realizar las simulaciones de desempeño del validador, sino
que se lo estudió en profundidad y se probaron variaciones al método propuestas
en la literatura analizando su aplicabilidad al problema entre manos.

\item[Desarrollo del ensayo Kolmogorov-Smirnov multivariable.] El ensayo de
bondad de ajuste Kolmogorov-Smirnov es conocido en su versión unidimensional. En
este trabajo se buscaron diferentes modificaciones para adaptarlo a variables
multidimensionales y se explica con profundidad una de ellas. También es
implementada y se detallan las simplificaciones realizadas para su
implementación.
\end{description}
