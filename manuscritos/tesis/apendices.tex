\appendix
\cleardoublepage
\phantomsection
\addcontentsline{toc}{chapter}{Apéndices}
\chapter[Bondad de ajuste y prueba de hipótesis]{Ensayo de bondad de ajuste y
prueba de hipótesis}
\label{cap:estadistica}

\section{Inferencia estadística}

La inferencia estadística es la ciencia de caracterizar y tomar decisiones
acerca de una población usando información de una muestra obtenida de dicha
población \cite{nutshell}. La inferencia ocupa una parte muy importante del uso
de la estadística y muchas y muy sofisticadas técnicas se han desarrollado para
facilitar estas tareas.

El nombre ``inferencia estadística'' sugiere que esta técnica es un ``refinamiento'' de las inferencias que uno puede hacer normalmente, el proceso de hacer generalizaciones sobre poblaciones no medidas usando sólo algunas muestras medidas. La inferencia estadística tiene la ventaja de poder cuantificar con que certeza se realiza la generalización.

Es común confundir la estadística descriptiva y la inferencia estadística, en parte porque muchas veces hasta comparten las mismas fórmulas, cambiando sólo la interpretación de ellas. Por ejemplo, la misma fórmula es usada para calcular la media de una población o la de una muestra, se suman todos los valores y se divide por la cantidad. Sin embargo suele haber diferencias en la notación usada al expresar la fórmula, por ejemplo para expresar la media de una población se utiliza la letra griega $\mu$ mientras que se usa $\bar{X}$ para representar la media de una muestra. Así mismo, para designar el tamaño de una población se suele utilizar la letra $N$ mientras que la cantidad de elementos de una muestra se suele denotar $n$.

Entonces, ¿cómo se puede distinguir una estadística de otra? Se necesita pensar en el propósito del estudio, es simplemente describir las personas u objetos que proveyeron los datos que se van a usar, ¿o es generalizar a un grupo más grande del cual los sujetos bajo estudio son considerados representativos?. Esto se reduce a la siguiente regla:
\begin{quote}
Siempre que se quiera generalizar más allá de los datos utilizados se debe utilizar la inferencia estadística.
\end{quote}
o, dicho de otra forma:
\begin{quote}
Siempre que los datos que se tienen no representan el total de la población, se debe utilizar inferencia estadística.
\end{quote}

% TODO: Explicar normal y uniforme en el apéndice?
La inferencia estadística suele asumir la forma en que los datos están distribuidos, o requiere realizar transformaciones de los datos para hacerlos ajustar mejor a una distribución conocida. Por lo tanto, es importante tener presente las características de las distribuciones más importantes, como la normal y la uniforme para comprender las siguientes explicaciones.

\section{Ensayo de hipótesis}
\label{sec:ensayo_de_hipotesis}

En ensayo de hipótesis es un instrumento fundamental de la inferencia estadística, ya que nos permite usar métodos estadísticos para tomar decisiones sobre problemas reales. Un ensayo de este tipo se divide en una serie de pasos conceptuales, a saber:
\begin{enumerate}
 \item Desarrollar una hipótesis a investigar que pueda ser probada matemáticamente.
\item Definir formalmente la hipótesis nula y la hipótesis alternativa.
\item Decidir un ensayo estadístico apropiado, y realizar los cálculos.
\item Tomar una decisión basado en los resultados.
\end{enumerate}

Tomemos el ejemplo de una medicación para tratar problemas de hipertensión. El fabricante quiere establecer que funciona mejor que los tratamientos actuales bajo las mismas condiciones, por lo que la hipótesis a investigar podría ser algo como ``Los pacientes de hipertensión bajo tratamiento con la droga X logran bajar más la presión que los tratados con las drogas actuales''. Si usamos $\mu_1$ para simbolizar la media de la presión de los tratados con la droga X, y $\mu_2$ la media del otro grupo, entonces las hipótesis se pueden formular de la siguiente forma:

\begin{description}
 \item[$H_0$:] $\mu_1 \geq \mu_2$
\item[$H_A$:] $\mu_1 < \mu_2$
 \end{description}

$H_0$ es llamada la hipótesis nula y significa que la droga X no presenta ninguna mejora sobre la droga común. $H_A$, que también se suele escribir $H_1$ es llamada hipótesis alternativa, que representa, en este caso, que la droga X es más efectiva que el tratamiento estándar. Es importante notar que las dos hipótesis siempre tienen que ser mutuamente excluyentes (no puede haber resultados que las cumplan las dos) y exhaustivas (todos los posibles resultados tienen que satisfacer una de las hipótesis).

En este ejemplo, la hipótesis alternativa tiene 1 cola, se dice que la presión sanguínea del grupo tratado con la droga X tiene que ser más bajo que el tratamiento tradicional para que la hipótesis nula pueda ser descartada. Se podría utilizar una hipótesis alternativa de 2 colas si eso se ajusta mejor a la hipótesis bajo estudio. Si nos interesa saber si la droga A se comparta igual que la droga X, entonces se podría definir una hipótesis alternativa a dos colas:
\begin{description}
\item[$H_0$:] $\mu_1 = \mu_2$
\item[$H_A$:] $\mu_1 \neq \mu_2$
\end{description}

Normalmente los dos primeros pasos se realizan antes de diseñar el experimento o la información es recolectada. La estadística que se va a utilizar para probar la hipótesis es a veces definida en esta etapa, o está implícita en la hipótesis y tipos de datos involucrados. Luego, se recolectan los datos y se realizan los cálculos estadísticos, en este caso, un test-t o ANOVA sería utilizado. Finalmente, basados en el resultado del ensayo se pueden tomar dos decisiones:
\begin{itemize}
\item Rechazar la hipótesis nula y aceptar la hipótesis alternativa.
\item No poder rechazar la hipótesis nula.
\end{itemize}

Se debe remarcar que no poder rechazar la hipótesis nula no significa que se la
probó verdadera, simplemente que el experimento realizado no encontró la
suficiente evidencia para rechazarla. El primer caso es a veces llamado
``encontrar significancia'', o ``encontrar resultados significativos''. El
proceso de ensayo estadístico implica establecer un nivel de probabilidad, o
\textit{valor-p} más allá del cual se van a considerar los resultados de nuestra
muestra suficientemente fuertes como para poder rechazar la hipótesis nula
(consultar \S\ref{sec:valor_p} para una explicación detallada del
\textit{valor-p}). En la práctica este \textit{valor-p} se fija en $0.05$, un
valor arbitrario en uso desde el principio del siglo 20, \cite{nutshell}. Se ha
criticado esta convención y se ha propuesto usar valores más bajos como $p<0.01$
o $p<0.001$ pero nadie a podido justificar el uso de valores más grandes, como
$p<0.10$.

La inferencia estadística nos permite hacer afirmaciones probabilísticas acerca de los datos, con una posibilidad de error inherente al proceso. Se han clasificado dos tipos de error al tomar una decisión usando inferencia estadística, y se han fijado valores aceptables para estos errores. En la siguiente tabla se muestras los dos tipos de errores.

\begin{center}
\begin{tabular}{crr}
\toprule
Resultado del ensayo & $H_0$ verdadera & $H_A$ verdadera\\
\midrule
$H_0$ aceptada & Decisión correcta & Error tipo II o $\beta$\\
$H_0$ rechazada & Error tipo I o $\alpha$ & Decisión correcta\\
\bottomrule
\end{tabular}
\end{center}

Los cuadros en la diagonal representan decisiones correctas, si $H_0$ es verdadera entonces es aceptada y si $H_0$ es falsa entonces es rechazada por nuestro ensayo. Los otros dos cuadros representan los errores tipo I y tipo II. El error tipo I, también conocido como error $\alpha$ o significancia representa el error cometido cuando la hipótesis nula es descartada siendo verdadera. El error tipo II, también llamado $\beta$, representa el error cometido cuando $H_0$ es falsa pero no es rechazada por nuestro estudio.

El nivel de aceptación del error tipo I es convencionalmente fijado en $0.05$, como se dijo anteriormente. Esto significa que aceptamos el hecho que nuestro ensayo tiene un $5\%$ de probabilidad de rechazar la hipótesis nula cuando la deberíamos aceptar. El error tipo II normalmente recibe menos atención en la teoría de la estadística porque históricamente se consideró menos serio fallar aceptando algo falso que rechazar algo verdadero (error $\alpha$). Los niveles convencionalmente aceptados de error tipo II son $\beta = 0.1$ o $\beta = 0.2$. Si se toma $\beta = 0.1$, significa que hay un $10\%$ de probabilidad que nuestro ensayo acepte una hipótesis nula falsa.

Para entender porque suele ser más relevante el error $\alpha$ que el $\beta$ vamos a volver a nuestro ejemplo de los tratamientos para hipertensión. Para la empresa que está realizando el estudio es importante que si afirma que su droga es mejor que la competencia lo sea. Es por eso que se fija un error $\alpha$ más bajo que el $\beta$. Dado que la hipótesis nula significa que la nueva existente es superior a la nueva, es importante saber que de ser rechaza, es muy poco probable que se esté tomando la decisión incorrecta.

El recíproco del error tipo II, definido como $1-\beta$ es llamado ``potencia
del ensayo'' \cite{Janssen} y será tratado en profundidad en
\S\ref{sec:potencia}. La importante de definir un nivel apropiado de potencia se
ha vuelto más apreciable en los últimos años. Particularmente en el campo
medicinal. Los investigadores (y las agencias que proveen el presupuesto) se
volvieron más preocupados por la potencia del ensayo, y por consiguiente con el
error tipo II, en parte porque no quieren gastar tiempo y esfuerzo realizando
estudios a menos que tengan una alta probabilidad de encontrar resultados
significativos.

\subsection{Nivel de probabilidad, o \textit{valor-p}}
\label{sec:valor_p}
Es un hecho de la vida que cuando se trabajo con inferencia estadística se está tratando de estimar algo que no se puede medir directamente. Por ejemplo, no tenemos la posibilidad de recolectar los datos de todos los adultos hipertensos del mundo, pero podemos seleccionar una muestra de esta población, diseñar un experimento y analizar la información que si podemos recolectar. Como entendemos que el error de muestreo es siempre una posibilidad en los estudios basados en muestras queremos saber que el resultado que obtuvimos no fue por una cuestión de azar. Si tenemos los medios de tomar muestras reiteradas veces y repetir el ensayo, ¿cuán probable es obtener siempre resultados similares?

Un valor de probabilidad, o \textit{valor-p} usualmente representa la probabilidad que resultados por lo menos tan extremos como los obtenidos en una muestra fueron debidos al azar. La frase ``por lo menos tan extremos'' es necesaria porque la mayoría de los ensayos estadísticos implican comparan su estadística contra una distribución hipotética (como la normal, por ejemplo) donde los valores más cercanos al centro de la distribución son más comunes y se vuelven menos probables a medida que nos alejamos del centro. Esto se puede ver más claro analizando otro ejemplo.

Supongamos que estamos realizando un experimento arrojando una moneda que creemos no fue modificada, es decir, cara o ceca tienen la misma probabilidad en una tirada cualquiera. Podemos expresar esto formalmente como $P(CA) = P(CE) = 0.5$, y llamamos a cada tirada una prueba. Esperamos que en 10 pruebas vamos a tener 5 caras, aunque sabemos que de un ser particular de 10 pruebas, podemos tener un número diferente de caras. Entonces, tiramos la moneda 10 veces, pero obtenemos 8 caras. Queremos saber cual es el valor-p para este resultado. Es decir, ¿cuán probable es que una moneda con probabilidad de $0.5$ para cara y ceca en una sola corrida de 10 tiradas, de 8 caras? Usando una tabla de la distribución binomial o una aplicación de computadora, encontramos que la probabilidad de este resultado es $0.0439$, lo que significa que menos del $5\%$ de las veces vamos a esperar 8 caras en 10 tiradas con una moneda ``justa''. La probabilidad para 9 caras es $0.0098$ y para 10 es de $0.0010$. Esto demuestra que a medida que los resultados se alejan del resultado esperado (5 de 10), se vuelve cada vez menos probable.

Si estamos evaluando la probabilidad de que la moneda sea realmente justa, resultados que se alejan de nuestra expectativa nos dan una fuerte evidencia para decidir si la moneda es justa o no. Por esta razón, es usual calcular la probabilidad no sólo del resultado obtenido en nuestro experimento, pero de resultados por lo menos tan extremos como el que obtuvimos. En este caso, la probabilidad de obtener 8, 9 o 10 caras en 10 tiradas es de $0.0547$. Este es el valor-p de obtener por lo menos 8 caras en 10 tiradas.

Los valores-p son siempre reportados junto con los resultados de un ensayo estadístico, en parte porque la intuición es una guía muy pobre para determinar cuan inusual un resultado particular es. Por ejemplo, muchas personas podrían pensar que es inusual obtener 8 o más caras en 10 tiradas de monedas, pero en este caso, la probabilidad binomial del resultado es de $0.0547$. Este resultado no nos permite rechazar la hipótesis nula y afirmar que la moneda fue modificada si seguimos la regla de adoptar un error $\alpha = 0.05$ para que el resultado sea considerado significante.

\subsection{Potencia de un ensayo}
\label{sec:potencia}

Esta sección trata con la teoría de la potencia del ensayo y el tamaño de la muestra tomada y presenta algunos ejemplos. Los cálculos de potencia y tamaño de muestra son frecuentemente simples, pero también son específicos, cada tipo de ensayo usa su fórmula. Listas de estas fórmulas se pueden encontrar en libros de estadística.

La inferencia estadística siempre introduce la posibilidad de hacer la decisión incorrecta, ya que utiliza cálculos en una muestra para hacer conclusiones de una población. Como se discutió anteriormente, hay dos tipos de errores en la inferencia estadística:
\begin{enumerate}
\item Error tipo I, o $\alpha$, cuando se rechaza incorrectamente la hipótesis nula.
\item Error tipo II, o $\beta$, cuando no se rechaza la hipótesis nula, siendo que era falsa.
\end{enumerate}

La potencia de un ensayo es $1-\beta$ y es la probabilidad de aceptar la hipótesis nula cuando la deberías rechazar. A todos nos gustaría tener siempre una potencia elevada en todos los ensayos, pero consideraciones prácticas, en particular, costo y disponibilidad de datos, nos fuerzan a un compromiso. Una regla a tener en cuenta es que se debe tener por lo menos una potencia de por lo menos $80\%$, es decir, tener un $80\%$ de probabilidad de encontrar resultados significativos en una muestra si existen en la población. Esto significa que el $20\%$ del tiempo, no se va a poder encontrar resultados significativos cuando se debería. También es usual definir una potencia del $90\%$.

Hay cuatro factores principales que afectan la potencia del ensayo:
\begin{enumerate}
\item El error $\alpha$. Aumentar $\alpha$ aumenta la potencia.
\item Diferencia en el resultado entre las poblaciones. Cuanto más distintos sean, mayor es la potencia.
\item Variabilidad. Poca variabilidad aumenta la potencia.
\item Tamaño de muestra. Aumentar el tamaño aumenta la potencia.
\end{enumerate}

Un cambio en cualquiera de estos factores, mientras se mantiene constantes los otros, va a modificar la potencia del ensayo. El nivel $\alpha$ se suele fijar en $0.05$ o menos. La diferencia entre los valores producidos por las distintas poblaciones se puede aumentar seleccionando poblaciones más diferentes entre ellas. La variabilidad se puede reducir tomando grupos de muestras muy selectas, por ejemplo, personas todas de la misma edad.

Eso nos deja con el número de muestras. El único factor fácilmente controlable por el experimentador durante las etapas de diseño del ensayo. Manteniendo las demás variables, aumentar el número de muestras, aumenta la potencia del ensayo. Sin embargo, aumentar la cantidad de muestras siempre implica mayor costo o esfuerzo, por lo que se debe buscar un compromiso entre la potencia deseada y el costo de ensayo, para no recolectar más información que la necesaria.

\begin{figure}
 \centering
 \includegraphics{./img/potencia_ensayo.jpg}
 % potencia_ensayo.jpg: 1120x512 pixel, 300dpi, 9.48x4.33 cm, bb=0 0 269 123
 \caption{Diagrama de potencia para dos poblaciones distribuidas normalmente.}
 \label{fig:potencia}
\end{figure}

La figura \ref{fig:potencia} ilustra los aspectos del cálculo de la potencia de un ensayo en el que hipótesis nula es que la media de una población en 100 mientras que la hipótesis alternativa es que la media es 115. En esta figura, la población de la izquierda (gris claro) es la población nula, que representa la distribución de población si la hipótesis nula es cierta. La distribución de al derecha (gris oscuro) es la de la población alternativa. Los cálculos de potencia son siempre realizados con respecto a una hipótesis alternativa en particular. En este caso, no es simplemente que la media sea mayor que 100, sino que sea 115. Para simplificar el ejemplo, se toman ambas poblaciones con un desvío estándar igual a 15. La hipótesis que se está probando es de 1 cola, por lo que se define un único valor crítico, representado por una linea punteada. Si la media de la muestra es superior a este nivel, la hipótesis nula se rechaza. Este nivel fue seleccionado según la hipótesis nula, que tiene una media de 100 y un desvío de 15: si se toma un nivel de significancia $\alpha = 0.05$, por el $95\%$ de la población nula vive a la izquierda de $112.5$ y sólo el $5\%$ está a la derecha.

El área de la población nula sobre el nivel de decisión representa el error tipo I cuando la hipótesis nula es verdadera, que en este ejemplo es $0.05$. El área de la población alternativa a la izquierda de dicho nivel, representa $\beta$ o error tipo II cuando la hipótesis alternativa es verdadera. Finalmente, el área de la población alternativa a la derecha del nivel es la potencia del ensayo para esta hipótesis nula específica. Representa la probabilidad de que si la hipótesis alternativa es verdadera (la media es 115), la media de la muestra tomada va a ser superior a $112.5$.

Ahora analicemos como se modificaría la potencia del ensayo variando los cuatro parámetros antes enumerados.
\begin{enumerate}
\item Si aumentamos $\alpha$ a $0.10$, por ejemplo, el nivel de decisión sería menor (más a la izquierda) y la potencia sería mayor, ya que disminuye el área a debajo del punto crítico, disminuyendo el error tipo II.
\item Si se toma una población alternativa con media 120 en lugar de 115, las poblaciones estarían más separadas, por lo que disminuye el error tipo II y aumenta la potencia del ensayo.
\item Si se disminuye el desvío estándar, las dos poblaciones se solaparían menos. Esto también provoca un aumento de la potencia del ensayo.
\item Finalmente, aumentar el tamaño de la población va a tener el mismo efecto que disminuir el desvío, aumentando la potencia.
\end{enumerate}

Una forma de familiarizarse con la influencia de los diferentes factores en la
potencia del ensayo es experimentando con una calculadora de potencia gráfica.
Una de estas se puede encontrar en \cite{calculadora_web}.

\chapter{Introducción a los estimadores de cuadrados mínimos}
\label{sec:cuadrados}

Supongamos que tenemos dos variables aleatorias $X$ e $Y$ con una función de densidad de probabilidad conjunta conocida $f_{x,y}$. Asumamos que en un experimento en particular la variable $Y$ se puede medir y toma el valor $y$. ¿Qué podremos decir acerca del valor $x$, correspondiente a la variable no observable $X$?

Supongamos que se tiene un estimador $\hat{x}$ del valor de $X$ cuando $Y=y$, según la fórmula:
\[
 \hat{x}=h(y)
\]
Si $x$ es el valor real de $X$ cuando $Y=y$, $h(.)$ es una función específica, podemos escribir:
\[
 e=x-\hat{x}=x-h(y)
\]
donde e es el error de nuestro estimador. No podemos pretender hacer $e=0$ para todo valor de $Y$ ya que $X$ e $Y$ son variables aleatorias. Por lo tanto todo lo que podemos intentar es elegir $h(.)$ de forma tal de minimizar el valor esperado (en promedio) de alguna función de ese error. Por ejemplo, el valor absoluto ($|e|$) o el cuadrado del error ($|e|^2$).

La elección de la función de error apropiada no es obvia y debe depender en el
problema que se quiera resolver. Dada la gran cantidad de opciones es llamativo
que el criterio del cuadrado del error sea el más estudiado. Los estimadores
resultantes de este criterio se van a llamar de cuadrados mínimos
(l.s.\footnote{l.s.: del inglés \textit{least-squares}}) aunque otros nombres
son usados, especialmente error cuadrático medio mínimo
(m.m.s.e\footnote{m.m.s.e.: del inglés minimun-mean-square-error}).

Algunas de las razones para estudiar estos estimadores son:
\begin{itemize}
 \item Los estimadores l.s. se pueden expresar como una media condicional.
\item Para variables aleatorias gaussianas el estimador l.s. es una función
lineal de las observaciones, por lo que goza de facilidades de cómputo e
implementación. Asimismo, para estas variables el estimador l.s. esencialmente
determina el estimador óptimo para una función de error arbitraria.
\item Estimadores l.s. subóptimos son relativamente fáciles de obtener. En particular, si están expresados como funciones lineales de las observaciones. En esos casos sólo dependen de estadística de primer y segundo orden (media y varianza) de las variables aleatorias involucradas.
\end{itemize}

\section{Estimadores l.s. - Propiedades básicas}

\subsection{Estimadores l.s. como esperanzas condicionales}

Como se dijo anteriormente, los estimadores l.s. se pueden escribir como
esperanzas condicionales y ahora vamos a mostrar que el estimador l.s. de una
variable $X$ dada otra variable $Y$ es la esperanza condicional de $X$ dado $Y$,
escrita $E[X/Y]$.

Supongamos que cuando $Y=y$ el valor real de $X$ es $x$ y que nuestro estimador es $h(y)$, entonces el error es $x-h(y)$, y ocurre con una probabilidad $f_{X,Y}(x,y)\ dx\ dy$. Por lo tanto el error cuadrático medio (m.s.e.) es:

\begin{equation}
m.s.e. = \iint f(X,Y)(x,y)[x-h(y)]^2\ dx\ dy
\label{eq:mse}
\end{equation}

y esto es lo que tenemos que minimizar para obtener la función $h(.)$. Para esto vamos a usar la descomposición $f_{X,Y}(x,y)=f_{X/Y}(x/y)f_{Y}(y)$ donde $f_{X,Y}(x/x)$ es la función de densidad de probabilidad condicional de $X$ dado $Y$ y $f_Y()$ es la función de densidad de probabilidad marginal de $Y$. Reescribimos \ref{eq:mse} y obtenemos:
\begin{equation}
 mse = \int f_{Y}(y)\ dy \left\{ \int f_{X/Y}(x/y)[x-h(y)]^2\ dx \right\}
\end{equation}

como $f_Y(y)$ es siempre una función no negativa, es claro que para minimizar el $m.s.e.$ vamos a tener que minimizar el término entre llaves. Se puede notar de muchas formas, por ejemplo derivando o mirando la ecuación como el momento de inercia de la ``masa'' $f_{X/Y}(x/y)$ en el punto $h(y)$. Una forma rápida pero desprolija es la siguiente:
\begin{eqnarray}
\int f_{X/Y}(x/y) [x-h(y)]^2\ dx = \int x^2 f_{X/Y}(x/y)\ dx\\
+h^2(y)\int f_{X/Y}(x/y)\ dx\ - 2h(y)\int x f_{X/Y}(x/y)\ dx
\end{eqnarray}
Se puede ver que el segundo término es 1 ya que se está integrando $f_{X/Y}$ en todo $X$. Si se deriva la ecuación anterior en $h(y)$ y se iguala a cero, se obtiene:
\begin{equation}
 2h(y)\ - 2\int x f_{X/Y}(x/y)\ dx = 0 \\
\end{equation}
Por lo que la  $h(y)$ que minimiza la ecuación \ref{eq:mse} es:
\begin{equation}
 h(y) = \int x f_{X/Y}(x,y)\ dx
\end{equation}
que por definición es la esperanza condicional $E[X/Y]$, por lo que podemos decir que $h(y)=E[X/Y]$. Como $Y$ es una variable aleatoria, no podemos saber que valor va a asumir en una realización en particular. Por lo tanto, el mejor estimador también es una variable aleatoria que llamaremos $\hat{X}$,
\begin{equation}
 \hat{X} = h(Y) = E[X/Y]
\end{equation}

\subsection{Estimación dada varias variables aleatorias}
Supongamos ahora que queremos estimar el valor de la variable $X$ pero dada la observación de dos variables aleatorias $Y_1$ e $Y_2$. Si hacemos el mismo planteo que antes:
\begin{equation}
mse=\iint [x-h(y_1,y_2)]^2 f_{X,Y_1,Y_2}(x/y_1,y_2)\ dx\ dy_1\ dy_2
\end{equation}
podemos decir que se va a minimizar con:
\begin{equation}
 h(y_1,y_2)=\int xf_{X/Y_1,Y_2}(x/y_1,y_2)\ dx = E[X/Y_1=y_1, Y_2=y_2]
\end{equation}
Por lo tanto:
\begin{equation}
 \hat{X} = h(Y_1,Y_2) = E[X/Y_1,Y_2]
\end{equation}
Es claro que el mismo razonamiento, pero con una notación más compleja va a servir para deducir que el estimador l.s. de $X$ dado ${Y_1, Y_2, \ldots, Y_n}$ es:

\begin{equation}
\hat{X} = E[X/Y_1,\ldots,Y_n]
\end{equation}

\chapter[Desarrollo del estimador EM]{Continuación del desarrollo del estimador
EM.}
\label{desarrollo_derivada}

En este apéndice se desarrolla en forma detallada una derivada matricial necesaria para aplicar el algoritmo EM en le canal propuesto. Esta explicación complemente la sección \S\ref{seq:aplicacion_al_canal}.

\begin{eqnarray*}
  A &=& \left( Y-HX \right)^T \Sigma^{-1} \left( Y-HX \right) \\
    &=& \left( Y-HX \right)^T \Sigma^{-\frac1{2}} \Sigma^{-\frac1{2}}
\left( Y-HX \right) \\
    &=& \left[ \Sigma^{-\frac1{2}} \left( Y-HX \right) \right]^T \left[
\Sigma^{-\frac1{2}} \left( Y-HX \right) \right] \\
    &=& B^T B
\end{eqnarray*}

Si tenemos la forma cuadrática $x^TCx$, siendo $x$ un vector columna y $C$ una
matriz cuadrada, la derivada de esta función con respecto a $x$ es la
siguiente\cite[p. 176]{magnus}:
\begin{equation}
\phi(x) = x^TCx \Longrightarrow \frac{\partial\phi(x)}{\partial x}
= x^T \left( C+C^T \right)
\label{cuad}
\end{equation} 

Usando la ecuación \ref{cuad}, podemos calcular la derivada de $A$.
\begin{equation}
\frac{\partial A}{\partial B} = \frac{\partial}{\partial B}
\left( B^T B \right) = 2B^T
\label{deriv_a}
\end{equation}

Ahora derivemos al vector $B$ con respecto a la matriz $H$ del canal.
\begin{eqnarray}
\frac{\partial B}{\partial H} &=& \frac{\partial}{\partial H}
\left[ \Sigma^{-\frac1{2}} \left( Y-HX \right) \right] \nonumber \\
 &=& \frac{\partial}{\partial H} \left( \Sigma^{-\frac1{2}} Y -
\Sigma^{-\frac1{2}} HX \right) \nonumber \\
 &=& -\frac{\partial}{\partial H} \left( \Sigma^{-\frac1{2}} HX \right) \nonumber \\
 &=& -\Sigma^{-\frac1{2}} \frac{\partial}{\partial H} \left( HX \right)
\label{deriv_b}
\end{eqnarray}

Si tenemos una función vectorial $f(X)=Xa$, siendo $X$ una matriz y $a$ un vector
columna, la derivada de la función es la siguiente\cite[p. 181]{magnus}:
\begin{equation}
f(X) = Xa \Longrightarrow Df(X) = a^T \otimes I \label{matr}
\end{equation}

Si ahora usamos la ecuación \ref{matr} en \ref{deriv_b} resulta:
\begin{equation}
\frac{\partial B}{\partial H} = -\Sigma^{-\frac1{2}} \ \left( X^T\!\otimes I
\right) \label{deriv_b2}
\end{equation}

Juntando las ecuaciones \ref{deriv_a} y \ref{deriv_b2} podemos obtener la
derivada total de la función $A(H)$.
\begin{eqnarray}
\frac{\partial A}{\partial H} &=& \frac{\partial A}{\partial B} \
\frac{\partial B}{\partial H} \nonumber \\
 &=& -2 \ (Y\!-\!H\!X)^T \ \Sigma^{-1} \ \left( X^T\!\otimes I \right)
\nonumber \\
 &=& -2 \ \left[ \underbrace{Y^T \ \Sigma^{-1} \left( X^T\!\otimes I \right)}_{L}
- \underbrace{X^T H^T \ \Sigma^{-1} \left( X^T\!\otimes I \right)}_{G} \right]
\label{deriv_kro}
\end{eqnarray}

Escribamos el desarrollo del primer término de la función anterior (término L).
\begin{eqnarray}
Y^T \ \Sigma^{-1} \left( X^T\!\otimes I \right) &=&
\left[ \begin{array}{cc}
 y_1 & y_2
\end{array} \right]
\left[ \begin{array}{cc}
         \sigma_{n_1}^{-1} & 0 \\
          0 & \sigma_{n_2}^{-1}
\end{array} \right]
\left[ \begin{array}{cccc}
         x_1 & 0 & x_2 & 0 \\
         0 & x_1 & 0 & x_2
\end{array} \right] \nonumber \\
 &=& \left[ \begin{array}{cc}
 y_1 \sigma_{n_1}^{-1} & y_2 \sigma_{n_2}^{-1}
\end{array} \right]
\left[ \begin{array}{cccc}
         x_1 & 0 & x_2 & 0 \\
         0 & x_1 & 0 & x_2
\end{array} \right] \nonumber \\
 &=& \left[ \begin{array}{cccc}
          y_1 \sigma_{n_1}^{-1}x_1 & y_2 \sigma_{n_2}^{-1}x_1 &
          y_1 \sigma_{n_1}^{-1}x_2 & y_2 \sigma_{n_2}^{-1}x_2
\end{array} \right] \label{L1}
\end{eqnarray}

En la tabla sobre derivadas de la referencia\cite[p. 176]{magnus}, encontramos
que al derivar una función escalar en función de una matriz, como es el caso de
$\frac{\partial A}{\partial H}$ tenemos las siguientes acepciones:
\begin{displaymath}
\phi(X) = \left\lbrace \begin{array}{ll}
 D\phi(X) = (\textrm{vec} C)^T & \textrm{lo llama \textit{derivative}} \\
 \frac{\partial \phi(X)}{\partial X} = C & \textrm{denominado \textit{other uses}}
\end{array} \right.
\end{displaymath}

Como nosotros queremos que la derivada nos quede con las mismas dimensiones que
la matriz $H$ del canal, acomodamos los elementos de la matriz de la ecuación
\ref{L1} teniendo en cuenta lo anterior.
\begin{equation}
L \equiv \left[ \begin{array}{cc}
                 y_1 \sigma_{n_1}^{-1}x_1 & y_1 \sigma_{n_1}^{-1}x_2 \\
                 y_2 \sigma_{n_2}^{-1}x_1 & y_2 \sigma_{n_2}^{-1}x_2
                \end{array} \right] = \Sigma^{-1} Y X^T \label{L2}
\end{equation}

Ahora desarrollemos el término G de la ecuación \ref{deriv_kro}.
\begin{eqnarray}
X^T H^T \ \Sigma^{-1} \left( X^T\!\otimes I \right) &=&
\left[ \begin{array}{cc}
        x_1 & x_2
       \end{array} \right]
\left[ \begin{array}{cc}
        h_{11} & h_{21} \\
        h_{12} & h_{22}
       \end{array} \right]
\left[ \begin{array}{cc}
        \sigma_{n_1}^{-1} & 0 \\
        0 & \sigma_{n_2}^{-1}
       \end{array} \right]
\left[ \begin{array}{ccc}
        x_1I & \vdots & x_2I
       \end{array} \right] \nonumber \\
 &=& \left[ \begin{array}{cc}
\sigma_{n_1}^{-1}x_1(x_1h_{11}+x_2h_{12}) & \sigma_{n_2}^{-1}x_1(x_1h_{21}+x_2h_{22})
\end{array} \right. \ldots \nonumber \\
 & & \quad \left. \begin{array}{cc}
\sigma_{n_1}^{-1}x_2(x_1h_{11}+x_2h_{12}) & \sigma_{n_2}^{-1}x_2(x_1h_{21}+x_2h_{22})
            \end{array} \right] \label{G1}
\end{eqnarray}

Del mismo modo que en el caso anterior, acomodamos los elementos de la matriz
de la ecuación \ref{G1}.
\begin{eqnarray}
G &\equiv& \left[ \begin{array}{cc}
\sigma_{n_1}^{-1}x_1(x_1h_{11}+x_2h_{12}) & \sigma_{n_1}^{-1}x_2(x_1h_{11}+x_2h_{12}) \\
\sigma_{n_2}^{-1}x_1(x_1h_{21}+x_2h_{22}) & \sigma_{n_2}^{-1}x_2(x_1h_{21}+x_2h_{22})
                \end{array} \right] \nonumber \\
 &=& \Sigma^{-1} H X X^T \label{G2}
\end{eqnarray}

Juntando las reformulaciones de los términos L y G (ecuaciones \ref{L2} y \ref{G2})
obtenemos la derivada de la función $A$ con las mismas dimensiones de la matriz
del canal.
\begin{equation}
\frac{\partial A}{\partial H} = -2\left( \Sigma^{-1} Y X^T - \Sigma^{-1} H X
X^T\right)
\label{derivada}
\end{equation}


\chapter{Estadística robusta}
\label{sec:es_robusta}

Es estadística, los métodos clásicos se basan fuertemente en hipótesis que
suelen no cumplirse en la práctica. En particular, se suele asumir de un grupo
de datos una distribución normal (o gaussiana), por lo menos aproximadamente. O
que se puede aplicar aplicar el teorema central del límite para justificar la
normalidad de una población. Desafortunadamente, cuando hay valores
discordantes\footnote{En inglés \textit{outliers}.} entre las muestras
disponibles, los métodos clásicos suelen tener muy bajo rendimiento. La
estadística robusta trata de proveernos con métodos que emulen los métodos
clásicos pero que no sean tan sensibles a la presencia de este tipo de datos
\cite{Maronna}.

Para poder cuantificar cuan robusto es un método, primero debemos definir algunas medidas de robustez, las más comunes siendo el punto de quiebre\footnote{En inglés, \textit{breakdown point}.} y la función de influencias, se describen a continuación.

\section{Punto de quiebre}

Intuitivamente, el punto de quiebre de un estimador es la proporción de observaciones incorrectas (por ejemplo, observaciones arbitrariamente más grandes que las demás) que un estimador puede soportar antes de dar un valor arbitrariamente más grande.

Por ejemplo, dadas $n$ variables aleatorias independientes $(X_1,\dots,X_n)\sim\mathcal{N}(0,1)$ y las correspondientes realizaciones $x_1,\dots,x_n$, podemos usar
\[\overline{X}_n:=\frac{X_1+\cdots+X_n}{n}\]
como estimador de la media. Este estimador tiene un punto de quiebre de $0$, ya que podemos hacer $\overline{x}$ arbitrariamente grande cambiando sólo un valor de los $x_n$.

Cuanto más alto sea el punto de quiebre de un estimador, más robusto es. Se puede intuir que el punto de quiebre no puede superar el $50\%$ porque si más de la mitad de las observaciones están contaminadas, no es posible distinguir entre la distribución real y las observaciones discordantes. Por lo tanto el mayor punto de quiebre es $0,5$ y hay estimadores que lo logran. Un ejemplos la mediana.

\section{Estimadores M}

Los estimadores M forman un grupo amplio de estadísticas obtenidas de la solución del problema de minimizar ciertas funciones de las datos.

Algunos autores definen los estimadores M como las raíces de un sistema de ecuaciones que surge de ciertas funciones de los datos. Usualmente estas funciones son las derivadas de las funciones que se quieren minimizar en la definición anterior.

Muchas estadísticas clásicas son estimadores M. Su principal utilidad, sin embargo, es crear alternativas robustas a los estimadores clásicos.

\subsection{Motivación}

Para una familia de funciones de densidad de probabilidad $f$ parametrizada por $\theta$, el estimador de máxima verosimilitud de $\theta$ se computa maximizando la función de verosimilitud en $\theta$. Es estimador es:

\[\widehat{\theta} = \operatorname{argmax}_{\theta}{ \left( \prod_{i=1}^n f(x_i, \theta) \right) }\]

o lo que es lo mismo,

\[\widehat{\theta} = \operatorname{argmin}_{\theta}{ \left( -\sum_{i=1}^n \log{( f(x_i, \theta) ) }\right)}\]

El desempeño de los estimadores de máxima verosimilitud depende fuertemente de
que la distribución de probabilidad asumida para las muestras sea
aproximadamente verdad. En particular, los estimadores máxima verosimilitud
pueden ser ineficientes y sesgados cuando los datos no son de la distribución
asumida. Otro factor que afecta a estos estimadores es la presencia de datos
discordantes.

\subsection{Definición}

En 1964, Peter Huber propuso generalizar la estimación máxima verosimilitud a la minimización de 
\[\sum_{i=1}^n\rho(x_i, \theta)\]
donde $\rho$ es una función con ciertas propiedades que se describen luego.

Las soluciones,
\[\hat{\theta} = \operatorname{argmin}_{\theta}\left(\sum_{i=1}^n\rho(x_i, \theta)\right) \,\!\]
son llamados estimadores M (``M'' por ser del tipo de máxima verosimilitud\footnote{Huber, 1981, página 43}). Otros tipos de estimadores robustos incluyen los estimadores L, los R y los S. Por lo tanto, los estimadores de máxima verosimilitud son un caso especial de los estimadores M.

La función $\rho$ o su derivada $\psi$ se pueden elegir de forma tal de proveer al estimador propiedades deseables (en términos de sesgo y eficiencia) cuando los datos son realmente de la distribución asumida y asegurar un comportamiento ``no muy malo'' cuando los datos pertenecen a una distribución diferente.

Por lo tanto, los estimadores M son los $\theta$ que minimizan:
\[\sum_{i=1}^n\rho(x_i,\theta)\]
Para minimizar esta ecuación usualmente es más sencillo derivarla respecto de $\theta$ y buscar las raíces. Cuando esta derivada existe se lo suele llamar un estimador M de tipo $\psi$, de no existir se los conoce como de tipo $\rho$.

En la mayoría de los casos prácticos, los estimadores son M son de tipo $\psi$.

\begin{figure}
 \centering
 \includegraphics[width=0.8\textwidth]{./img/ejemplos-rho.pdf}
 % ejemplos-rho.pdf: 347x244 pixel, 72dpi, 12.24x8.61 cm, bb=
 \caption{Ejemplos de diferentes funciones $rho$.}
 \label{fig:ej_rho}
\end{figure}

\subsection{Cálculo}

Para muchas $\rho$ o $\psi$, no se puede encontrar una forma cerrada para
resolver el estimador. Por lo tanto se emplean métodos iterativos de cálculo. Es
posible usar alguno de los algoritmos estándar como el de Newton-Raphson. Sin
embargo, en muchos casos se suele usar un método de regresión por cuadrados
mínimos con pesos variables.

Para algunos $\psi$, en particular aquellas donde $\lim_{x\rightarrow\pm\infty}
\psi(x) = 0$ (llamadas \textit{redescending}), la solución puede no ser única.
Por lo tanto se debe tener cuidado de seleccionar buenos valores iniciales. Por
ejemplo, se puede usar la mediana como un estimador de posición y el desvío
mediano absoluto (MAD) como un estimador de escala.

\subsection{Ejemplos}

Cómo se explicó anteriormente un estimador M no es necesariamente robusto. Lo
va a ser o no, dependiendo de la función $\rho$ que se use. Algunos ejemplos de
funciones $\rho$ que producen estimadores robustos se muestran graficados en la
figura \ref{fig:ej_rho}.

