\chapter{Discusión de resultados}
\label{sec:resultados}

\section{Validación ciega de modelos}

En la sección \S\ref{sec:ensayo_bondad} se explicó el método de validación que
se utiliza en este trabajo. Éste consiste en calcular los residuos que se
obtienen entre la señal recibida y la señal detectada utilizando la ganancia de
canal estimada. De ser correcta la estimación, estos residuos deberían tener una
distribución estadística cercana a la del ruido aditivo del canal. Es decir, ser
normal de media nula y varianza $\varSigma$.

Para probar si la distribución de los residuos se ajusta o no a la del ruido se
emplea el test de bondad de ajuste Kolmogorov-Smirnov, en su variante
multivariable explicada en \S\ref{sec:ks_multi}. En particular, se implementa el
algoritmo de complejidad reducida para variables bidimensionales descripto en
\S\ref{sec:implementacion_ks}.

\subsection{Prueba de la validez del método}

Como se explica al comienzo de esta sección, se usan las propiedades
estadísticas de los errores de estimación\footnote{Residuos $\varepsilon$,
descriptos en \S\ref{sec:calc_res}} para verificar la validez de una
identificación. Si $\varepsilon$ tiene una distribución cercana a la del ruido
$N$ (simbolizado como $\varepsilon \cong N$) se decide correcta la estimación.

Se observa que se parte de una implicación correcta (Si $\hat H \hat X = HX
\Rightarrow \varepsilon \cong N$) y se postula $\varepsilon \cong N
\Rightarrow \hat H = H$. Esta implicación no es necesariamente cierta, ya que no
cualquier realización de una variable de igual distribución que $N$ proviene de
$\hat H = H$. Para probar la validez de este análisis se realizaron
simulaciones donde se fijó un $\hat H \neq H$ y se contó cuántas veces se
obtiene $\varepsilon \cong N$. Se probó numéricamente que en el $99.9\%$ de las
veces $\hat H \neq H \Rightarrow \varepsilon \ncong N$.

Así mismo, se evaluó la capacidad de detectar las estimaciones correctas. Para
probarlo se reemplazó el valor de ganancias del canal calculado por el EM
por $\hat H = H$ y se analizó el resultado. Sólo fue invalidado el $18\%$ de las
identificaciones.

Es importante aclarar que estos casos no representan una identificación correcta
o una incorrecta ya que al forzar un valor de $\hat{H}$ que no se obtiene a
partir de las muestras se está perdiendo la correlación entre $\hat{H}$ y
$\hat{X}$. El objetivo del ensayo descripto anteriormente es probar la validez
de la hipótesis y no estimar el desempeño del algoritmo.

\section{Desempeño del algoritmo de validación}

\begin{figure}
	\begin{center}
	\includegraphics[width=0.8\textwidth]{img/grafico_errores.pdf}
	\end{center}
\caption{Análisis del desempeño del algoritmo.}
\label{img:histograma}
\end{figure}

Antes de poder analizar el desempeño del algoritmo propuesto es necesario
repasar algunos conceptos de estadística relacionados con el ensayo de
hipótesis.

Un ensayo de hipótesis puede cometer dos tipos de errores, tipo I y tipo
II. El error tipo I, representa el error cometido cuando la hipótesis nula es
descartada siendo verdadera. El error tipo II representa el error cometido
cuando la hipótesis nula es falsa pero no es rechazada por nuestro estudio. Es
importante notar que los errores no son complementarios, es decir la
probabilidad de que ocurra el error I (conocida como $\alpha$) no es $1$ menos
la probabilidad de que ocurra el error II (conocida como $\beta$). En el
apéndice \S\ref{sec:ensayo_de_hipotesis} se presenta un resumen de inferencia
estadística y se desarrollan con detalle los conceptos más relevantes para este
trabajo.
\begin{figure}
 \centering
 \includegraphics[width=0.8\textwidth]{img/grafico_ks_fig_error_cero.pdf}
 \caption{Valores de $D_{max}$ en función de $\vartheta$.}
 \label{img:ks_error}
\end{figure}

Para realizar este análisis se van a realizar diferentes simulaciones numéricas,
variando la relación señal a ruido de la transmisión y la cantidad de muestras
utilizadas para hacer la validación del canal. Para todas estas simulaciones se
van a mantener fijo el modelo de canal, la forma (constelación) de las señales
transmitidas y la cantidad de iteraciones del algoritmo EM, de forma tal de
poder comparar los resultados entre ellos. Se va a utilizar el modelo de canal
explicado en \S\ref{sec:sistema}. Los datos transmitidos son generados a partir
de una constelación ASK de 4 niveles, y se deja iterar $15$ veces el algoritmo
EM ya que se observó que es suficiente para que siempre se obtenga un valor
estable. Para todos los casos se realizaron $8000$ simulaciones, cantidad
suficiente para mostrar el comportamiento del algoritmo.

\subsection{Comportamiento general}
\begin{figure}
 \centering
 \includegraphics[width=.8\textwidth]{./img/multi_snr.pdf}
 % multi_snr.pdf: 346x247 pixel, 72dpi, 12.21x8.71 cm, bb=
 \caption{Rendimiento del validador con diferente SNR.}
 \label{fig:multi_snr}
\end{figure}

Primero se realizaron simulaciones del sistema descripto con $SNR=20dB$ usando
dos bloques independientes de $P=50$ muestras cada uno, uno para identificar el
canal y otro para realizar la validación del modelo. Se calculó el error
$\vartheta$ (definido en (\ref{eq:fig_error}) en la página
\pageref{eq:fig_error}) para cada simulación. Para graficar los resultados se
separaron las simulaciones cuyos resultados fueron aceptadas y las que
generaron resultados rechazados y se calcularon dos histogramas que
representan la cantidad de simulaciones que tuvieron un error $\vartheta$
determinado. El primero de los histogramas muestra la distribución del error
$\vartheta$ entre los modelos aceptados y el segundo muestra la misma curva para
los modelos rechazados. En la fig. \ref{img:histograma} se grafican ambos
histogramas superpuestos.

Utilizando los mismos resultados, en la fig. \ref{img:ks_error} se grafican los
valores de $D_{max}$ en función del error $\vartheta$ de las identificaciones
y el umbral de decisión $K$. Se observa que en la región de $\vartheta$
pequeños hay una baja cantidad de identificaciones rechazadas. La media de
$D_{max}$ aumenta a medida que aumenta $\vartheta$, sin embargo, también
aumenta la varianza de $D_{max}$, aceptando así identificaciones incorrectas.

En estos dos gráficos se puede ver que el algoritmo EM no produce una
distribución uniforme del error de estimación, sino que los resultados se
concentran en dos regiones alrededor de dos valores de $\hat{H}$: una de bajo
error y otra de alto error. Esta separación en grupos se explica analizando la
función de verosimilitud del problema, que tiene máximos locales en las zonas
donde se produce la mayor cantidad de estimaciones. Éste es un comportamiento
predecible considerando que Xu y Jordan, en \cite{jordan} probaron que el
algoritmo EM es de gradiente ascendente.

A partir de esta observación se definen dos cotas de error que distinguen una
identificación correcta de una incorrecta. Si $\vartheta < \vartheta_{B}$, con
$\vartheta_{B}=0.04$ corresponde a una identificación correcta. Si $\vartheta >
\vartheta_{M}$, con $\vartheta_{M}=0.1$ corresponde a una identificación
incorrecta.

Para las simulaciones aceptadas se observa la mayor frecuencia para errores
entre $0.01 < \vartheta < M_{A}$. En cambio en el caso de las rechazadas se
obtiene la mayor frecuencia para errores más altos, entre $M_{R} < \vartheta <
1.2$. Este análisis indica que el algoritmo propuesto es capaz de discernir
entre identificaciones correctas e incorrectas.

Con el objetivo de cuantificar el desempeño del algoritmo validador, se midió el
número de identificaciones correctas rechazadas y el número de identificaciones
incorrectas aceptadas. Del total de identificaciones cuyo $\vartheta > M_{A}$,
sólo $12.5\%$ es aceptado, y del total de las estimaciones cuyo $\vartheta <
M_{R}$, el $20.5\%$ es rechazado.

Es importante ver como se modifica el comportamiento del algoritmo variando la
relación señal a ruido. Para eso se tomaron 7 valores, a saber $5$, $10$, $15$,
$20$, $25$, $30$ y $35dB$ y se simularon $8000$ identificaciones de canal para
cada $SNR$. Para cada relación señal a ruido, se realizó el siguiente
procedimiento:
\begin{itemize}
\item Se separaron los modelos en aceptados ($C_{A}$) y rechazados ($C_{R}$)
por el validador.
\item Se contaron los modelos rechazados con error menor a $\vartheta_{B}$
($C_{RB}$).
\item Se calculó el error tipo I como $100 C_{RB}/C_{R}$, es decir, el
porcentaje de identificaciones correctamente identificadas rechazadas por el
validador.
\item Se contaron los modelos aceptados con error mayor a $\vartheta_{M}$
($C_{AM}$).
\item Se calculó el error tipo II como $100 C_{AM}/C_{A}$. es decir, el
porcentaje de identificaciones mal identificadas aceptadas por el
validador.
\end{itemize}

\subsection{Comportamiento a distintos $SNR$}
\begin{figure}
\centering
 \subfigure[Bien identificados aceptados]{
 \includegraphics[width=.40\textwidth]{img/bien-aceptado.pdf}
  \label{fig:pesos_bien_aceptado}
 }
 \subfigure[Bien identificados rechazados]{
 \includegraphics[width=.40\textwidth]{img/bien-rechazado.pdf}
  \label{fig:pesos_bien_rechazado}
 }
 \subfigure[Mal identificados aceptados]{
 \includegraphics[width=.40\textwidth]{img/mal-aceptado.pdf}
  \label{fig:pesos_mal_aceptado}
 }
 \subfigure[Mal identificados rechazados]{
 \includegraphics[width=.40\textwidth]{img/mal-rechazado.pdf}
  \label{fig:pesos_mal_rechazado}
 }
 \subfigure{
 \includegraphics[width=.80\textwidth]{img/leyenda_errores.pdf}
 }
 \caption{Distribución de los errores en los cuatro posibles escenarios.}
 \label{fig:pesos}
\end{figure}

Una vez obtenidos los valores de los errores tipo I y II para cada relación a
ruido, se graficó su evolución en \ref{fig:multi_snr}. Lo deseable sería que
estos valores sean lo más bajo posible, mejorando así el comportamiento del
sistema.

Antes de analizar el comportamiento, es importante notar que no se está
evaluando el validador independientemente. Se necesita el resultado del
estimador de canal para poder caracterizarlo. Por lo tanto, al variar la
relación señal a ruido se está evaluando el comportamiento de todo el sistema.
Se afecta al estimador de canal en forma directa, al mejorar las condiciones de
la transmisión, éste va a poder identificar mejor la matriz de ganancias. Por lo
tanto, se va a modificar la cantidad de estimaciones correctas, y es por eso que
siempre se va a trabajar en porcentajes de identificaciones y no usando la
cantidad de simulaciones simplemente. 

Así mismo, la potencia de ruido también influye en el validador al ser un
parámetro necesario para generar la distribución del ruido. Recordemos que se va
a comparar la distribución de los residuos contra la distribución del ruido. Los
residuos que utiliza el validador para decidir si acepta o no un modelo se
calculan como la diferencia entre la señal $Y$ recibida y la señal
$\hat{Y}=\hat{H}\hat{X}$ estimada. Si $\hat{H} = H - \Delta H$ y $\hat{X} = X -
\Delta X$, entonces la diferencia entre la señal recibida y la estimada se
puede escribir:

\begin{eqnarray}
 \varepsilon &=& Y - \hat{H} \hat{X}\nonumber\\
 &=& HX+N - (H-\Delta H)(X-\hat{X}) \nonumber\\
 &=& N + H\Delta X + \Delta H X - \Delta H \Delta X \nonumber\\
 &=& N + \Delta H X + \hat{H} \Delta X
 \label{eq:ymenosy}
\end{eqnarray}
Se observa que dentro de los residuos se están representando dos errores, el
error de estimación del canal y el error de detección de los símbolos.

Es interesante analizar la ecuación (\ref{eq:ymenosy}), ya que va a caracterizar
el comportamiento del validador. Con varianzas de ruido muy altas ($SNR=5dB$) se
va a cometer un error de detección elevado y no se va a poder identificar
correctamente el canal. Por lo tanto, se torna muy difícil distinguir entre el
ruido y el error acumulado de detección más estimación. Es por este motivo que
aunque la identificación sea buena, el error de detección invalida el modelo.
Así mismo, de estimar mal las ganancias del canal, sumado al alto error de
detección, hace que se acepten pocos modelos mal identificados, ya que el error
conjunto es muy elevado. Se concluye que con este valor de $SNR$ este método de
transmisión no se puede utilizar.

Al aumentar la relación señal a ruido, se observa como cambia el comportamiento
del sistema. A partir de $SNR=15dB$ se ve un cambio en la tendencia de los dos
tipos de errores. Para el error tipo I, se ve que al tener mejores condiciones
de canal, se pueden lograr mejores estimaciones de canal y detecciones.
Así mismo, baja la potencia de ruido, lo que dificulta distinguir entre una
identificación buena pero no perfecta y el ruido. Por otro lado, la baja
potencia de ruido hace más fácil distinguir identificaciones erróneas del ruido.

\begin{figure}
\centering
 \subfigure[Bien identificados aceptados]{
 \includegraphics[width=.40\textwidth]{img/suma-bien-aceptado.pdf}
  \label{fig:error_acum_bien_aceptado}
 }
 \subfigure[Bien identificados rechazados]{
 \includegraphics[width=.40\textwidth]{img/suma-bien-rechazado.pdf}
  \label{fig:error_acum_bien_rechazado}
 }
 \subfigure[Mal identificados aceptados]{
 \includegraphics[width=.40\textwidth]{img/suma-mal-aceptado.pdf}
  \label{fig:error_acum_mal_aceptado}
 }
 \subfigure[Mal identificados rechazados]{
 \includegraphics[width=.40\textwidth]{img/suma-mal-rechazado.pdf}
  \label{fig:error_acum_mal_rechazado}
 }
 \subfigure{
 \includegraphics[width=.80\textwidth]{img/leyenda_suma.pdf}
 }
 \caption{Comparación entre el error acumulado de detección más identificación
y el desvío del ruido para los cuatro posibles escenarios.}
 \label{fig:error_acum}
\end{figure}

Esta tendencia permanece a medida que aumenta la relación señal a ruido. El
error tipo I se mantiene mientras que el error tipo II baja hasta niveles
despreciables. Esto sugiere que para transmisiones en alta $SNR$, mayor a
$25dB$, se debería tomar otra cota de aceptación para el test de
Kolmogorov-Smirnov, de forma tal de disminuir el error tipo II.

Para facilitar la comprensión de este comportamiento, en la figura
\ref{fig:pesos} se grafica la evolución del error de identificación, el de
detección y la potencia de ruido a medida que aumenta la relación señal a
ruido. Se verifica que en los casos que el validador falla, tanto error de
estimación como el de detección son grandes, pero comparables con la varianza
de ruido.

En la figura \ref{fig:error_acum} se muestran las amplitudes de los errores
acumulados ($\Delta H X + \hat{H} \Delta X$) en relación con los desvíos del
ruido para $SNR=20dB$. La potencia de ruido se marca delimitando con lineas
horizontales el valor del desvío del ruido en cada canal. Se grafica para los 4
posibles casos: bien identificado aceptado (decisión correcta), bien
identificado rechazado (error tipo I), mal identificado aceptado (error tipo
II), mal identificado rechazado (decisión correcta). En ellas se ve que en los
casos donde el validador se equivoca, el error de estimación es comparable al
ruido, que es lo mismo que se dedujo de los gráficos anteriores.

\subsection{Comportamiento tomando distinto largo de bloques}

\begin{figure}
 \centering
 \includegraphics[width=0.8\textwidth]{./img/cant_muestras.pdf}
 % cant_muestras.pdf: 344x242 pixel, 72dpi, 12.14x8.54 cm, bb=
 \caption{Variación de los errores tipo I y II al cambiar el largo del bloque.
$SNR=20dB$}
 \label{fig:cant_muestras}
\end{figure}

Para completar el análisis del método presentado es necesario analizar su
funcionando con diferentes largos de bloques. Si bien se pretende que el diseño
tenga un rendimiento aceptable con bloques cortos, como en los resultados
mostrados anteriormente, mostrar que su comportamiento mejora al aumentar el
largo de bloque asegura que el método estadístico es válido.

Para probar el rendimiento del algoritmo validador utilizando distintos tamaños
de bloques se realizaron otras simulaciones donde se mantuvo constante la
cantidad de símbolos utilizados para la identificación del canal ($P=50$) pero
se modificó el lago del bloque que utilizó el validador. Se realizó de esta
forma para independizar los cambios de rendimiento del validador de las del
identificador.

Se analizó el comportamiento del algoritmo validador con bloques de $300$ y
$500$ muestras, ambos con $SNR=20dB$. Los valores usados para la constante de
aceptación del ensayo se encuentran en la página \pageref{pag:ks_valores}. Se
utilizó este nivel de señal a ruido para que se pueda apreciar fácilmente la
mejora del desempeño comparado con el bloque de $50$ muestras simulado
anteriormente. Para estas simulaciones se realizó el mismo procedimiento para
calcular la probabilidad de error tipo I y error tipo II que en el análisis de
variación del SNR. Los resultados se presentan primero en la tabla
\ref{tab:errores_cant_muestras} para mayor claridad. 

\begin{table}[h]
\begin{center}
\begin{tabular}{crrr}
\toprule
error & 50 muestras & 300 muestras & 500 muestras\\
\midrule
tipo I & $20.5\%$ & $28.2\%$ & $33.55\%$\\
tipo II & $12.5\%$ & $3.0\%$ & $1.57\%$ \\
\bottomrule
\end{tabular}
\end{center}
\caption{Comparación del desempeño usando diferentes cantidades de muestras.}
\label{tab:errores_cant_muestras}
\end{table}

Estos valores se prestan para una lectura equivocada. Se podría concluir
que se obtuvieron mejoras en el error tipo II a costa de empeorar el error tipo
I. Para probar que el rendimiento del algoritmo mejora al aumentar el número de
muestras, se modificó la cota de aceptación para el ensayo con $50$ muestras
para obtener un error tipo II = $3.3 \%$ (simular al obtenido con $300$
muestras). En esa simulación se obtuvo un error tipo I = $54.1\%$. Esto
demuestra que al aumentar el número de muestras el desempeño mejora y no es
simplemente que disminuyó un error para aumentar el otro.
\begin{figure}[htb]
\centering
 \subfigure[Bien identificados aceptados]{
 \includegraphics[width=.45\textwidth]{img/suma-bien-aceptado-300.pdf}
  \label{fig:error_acum_bien_aceptado_300}
 }
 \subfigure[Bien identificados rechazados]{
 \includegraphics[width=.45\textwidth]{img/suma-bien-rechazado-300.pdf}
  \label{fig:error_acum_bien_rechazado_300}
 }
  \subfigure[Mal identificados aceptados]{
 \includegraphics[width=.45\textwidth]{img/suma-mal-aceptado-300.pdf}
  \label{fig:error_acum_mal_aceptado_300}
 }
 \subfigure[Mal identificados rechazados]{
 \includegraphics[width=.45\textwidth]{img/suma-mal-rechazado-300.pdf}
  \label{fig:error_acum_mal_rechazado_300}
 }
 \subfigure{
 \includegraphics[width=.80\textwidth]{img/leyenda_suma.pdf}
 }
 \caption{Comparación entre el error acumulado para las para los cuatro posibles
escenarios usando 300 muestras.}
 \label{fig:error_acum_300}
\end{figure}

Estos valores también están representados en la figura \ref{fig:cant_muestras}.
Se aprecia como el error tipo II decae rápidamente al aumentar la cantidad de
muestras por bloque tomadas (sin modificar la relación señal a ruido). Esto se
debe a que se puede estimar mejor la CDF de las muestras utilizando mayor
cantidad de puntos.

También se observa que el error tipo I aumenta. Se tendrían que buscar mejoras
cotas de aceptación para lograr mantener más constante el error tipo II en
$5\%$ y así evitar el aumento del error tipo I. Sin embargo, el ensayo mostrado
en este trabajo es suficiente evidencia de la mejora de comportamiento al
aumentar la cantidad de muestras.

Para bloques de identificación de $300$ muestras, en la figura
\ref{fig:error_acum_300}, se puede comparar el error acumulado de estimación más
el de identificación contra la potencia de ruido y se ve que el algoritmo
funciona de igual manera, al tener errores cercanos al ruido lo rechaza y para
que lo acepte los errores tienen que ser menores que el desvío del ruido.
