% \documentclass[%
%         %draft,
%         %submission,
%         compressed,
%         %final,
%         %
%         %technote,
%         internal,
%         %submitted,
%         %inpress,
%         %reprint,
%         %
%         %titlepage,
%         notitlepage,
%         %anonymous,
%         narroweqnarray,
%         inline,
%         %twoside,
%         ]{ieee}
\documentclass[a4paper,10pt]{article}
\usepackage{rpicstyle}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}

%opening
\begin{document}

\title{MIMO channels blind identification: model validation}

\author{A. L. Chiesa\and C. G. Galarza}%\dag
\affiliation{
LPSC, School of Engineering\\
Universidad de Buenos Aires, Buenos Aires, Argentina\\
{\ lucas.chiesa@gmail.com} \\
%\dag CONICET
}
% \author[A.L.Chiesa, C.G. Galarza]{%
%       A. Lucas Chiesa
%       \authorinfo{%
%       LPSC; Departamente de electrónica, Facultad de Ingeniería\\
%       Universidad de Buenos Aires, Buenos Aires, Argentina\\
%       email: lucas.chiesa@gmail.com}
%     \and
%       Cecilia G. Galarza\member{fellow}
%   }
%\confplacedate{Rosario, Argentina, Sep 16--18, 2009}

\maketitle

%TOO: Agregar notación al final de la intro?

%\begin{abstract}
\abstract
A method to validate blind EM-based estimations on MIMO channels is described
in this work. The EM algorithm is widely used in blind estimations
of communication channels, however, its performance is severely reduced in
complex communication systems. An algorithm capable of detecting wrong
identifications based on the Kolmogorov-Smirnov test is proposed. This
procedure is applied to the data provided by the estimator. The method
invalidates 87\% of the wrong identifications, allowing for a new estimation
using different initial conditions.
\endabstract
%\end{abstract}
% do the keywords
%\begin{keywords}

\keywords
Kolmogorov-Smirnov, MIMO, EM
\endkeywords
%\end{keywords}

\section{Introduction}
\label{sec:intro}

%\PARstart
The adoption of WLAN (Wireless Local Area Network) technologies is in constant
expansion, and with it, the need for these technologies to better cope with
multipath and multiuser interference. To fight these problems, systems with
many antennas in both, transmitter and receiver, are being implemented. These
systems, known as MIMO (Multiple Input, Multiple Output) are able to use the
spacial diversity to improve the performance of the system.

However, the objective is not just to overcome the actual limitations,
the challenge is to accomplish that and, at the same time, increase the data
transmition rate. A way of obtaining this is to better use the channel capacity
already available, avoiding retransmissions and reducing overhead. From the
physical layer, a coherent detection scheme can be used to reduce the error
rate during this step. Doing this requires channel identification, and how to
do it depends on the transmition technic being used. OFDM (Orthogonal Frequency
Division Multiplexing) is a technic that has gained popularity during the last
decade. Using it, the broadband channel is divided into $n$ independent
norrow band subchannels, each one represented by a constant gain value. This
method reduces the decoding problem into $n$ independent simple problems.
This work focus its attention in identification of MIMO-OFDM channels.

In order to reduce the overhead, it is desirable to eliminate all of the
training information from the transmitions, which implies using blind
estimators. One family of popular blind estimation methods for MIMO channels is
based on the subspaces methods \cite{fang}. However, these technics assume
noiseless channels and their performance is severely reduced in channels with
low signal to noise ratio. One alternate method is the EM (Expectation
Maximization) algorithm \cite{dempster}. The later does not require
modifications in the structure of the receiver and it has been used in blind and
semi blind identifications of MISO (Multiple Input, Single Output) channels
\cite{cioffi}.

In this study we propose to use the EM algorithm in MIMO channels. EM based
algorithm are fast and of relative low computational complexity (compared to
the processing power available on even small devices) making them a popular
choice \cite{emciego}. However, a blind estimator which uses few samples is a
complex problem because the involved cost function has numerous maximums as it
is not convex. This has motivated many papers which try to improve the EM
method applying different technics. For example, the use of robust statistics
in \cite{robusto}. However, even with the best of the results, the EM does not
provide enough performance in the MIMO configuration due to the many
singularities that can not be avoided.

The EM algorithm is an iterative approximation of the maximum likelihood
estimator. When the likelihood function is complex, with many local maximum
values, the result depends strongly on the initial values used. Should a
training sequence be available, it can be used to calculate the initial
condition or to verify if the estimation is correct. If the estimation is not
coherent with the input-output data, then the obtained model is invalid. In
this situation, the algorithm can be restarted in another point of the space.
The blind estimator does not have this input-output information to invalidate
an identification, thus, it is necessary to obtain information from the
identification process itself and the noise statistics.

To compare the statistics of the error of the output signal estimation, obtained
using the identified model, to the statistic of the noise present in the
channel is presented in this paper. The comparison is done using a goodness of
fit test to a two-dimensional variable with diagonal covariance based on the
Kolmogorov-Smirnov \cite{sthepens} test.

This paper is organized as follows: In the next section the blind
identification for MIMO channels problematic is presented, along with the
involved algorithms. In the section \ref{sec:descripcion} the proposed test
and its implementation are described. Finally, the performance is evaluated in
the section \ref{sec:resultados}.

\section{MIMO Identification}
\label{sec:identificacion}

\subsection{Channel model}
\label{sec:sistema}
A link with one transmitter and one receiver is adopted, each node has two
antennas. The tone $i$ of an OFDM system is considered, transmitting in base
band. In the instant $j$, the received vector $Y_{ij}\in \mathbb{R}^2$ is
related to the transmitted one $X_{ij}\in \mathbb{R}^2$ according to the
following equation:
\begin{equation}
 Y_{ij}=H_{ij} X_{ij}+N_{ij},
\label{eq:canal}
\end{equation}
where
\begin{eqnarray}
X_{ij}&=& \left[\begin{matrix}
 x_{ij}^1 \\
 x_{ij}^2
\end{matrix}\right]
\ N_{ij}= \left[\begin{matrix}
 n_{ij}^1 \\
 n_{ij}^2
\end{matrix}\right] \\
Y_{ij}&=& \left[\begin{matrix}
 y_{ij}^1 \\
 y_{ij}^2
\end{matrix}\right]
\ H_{ij}= \left[\begin{matrix}
 h_{ij}^{11} & h_{ij}^{12} \\
 h_{ij}^{21} & h_{ij}^{22}
\end{matrix}\right].
\end{eqnarray}

The $X_{ij}$ vector contains the transmitted data from both antennas through the
subchannel $i$ in the instant $j$. The component $x_{ij}^{k}$, $k=1,2$
represents the symbol transmitted from the antenna $k$ and $X_{ij}$ belongs to
the finite alphabet $\mathcal{A} \subset \mathbb{R}^2$. $\mathcal{A}$ is
symmetric around the origin. The term $N_{ij}$ is the additive gaussian noise of
diagonal covariance present in the same subchannel. The vector $Y_{ij}$ is the
channel output, being each component $y_{ij}^{l}$, $l=1,2$ the received
signal from the antenna $l$. $H_{ij}$ is the gain matrix for the channel $i$
at the instant $j$, each component $h_{ij}^{kl}$ corresponds to the link
between the transmitter antenna $k$ and the receiver one $l$.

All the transmitted symbols $x_{ij}^{k}$ have the same probability and are
independent between them. Furthermore, the noise component $N_{ij}$ is white
and gaussian, and its covariance matrix is:
\begin{equation}
 \varSigma_{i} =\left[\begin{matrix}
 \sigma_{i}^{1} & 0\\
 0 & \sigma_{i}^{2}
\end{matrix}\right].
\label{eq:sigma}
\end{equation}

As with any OFDM system, each subchannel is treated as an independent problem.
Hence, from now on, only one subchannel is taken into account, so the subscript
$i$ is omitted to simplify the notation. Even though the final objective is to
identify every subchannel, the process is described for only one of them. The
complete solution is a repetition of this process on every subchannel.

\subsection{Identification algorithm}

The blind estimator takes $P$ consecutive samples from the output vector $Y_j,\
j=1,\ldots,P$ and generates an estimation $\hat H$ of the channel gain. Assuming
the channel remains constant and equal to $H$ during the block, the $j$
subscript is omitted from this matrix. In particular, the EM algorithm obtains
$\hat{H}$ in an iterative fashion:
\begin{equation}
  H_{k+1} = \arg\max_H \left( E_{X/Y} \left[ \log
\dfrac{P(X,Y;H)}{P(X,Y;H_k)} \right] \right)
\label{eq:em}
\end{equation}
where $k$ represents the iteration number of the algorithm.

Using the definitions presented in the section \ref{sec:sistema}, it is
possible to solve in a close form the maximization in (\ref{eq:em}), obtaining
the following expression:
\begin{eqnarray}
H_{k+1} = \sum_{j=1}^{P}Y_j \sum_{X \in \mathcal{A}} \left[ \dfrac{X^T \exp
\left( \frac{-\mathcal{E}_{jk}(X)}{2} \right) }{\sum_{X \in \mathcal{A}} \exp
\left( \frac{-\mathcal{E}_{jk}(X)}{2} \right)} \right] \times \notag \\
\times \left\lbrace \sum_{j=1}^{P} \sum_{X \in \mathcal{A}} \left[\dfrac{XX^T
\exp \left( \frac{-\mathcal{E}_{jk}(X)}{2} \right) }{\sum_{X \in \mathcal{A}}
\exp \left( \frac{-\mathcal{E}_{jk}(X)}{2} \right)} \right] \right\rbrace^{-1}
\label{eq:h_km1}
\end{eqnarray}
where $\mathcal{E}_{jk}(X) = \left((Y_j-H_{k}X)^T \varSigma^{-1}
(Y_j-H_{k}X)\right)$. Notice that the structure of (\ref{eq:h_km1}) is similar
to that obtained using the minimum quadratic error in a semi blind
identification \cite{ljung} if we concider that the system inputs have been
replaced with an average of every possible input vectors $X\in \mathcal{A}$.
Moreover, the estimation $H_{k+1}$ in (\ref{eq:h_km1}) is also solution of the
following optimization problem:
\begin{equation}
 H_{k+1} = \arg\max_{H} \sum_{j=0}^P E_{X/Y_j}\left[\lVert Y_j-HX \rVert^{2};
H_k \right].
\label{eq:h_ls}
\end{equation}

Working with (\ref{eq:h_ls}),
\begin{equation}
H_{k+1} = \left[ \sum_{j=0}^P {\mathbf {R}}_j \right]^{-1} \sum_{j=0}^P
\hat{\mathbf{X}}_jY_j
\label{eq:hcuadrados}
\end{equation}
is obtained, where $j$ is the sample index, $k$ the iteration, and
\begin{eqnarray}
 \hat{\mathbf{X}}_j &=& E_{X/Y_j}[X;H_{k}], \label{eq:hatx}\\
{\mathbf{R}}_j &=& E_{X/Y_j}[XX^{T};H_{k}].
\end{eqnarray}

This reinterpretation of the EM allows the researchers to introduce improvements
to the algorithm. For instance, the estimator (\ref{eq:hcuadrados}) and the
detector (\ref{eq:hatx}) can be modified to add robust statistic to improve the
EM performance when many outliers are present, as shown in \cite{robusto}.

\section{Description of the validation method}
\label{sec:descripcion}

Once the channel estimation $\hat{H}$ is obtained, it is used to obtain the
transmitted data sequence, $\hat{X}_j$, using a hard detector. The following
estimation error of the output in the instant $j$ given $\hat H$ y $\hat
{X}_j$ is considered:
\begin{equation}
 Y_j-\hat X_j \hat H=\hat N_j \qquad 1 < j < P.
\label{eq:despeje}
\end{equation}

From the hypothesis of independence of the transmitted symbols and remembering
that $H$ is a constant matrix, it is possible to define an extended matrix $Y$
with the $P$ output vectors juxtaposed. In the same manner, the extended
matrices $\hat{X}$ (of the outputs), $N$ (of the additive noise) and $\hat N$
(of the estimation errors) are defined. All of these matrices comply
with (\ref{eq:despeje}) if the subscript $j$ is removed.

The task consists of invalidating incorrect $\hat H$ estimations without using
trained identification methods. An algorithm which uses a goodness of fit test
is proposed. It compares the two-dimensional distribution of $\hat N$ and $N$,
which is a two-dimensional gaussian with zero mean and known variance
$\varSigma$. In practice, the fewer the samples used ($P$) the more accurate
will be the assumption of a constant $H$. This forces the algorithm to produce
correct results even with few samples. The presented solution uses the
Kolmogorov-Smirnov (KS) \cite{sthepens} test, which quantifies how far apart
two cumulative distribution function (CDF) are from each other. To take
advantage of the information present on a MIMO environment, a modified KS test
described in is \cite{ksmulti} adopted.

Given $\hat{n}_1,\ldots,\hat{n}_P$ i.i.d (independent and
identically-distributed) samples of a random variable with a CDF $F$, consider
the problem of verifying the hypothesis $F=F_0$ against $F \neq F_0$ where
$F_0$ is an specific distribution function. In the unidimensional case, the
$D_P$ is defined as:
\begin{equation}
\label{eq:ksuni}
 D_P = \sup_{\hat n \in \mathbb{R}} | F_P(\hat n)-F_0(\hat n)|,
\end{equation} 
where $F_P$ is the empirical CDF of the samples $\hat{n}_1,\ldots,\hat{n}_P$.

The random variable $\hat{n}$, with its CDF $F$, is transformed to a uniform
variable $\nu$ applying the transformation $\nu=F(\hat{n})$ \cite{leon-garcia}.
Using this procedure to the samples $\hat {n}$, $\nu_j = F_0(\hat {n}_j)$ is
obtained, with $j = 1,\ldots,P$. Thus, calculating the empirical distribution
function $G_P(\nu)$, the equation (\ref{eq:ksuni}) can be rewritten:
\begin{equation}
 D_P = \sup_{0 \leq \nu \leq 1} | G_P(\nu) -\nu |.
\label{eq:indep}
\end{equation}

This last equation can be used to evaluate how uniform the result of applying
the supposed CDF to the samples under study is, instead of comparing the CDFs,
as in (\ref{eq:ksuni}). It can be demonstrated that, unlike (\ref{eq:ksuni}),
the statistic of (\ref{eq:indep}) is independent of the distribution $F$.

This test has to be modified in order to use it with multidimensional variables.
One alternative is to do it according to \cite{ksmulti}. The Rosenblatt
transformation \cite{rosenblatt}, key to this modification, transforms a random
vector $\hat N$ with a distribution $F$ into a new vector $\Upsilon = T(\hat N)$
of equal dimensions but uniformly distributed, with components between 0 and 1.
By using this transformation, (\ref{eq:indep}) is rewritten as:
\begin{equation}
D_P = \sup_{\Upsilon} \left|G_{P}(\Upsilon)- \prod_{j=1}^{P}
{\Upsilon_{j}} \right|
\end{equation}
where $G_{P}$ is the empirical distribution function of the transformed samples
$\Upsilon_j$.

Given the vector $\hat N = \left[\hat{N}^1 \hat{N}^2 \right]'$, if both
components are independent with each other, the Rosenblatt transformation
results:
\begin{equation}
\label{eq:transformacion}
 \Upsilon = \left[F_{1}(\hat {N}^{1})\ F_{2}(\hat{N}^{2})\right]',
\end{equation}
where $F_1$ and $F_2$ are the marginal distribution of $\hat{N}$.

\subsection{Validity of the test}

As explained in section \ref{sec:descripcion}, statistical properties of the
estimation errors are used to verify the validity of an estimation. If $\hat N$
has a distribution function close to the one of $N$ (referred as $\hat N \cong
N$) the estimation is marked as valid.

It is noticeable that from a correct implication (if $\hat H \hat X = HX
\Rightarrow \hat N \cong N$), a new one is proposed, $\hat N \cong N \Rightarrow
\hat H = H$. This new implication is not necessarily true, because not any
realization of a variable of equal distribution to $N$ comes from $\hat H = H$.
Thus, the validity of the analysis was proven counting how many times $\hat N
\cong N$ when $\hat H \neq H$. The results indicated that in $99.9\%$ of the
simulations $\hat H \neq H \Rightarrow \hat N \ncong N$.

Also, the capacity to detect correct estimations was tested. The value returned
from the EM was replaced with $\hat H = H$ and the result analyzed with the
validation algorithm. Only the $18\%$ of the identifications where rejected.

\section{Implementation of the algorithm}

The described method assumes the noise variance known during the
channel identification stage. According to (\ref{eq:transformacion}), $F_0 =
\mathcal{N}(0,\varSigma)$, $F_{1} = \mathcal{N}(0,\sigma^1)$ and
$F_{2} = \mathcal{N}(0,\sigma^2)$.

The implementation of the described method requires the calculation of the
empirical CDF of $\Upsilon$, which implies a high computational cost. To reduce
the complexity, in the two-dimensional case, the CDF is only computed on
selected points in the space \cite{ksmulti}, denominated relevant points.

Let the pairs $\{(\nu^1_{1},\nu^2_{1}), \ldots, (\nu^1_{j},\nu^2_{j}),
(\nu^1_{l},\nu^2_{l}), \ldots, (\nu^1_{P},\nu^2_{P})\}$ be the samples of
$\Upsilon$. Then, the relevant points set $\mathcal{B} \subset \mathbb{R}^2$
contains the elements $\beta \in \mathbb{R}^2$ that satisfy the following
conditions:
\begin{itemize}
\item The samples: $\{(\beta^1, \beta^2) | \beta_1 = \nu^1_j, \beta_2 = \nu^2_j
, j = 1,\cdots,P\}$,
 \item the intersection points:
 $\{(\beta^1,\beta^2) | \beta^1 = \nu^1_{l}, \beta_2 = \nu^2_{j}$  y $\nu^1_{l}
> \nu^1_{j},\ \nu^2_{l} < \nu^2_{j}\, j,l=1,\cdots,P \}$,
\item the projections on the top unit-square border:
$\{(\beta^1,\beta^2) | \beta^1 = 1, \beta_2 = \nu^2_{j}, j=1,\cdots, P\}$,
\item or the projections on the right unit-square border:
$\{(\beta^1,\beta^2) | \beta^1 = \nu^1_{j}, \beta_2 = 1, j=1,\cdots, P\}$.
\end{itemize}

With all this considerations, the algorithm results:
\begin{enumerate}
 \item The Rosenblatt transformation is applied for a two-dimensional case.
 \item $\mathcal{B} = \{ \beta \}$ is calculated.
 \item $G_P(\beta)\ \forall \beta \in \mathcal{B}$ is calculated.
 \item $\beta^1.\beta^2\ \forall \beta \in \mathcal{B}$ is calculated.
 \item The maximum distance, $D_{max} = \max_{\beta \in \mathcal{B}}
\left[|G_P(\beta)-\beta^1.\beta^2|\right]$ is calculated.
 \item The identification is marked as valid if $D_{max} < K$.
\end{enumerate}
where K is a constant.
\label{sec:resultados}
\begin{figure}
	\begin{center}
	\includegraphics[width=1\columnwidth]{verosimilitud.pdf}
	\end{center}
\caption{Maximum likelihood function that needs to be maximized.}
\label{img:l}
\end{figure}

To determine $K$ the behavior of the algorithm was studied with samples of the
same distribution that it was testing. It was observed that the $82\%$ of
all the tests resulted on a $D_{max}< 0.20$, and thus $K=0.20$ was adopted.

\section{Results}

The EM based estimators are an approximation of the maximum likelihood
estimator. Thus, the efficiency of the estimator strongly depends on the shape
of the likelihood function. Results will not be always correct it this function
has more than one maximum.

In particular for a system with two antennas in the transmitter and one in the
receiver, the likelihood function $L = P(Y/H=h)$ is shown in fig. \ref{img:l}.
The many local maximum points of $L$ can be seen in this figure. This results
on the EM algorithm giving different results for the same samples, depending
on the initial values that were used. From this possibility arises the
necessity of being able to invalidate an incorrect channel identification, in
order to re try with different initial conditions. For example, let
\begin{equation}
 H =\left[\begin{matrix}
1.025 &  1.199 \\
0.082 &  0.349
\end{matrix}\right],
\end{equation}
be the gain matrix of a channel, through which a 4 level ASK signal is
transmitted. Using $P=50$ samples, two different results where obtained using
two different initial conditions:
\begin{equation}
 H_{in}^1 =\left[\begin{matrix}
0.968 & 3.154 \\
0.321 & 0.160
\end{matrix}\right], \\
H_{in}^2 =\left[\begin{matrix}
0.557 & 0.169 \\
1.602 & 0.016
\end{matrix}\right].
\end{equation}

In each graphic of the fig. \ref{img:doble} the four real values of the channel
(the dashed lines) and the evolution of the estimated values is shown. In the
right the algorithm used $H_{in}^1$ as the original values and provided a wrong
estimation. However, in the left graphic, using $H_{in}^2$ as initial
conditions, the results are correct. This result proves the usefullness of a
validation step after a blind identification.
\begin{figure}
 \centering
 \includegraphics[width=1\columnwidth]{2do_resultado_eng.pdf}
\caption{Two identifications of the same transmition, one accepted and the
other rejected.}
\label{img:doble}
\end{figure}

It is important to note that this problem was not present when a simple,
binary constellation was used. This results evident when the likelihood function
for this constellation is observed (fig. \ref{fig:l_binaria}).

Finally, the performance of the proposed validation algorithm was analyzed. The
channel model described in section \ref{sec:descripcion} is used. The
transmitted data is generated using a 4 level ASK constellation, and blocks of
$P=50$ samples are used. A system with two antennas in the receiver and the
transmitter is used, a $SNR=20 dB$ and the EM algorithm is allow to iterate
$15$ times. 5000 simulations where generated using $P$ samples for the EM
estimator described in \cite{emciego} and with its results and a different set
of $P$ samples the validation algorithm was ran.

Besides of the described validation algorithm, tree variations where tested.
In them, the hard detector of $\hat{X}$ was replaced with a soft one. Another
modification was to replace the two-dimensional KS test for two, independent
unidimensional tests. The obtained results show that the described algorithm is
the one with the best performance. However, the improvement over the other three
methods is small.

To quantify the performance of the validation algorithm, the quadratic error of
the estimation was measured, $\varepsilon =
\mathtt{sqrt}\left[(||\mathtt{vec}(\hat H) -
\mathtt{vec}(H)||^2)/||\mathtt{vec}(\hat H)||^2\right]$, where $\mathtt{vec}$ is
the operation of stacking the columns of a matrix one above the other. To
represent the obtained results, they where separated into two groups, the
accepted and the rejected estimations and histograms for each group where
calculated, and are shown in fig. \ref{img:histograma}. This graphics represents
the amount of estimations with a given $\varepsilon$ that is present in each
group.
\begin{figure}
 \centering
 \includegraphics[width=\columnwidth]{vero_uni.pdf}
 \caption{Likelihood function for the binary constellation.}
 \label{fig:l_binaria}
\end{figure}

Using the same results, in fig. \ref{img:ks_error} the values of $D_{max}$ in
reference to the values of $\varepsilon$ are plotted, also the acceptance
threshold $K$ is shown. It is observed that for the low $\varepsilon$ region,
there are few rejected identifications, whereas in the region of high
$\varepsilon$ the variance of $D_{max}$ increases, accepting wrong
identifications.
\begin{figure}
	\begin{center}
	\includegraphics[width=1\columnwidth]{grafico_errores_eng.pdf}
	\end{center}
\caption{Performance analysis of the algorithm.}
\label{img:histograma}
\end{figure}

In both graphics it can be observe that the EM algorithm does not produce a
uniform distribution of the estimation error, the results are concentrated in
two regions. This separation in groups is explained looking at the likelihood
function, which has local maximum values in both zones. This behavior is
% TODO: ascendent gradient.
predictable taking into account that Xu and Jordan, proved in \cite{jordan} that
the EM algorithm is a ascendent gradient.

For the accepted simulations, the bigger frequency is found for errors between
$0.01 < \varepsilon < 0.04$. For the rejected ones, the higher frequency is
obtained at higher errors, $0.1 < \varepsilon < 1.2$. This analysis indicates
that the proposed algorithm is able to distinguish between correct and
incorrect identifications.

With the objective of quantifying the performance of the algorithm, the number
of correct identifications that where rejected and the number of wrong
identifications that where accepted was measured. From the total of
identifications with $\varepsilon > 0.1$, only the $12.9\%$ was accepted, and
from the total of estimations with $\varepsilon < 0.04$, only the $13.9\%$ was
rejected. This values are considered acceptable when compared to the typical
performance of the traditional EM algorithm.

\begin{figure}
 \centering
 \includegraphics[width=\columnwidth]{grafico_ks_fig_error_eng.pdf}
 % grafico_ks_fig_error_cero.pdf: 344x247 pixel, 72dpi, 12.14x8.71 cm, bb=
 \caption{$D_{max}$ values in reference to $\varepsilon$.}
 \label{img:ks_error}
\end{figure}

\section{Conclusions}

A mechanism to blindly estimate the gain of an MIMO-OFDM channel was elaborated.
When this method is applied to systems with non convex likelihood functions, the
algorithm may converge to different results depending on the initial conditions
used. This makes a validation step usefull, because it allows to re-analyze a
sample block.

An algorithm that uses a modified version of the well known Kolmogorov-Smirnov
goodness of fit test, which can be used with multidimensional variables in
order to better use the information available on a MIMO channel, was proposed
and its performance studied using the model described in the section
\ref{sec:sistema}. From these simulations it is obtained that the algorithm is
capable of rejecting the $86\%$ of incorrect identifications. This value is
considered satisfactory taking into account the characteristics of the EM
algorithm.

In further studies ways of improving the performance will be analyzed. We
propose the use of the modifications to the KS test described by Khamis
\cite{khamis} called \textit{two-stage $\delta$-corrected Kolmogorov-Smirnov
test} which imprives the performance of the test for small sample quantities.

\section*{Acknowledgement}
This work was partially supported by the ANPCyT PICT2005-32610 and CONICET
PIP2004-6344 projects. A.L.C. has a scholarship from Telefónica de Argentina.
C.G.G. is a CONICET researcher.

%\bibliographystyle{IEEEtran.bst}
\bibliographystyle{plain}
\bibliography{IEEEfull,rpic}

\end{document}
