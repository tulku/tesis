\documentclass[a4paper,11pt]{article}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}

%opening
\title{Presentación de Plan de Tesis de Ingeniería Electrónica\\
	Identificación ciega de canales MIMO:\\ Algoritmo de validación del resultado}

\author{Alumno: Alberto Lucas Chiesa Vaccaro\\
	Tutor: Dra. Cecilia Galarza} 

\begin{document}

% Carátula
\thispagestyle{empty}
\includegraphics[width=0.4\textwidth]{logo_fiuba_alta.jpg}
\vfill
\begin{center}
 {\Large Presentación de Plan de Tesis de Ingeniería Electrónica\\}
\vspace{1cm}
 {\huge Identificación ciega de canales MIMO:\\ Algoritmo de validación del resultado}
\end{center}
\vfill
{\large \begin{description}
 \item[Alumno:] Alberto Lucas Chiesa Vaccaro
\item [Padrón:] 83417
\item [mail:] lucas.chiesa@gmail.com
\vspace{1cm}
\item [Tutor:] Dra. Cecilia Galarza
\item [Legajo:] 104812
\item [mail:] cgalar@fi.uba.ar
\vspace{2cm}
\item [Fecha:] \today
 \end{description}}
\vfill
\newpage
% Fin carátula

\section*{Introducción}

El uso de las tecnologías inalámbricas de área local (WLAN, “Wireless Local Area Network”) como la familia de estándares Wi-Fi se encuentra en constante crecimiento, tanto para el uso hogareño como el corporativo. Sin embargo, las tecnologías actuales presentan inconvenientes que limitan su calidad de servicio. El aumento de la cantidad de dispositivos que utilizan estas redes hace que cada vez sea más relevante la robustez del sistema ante el ruido por interferencia de equipos vecinos. También es necesario ampliar el alcance de cobertura de los dispositivos que brindan estos servicios para poder cubrir fácilmente grandes pisos de oficinas, que además de ser extensos tienen muchas divisiones que degradan el alcance de las tecnologías actuales. Para ello es necesario combatir el problema de devanecimiento por múltiples caminos.

Una técnica que se utiliza actualmente para mitigar estos problemas es la adopción de canales de múltiples entradas y múltiples salidas (MIMO, del inglés Multiple Input, Multiple Output). Un ejemplo de esta tecnología es el estándar IEEE 802.11n que se está desarrollando actualmente. Mediante el uso de múltiples antenas tanto en el transmisor como en el receptor se logra diversidad espacial, lo que hace que el sistema sea más robusto ante canales con interferencia o desvanecimientos. Estas técnicas ya están en uso en otras tecnologías como WiMAX o algunas redes celulares de tercera generación.

Sin embargo, no sólo se trata de solucionar los problemas actuales. El desafío es mejorar el servicio aumentando la tasa de transferencia de datos. Una forma de lograr eso es utilizar mejor la capacidad actual de los canales, por ejemplo evitando retransmisiones de datos que no se pudieron decodificar correctamente o reduciendo el overhead. Desde la capa física, se puede colaborar utilizando detección coherente (detectar los símbolos transmitidos conociendo el estado actual del canal) para así lograr una menor tasa de error en la detección. Para ello se requiere identificar el canal.

Detectar el canal es un proceso complejo que se puede solucionar de diferentes maneras. En ciertos casos, es posible transmitir símbolos conocidos en una secuencia de entrenamiento. Sin embargo, con el objetivo de maximizar el uso del canal es deseable suprimir todo tipo de entrenamiento, es decir, usar estimadores ciegos. Asimismo, se debe tener en cuenta la rápida variación del estado de los canales inalámbricos, sobre todo en grandes redes. Por lo tanto, la estimación del canal se tiene que poder realizar con pocas muestras para poder adaptarse rápidamente a cambios en el medio.

Desafortunadamente, un estimador ciego que use pocas muestras para un canal MIMO es un problema complejo. Las funciones costo a considerar no son convexas, por lo que existen muchos máximos locales. Por otro lado, estos algoritmos son rápidos y de relativamente baja complejidad computacional, considerando el capacidad de procesamiento disponible actualmente.

Se propone el uso de un algoritmo que sea capaz de verificar la calidad de una estimación de canal. Luego de ejecutar el estimador de canal, los datos obtenidos son analizados por el algoritmo validador y éste decide si se acepta o no el resultado. De esta forma se puede realizar otra estimación, con diferentes condiciones iniciales y tratar así de obtener un mejor resultado.

De no contar con un algoritmo como el algoritmo validador descripto, luego de una estimación incorrecta alguna capa superior va a decidir que el paquete es incorrecto (utilizando CRCs por ejemplo) y pedir una retransmisión. Si se puede detectar una mala estimación y corregirla, entonces esa retransmisión no sería necesaria. Se mejora así el uso del canal de dos aspectos: se evitan las secuencias de entrenamiento y se reduce la tasa de retransmisiones.


\section*{Objetivo}

Como se explicó anteriormente, para poder realizar una detección coherente de los símbolos transmitidos, se necesita estimar la dinámica del canal. Por otro lado, para cumplir con las demandas de las nuevas tecnologías, se propone utilizar estimadores ciegos, con bloques de muestras pequeños.

Estos requerimientos son exigentes y constituyen un problema de difícil solución. Esto se traduce en un gran número de estimaciones erróneas. Poder detectar una estimación incorrecta se vuelve de gran importancia y utilidad ya que evita posibles retransmisiones.
\paragraph*{}
El objetivo de esta tesis es diseñar un algoritmo de validación de estimaciones ciegas para canales MIMO con baja cantidad de muestras. Una vez diseñado el algoritmo se estudiará el desempeño del mismo utilizado en conjunto con diferentes estimadores de canal.
\paragraph*{}
El algoritmo tiene que tomar los datos producidos por el estimador y decidir si es un resultado válido o no. Es importante recordar que al tratarse de una estimación ciega no se puede comparar contra algún dato conocido, ya que no se dispone de ninguno. Asimismo, al estar trabajando en la capa física no se hace uso de códigos tipo CRC para probar integridad de datos. El algoritmo validador debe estar basado sólo en las características estadísticas de las señales involucradas. Se deja para las capas superiores el uso de códigos.
\paragraph*{}
En este trabajo se pretende, mediante simulaciones en GNU Octave, modelizar diferentes estimadores ciegos para canales MIMO, implementar el algoritmo validador y caracterizar el funcionamiento del mismo.

\section*{Metodología de trabajo}

La lista que se presenta para resumir la metodología de trabajo no constituye una descripción cronológica de las actividades a realizar, sino una enumeración de las mismas. En general, el trabajo consiste en alternar la especulación teórica con las simulaciones experimentales que ilustren los resultados teóricos obtenidos. Teniendo presente esta aclaración, el listado de tareas a desarrollar es el siguiente:

\begin{itemize}
 \item Búsqueda de soluciones ya existentes.
\item Búsqueda de trabajos publicados en la bibliografía especializada, proponiendo posibles soluciones.
\item Análisis comparativo de los diferentes métodos encontrados teniendo en cuenta el costo computacional de los diferentes algoritmos.
\item Simulación en GNU Octave.
\item Evaluación de la performance del sistema.
\end{itemize}

\section*{Bibliografía preliminar a utilizar}

\begin{itemize}
\item D. Tse, P. Viswanath. ``Fundamentals of Wireless Communication'', 1\textordmasculine ed., Cambridge University Press (2005).
\item J. G. Proakis. ``Digital Communications'', 4º ed., McGraw Hill (2001).
\item E. Biglieri, R. Calderbank, A. Constantinides., ``MIMO Wireless Communications'', 1\textordmasculine ed., Cambridge University Press (2007)
\item R. A. Maronna, D. R. Martin, V. J. Yohai. ``Robust Statistics: Theory and Methods'', 1\textordmasculine ed., Wiley (2006)
\item C. H. Aldana, J. Cioffi., ``Channel Tracking for Multiple Input, Single Output Slystems using: EM algorithm'', ICC 2001. 11-14 June 2001 Page(s):586 - 590 vol.2.

\end{itemize}

\section*{Alcance de la tesis}

\begin{itemize}
\item Profundizar el estudio de canales MIMO, algoritmos de estimación ciega y distintas técnicas de análisis estadístico utilizadas cuando se cuentan con pocas muestras.
\item Familiarizarse con técnicas de programación numérica óptimas en cuanto a los recursos computacionales utilizados. \item Diseñar un algoritmo que permita validar una estimación para una colección aleatoria de canales de transmisión.
\end{itemize}

\section*{Plan de trabajo}

Se enumeran a continuación las tareas a realizar, cuya duración total estimada es de aproximadamente un año.

\begin{itemize}
 \item Estudio de los sistemas MIMO y los estimadores ciegos utilizados.
\item Búsqueda de soluciones existentes o planteadas como posibles.
\item Análisis teórico de los estimadores para comprender su funcionamiento.
\item Análisis práctico de los algoritmos estudiados mediante simulaciones en GNU Octave.
\item Desarrollo de una plataforma de simulación para el validador.
\item Desarrollo de un algoritmo de validación.
\item Análisis mediante experimentación numérica del desempeño del validador.
\end{itemize}

\vfill

\begin{center}
\begin{tabular*}{0.9\textwidth}{rp{0.1\textwidth}l}
 & & \\
\cline{1-1} \cline{3-3}\\
Sr. Alberto Lucas Chiesa Vaccaro &  & Dra. Cecilia Galarza
\end{tabular*}
\end{center}

\end{document}
