\documentclass[a4paper,11pt]{report}
\usepackage{amsmath}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
% Title Page
\title{Desarrollo de un estimador ciego para canales MIMO}
\author{Lucas Chiesa}

\begin{document}
\begin{abstract}
 Esta es la tesis de lucas
\end{abstract}
\maketitle

\tableofcontents
\chapter{Introducción}
\section{Introducción a los estimadores de cuadrados mínimos}

Supongamos que tenemos dos variables aleatorias $X$ e $Y$ con una función de densidad de probabilidad conjunta conocida $f_{x,y}$. Asumamos que en un experimento en particular la variable $Y$ se puede medir y toma el valor $y$. ¿Qué podremos decir acerca del valor $x$, correspondiente a la variable no observable $X$?

Supongamos que se tiene un estimador $\hat{x}$ del valor de $X$ cuando $Y=y$, según la fórmula:
\[
 \hat{x}=h(y)
\]
Si $x$ es el valor real de $X$ cuando $Y=y$, $h(.)$ es una función específica, podemos escribir:
\[
 e=x-\hat{x}=x-h(y)
\]
donde e es el error de nuestro estimador. No podemos pretender hacer $e=0$ para todo valor de $Y$ ya que $X$ e $Y$ son variables aleatorias. Por lo tanto todo lo que podemos intentar es elegir $h(.)$ de forma tal de minimizar el valor esperado (en promedio) de alguna función de ese error. Por ejemplo, el valor absoluto ($|e|$) o el cuadrado del error ($|e|^2$).

La elección de la función de error apropiada no es obvia y debe depender en el problema que se quiera resolver. Dada la gran cantidad de opciones es llamativo que el criterio del cuadrado del error sea el más estudiado. Los estimadores resultantes de este criterio se van a llamar de cuadrados mínimos (l.s.\footnote{l.s.: del inglés \textit{least-squares}}) aunque otros nombres son usados, especiamente error cuadrático medio mínimo (m.m.s.e\footnote{m.m.s.e.: del inglés minimun-mean-square-error}).

Algunas de las razones para estudiar estos estimadores son:
\begin{itemize}
 \item Los estimadores l.s. se pueden expresar como una media condicional.
\item Para variables aleatorias gaussianas el estimador l.s. es una función lineal de las observaciones, por lo que goza de facilidades de cómputo e implementación. Asímismo, para estas variables el estimador l.s. esencialmente determina el estimador óptimo para una función de error arbitraria.
\item Estimadores l.s. subóptimos son relativamente fáciles de obtener. En particular, si están expresados como funciones lineales de las observaciones. En esos casos sólo dependen de estadística de primer y segundo orden (media y varianza) de las variables aleatorias involucradas.
\end{itemize}

\section{Estimadores l.s. - Propiedades básicas}

\subsection{Estimadores l.s. como esperanzas condicionales}

Como se dijo anteriormente, los estimadores l.s. se pueden escribir como esperazas condicionales y ahora vamos a mostrar que el estimador l.s. de una variable $X$ dada otra variable $Y$ es la esperanza condicional de $X$ dado $Y$, escrita $E[X/Y]$.

Supongamos que cuando $Y=y$ el valor real de $X$ es $x$ y que nuestro estimador es $h(y)$, entonces el error es $x-h(y)$, y ocurre con una probabilidad $f_{X,Y}(x,y)\ dx\ dy$. Por lo tanto el error cuadrático medio (m.s.e.) es:

\begin{equation}
m.s.e. = \iint f(X,Y)(x,y)[x-h(y)]^2\ dx\ dy
\label{eq:mse}
\end{equation}

y esto es lo que tenemos que minimizar para obtener la función $h(.)$. Para esto vamos a usar la descomposición $f_{X,Y}(x,y)=f_{X/Y}(x/y)f_{Y}(y)$ donde $f_{X,Y}(x/x)$ es la función de densidad de probabilidad condicional de $X$ dado $Y$ y $f_Y()$ es la función de densidad de probabilidad marginal de $Y$. Reescribimos \ref{eq:mse} y obtenemos:
\begin{equation}
 mse = \int f_{Y}(y)\ dy \left\{ \int f_{X/Y}(x/y)[x-h(y)]^2\ dx \right\}
\end{equation}

como $f_Y(y)$ es siempre una función no negativa, es claro que para minimizar el $m.s.e.$ vamos a tener que minimizar el término entre llaves. Se puede notar de muchas formas, por ejemplo derivando o mirando la ecuación como el momento de inercia de la ``masa'' $f_{X/Y}(x/y)$ en el punto $h(y)$. Una forma rápida pero desprolija es la siguiente:
\begin{eqnarray}
\int f_{X/Y}(x/y) [x-h(y)]^2\ dx = \int x^2 f_{X/Y}(x/y)\ dx\\
+h^2(y)\int f_{X/Y}(x/y)\ dx\ - 2h(y)\int x f_{X/Y}(x/y)\ dx
\end{eqnarray}
Se puede ver que el segundo término es 1 ya que se está integrando $f_{X/Y}$ en todo $X$. Si se deriva la ecuación anterior en $h(y)$ y se iguala a cero, se obtiene:
\begin{equation}
 2h(y)\ - 2\int x f_{X/Y}(x/y)\ dx = 0 \\
\end{equation}
Por lo que la  $h(y)$ que minimiza la ecuación \ref{eq:mse} es:
\begin{equation}
 h(y) = \int x f_{X/Y}(x,y)\ dx
\end{equation}
que por definición es la esperanza condicional $E[X/Y]$, por lo que podemos decir que $h(y)=E[X/Y]$. Como $Y$ es una variable aleatoria, no podemos saber que valor va a asumir en una realización en particular. Por lo tanto, el mejor estimador también es una variable aleatoria que llamaremos $\hat{X}$,
\begin{equation}
 \hat{X} = h(Y) = E[X/Y]
\end{equation}

\subsection{Estimación dada varias variables aleatorias}
Supongamos ahora que queremos estimar el valor de la variable $X$ pero dada la observación de dos variables aleatorias $Y_1$ e $Y_2$. Si hacemos el mismo planteo que antes:
\begin{equation}
mse=\iint [x-h(y_1,y_2)]^2 f_{X,Y_1,Y_2}(x/y_1,y_2)\ dx\ dy_1\ dy_2
\end{equation}
podemos decir que se va a minimizar con:
\begin{equation}
 h(y_1,y_2)=\int xf_{X/Y_1,Y_2}(x/y_1,y_2)\ dx = E[X/Y_1=y_1, Y_2=y_2]
\end{equation}
Por lo tanto:
\begin{equation}
 \hat{X} = h(Y_1,Y_2) = E[X/Y_1,Y_2]
\end{equation}
Es claro que el mismo razonamiento, pero con una notación más compleja va a servir para deducir que el estimador l.s. de $X$ dado ${Y_1, Y_2, \ldots, Y_n}$ es:

\begin{equation}
\hat{X} = E[X/Y_1,\ldots,Y_n]
\end{equation}

\section{Estimador de máxima verosimilitud}

Para un conjunto de datos cuyo modelo de densidad de probabilidad es fija y conocida, el estimador de máxima verosimilitud, va a dar los valores de los parámetros del modelo que hagan que obtener esos datos sea más probable con cual cualquiera otro valor de los parámetros. Se asume como prerrequisito para usar estos estimadores que se conoce la familia de funciones de densidad de probabilidad a la cual las muestras pertenecen.

En el caso de distribuciones normales este tipo de estimador proveen una solución única y sensilla, aunque en algunos problemas complejos su resolución puede resultar demasiado costosa.

\subsection{Principios}

Consideren una familia de funciones de distribución de probabilidad $D_\theta$ que están parametrizas por el parámetro $\theta$. Cada una asociadada a una función de densidad de probabilidad llamada $f_\theta$ (ya sea continuas o discretas). Obtenemos una muestra $x_1,x_2,\dots,x_n$ de $n$ valores de esta distribución y luego usamos $f_\theta$ para calcular la densidad de probabilidad asociada nuestros datos $f_\theta(x_1,\dots,x_n)$.
Siendo ella una función de $\theta$ con los valores de $x_1, \dots, x_n$ fijos. Esta es conocida como la función de verosimilitud.
    \[\mathcal{L}(\theta) = f_{\theta}(x_1,\dots,x_n) \]

El método de máxima verosimilitud estima $\theta$ buscando el valor que maximiza $\mathcal{L}(\theta)$. El estimador de máxima verosimilitud (MLE\footnote{MLE: Del inglés \textit{maximum likelihood estimator}}) para $\theta$ resulta:
\[\widehat{\theta} = \underset{\theta}{\operatorname{arg\ max}}\ \mathcal{L}(\theta)\]

Comúnmente, se asume que los datos extraidos de una distribución en particular son independientes e idénticamente distribuidos. Esta hipótesis simplifica en gran medida el problema ya que función de verosimilitud se puede reescribir como el producto de $n$ funciones de densidad de probabilidad de una sala variable:
\[\mathcal{L}(\theta) = \prod_{i=1}^n f_{\theta}(x_i)\]

y como uno el máximo no se modifica por una función monótona, uno puedo tomar logaritmo de la expresión y convertirla en una suma:
\[\mathcal{L}^*(\theta) = \sum_{i=1}^n \log f_{\theta}(x_i)\]

Esta función se puede maximizar numéricamente usando algún algoritmo de optimización. Es importante notar que el estimador de máxima verosimilitud puede no ser único o puedo directamente no existir.

\subsection{Propiedades}

\subsubsection{Invarianza}

El estimador de máxima verosimilitud selecciona el valor del parámetro que da la máxima probabilidad al conjunto de datos. Si el parámetro consiste en un conjunto de componentes, definimos un estimador para cada uno, como la parte correspondiente del estimador MLE del parámetro completo. Consistentemente con esto, si $\widehat{\theta}$ es el estimador de $\theta$ y $g$ es cualquier función de $\theta$, entonces el estimador MLE para $\alpha = g(\theta)$ es, por definición:
\[\widehat{\alpha} = g(\widehat{\theta})\]

%TODO agregar las propiedades que faltan.

\section{Estadística robusta}

Es estadística, los métodos clásicos se basan fuertemente en hipótesis que suelen no cumplirse en la práctica. En particular, se suele asumir de un grupo de datos una distribución normal\footnote{También conocido como Gaussiana}, por lo menos aproximadamente. O que se puede aplicar aplicar el teorema central del límite para justificar la normalidad de una población. Desafortunadamente, cuando hay valores discordantes\footnote{En inglés \textit{outliers}.} entre las muestras disponibles, los métodos clásicos suelen tener muy baja performance. La estadística robusta trata de proveernos con métodos que emulen los métodos clásicos pero que no sean tan sensibles a la presencia de este tipo de datos.

Para poder cuantificar cuan robusto es un método, primero debemos definir algunas medidas de robustez, las más comunes siendo el punto de quiebre\footnote{En inglés, \textit{breakdown point}.} y la función de influencias, se describen a continuación.

\subsection{Punto de quiebre}

Intuitivamente, el punto de quiebre de un estimador es la proporción de observaciones incorrectas (por ejemplo, observaciones arbitrariamente más grandes que las demás) que un estimador puede soportar antes de dar un valor arbitrariamente más grande.

Por ejemplo, dadas $n$ variables aleatorias independientes $(X_1,\dots,X_n)\sim\mathcal{N}(0,1)$ y las correspondientes realizaciones $x_1,\dots,x_n$, podemos usar
\[\overline{X}_n:=\frac{X_1+\cdots+X_n}{n}\]
como estimador de la media. Este estimador tiene un punto de quiebre de $0$, ya que podemos hacer $\overline{x}$ arbitrariamente grande cambiando sólo un valor de los $x_n$.

Cuanto más alto sea el punto de quiebre de un estimador, más robusto es. Se puede intuir que el punto de quiebre no puede superar el $50\%$ porque si más de la mitad de las observaciones están contaminadas, no es posible distinguir entre la distribución real y las observaciones discordantes. Por lo tanto el mayor punto de quiebre es $0,5$ y hay estimadores que lo logran. Un ejemplos la mediana.

\subsection{Función de influencia}

%TODO explicar las funciones de influencia.

Tratan de medir cuan tolerante es un estimador a una pequeña variación de la distribución de las muestras. No es la tolerancia a valores que no perteneces a la misma distribución que las muestras, pero es tolerancia a que las muestras no sean de la distribución que nosotros pensamos.

\subsection{Estimadores M}

Los estimadores M forman un grupo amplio de estadísticas obtenidas de la solución del problema de minimizar ciertas funciones de las datos.

Algunos autores definen los estimadores M como las raíces de un sistema de ecuaciones que surge de ciertas funciones de los datos. Usualmente estas funciones son las derivadas de las funciones que se quieren minimizar en la definición anterior.

Muchas estadísticas clásicas son estimadores M. Su principal utilidad, sin embargo, es crear alternativas robustas a los estimadores clásicos.

\subsubsection{Motivación}

Para una familia de funciones de densidad de probabilidad $f$ parametrizada por $\theta$, el estimador de máxima verosimilitud de $\theta$ se computa maximizando la función de verosimilitud en $\theta$. Es estimador es:

\[\widehat{\theta} = \operatorname{argmax}_{\theta}{ \left( \prod_{i=1}^n f(x_i, \theta) \right) }\]

o lo que es lo mismo,

\[\widehat{\theta} = \operatorname{argmin}_{\theta}{ \left( -\sum_{i=1}^n \log{( f(x_i, \theta) ) }\right)}\]

La performance de los estimadores de máxima verosimilitud depende fuertemente de que la distribución de probabilidad asumida para las muestras sea aproximadamente verdad. En particular, los estimadores máxima verosimilitud pueden ser ineficientes y sesgados cuando los datos no son de la distribución asumida. Otro factor que afecta a estos estimadores es la presencia de datos discordantes.

\subsubsection{Definición}

En 1964, Peter Huber propuso generalizar la estimación máxima verosimilitud a la minimización de 
\[\sum_{i=1}^n\rho(x_i, \theta)\]
donde $\rho$ es una función con ciertas propiedadres que se describen luego.

Las soluciones,

\[\hat{\theta} = \operatorname{argmin}_{\theta}\left(\sum_{i=1}^n\rho(x_i, \theta)\right) \,\!\]

son llamados estimadores M (``M'' por ser del tipo de máxima verosimilitud\footnote{Huber, 1981, página 43}). Otros tipos de estimadores robustos incluyen los estimadores L, los R y los S. Por lo tanto, los estimadores de máxima verosimilitud son un caso especial de los estimadores M.

La función $\rho$ o su derivada $\psi$ se pueden elegir de forma tal de proveer al estimador propiedades deseables (en términos de sesgo y eficiencia) cuando los datos son realmente de la distribución asumida y asegurar un comportamiento ``no muy malo'' cuando los datos pertenecen a una distribución diferente.

Por lo tanto, los estimadores M son los $\theta$ que minimizan:
\[\sum_{i=1}^n\rho(x_i,\theta)\]
Para minimizar esta ecuación usualmente es más sencillo derivarla respecto de $\theta$ y buscar las raíces. Cuando esta derivada existe se lo suele llamar un estimador M de tipo $\psi$, de no existir se los conoce como de tipo $\rho$.

En la mayoría de los casos prácticos, los estimadores son M son de tipo $\psi$.

\subsubsection{Cálculo}

Para muchas $\rho$ o $\psi$, no se puede encontrar una forma cerrada para resolver el estimador. Por lo tanto se emplean métodos iterativos de cálculo. Es posible usar alguno de los algoritmos estándar como el de Newton-Raphson. Sin embargo, en muchos casos se suele usar un método de regresión por cuadrados mínimos con pesos variables.

Para algunos $\psi$, en particular aquellas donde $\lim_{x\rightarrow\pm\infty} \psi(x) = 0$ (llamadas \textit{redescending}), la solución puede no ser única. Por lo tanto se debe tener cuidado de seleccionar buenos valores iniciales. Por ejemplo, se puede usar la mediana como un estimador de posición y el desvio mediano absoluto (MAD) como un estimador de escala.

\subsubsection{Ejemplos}

Cómo se explicó anteriormente un estimador M no es necesariamente robusto. Lo va a ser o no, dependiendo de la función $\rho$ que se use. Algunos ejemplos de funciones $\rho$ que producen estimadores robustos son los siguientes:
\begin{center}
 \includegraphics[width=0.9\textwidth]{graficos/ejemplos-rho.pdf}
\end{center}

\end{document}
