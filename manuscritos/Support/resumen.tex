
\documentclass[12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[spanish]{babel}
\usepackage{graphicx}
\usepackage[top=3cm,bottom=3cm,left=3cm,right=3cm]{geometry}
%\usepackage{iwona}

\title{Desarrollo de un estimador ciego para canales MIMO}
\author{A. Lucas Chiesa \\ Directora: Cecilia G. Galarza}



\begin{document}
\maketitle
\tableofcontents
\newpage

\section{Introducción}
En una comunicación es necesario siempre estimar la respuesta del canal de comunicaciones para poder decodificar correctamente el mensaje transmitido.

Dentro del marco de las redes de sensores, se plantean nuevos requerimientos por los limitados recursos con los que se cuentan. Sin embargo, también hay nuevas oportunidades que se pueden aprovechar.

El objetivo de este trabajo es diseñar un estimador ciego de canal que sea de baja complejidad computacional y que se pueda eventualmente distribuir entre más de un nodo de la red, reduciendo así la carga sobre cada uno de ellos.

Se toma como base el estimador EM (\textit{Expectation Maximization}) para estimar canales MIMO.

\section{Modelo de canal MIMO}
\label{sec:canal}

Se tiene un sistema como el de la figura:
\begin{center}
 \includegraphics{img/estimador_mimo.pdf}
\end{center}

Donde $X1$ y $X2$ son los datos transmitidos por dos nodos de una red. $H$ es la
matriz de ganancias del canal. $N$ es el ruido blanco aditivo guassiano de
media nula y varianza $\sigma^{2}$. $y1$, $y2$ son las salidas del canal. En
este
caso un nodo receptor escucha las dos salidas, por ejemplo, usando dos antenas.

Sin embargo, también podrían ser dos nodos con un canal de comunicación entre ellos.

Matricialmente, el sistema se representa usando:
\begin{equation}
X= \left[\begin{matrix}
 x_1 \\
 x_2
\end{matrix}\right]
\ N= \left[\begin{matrix}
 n_1 \\
 n_2
\end{matrix}\right]
\ Y= \left[\begin{matrix}
 y_1 \\
 y_2
\end{matrix}\right]
\ \ H= \left[\begin{matrix}
 h_{11} & h_{12} \\
 h_{21} & h_{22}
\end{matrix}\right]
\end{equation}

Donde $Y$ resulta de:

\begin{equation}
 Y=HX+N
\label{canal}
\end{equation}

Se sabe que las ganancias $h_{ij}$ son valores reales. Se conoce la
distribución utilizada para transmitir, y que los símbolos son equiprobables.
Por ejemplo, si se tienen $B$ símbolos diferentes en la constelación:

\begin{equation}
 P(x)=\dfrac{1}{B} \delta(x-x_1) + \ldots +\dfrac{1}{B} \delta(x-x_B)
\label{pdex}
\end{equation}

Como se ve en la ecuación (\ref{canal}) a cada salida $y_{i}$ se le va a sumar
una realización de ruido $n_i$ diferente, por lo que la distribución de
probabilidad de $N$ es una normal multivariable.

\begin{equation}
 P(N) = \dfrac{1}{2\pi |\varSigma|^{1/2}} \exp \left(-\dfrac{1}{2} N^T
\varSigma^{-1} N\right)
\label{pden}
\end{equation}

Donde $\varSigma$ es la matriz de covarianza de $n_1$ y $n_2$:

\begin{equation}
 \varSigma = \left[\begin{matrix}
 \sigma^2_{1} & 0 \\
 0 & \sigma^2_{2}
\end{matrix}\right]
\end{equation}

Como tomamos dos nodos transmisores, la distribución de $X$, que contiene las
dos señales transmitidas es:
\begin{equation}
 P(X)=\dfrac{1}{B^2} \delta(x-x_1) + \ldots +\dfrac{1}{B^2} \delta(x-x_{B^2})
\end{equation}
Para simplificar las ecuaciones creamos el conjunto $\mathcal{X}$ que contiene
todos los $B^2$ posibles valores de $X$.

\section{Desarrollo experimental}

El objetivo de este trabajo se puede separar en varios actividades específicas,
\begin{itemize}
\item Diseño del estimador.
\item Diseño de un método de validación de la estimación.
\item Evaluación de la performance de los estimadores.
\item Evaluar la complejidad del estimador.
\item Proponer una forma de distribuir el algoritmo entre varios nodos.
\end{itemize}

Lo primero que se busca es un estimador que funcione razonablemente bien y que, acompañado de un validador sea capaz de distinguir una buena estimación de una mala. Para esto se está trabajando con dos algoritmos iterativos de estimación ciega del canal, descriptos a continuación.

Se tomó el algoritmo EM como base para el desarrollo debido a su baja complejidad numérica y su popularidad.

El primero es un planteo directo. estimar el canal (matriz $H$) directamente de un conjunto de muestras captadas por el receptor.

En segundo lugar se prueba un algoritmo que separa el problema en dos etapas: a) detección de los símbolos transmitidos y b) la estimación del canal.

Para probar si una estimación es correcta, se desarrolló un validador de estimación basado en el análisis estadístico de los errores en la detección.

\subsection{Estimador directo}
Se quiere formular un estimador de la matriz $H$ de ganancias del canal, usando
el método EM.
Como se sabe, aplicando este método se obtiene el siguiente estimador
recursivo:

\begin{equation}
 H_{k+1} = \arg\max_H \left( E_{X/Y} \left[ \log
\dfrac{P(X,Y;H)}{P(X,Y;H_k)};H_k \right] \right)
\label{estimador}
\end{equation}

Resolviendo como se explica en el apéndice \ref{ap:estimador}, se obtiene la siguiente expresión:

\begin{equation}
H_{k+1} = \sum_{i=1}^{M} \sum_{x \in \mathcal{X}} \left[ \dfrac{Y(i)X^T \exp
\left( \frac{-\mathcal{E}(i)_k}{2} \right) }{\sum_{x \in \mathcal{X}} \exp
\left( \frac{-\mathcal{E}(i)_k}{2} \right)} \right] \cdot \left\lbrace
\sum_{i=1}^{M} \sum_{x \in \mathcal{X}} \left[\dfrac{XX^T \exp \left(
\frac{-\mathcal{E}(i)_k}{2} \right) }{\sum_{x \in \mathcal{X}} \exp \left(
\frac{-\mathcal{E}(i)_k}{2} \right)} \right] \right\rbrace^{-1}
\end{equation}

Donde $\mathcal{E} = \left((Y-HX)^T \varSigma^{-1} (Y-HX)\right)$ y $\mathcal{E}_{k} = \left((Y-H_{k}X)^T \varSigma^{-1} (Y-H_{k}X)\right)$.

\subsection{Estimador DFE}

A diferencia del estimador anterior, este algoritmo separa el problema en dos partes identificables. En cada iteración, se hace una estimación del valor del símbolo transmitido, que se puede considerar una \textit{soft detection}. Luego, con ese valor, se estima el valor de $H$, como se explica en la siguiente figura:

\begin{center}
 \includegraphics[width=0.5\textwidth]{img/loop.pdf}
 % loop.pdf: 595x842 pixel, 72dpi, 20.99x29.70 cm, bb=0 0 595 842
\end{center}

Recibe el nombre de estimador DFE ya que se usan las decisiones de detección para alimentar el estimador de canal en cada iteración.

Para estimar el valor de $H$ se va a maximizar $P(Y(i)/H,X(i))$. Sin embargo, como es una estimación ciega, no se conoce el valor de $X$, los símbolos transmitidos. Bajo la hipótesis de que los símbolos recibidos son independientes, se plantea:

\begin{equation}
 E_{X/Y}[P(\overline{Y}/H,\overline{X})] = E_{X/Y}\left[\prod_{i=0}^N P(Y(i)/H,X(i))\right]
\end{equation}

Usando las distribuciones de probabilidad descriptas en la sección \ref{sec:canal}, se obtiene que maximizar esa probabilidad equivale a:

\begin{equation}
 H_{k+1} = \arg\max_{H} E_{X/Y}\left[\sum_{i=0}^N \lVert Y(i)-HX(i) \rVert^{2} \right]
\label{eq:h_ls}
\end{equation}

Redefiniendo la matriz $H$ como:

\begin{equation}
H= \left[\begin{matrix}
 h^{(a)T} \\
 h^{(b)T}
\end{matrix}\right]
\ h^{(a)} = \left[\begin{matrix}
 h_{11} \\
 h_{12}
\end{matrix}\right]
\ h^{(b)} = \left[\begin{matrix}
 h_{21} \\
 h_{22}
\end{matrix}\right]
\end{equation}

Resolviendo la ecuación \ref{eq:h_ls}, derivando e igualando a cero se obtiene:

\begin{equation}
h_{k+1}^{(a)} = \left[ \sum_{i=0}^N \tilde{X}(i) \right]^{-1} \sum_{i=0}^N \hat{X}(i)y_{1}(i)
\end{equation}
\begin{equation}
h_{k+1}^{(b)} = \left[ \sum_{i=0}^N \tilde{X}(i) \right]^{-1} \sum_{i=0}^N \hat{X}(i)y_{2}(i)
\end{equation}
Donde $i$ es el número de muestra y $k$ la iteración y,
\begin{equation}
 \hat{X}_{k}(i) = E_{X/Y}[X(i);H_{k-1}] \ \ \ \
\tilde{X}_{k}(i) = E_{X/Y}[X(i)X(i)^{T};H_{k-1}]
\end{equation}

El desarrollo de estas ecuaciones se encuentra en el apéndice \ref{sec:dfe}.

\subsection{Validador de estimación}

Como ya vimos, al receptor llega:

\begin{equation*}
 Y=HX+N
\end{equation*}

Por lo tanto, si se tiene una estimación de $H$ correcta ($\hat{H}$) y un vector de símbolos detectados ($\hat{X}$),

\begin{equation}
 Y-\hat{H}\hat{X} = N
\end{equation}

Este hecho se podría utilizar para verificar si el valor de $H$ obtenido es correcto. Sin embargo, como no se conoce la realización de ruido que se suma durante la transmisión, se determinó un criterio para evaluar si la diferencia $Y-\hat{H}\hat{X}$ es equivalente al ruido del canal.

Se toma la traza de la matriz de covarianza del ruido como una variable aleatoria. Como dicha matriz ($\varSigma$) se supone conocida, el estimador puede calcular su traza y la varianza de su traza. (El desarrollo de esta ecuación se puede consultar en el apéndice \ref{sec:traza})
\begin{equation}
 \textrm{Var}\left[tr(\varSigma)\right] = \dfrac{2}{4}tr\left(\varSigma^{2}\right)
\end{equation}

Una vez finalizada la estimación de $H$ y realizada la detección, se calculan los errores $Y-\hat{H}\hat{X}$. Se calcula su matriz de covarianza ($\hat{\varSigma}$) y luego se computa su traza.

Finalmente se evalúa si el valor de traza obtenido pertenece a un intervalo definido por el valor de la traza real y su varianza:

\begin{equation}
\lvert tr(\hat{\varSigma}) - tr(\varSigma) \rvert < \alpha \textrm{Var}\left[tr(\varSigma)\right]
\label{eq:rango}
\end{equation}

Se decide que una estimación es correcta si se cumple la desigualdad \ref{eq:rango}.

\subsection{Evaluación de performance de los estimadores}

Para evaluar la performance de los estimadores se programaron simulaciones en computadora usando programas de cálculo numérico.

Las simulaciones generan una señal de datos a transmitir con símbolos de un alfabeto definido. Manteniendo un nivel de señal a ruido especificado, generan un canal con una matriz de ganancias y ruido aleatorio.

Con los datos a transmitir y el canal, se genera la señal recibida. Ésta es analizada por el estimador para obtener una matriz de ganancias.

Es interesante notar que el problema que está tratando de resolver el estimador no está del todo definido. Por lo tanto, existen varias soluciones correctas. Éstas corresponden a diferentes permutaciones en $H$. Los algoritmos empleados en estas simulaciones tienen en cuenta este hecho para decidir si una estimación es correcta.

Una vez finalizada la estimación se detecta el mensaje enviado y se usa el validador para evaluar si la estimación fue correcta.

Inicialmente se trabajó con un alfabeto binario. Con él se obtienen estimaciones muy satisfactorias.
\begin{equation*}
 x_i \in \{-1,\ \ 1\}
\end{equation*}

Luego se utilizó un alfabeto de 4 niveles con el objetivo de aumentar la complejidad del problema a resolver.
\begin{equation*}
 x_i \in \{-3,\ \ -1,\ \ 1,\ \ 3\}
\end{equation*}

El sistema simulado es un sistema como el descripto en la sección \ref{sec:canal}, con dos transmisores y dos receptores. Es decir, un canal MIMO de 2x2.

Luego de realizar las simulaciones se grafica la evolución del valor de cada elemento de la matriz del canal y se lo compara con el valor real.

Es importante notar que todas las simulaciones se realizan en las mismas condiciones. Se toman 50 muestras y se trabaja con un SNR = 20 dB.

\subsubsection{Estimador directo}

A continuación se muestra el gráfico resultado de una simulación para un sistema de 2 símbolos. En este caso la solución fue correcta. Cada línea horizontal corresponde a un valor de la matriz $H$ original, mientras que las curvas representan la evolución del estimador a lo largo de las distintas iteraciones.

\begin{center}
 \includegraphics[width=0.8\textwidth]{img/2_niveles_directo.pdf}
\end{center}

Si se corre la simulación usando una constelación de 4 puntos, se obtiene el mismo tipo de gráficos. A continuación se observan dos gráficos usando esta constelación, la primera correcta y la segunda errónea.

\begin{center}
 \includegraphics[width=0.8\textwidth]{img/4_niveles_directo_bien.pdf}
\end{center}
\begin{center}
 \includegraphics[width=0.8\textwidth]{img/4_niveles_directo_mal.pdf}
\end{center}

Una forma de evaluar la performance del estimador es hacer un número significativo de simulaciones y usar el validador para ver si la estimación fue correcta.

Se realizaron dos experiencias de 1000 simulaciones cada una. La primera con una constelación binaria y la segunda con una constelación de 4 niveles.
\begin{itemize}
\item En el sistema binario la estimación fue correcta en el 74\% de los casos.
\item En el sistema de 4 niveles la estimación fue correcta en el 48\% de los casos.
\end{itemize}


\subsubsection{Estimador DFE}

Para facilitar la comparación de los estimadores se muestran los mismos resultados que en el caso anterior.

Estimación correcta para una constelación de 2 niveles:
\begin{center}
 \includegraphics[width=0.8\textwidth]{img/2_niveles_dfe.pdf}
\end{center}
Estimación correcta para una constelación de 4 niveles:
\begin{center}
 \includegraphics[width=0.8\textwidth]{img/4_niveles_dfe_bien.pdf}
\end{center}
Y una triste estimación incorrecta:
\begin{center}
 \includegraphics[width=0.8\textwidth]{img/4_niveles_dfe_mal.pdf}
\end{center}

Para este estimador se realizaron las mismas corridas de 1000 simulaciones cada una, en las cuales:
\begin{itemize}
\item En el sistema binario la estimación fue correcta en el 73.6\% de los casos.
\item En el sistema de 4 niveles la estimación fue correcta en el 25\% de los casos.
\end{itemize}


\section{Discusión de resultados}

Primero es apropiado analizar el modelo de canal usado. Se está trabajando con un canal muy simple, que sólo afecta la transmisión con una ganancia y no con una respuesta impulsiva. Más aun, estas ganancias son modeladas con números reales y no complejos.

Si bien en el análisis de los estimadores se usaron estadísticas obtenidas con el validador, es importante notar que todavía no es posible evaluar correctamente la performance de los estimadores. Ésto se debe a las falencias en el validador, que puede arrojar tanto falsos positivos como falsos negativos.

Haciendo una inspección manual de cada simulación, se puede concluir que el estimador directo tiene mejor performance que el DFE.

Ambos estimadores logran, en determinadas condiciones, estimar correctamente el canal. Por lo tanto, creemos que son muy sensibles a los valores iniciales con los que comienza la iteración.

Para contrastar esta hipótesis se alteró el estimador con el objeto de variar en forma aleatoria el valor de los parámetros estimados durante el proceso.

Se observó que al modificar la evolución natural del proceso, se logra cambiar el punto de convergencia del algoritmo.

Este comportamiento se puede apreciar en el siguiente gráfico:
\begin{center}
 \includegraphics[width=0.8\textwidth]{img/directo_shake.pdf}
\end{center}
Se observa como el estimador se orienta hacia los valores reales. En la iteración 10, los valores son modificados aleatoriamente y se ve que el estimador converge hacia una solución errónea. Recién con la tercera modificación, en la iteración 30 el estimador vuelve a converger a la solución deseada.

De esta simulación se observa que algunos valores de condiciones iniciales conducen a resultados correctos, mientras que otros (resolviendo el mismo sistema) arrojan resultados incorrectos. Estos resultados sugieren que el punto de convergencia se modifica dependiendo de las condiciones iniciales.

\section{Conclusiones parciales y próximos desarrollos}

En la realización de este trabajo primero se desarrollo el estimador directo, aplicando el algoritmo EM directamente al problema. Sin embargo, si bien se estudió con mucho detalle no se llegó a obtener una performance desea.

Es por esto que se decidió implementar un estimador de tipo DFE. Si bien, en primera instancia, éste parece ser peor que el anterior, su estructura permite mayores modificaciones que el directo. Un ejemplo de esto es reemplazar la estimación de $H$ basada en el método de cuadrados mínimos, que se usa actualmente, por una basada en estadística robusta.

El validador también debe ser modificado para que tenga mayor confiabilidad. Para lo cual se propone también, usar estadística robusta para estimar las varianzas necesarias.

Como próximos trabajos, una vez obtenido un estimador con un probado nivel de performance, se debe analizar su complejidad computacional y evaluar si se puede optimizar el algoritmo.

Como último paso se identificarán los elementos del algoritmo que se deberían compartir para que dos nodos receptores puedan cooperar en la estimación del canal.

\newpage
\appendix
\section{Desarrollo del estimador directo}
\label{ap:estimador}

A continuación se detalla el procedimiento matemático para obtener el estimador a partir de la ecuación \ref{estimador}.

Si resolvemos la esperanza:
\begin{equation}
E_{X/Y} \left[ \log \dfrac{P(X,Y;H)}{P(X,Y;H_k)};H_k \right] = \sum_{x \in
\mathcal{X}} P(X/Y;H_k)\log \left(\dfrac{P(X,Y;H)}{P(X,Y;H_k)}
 \right)
\label{esperanza}
\end{equation}
Para encontrar la forma cerrada del estimador es necesario obtener $P(X/Y;H)$ y
$P(X,Y;H)$. Usando $P(X,Y;H) = P(Y/X;H)P(X;H)$, como sabemos $P(X;H)$ (ecuación
\ref{pdex}), sólo necesitamos $P(Y/X;H)$. Si de la ecuación (\ref{canal})
despejamos $N$ en función  de $Y$ y $X$, y luego reemplazamos ese resultado en
(\ref{pden}), obtenemos:

\begin{equation}
  P(Y/X;H) = \dfrac{1}{2\pi |\varSigma|^{1/2}} \exp \left(-\dfrac{1}{2} (Y-HX)^T
\varSigma^{-1} (Y-HX)\right)
\label{pdeydadox}
\end{equation}

Multiplicando \ref{pdeydadox} por \ref{canal}, se obtiene:

\begin{equation}
 \left. P(X,Y;H) = \dfrac{1}{B^2\ 2\pi |\varSigma|^{1/2}} \exp
\left(-\dfrac{1}{2} (Y-HX)^T \varSigma^{-1} (Y-HX)\right) \right|_{x \in
\mathcal{X}}
\end{equation}

A partir de esta ecuación, obtenemos la función $P(Y;H)$:

\begin{eqnarray}
P(Y;H) &=& \sum_{x \in \mathcal{X}} P(X,Y;H)  \nonumber \\
&=& \sum_{x \in \mathcal{X}}
\dfrac{1}{B^2\ 2\pi |\varSigma|^{1/2}} \exp \left(-\dfrac{1}{2}
(Y-HX)^T \varSigma^{-1} (Y-HX)\right)
\label{pdey}
\end{eqnarray}

Luego:

\begin{eqnarray}
P(X/Y;H) &=& \dfrac{P(X,Y;H)}{P(Y;H)} \nonumber \\
&=& \dfrac{1}{B^2\ 2\pi |\varSigma|^{1/2}} \exp \left(-\dfrac{1}{2}
(Y-HX)^T \varSigma^{-1} (Y-HX)\right) \cdot \nonumber \\
&& \left. \left[ \sum_{x \in \mathcal{X}} \dfrac{1}{B^2\ 2\pi |\varSigma|^{1/2}}
\exp \left(-\dfrac{1}{2} (Y-HX)^T \varSigma^{-1} (Y-HX)\right)
\right]^{-1} \right|_{x \in \mathcal{X}}
\label{pdexdadoy}
\end{eqnarray}

Ahora tenemos todo lo necesario para escribir la esperanza de la ecuación
(\ref{esperanza}), pero antes vamos a resolver el $\log$.

% TODO: Chequear que este bien el logaritmo. Sobre todo los signos.

\begin{eqnarray}
\log \dfrac{P(X,Y;H)}{P(X,Y;H_k)} = \dfrac{1}{2} \left[ \left((Y-H_{k}X)^T
\varSigma^{-1} (Y-H_{k}X)\right)\right. \nonumber \\
\left. - \left((Y-HX)^T \varSigma^{-1} (Y-HX)\right)\right]
\label{log}
\end{eqnarray}

Para simplificar las ecuaciones, nombramos $\mathcal{E} = \left((Y-HX)^T
\varSigma^{-1} (Y-HX)\right)$ y $\mathcal{E}_{k} = \left((Y-H_{k}X)^T
\varSigma^{-1} (Y-H_{k}X)\right)$. Reemplazando (\ref{pdexdadoy}) y (\ref{log})
en (\ref{esperanza}) obtenemos:

\begin{eqnarray}
E_{X/Y} \left[ \log \dfrac{P(X,Y;H)}{P(X,Y;H_k)};H_k \right] &=& \nonumber \\
&=& \sum_{x \in \mathcal{X}} \left\lbrace \dfrac{1}{2} \left[ \mathcal{E}_k -
\mathcal{E} \right] \exp \left( -\dfrac{\mathcal{E}_k}{2} \right) \cdot \right.
\nonumber \\
&\cdot& \left.  \left[ \sum_{x \in \mathcal{X}} \exp \left(
-\dfrac{\mathcal{E}_k}{2} \right)\right]^{-1} \right\rbrace = \nonumber \\
&=& Q(H,H_k)
\label{q}
\end{eqnarray}

Siguiendo la ecuación (\ref{estimador}), debemos buscar el $H$ que maximiza
la función $Q(H,H_k)$ obtenida en (\ref{q}). Para ello se calcula
$\frac{\partial Q}{\partial H}$. Si se analiza la función (\ref{q}) se ve que
se puede separar en dos sumatorias sobre $x$, una que tiene sólo $H_k$ y la
otra que tiene un término con $H$ multiplicando todos términos con $H_k$. Por
lo tanto, $\frac{\partial Q}{\partial H}$ va a ser $\frac{\partial
\mathcal{E}}{\partial H}$, multiplicado por las exponenciales. Como realizar
esta derivada se explica en detalle en el apéndice \ref{desarrollo_derivada} en
la página \pageref{desarrollo_derivada}.

Usando el resultado de la ecuación (\ref{derivada}), obtenemos $\frac{\partial
Q}{\partial H}$ y la igualamos a $0$.

\begin{equation}
\sum_{x \in \mathcal{X}} \left[ \dfrac{YX^T \exp \left(
\frac{-\mathcal{E}_k}{2} \right) }{\sum_{x \in \mathcal{X}} \exp \left( 
\frac{-\mathcal{E}_k}{2} \right)} \right] - \sum_{x \in \mathcal{X}} \left[
\dfrac{HXX^T \exp \left( \frac{-\mathcal{E}_k}{2} \right) }{\sum_{x \in
\mathcal{X}} \exp \left( \frac{-\mathcal{E}_k}{2} \right)} \right] = 0
\label{qderiv}
\end{equation}

Si generalizamos el desarrollo para muchas muestras del canal y no una sola, el
vector $Y$ sería de $2xM$ donde $M$ es la cantidad de muestras tomadas y la
ecuación (\ref{qderiv}) queda:

\begin{equation}
\sum_{i=1}^{M} \sum_{x \in \mathcal{X}} \left[ \dfrac{Y(i)X^T \exp \left(
\frac{-\mathcal{E}(i)_k}{2} \right) }{\sum_{x \in \mathcal{X}} \exp \left(
\frac{-\mathcal{E}(i)_k}{2} \right)} \right] = \sum_{i=1}^{M} \sum_{x \in
\mathcal{X}} \left[\dfrac{HXX^T \exp \left( \frac{-\mathcal{E}(i)_k}{2} \right)
}{\sum_{x \in \mathcal{X}} \exp \left( \frac{-\mathcal{E}(i)_k}{2} \right)}
\right]
\end{equation}

Finalmente, despejando $H$ de la ecuación anterior, se obtiene el valor de
$H$ que maximiza $Q(H,H_k)$, por lo que se obtiene el valor $H_{k+1}$:

\begin{equation}
H_{k+1} = \sum_{i=1}^{M} \sum_{x \in \mathcal{X}} \left[ \dfrac{Y(i)X^T \exp
\left( \frac{-\mathcal{E}(i)_k}{2} \right) }{\sum_{x \in \mathcal{X}} \exp
\left( \frac{-\mathcal{E}(i)_k}{2} \right)} \right] \cdot \left\lbrace
\sum_{i=1}^{M} \sum_{x \in \mathcal{X}} \left[\dfrac{XX^T \exp \left(
\frac{-\mathcal{E}(i)_k}{2} \right) }{\sum_{x \in \mathcal{X}} \exp \left(
\frac{-\mathcal{E}(i)_k}{2} \right)} \right] \right\rbrace^{-1}
\end{equation}


\section{Desarrollo del estimador DFE}
\label{sec:dfe}

$\overline{Y}$ es un vector de \emph{cantidad de receptores (M)} x \emph{cantidad de muestras (N)} que contiene la señal recibida y $\overline{X}$ es un vector de igual dimensiones que contiene los símbolos transmitidos. Se consideran los símbolos independientes.

Se busca maximizar la probabilidad de recibir un Y dado el X detectado y la estimación del canal,

\begin{equation}
 P(\overline{Y}/\overline{X};H) = \prod_{i=0}^{N-1} P(Y(i)/X(i);H)
\end{equation}

Reemplazando la ecuación \ref{pdeydadox} y aplicando logaritmo, se obtiene:

\begin{equation}
 P(\overline{Y}/\overline{X};H) = \log \left(\dfrac{1}{2\pi \sqrt{\varSigma}}\right) - \sum_{i=0}^{N-1} \dfrac{1}{2} (Y(i)-HX(i))^{T} \varSigma^{-1} (Y(i)-HX(i))
\end{equation}

Si se desea obtener la máxima probabilidad se va a buscar minimizar el segundo término. Cómo el estimador es ciego, no se conocen los símbolos transmitidos. Por lo tanto se va a minimizar la esperanza en X de dicho término:

\begin{equation}
 \arg\min_{H} E_{X/Y}\left[\sum_{i=0}^{N-1} \dfrac{1}{2} (Y(i)-HX(i))^{T} \varSigma^{-1} (Y(i)-HX(i))\right]
\label{posta}
\end{equation}

Como lo que se busca es el valor de $H$ que minimiza esa ecuación, se puede reemplazar $\varSigma$ por una matriz identidad. También, para simplificar la matemática se va a redefinir el $X(i)$ y $H$ de la siguiente forma:

\begin{equation}
H= \left[\begin{matrix}
 h^{(a)T} \\
 h^{(b)T}
\end{matrix}\right]
\ h^{(a)} = \left[\begin{matrix}
 h_{11} \\
 h_{12}
\end{matrix}\right]
\ h^{(b)} = \left[\begin{matrix}
 h_{21} \\
 h_{22}
\end{matrix}\right]
\ X(i) = \left[\begin{matrix}
 x_1(i) & x_2(i) \\
 \end{matrix}\right]
\ Y(i) = \left[\begin{matrix}
 y_1(i) \\
 y_2(i)
 \end{matrix}\right]
\end{equation}

Obteniendo así dos problemas desacoplados, uno para obtener $h_{a}$ y otro para $h_{b}$. Como son idénticos se desarrollo uno sólo. Para simplificar la notación se escribe $E[f(x,y)] = E_{X/Y} [f(x,y)]$.

\begin{equation}
 E\left[\sum_{i=0}^{N-1} \lVert y_1(i)-X(i)h_{a} \rVert^2 \right] = E\left[\sum_{i=0}^{N-1} (y_1(i)-h_{a}^{T}X^{T}(i))(y_1(i)-X(i)h_{a}) \right]
\end{equation}

Desarrollando el cuadrado, sacando la sumatoria de la esperanza y distribuyendo la esperanza, se obtiene:

\begin{equation}
\sum_{i=0}^{N-1} \left\{ y_{1}^{2}(i) - y_{1}(i)E[X(i)]h_{a} - h_{a}^{T} E[X^{T}(i)] y_{1}(i) + h_{a}^{T} E[X^{T}(i)X(i)] h_{a} \right\}
\label{eq:e_desarrollada}
\end{equation}
Va a ser trabajo del detector o estimador de símbolos calcular las esperanzas:
\begin{eqnarray}
\hat{X}(i) &=& E_{X/Y(i)} [X;H] = \sum_{x \in \mathcal{X}} x P(X/Y(i); H) \\
\tilde{X}(i) &=& E_{X/Y(i)} [X^{T}X;H] = \sum_{x \in \mathcal{X}} x^{T}x P(X/Y(i); H)
\end{eqnarray}
Si se completa cuadrados, se puede reescribir \ref{eq:e_desarrollada} de la siguiente forma: (no se escribe la sumatoria en $i$ para reducir la notación).

\begin{equation}
E[\lVert y_1 - Xh_a \rVert^2] = \lVert y_1 - \hat{X}h_a \rVert^2 + h^{T}_a(\tilde{X}-\hat{X}^{T}\hat{X}) h_a
\label{eq:e_completa}
\end{equation}

Para obtener el valor de $H$ que cumple \ref{posta}, se va a derivar \ref{eq:e_completa} e igualar a cero.

Se plantea un análisis término a término para facilitar el desarrollo. Para resolver el primer término se va a aplicar la regla de derivada de un producto y para el segundo la fórmula de derivación de una forma cuadrática:
\begin{eqnarray}
 \dfrac{\partial}{\partial h_a} h^{T}_a\left(\tilde{X}-\hat{X}^{T}\hat{X}\right) h_a &=& \left\{(\tilde{X}-\hat{X}^{T}\hat{X}) + (\tilde{X}-\hat{X}^{T}\hat{X})^T\right\} h_a \\
\dfrac{\partial}{\partial h_a} \left(y_1-h^T_a \hat{X}^T\right) &=& -\hat{X}^T \\
\dfrac{\partial}{\partial h_a} \left(y_1-\hat{X}h_a\right) &=& -\hat{X}^T
\end{eqnarray}
Juntando todos los resultados anteriores, la derivada se escribe:
\begin{equation}
-2\hat{X}^{T}y_1 + 2\hat{X}^{T}\hat{X} h_a + \left(\tilde{X}-\hat{X}^{T}\hat{X}\right) h_a
\end{equation}
Igualando a cero, y despejando $h_a$ y volviendo a introducir la sumatoria en $i$, se obtiene:

\begin{equation}
h_{k+1}^{(a)} = \left[ \sum_{i=0}^N \tilde{X}(i) \right]^{-1} \sum_{i=0}^N \hat{X}(i)y_{1}(i)
\end{equation}
\begin{equation}
h_{k+1}^{(b)} = \left[ \sum_{i=0}^N \tilde{X}(i) \right]^{-1} \sum_{i=0}^N \hat{X}(i)y_{2}(i)
\end{equation}


\section{Desarrollo de la derivada de $\mathcal{E}$\label{desarrollo_derivada}}
\begin{eqnarray*}
  A &=& \left( Y-HX \right)^T \Sigma^{-1} \left( Y-HX \right) \\
    &=& \left( Y-HX \right)^T \Sigma^{-\frac1{2}} \Sigma^{-\frac1{2}}
\left( Y-HX \right) \\
    &=& \left[ \Sigma^{-\frac1{2}} \left( Y-HX \right) \right]^T \left[
\Sigma^{-\frac1{2}} \left( Y-HX \right) \right] \\
    &=& B^T B
\end{eqnarray*}

Si tenemos la forma cuadrática $x^TCx$, siendo $x$ un vector columna y $C$ una
matriz cuadrada, la derivada de esta función con respecto a $x$ es la
siguiente\cite[p. 176]{magnus}:
\begin{equation}
\phi(x) = x^TCx \Longrightarrow \frac{\partial\phi(x)}{\partial x}
= x^T \left( C+C^T \right)
\label{cuad}
\end{equation} 

Usando la ecuación \ref{cuad}, podemos calcular la derivada de $A$.
\begin{equation}
\frac{\partial A}{\partial B} = \frac{\partial}{\partial B}
\left( B^T B \right) = 2B^T
\label{deriv_a}
\end{equation}

Ahora derivemos al vector $B$ con respecto a la matriz $H$ del canal.
\begin{eqnarray}
\frac{\partial B}{\partial H} &=& \frac{\partial}{\partial H}
\left[ \Sigma^{-\frac1{2}} \left( Y-HX \right) \right] \nonumber \\
 &=& \frac{\partial}{\partial H} \left( \Sigma^{-\frac1{2}} Y -
\Sigma^{-\frac1{2}} HX \right) \nonumber \\
 &=& -\frac{\partial}{\partial H} \left( \Sigma^{-\frac1{2}} HX \right) \nonumber \\
 &=& -\Sigma^{-\frac1{2}} \frac{\partial}{\partial H} \left( HX \right)
\label{deriv_b}
\end{eqnarray}

Si tenemos una función vectorial $f(X)=Xa$, siendo $X$ una matriz y $a$ un vector
columna, la derivada de la función es la siguiente\cite[p. 181]{magnus}:
\begin{equation}
f(X) = Xa \Longrightarrow Df(X) = a^T \otimes I \label{matr}
\end{equation}

Si ahora usamos la ecuación \ref{matr} en \ref{deriv_b} resulta:
\begin{equation}
\frac{\partial B}{\partial H} = -\Sigma^{-\frac1{2}} \ \left( X^T\!\otimes I
\right) \label{deriv_b2}
\end{equation}

Juntando las ecuaciones \ref{deriv_a} y \ref{deriv_b2} podemos obtener la
derivada total de la función $A(H)$.
\begin{eqnarray}
\frac{\partial A}{\partial H} &=& \frac{\partial A}{\partial B} \
\frac{\partial B}{\partial H} \nonumber \\
 &=& -2 \ (Y\!-\!H\!X)^T \ \Sigma^{-1} \ \left( X^T\!\otimes I \right)
\nonumber \\
 &=& -2 \ \left[ \underbrace{Y^T \ \Sigma^{-1} \left( X^T\!\otimes I \right)}_{L}
- \underbrace{X^T H^T \ \Sigma^{-1} \left( X^T\!\otimes I \right)}_{G} \right]
\label{deriv_kro}
\end{eqnarray}

Escribamos el desarrollo del primer término de la función anterior (término L).
\begin{eqnarray}
Y^T \ \Sigma^{-1} \left( X^T\!\otimes I \right) &=&
\left[ \begin{array}{cc}
 y_1 & y_2
\end{array} \right]
\left[ \begin{array}{cc}
         \sigma_{n_1}^{-1} & 0 \\
          0 & \sigma_{n_2}^{-1}
\end{array} \right]
\left[ \begin{array}{cccc}
         x_1 & 0 & x_2 & 0 \\
         0 & x_1 & 0 & x_2
\end{array} \right] \nonumber \\
 &=& \left[ \begin{array}{cc}
 y_1 \sigma_{n_1}^{-1} & y_2 \sigma_{n_2}^{-1}
\end{array} \right]
\left[ \begin{array}{cccc}
         x_1 & 0 & x_2 & 0 \\
         0 & x_1 & 0 & x_2
\end{array} \right] \nonumber \\
 &=& \left[ \begin{array}{cccc}
          y_1 \sigma_{n_1}^{-1}x_1 & y_2 \sigma_{n_2}^{-1}x_1 &
          y_1 \sigma_{n_1}^{-1}x_2 & y_2 \sigma_{n_2}^{-1}x_2
\end{array} \right] \label{L1}
\end{eqnarray}

En la tabla sobre derivadas de la referencia\cite[p. 176]{magnus}, encontramos
que al derivar una función escalar en función de una matriz, como es el caso de
$\frac{\partial A}{\partial H}$ tenemos las siguientes acepciones:
\begin{displaymath}
\phi(X) = \left\lbrace \begin{array}{ll}
 D\phi(X) = (\textrm{vec} C)^T & \textrm{lo llama \textit{derivative}} \\
 \frac{\partial \phi(X)}{\partial X} = C & \textrm{denominado \textit{other uses}}
\end{array} \right.
\end{displaymath}

Como nosotros queremos que la derivada nos quede con las mismas dimensiones que
la matriz $H$ del canal, reacomodamos los elementos de la matriz de la ecuación
\ref{L1} teniendo en cuenta lo anterior.
\begin{equation}
L \equiv \left[ \begin{array}{cc}
                 y_1 \sigma_{n_1}^{-1}x_1 & y_1 \sigma_{n_1}^{-1}x_2 \\
                 y_2 \sigma_{n_2}^{-1}x_1 & y_2 \sigma_{n_2}^{-1}x_2
                \end{array} \right] = \Sigma^{-1} Y X^T \label{L2}
\end{equation}

Ahora desarrollemos el término G de la ecuación \ref{deriv_kro}.
\begin{eqnarray}
X^T H^T \ \Sigma^{-1} \left( X^T\!\otimes I \right) &=&
\left[ \begin{array}{cc}
        x_1 & x_2
       \end{array} \right]
\left[ \begin{array}{cc}
        h_{11} & h_{21} \\
        h_{12} & h_{22}
       \end{array} \right]
\left[ \begin{array}{cc}
        \sigma_{n_1}^{-1} & 0 \\
        0 & \sigma_{n_2}^{-1}
       \end{array} \right]
\left[ \begin{array}{ccc}
        x_1I & \vdots & x_2I
       \end{array} \right] \nonumber \\
 &=& \left[ \begin{array}{cc}
\sigma_{n_1}^{-1}x_1(x_1h_{11}+x_2h_{12}) & \sigma_{n_2}^{-1}x_1(x_1h_{21}+x_2h_{22})
\end{array} \right. \ldots \nonumber \\
 & & \quad \left. \begin{array}{cc}
\sigma_{n_1}^{-1}x_2(x_1h_{11}+x_2h_{12}) & \sigma_{n_2}^{-1}x_2(x_1h_{21}+x_2h_{22})
            \end{array} \right] \label{G1}
\end{eqnarray}

Del mismo modo que en el caso anterior, reacomodamos los elementos de la matriz
de la ecuación \ref{G1}.
\begin{eqnarray}
G &\equiv& \left[ \begin{array}{cc}
\sigma_{n_1}^{-1}x_1(x_1h_{11}+x_2h_{12}) & \sigma_{n_1}^{-1}x_2(x_1h_{11}+x_2h_{12}) \\
\sigma_{n_2}^{-1}x_1(x_1h_{21}+x_2h_{22}) & \sigma_{n_2}^{-1}x_2(x_1h_{21}+x_2h_{22})
                \end{array} \right] \nonumber \\
 &=& \Sigma^{-1} H X X^T \label{G2}
\end{eqnarray}

Juntando las reformulaciones de los términos L y G (ecuaciones \ref{L2} y \ref{G2})
obtenemos la derivada de la función $A$ con las mismas dimensiones de la matriz
del canal.
\begin{equation}
\frac{\partial A}{\partial H} = -2\left( \Sigma^{-1} Y X^T - \Sigma^{-1} H X
X^T\right)
\label{derivada}
\end{equation}

\section{Estimación de la traza y varianza de la traza de la covarianza del ruido}
\label{sec:traza}

La traza de la matriz de covarianza del ruido la estimaremos de la siguiente
forma:
\begin{equation}
tr(\Sigma) = \frac1{N} \sum_{i=1}^{N} \sum_{j=1}^{M} n_j(i)^2, \label{traza1}
\end{equation}
siendo \emph{N} la cantidad de muestras usadas en la estimación, y \emph{M}, la
cantidad de canales receptores.

La ecuación \ref{traza1} puede modificarse de la siguiente manera agregando la
varianza del ruido y variando el orden de las sumatorias:
\begin{equation}
tr(\Sigma) = \frac1{N} \sum_{j=1}^{M} \sigma_{n_j}^2 \sum_{i=1}^{N}
\frac{n_j(i)^2}{\sigma_{n_j}^2} =  \frac1{N} \sum_{j=1}^{M} \sigma_{n_j}^2
\left( \mathcal{X}_N^2 \right)_j \label{traza2}
\end{equation}

Se puede ver que la sumatoria más interna es una distribución de probabilidad
\emph{Chi cuadrado} que tiene las siguientes propiedades:
\begin{eqnarray}
\textrm{E} \! \left[ \left( \mathcal{X}_N^2 \right)_j \right] &=& N
\label{e_chi} \\
\textrm{Var} \! \left[ \left( \mathcal{X}_N^2 \right)_j \right] &=& 2N
\label{var_chi}
\end{eqnarray}

Entonces, empleando las ecuaciones \ref{traza2}, \ref{e_chi} y \ref{var_chi}
podemos calcular la esperanza y la varianza de la estimación de la traza de la
matriz de covarianza del ruido. Recordemos que los $\left( \mathcal{X}_N^2
\right)_j$ son independientes entre sí.

\begin{eqnarray}
\textrm{E} [tr(\Sigma)] &=& \frac1{N} \sum_{j=1}^{M} \sigma_{n_j}^2 \textrm{E}
\! \left[ \left( \mathcal{X}_N^2 \right)_j \right] = \frac1{N} \sum_{j=1}^{M}
\sigma_{n_j}^2 N = \sum_{j=1}^{M} \sigma_{n_j}^2 \label{e_tr} \\
\textrm{Var} [tr(\Sigma)] &=& \frac1{N^2} \sum_{j=1}^{M} \sigma_{n_j}^4
\textrm{E} \! \left[ \left( \mathcal{X}_N^2 \right)_j \right] = \frac1{N^2}
\sum_{j=1}^{M} \sigma_{n_j}^4 2N = \frac{2}{N} \sum_{j=1}^{M} \sigma_{n_j}^4
\label{var_tr}
\end{eqnarray}

\bibliography{resumen}
\bibliographystyle{plain}

\end{document}
