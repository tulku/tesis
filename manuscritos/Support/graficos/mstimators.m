% Script para generar el grafico de ejemplo de funciones rho.
clf;
axis('ticx off');
x=[-3:0.1:3];
% Cuadrático
subplot(2,2,1);
line(x,x.^2,'linewidth', 1.5);
title('Error cuadrático','FontSize',11);
ylabel ('\rho(x)','FontSize',11);
xlabel ('x','FontSize',11);
axis('ticx off');
% Absoluto
subplot(2,2,2);
line(x,abs(x),'linewidth', 1.5);
ylabel ('\rho(x)','FontSize',11);
xlabel ('x','FontSize',11);
title('Errores absolutos','FontSize',11);
axis('ticx off');
% Windsorizing
subplot(2,2,3);
line(x,[abs(x(x<=-1.5)-1.5/2) x(abs(x)<1.5).^2 x(x>=1.5)+1.5/2],'linewidth', 1.5);
ylabel ('\rho(x)','FontSize',11);
xlabel ('x','FontSize',11);
title('Windsorizing en 1.5','FontSize',11);
axis('ticx off');
% Biweight
subplot(2,2,4);
line(x,[2*ones(1, length((x(x<=-1.5)))) x(abs(x)<1.5).^2 2*ones(1,length(x(x>=1.5)))],'linewidth', 1.5);
axis([-3 3 0 2.5]);
ylabel ('\rho(x)','FontSize',11);
xlabel ('x','FontSize',11);
title('Biweight en 1.5','FontSize',11);
axis('ticx off');
print -solid -color ejemplos-rho.pdf

